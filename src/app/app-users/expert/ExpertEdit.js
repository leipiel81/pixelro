import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Form, Modal } from "react-bootstrap";
import ReactTags from "react-tag-autocomplete";
import Select from "react-select";
import { SimpleNotification } from "components/SimpleNotification";
import { Store } from "graphql/query/select";
import { UPDATE_STORE, UPDATE_STORE_INTRO } from "graphql/gql/update";
import { MULTI_FILE, MULTI_FILE_RESIZE } from "graphql/gql/insert";
import DaumPostcode from "react-daum-postcode";
import { Editor } from "@tinymce/tinymce-react";

const EditPage = ({ match }) => {
  const { id } = match.params;

  let items;
  if (id !== undefined && id !== "undefined") {
    const { result } = Store(id);
    items = result;
  }

  return <div>{items && <EditForm items={items} />}</div>;
};

const EditForm = ({ items }, props) => {
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  // --------------------------

  const [mdShow, setMdShow] = useState(false);

  // -- 데이터 값 저장 --
  const [name, setName] = useState(items.name);
  const [tel, setTel] = useState(items.tel);
  const [site, setSite] = useState(items.site);
  const [blog, setBlog] = useState(items.blog);
  const [onLine, setOnLine] = useState(items.onLine);
  const [offLine, setOffLine] = useState(items.offLine);
  const [image, setImage] = useState(items.image ? JSON.parse(items.image) : []);

  // 거래정보
  const [businessNumber, setBusinessNumber] = useState(items.businessNumber);
  const [bankNumber, setBankNumber] = useState(items.bankNumber);
  const [accountHolder, setAccountHolder] = useState(items.accountHolder);

  const [bankImage, setBankImage] = useState(items.bankImage);
  const [businessNumberImage, setBusinessNumberImage] = useState(items.bankImage);

  // 소개 정보
  const [intro, setIntro] = useState("");
  const [introImages, setIntroImages] = useState([]);
  useEffect(() => {
    fetch(items.intro)
      .then((res) => res.text())
      .then((body) => {
        setIntro(body);
      });
  }, []);

  let tempBankName;
  if (items.bankName) {
    tempBankName = { value: `${items.bankName}`, label: `${items.bankName}` };
  }

  const [bankName, setBankName] = useState(tempBankName);

  // 주소
  const [post, setPost] = useState(items.post);
  const [address, setAddress] = useState(items.address);
  const [detailAddress, setDetailAddress] = useState(items.detailAddress);
  const [gpsLatitude, setgpsLatitude] = useState(items.gpsLatitude);
  const [gpsLongitude, setGpsLongitude] = useState(items.gpsLongitude);

  // 키워드 값 저장
  let tags = [];
  if (items.keyword && items.keyword !== []) {
    tags = JSON.parse(items.keyword).map((m) => ({ name: m }));
  }

  const [tagTest, setTagTest] = useState(tags);

  // 수정 안하는 값들.
  const { id, star } = items;

  // ----------------------------

  // 키워드 추가 삭제 기능 함수
  const handleDelete = (i) => {
    const tags = tagTest.slice(0);
    tags.splice(i, 1);
    setTagTest(tags);
  };

  const handleAddition = (tag) => {
    const tags = [].concat(tagTest, tag);
    setTagTest(tags);
  };

  // 소개 글 등록
  const [storeIntroUpdate, { data: updateIntroData }] = useMutation(UPDATE_STORE_INTRO, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateIntroData !== undefined) {
      goBack();
    }
  }, [updateIntroData]);

  const [storeUpdate, { data: updateData }] = useMutation(UPDATE_STORE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      storeIntroUpdate({ variables: { id: items.id, intro: `${intro}` } });
    }
  }, [updateData]);

  const handleUpdate = () => {
    if (!name || !tel || !post || !address) {
      SimpleNotification({
        title: "",
        message: "기본 정보 입력해주세요.",
      });
      return;
    }

    if (!businessNumber || !bankName || !bankNumber || !accountHolder) {
      SimpleNotification({
        title: "",
        message: "거래 정보 입력해주세요.",
      });
      return;
    }

    storeUpdate({
      variables: {
        id,
        name,
        tel,
        post,
        address,
        detailAddress,
        onLine,
        offLine,
        category: "",
        keyword: `[${tagTest && tagTest.map((m) => `"${m.name}"`)}]`,
        site,
        blog,
        star,
        businessNumber,
        bankName: `${bankName ? bankName.value : ""}`,
        bankNumber,
        accountHolder,
        image: `[${image && image.map((m) => `"${m}"`)}]`,
        bankImage: bankImage ? `${bankImage}` : "",
        businessNumberImage: businessNumberImage ? `${businessNumberImage}` : "",
        gpsLatitude,
        gpsLongitude,
      },
    });
  };

  // 주소 값 저장
  const handlePostComplete = (data) => {
    console.log(data);
    let fullAddress = data.address;
    let extraAddress = "";
    let zonecode = "";

    if (data.addressType === "R") {
      if (data.bname !== "") {
        extraAddress += data.bname;
      }
      if (data.buildingName !== "") {
        extraAddress += extraAddress !== "" ? `, ${data.buildingName}` : data.buildingName;
      }
      fullAddress += extraAddress !== "" ? ` (${extraAddress})` : "";
    }

    zonecode = data.zonecode;

    setMdShow(false);
    setPost(zonecode);
    setAddress(fullAddress);

    // 다음 API 호출 ,
    const encodedFullAddress = encodeURIComponent(fullAddress);
    const URL = `https://dapi.kakao.com/v2/local/search/address.json?query=${encodedFullAddress}`;
    fetch(URL, {
      headers: { Authorization: "KakaoAK 27c0f7d907c519c3195db1360600eb68" },
    })
      .then((res) => {
        res.json().then((json) => {
          if (json && json.documents && json.documents.length > 0) {
            const { x, y } = json.documents[0];
            // DB에 저장 (API 코드 참조할 것)
            setGpsLongitude(x);
            setgpsLatitude(y);
          }
        });
      })
      .catch((err) => {
        console.log("error : ", err);
      });
  };

  // -------------------------------------------------- 이미지관련
  // 통장 & 사업자 등록증 사진 변경
  const [businessNumberImageUpload, { data: businessNumberImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (businessNumberImageData !== undefined) {
      const result = `${businessNumberImageData.multiUpload[0].filename}`;
      setBusinessNumberImage(result);
    }
  }, [businessNumberImageData]);

  // 통장 & 사업자 등록증 사진 변경
  const [bankImageUpload, { data: bankImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (bankImageData !== undefined) {
      const result = `${bankImageData.multiUpload[0].filename}`;
      setBankImage(result);
    }
  }, [bankImageData]);

  // 통장사본 변경
  const handleBankImageChange = async (e) => {
    const file = e.target.files[0];

    // 이미지 저장
    await bankImageUpload({ variables: { files: file, size: `[${file.size}]` } });
  };

  // 사업자등록증 변경
  const handlebusinessNumberImageChange = async (e) => {
    const file = e.target.files[0];

    // 이미지 저장
    await businessNumberImageUpload({ variables: { files: file, size: `[${file.size}]` } });
  };
  //  상점 이미지 저장
  const [storeImageUpload, { data: storeImageData }] = useMutation(MULTI_FILE_RESIZE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (storeImageData !== undefined) {
      const tempDetailImage = [];
      image.map((m) => tempDetailImage.push(m));
      storeImageData.multiUploadResize.map((m) => tempDetailImage.push(m.filename));

      setImage(tempDetailImage);
    }
  }, [storeImageData]);

  // 상점 이미지 추가
  const handleDetailImageChange = async (e) => {
    const { files } = e.target;

    if (image.length >= 5) {
      SimpleNotification({
        title: "",
        message: "상점 이미지는 5개까지만 등록이 가능합니다.",
      });
      return;
    }

    if (files.length > 5) {
      SimpleNotification({
        title: "",
        message: "상점 이미지는 5개까지만 등록이 가능합니다.",
      });
      return;
    }

    const fileArray = [];
    if (files) {
      for (let i = 0; i < files.length; i += 1) {
        fileArray.push(files[i]);
      }
    }
    const filesSize = `[${fileArray.map((m) => m.size)}]`;
    // 바로 추가 보내기
    await storeImageUpload({ variables: { files: fileArray, size: filesSize } });
  };

  // 상점 이미지 삭제
  const handleImageDelete = async (value) => {
    const tempImage = image.map((m) => m);
    const idx = tempImage.indexOf(value);
    if (idx > -1) tempImage.splice(idx, 1);

    await setImage(tempImage);
  };

  // 배열값 이동
  const arrayMove = (arr, old_index, new_index) => {
    while (old_index < 0) {
      old_index += arr.length;
    }
    while (new_index < 0) {
      new_index += arr.length;
    }
    if (new_index >= arr.length) {
      let k = new_index - arr.length + 1;
      while ((k -= 1)) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing purposes
  };

  // 상점 이미지 왼쪽으로 이동
  const handleImageLeftMove = async (value) => {
    let tempImage = image.map((m) => m);
    const idx = tempImage.indexOf(value);

    if (idx !== 0) {
      tempImage = arrayMove(tempImage, idx, idx - 1);
    }

    await setImage(tempImage);
  };

  // 상세 이미지 오른쪽으로 이동
  const handleImageRightMove = async (value) => {
    let tempImage = image.map((m) => m);
    const idx = tempImage.indexOf(value);

    if (idx !== tempImage.length - 1) {
      tempImage = arrayMove(tempImage, idx, idx + 1);
    }

    await setImage(tempImage);
  };

  // 소개 관련

  const introUpdateFunction = async (tempDetailImage) => {
    let tempIntro = intro;
    // 이미지 저장과 동시에 editor에 표현
    for (let i = 0; i < tempDetailImage.length; i += 1) {
      tempIntro += `<p><img style="width : 100%;" src="${tempDetailImage[i]}" alt=""></p>`;
    }

    await setIntro(tempIntro);
  };

  // 이미지 저장
  const [introImageUpload, { data: imageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (imageData !== undefined) {
      const tempDetailImage = [];
      introImages.map((m) => tempDetailImage.push(m));
      imageData.multiUpload.map((m) => tempDetailImage.push(m.filename));

      // 이미지 저장과 동시에 editor에 표현
      introUpdateFunction(tempDetailImage);

      setIntroImages(tempDetailImage);
    }
  }, [imageData]);

  // 이미지 추가
  const handleImageChange = async (e) => {
    const { files } = e.target;

    const fileArray = [];
    if (files) {
      for (let i = 0; i < files.length; i += 1) {
        fileArray.push(files[i]);
      }
    }
    const filesSize = `[${fileArray.map((m) => m.size)}]`;
    // 바로 추가 보내기
    await introImageUpload({ variables: { files: fileArray, size: filesSize } });
  };
  // 이미지 선택
  const handleImageClick = async (image) => {
    const tempIntro = `${intro}<p><img style="width : 100%;" src="${image}" alt=""></p>`;
    setIntro(tempIntro);
  };
  const handleEditorChange = (value) => {
    setIntro(value);
  };
  return (
    //<ㅇ>
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">사용자 관리</li>
            <Link to="/app-users/expert-table" className="breadcrumb-item active mt-1" aria-current="page">
              전문가 목록
            </Link>
            <Link to={`/app-users/expert-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              전문가 정보
            </Link>
            <Link to={`/app-users/expert-edit/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              전문가 정보 수정
            </Link>
          </ol>
        </nav>
      </div>

      <Tabs />
      <div className="row">
        <div className="col-md-12">
          {/* 상점 기본 정보 */}
          <div className="card">
            <div className="card-body">
              <div className="">
                <div className="card-title-wrap">
                  <h4 className="card-title">전문가 정보 수정</h4>
                </div>
                <div className="mt-6 card-wrap">
                  <table className="app-table w-full">
                    <tr>
                      <td className="th">분류</td>
                      <td colSpan="3">
                        {items.place}
                      </td>
                    </tr>
                    <tr className="">
                      <td className="th w-2/12">전문가명</td>
                      <td className="w-4/12">
                        <Form.Control type="text" value={name} onChange={(e) => setName(e.target.value)} />
                      </td>
                      <td className="th w-2/12">연락처</td>
                      <td className="w-4/12">
                        <Form.Control type="text" value={tel} onChange={(e) => setTel(e.target.value)} />
                      </td>
                    </tr>
                    <tr>
                      <td className="th">주소</td>
                      <td colSpan="3">
                        <div className="flex mb-2">
                          <Form.Control className="w-2/12" type="text" value={post} readOnly placeholder="우편번호" />
                          <button onClick={(e) => setMdShow(true)} type="button" className="btn btn-outline-primary btn-sm ml-2">
                            주소찾기
                          </button>
                        </div>
                        <Form.Control className="mb-2" type="text" readOnly value={address} placeholder="주소" />
                        <Form.Control
                          type="text"
                          value={detailAddress}
                          placeholder="상세주소"
                          onChange={(e) => setDetailAddress(e.target.value)}
                        />

                        <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
                          <Modal.Header closeButton>
                            <DaumPostcode className="h-full" onComplete={handlePostComplete} {...props} />
                          </Modal.Header>
                        </Modal>
                        {/* Modal Ends */}
                      </td>
                    </tr>
                    <tr className="">
                      <td className="th w-2/12">업체명</td>
                      <td className="w-4/12">
                        <Form.Control type="text" placeholder="업체명을 입력하세요."/>
                      </td>
                      <td className="th w-2/12">영업시간</td>
                      <td className="w-4/12">
                        <Form.Control type="text" placeholder="영업시간을 입력하세요." />
                      </td>
                    </tr>
                    <tr>
                      <td className="th">등록 URL</td>
                      <td colSpan="3">
                        <Form.Control type="text" value={site} onChange={(e) => setSite(e.target.value)} placeholder="www.example.com" />
                      </td>
                    </tr>
                    <tr>
                      <td className="th">입력 태그</td>
                      <td colSpan="3">
                        <ul className="tag-wrap select-mode">
                          <li className="tag-item select">자가테스트</li>
                          <li className="tag-item">노안</li>
                          <li className="tag-item">눈건강</li>
                          <li className="tag-item select">눈운동</li>
                          <li className="tag-item">컨텐트렌즈</li>
                          <li className="tag-item">시력</li>
                          <li className="tag-item">라섹</li>
                          <li className="tag-item">빛번짐</li>
                          <li className="tag-item">색약</li>
                          <li className="tag-item">색맹</li>
                          <li className="tag-item">아이웨어</li>
                          <li className="tag-item">황반변성</li>
                          <li className="tag-item">잘모르겠어요</li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td className="th">프로필 사진</td>
                      <td colSpan="3">
                        <div className="form-file">
                          <img src="/static/images/temp/img_expert_profile.jpg" className="profile_preview" />
                          <label className="" for="input_file">파일을 선택하세요.<i className="ti-upload"></i></label>
                          <Form.Control 
                            type="file" 
                            accept='image/jpg,impge/png,image/jpeg,image/gif' 
                            name='profile_img' 
                            onChange=""
                            id="input_file"
                          />
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td className="th">소개</td>
                      <td colSpan="3">
                        <textarea value="" onChange="" className="expert-text" />
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 btnbar">
        <div className="row foot-edit">
          <button className="btn btn-warning" onClick={goBack}>
            취소하기
          </button>
          <button type="button" className="btn btn-primary" onClick={handleUpdate}>
            저장하기
          </button>
        </div>
      </div>
    </>
  );
};

//탭 영역
const Tabs = () => {
  return (
    <div className="tab-wrap">
      <div className="tab-item active">
        <Link to={`#tab1`}>한국어</Link>
      </div>
      <div className="tab-item">
        <Link to={`#tab2`}>영어</Link>
      </div>
      <div className="tab-item">
        <Link to={`#tab3`}>일본어</Link>
      </div>
      <div className="tab-item">
        <Link to={`#tab4`}>중국어</Link>
      </div>
    </div>
  );
};

// 상점 이미지 보여주기
const MultiImages = ({ images, handleImageDelete, handleImageLeftMove, handleImageRightMove }) => {
  // 이미지 삭제
  const handleDelete = (e, index) => {
    handleImageDelete(e, index);
  };

  const handleLeftMove = (e, index) => {
    handleImageLeftMove(e, index);
  };

  const handleRightMove = (e, index) => {
    handleImageRightMove(e, index);
  };

  if (!images || images === "") {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
  try {
    const detailImages = images;
    if (!detailImages || detailImages.length === 0) {
      return <img src={null} className="w-12 h-12" alt="" />;
    }
    const multiImages = detailImages.map((image, index) => {
      return (
        <div>
          <img src={image} className="w-12 h-12 mr-2" alt="" />
          <button onClick={(e) => handleDelete(image)}>x</button>
          <div className={`flex ${index === 0 ? "justify-end" : index === detailImages.length - 1 ? "justify-start" : "justify-between"} `}>
            {index !== 0 && (
              <button onClick={(e) => handleLeftMove(image)} className="bg-blue-700 px-2 text-white ml-1">
                {"< "}
              </button>
            )}

            {index !== detailImages.length - 1 && (
              <button onClick={(e) => handleRightMove(image)} className="bg-blue-700 px-2 text-white mr-1">
                {" >"}
              </button>
            )}
          </div>
        </div>
      );
    });
    return multiImages;
  } catch (e) {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
};

// 소개 이미지
const MultiIntroImages = ({ images, handleImageClick }) => {
  // 이미지 삭제
  const handleClickImage = (e, index) => {
    handleImageClick(e, index);
  };

  if (!images || images === "") {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
  try {
    const detailImages = images;
    if (!detailImages || detailImages.length === 0) {
      return <img src={null} className="w-12 h-12" alt="" />;
    }
    const multiImages = detailImages.map((image, index) => {
      return (
        <div className="items-center">
          <img src={image} className="w-12 h-12 mr-2" alt="" />
          <button onClick={(e) => handleClickImage(image)} className="border border-blue-500 rounded-md text-blue-700 px-1">
            선택
          </button>
        </div>
      );
    });
    return multiImages;
  } catch (e) {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
};
EditPage.prototype = {
  id: PropTypes.string.isRequired,
};

export default EditPage;
