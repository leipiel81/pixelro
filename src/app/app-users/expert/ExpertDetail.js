import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Store } from "graphql/query/select";
import { useHistory, Link } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { WITHDRAW_STORE } from "graphql/gql/update";
import { FormatDate } from "components/FormatDate";
import { Modal, Button } from "react-bootstrap";
import { PhoneFormatter } from "components/PhoneFormatter";
import { observer } from "mobx-react-lite";
import useSettings from "stores/settings";
import { SimpleNotification } from "components/SimpleNotification";

const DetailPage = observer(({ match }) => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const { id } = match.params;
  const [mdShow, setMdShow] = useState(false);

  let items;
  if (id !== undefined && id !== "undefined") {
    const { result } = Store(id);
    items = result;
  }

  const [withdrawStore, { data: updateData }] = useMutation(WITHDRAW_STORE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });
  useEffect(() => {
    if (updateData !== undefined) {
      if (updateData.withdrawStore.result === "cancel") {
        SimpleNotification({
          message: updateData.withdrawStore.error || "Error",
        });
        return;
      }
      if (updateData.withdrawStore.result === "ok") {
        history.goBack();
      }
    }
  }, [updateData]);

  const handleWithdrawModal = () => {
    const date = FormatDate(new Date());
    const { adminId } = items.storeGroup;
    setMdShow(false);
    withdrawStore({
      variables: { storeId: items.id, withdrawDate: `${date}`, memberId: `${adminId}` },
    });
  };
  return (
    <>
      {items && (
        <div>
          {/* breadcrumb s */}
          <div>
            <nav aria-label="breadcrumb" role="navigation">
              <ol className="breadcrumb">
                <li className="breadcrumb-item active">사용자 관리</li>
                <Link to="/app-users/expert-table" className="breadcrumb-item active mt-1" aria-current="page">
                  전문가 목록
                </Link>
                <Link to={`/app-users/expert-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
                  전문가 정보
                </Link>
              </ol>
            </nav>
          </div>
          {/* breadcrumb e */}
          <Tabs />
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between card-title-wrap">
                <h4 className="card-title">전문가 정보</h4>
              </div>
              
              <InfoForm items={items} />
              <IntroForm items={items} />
              {/*
              <SubInfoForm items={items} />
              <AdminForm items={items} />
              <BusinessForm items={items} />
              */}
            </div>
          </div>

          <div className="col-12 btnbar">
            <div className="row justify-between">
              <button className="btn btn-danger" onClick={() => setMdShow(true)}>
                삭제하기
              </button>
              <Link className="btn btn-primary" to={`/app-users/expert-edit/${id}`}>
                수정하기
              </Link>
            </div>
          </div>
          {/* Modal Starts  */}
          <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
            <Modal.Header closeButton>
              <Modal.Title>가맹 취소 신청</Modal.Title>
            </Modal.Header>

            <Modal.Body>
              <p>정말 취소하시겠습니까?</p>
            </Modal.Body>

            <Modal.Footer className="flex-wrap">
              <Button variant="btn btn-primary m-2" onClick={handleWithdrawModal}>
                확인
              </Button>
              <Button variant="btn btn-light m-2" onClick={() => setMdShow(false)}>
                취소
              </Button>
            </Modal.Footer>
          </Modal>
          {/* Modal Ends */}
        </div>
      )}
    </>
  );
});

const InfoForm = ({ items }) => {
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="">
          <div className="expert-profile">
            <img className="profile-img" src="/static/images/temp/img_expert_profile.jpg" />
          </div>
          <div className="mt-6 card-wrap">
            <table className="app-table w-full">
              <tr>
                <td className="th">전문가명</td>
                <td colSpan="3" className="">
                  {items.name}
                </td>
              </tr>
              <tr>
                <td className="th w-2/12">분류</td>
                <td className="w-4/12">{items.place}</td>
                <td className="th w-2/12">연락처</td>
                <td className="w-4/12">{PhoneFormatter(items.tel)}</td>
              </tr>
              <tr>
                <td className="th">주소</td>
                <td colSpan="3" className="">
                  <div className="mb-1">{items.post}</div>
                  <div>
                    {items.address} {items.detailAddress}
                  </div>
                </td>
              </tr>
              <tr>
                <td className="th">등록날짜</td>
                <td colSpan="3" className="">
                  {FormatDate(new Date(parseInt(items.createAt, 10)))}
                </td>
              </tr>
              <tr>
                <td className="th w-2/12">업체명</td>
                <td className="w-4/12"></td>
                <td className="th w-2/12">영업시간</td>
                <td className="w-4/12"></td>
              </tr>
              <tr>
                <td className="th">등록 URL</td>
                <td colSpan="3" className="">
                  {items.site}
                </td>
              </tr>
              <tr>
                <td className="th">입력 태그</td>
                <td colSpan="3" className="">
                  <ul className="tag-wrap">
                    <li className="tag-item select">자가테스트</li>
                    <li className="tag-item">노안</li>
                    <li className="tag-item">눈건강</li>
                    <li className="tag-item select">눈운동</li>
                    <li className="tag-item">컨텐트렌즈</li>
                    <li className="tag-item">시력</li>
                    <li className="tag-item">라섹</li>
                    <li className="tag-item">빛번짐</li>
                    <li className="tag-item">색약</li>
                    <li className="tag-item">색맹</li>
                    <li className="tag-item">아이웨어</li>
                    <li className="tag-item">황반변성</li>
                    <li className="tag-item">잘모르겠어요</li>
                  </ul>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

/*
const AdminForm = ({ items }) => {
  // 관리자 정보
  const { adminId } = items.storeGroup;

  const adminEmail = items.storeGroup.members.filter((f) => f.id === adminId).map((v) => v.email);

  const adminName = items.storeGroup.members.filter((f) => f.id === adminId).map((v) => v.name);

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">관리자 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">이름</th>
                <td colSpan="3" className="border px-8 py-2 text-sm">
                  {adminName}
                </td>
              </tr>
              <tr className="m-0 p-0">
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">이메일</th>
                <td className="border px-8 py-2 text-sm w-4/12">{adminEmail}</td>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">전화번호</th>
                <td className="border px-8 py-2 text-sm w-4/12">{PhoneFormatter(items.tel)}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};
*/

/*
const SubInfoForm = ({ items }) => {
  let keywordCheck = false;
  if (items.keyword) {
    keywordCheck = JSON.parse(items.keyword).length !== 0;
  }

  let storeImages;
  if (items.image) {
    storeImages = JSON.parse(items.image);
  }

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="">
          <div className="card-subtitle">
            <h3 className="">상점 부가 정보</h3>
          </div>
          <div className="mt-6">
            <table className="app-table w-full">
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">상점 소개</th>
                <td colSpan="3" className="border px-8 py-2 text-sm">
                  {items.intro}
                </td>
              </tr>
              <tr className="m-0 p-0">
                <td className="th w-2/12">홈페이지 주소</td>
                <td className="w-4/12">{items.site}</td>
                <td className="th w-2/12">블로그 주소</td>
                <td className="w-4/12">{items.blog}</td>
              </tr>

              <tr>
                <td className="th">키워드</td>
                <td colSpan="3" className="">
                  <div className="flex">
                    {keywordCheck &&
                      JSON.parse(items.keyword).map((m) => (
                        <div className="flex">
                          <span className="border p-2 mr-1">{m}</span>
                        </div>
                      ))}
                  </div>
                </td>
              </tr>
              <tr>
                <td className="th">온라인 / 오프라인</td>
                <td colSpan="3" className="">
                  <div className="max-w-xs">
                    <span className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                      <div className="flex col-sm-9 col-form-label p-0">
                        <div className="form-check mr-3">
                          <label className="form-check-label">
                            <input type="checkbox" disabled checked={items.onLine} className="form-check-input" />
                            <i className="input-helper"></i>
                            온라인
                          </label>
                        </div>
                        <div className="form-check">
                          <label className="form-check-label">
                            <input type="checkbox" disabled checked={items.offLine} className="form-check-input" />
                            <i className="input-helper"></i>
                            오프라인
                          </label>
                        </div>
                      </div>
                    </span>
                  </div>
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">상점 이미지</th>
                <td colSpan="3" className="border px-8 py-2 text-sm  ">
                  <img src={items.image} alt="" className="w-64 h-64" />
                </td>
              </tr>
              <tr>
                <td className="th">상점 이미지</td>
                <td colSpan="3" className="">
                  {storeImages && storeImages.map((m) => <img className="h-24 w-24 m-3" alt="" src={`${m}`} key={m} />)}
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};
*/

/*
const BusinessForm = ({ items }) => {
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상점 거래 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">사업자 번호</th>
                <td colSpan="3" className="border px-8 py-2 text-sm ">
                  {items.businessNumber}
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">거래은행</th>
                <td colSpan="3" className="border px-8 py-2 text-sm ">
                  {items.bankName}
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">계좌번호</th>
                <td colSpan="3" className="border px-8 py-2 text-sm ">
                  {items.bankNumber}
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">예금주</th>
                <td colSpan="3" className="border px-8 py-2 text-sm ">
                  {items.accountHolder}
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};
*/


const IntroForm = ({ items }) => {
  const [intro, setIntro] = useState("");

  fetch(items.intro)
    .then((res) => res.text())
    .then((body) => {
      setIntro(body);
    });
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6">
          <div className="textarea-wrap">
            <div
              introeditable="true"
              dangerouslySetInnerHTML={{ __html: intro }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

//탭 영역
const Tabs = () => {
  return (
    <div className="tab-wrap">
      <div className="tab-item active">
        <Link to={`#tab1`}>한국어</Link>
      </div>
      <div className="tab-item">
        <Link to={`#tab2`}>영어</Link>
      </div>
      <div className="tab-item">
        <Link to={`#tab3`}>일본어</Link>
      </div>
      <div className="tab-item">
        <Link to={`#tab4`}>중국어</Link>
      </div>
    </div>
  );
};


DetailPage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default DetailPage;
