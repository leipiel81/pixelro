import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Form, Modal } from "react-bootstrap";
import ReactTags from "react-tag-autocomplete";
import Select from "react-select";
import { SimpleNotification } from "components/SimpleNotification";
import { CREATE_STORE, MULTI_FILE, MULTI_FILE_RESIZE } from "graphql/gql/insert";
import { UPDATE_MEMBER_DEFAULT_STORE } from "graphql/gql/update";
import DaumPostcode from "react-daum-postcode";

const CreatePage = ({ match }, props) => {
  const { id, memberId, defaultStoreId } = match.params;

  const history = useHistory();
  const goBack = () => {
    history.push("/app-users/expert-table");
  };

  // --------------------------
  const [mdShow, setMdShow] = useState(false);

  // -- 데이터 값 저장 --
  const [place, setPlace] = useState([]);
  const [name, setName] = useState();
  const [tel, setTel] = useState();
  const [onLine, setOnLine] = useState(false);
  const [offLine, setOffLine] = useState(false);
  const [site, setSite] = useState();
  const [blog, setBlog] = useState();
  const star = "";
  const [image, setImage] = useState([]);

  // 상점 거래 정보
  const [businessNumber, setBusinessNumber] = useState();
  const [bankName, setBankName] = useState();
  const [bankNumber, setBankNumber] = useState();
  const [accountHolder, setAccountHolder] = useState();

  const [bankImage, setBankImage] = useState();
  const [businessNumberImage, setBusinessNumberImage] = useState();

  // 주소
  const [post, setPost] = useState();
  const [address, setAddress] = useState();
  const [detailAddress, setDetailAddress] = useState();
  const [gpsLatitude, setgpsLatitude] = useState();
  const [gpsLongitude, setGpsLongitude] = useState();

  // 키워드 값 저장
  const [tagTest, setTagTest] = useState([]);

  // 키워드 추가 삭제 기능 함수
  const handleDelete = (i) => {
    const tags = tagTest.slice(0);
    tags.splice(i, 1);
    setTagTest(tags);
  };

  const handleAddition = (tag) => {
    const tags = [].concat(tagTest, tag);
    setTagTest(tags);
  };

  // ----------------------------

  // default 상점 미등록시  등록
  const [memberUpdate, { data: updateData }] = useMutation(UPDATE_MEMBER_DEFAULT_STORE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      goBack();
    }
  }, [updateData]);

  // 상점 등록
  const [storeCreate, { data: createData }] = useMutation(CREATE_STORE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (createData !== undefined) {
      const resultId = createData.createStore.id;
      // default 상점 있는지 체크
      if (defaultStoreId === "1") {
        memberUpdate({ variables: { id: memberId, defaultStoreId: resultId } });
      } else {
        goBack();
      }
    }
  }, [createData]);

  // 등록 버튼 클릭시 실행
  const handleCreate = () => {
    if (!place || !name || !tel || !post || !address) {
      SimpleNotification({
        title: "",
        message: "기본 정보 입력해주세요.",
      });
      return;
    }

    if (!businessNumber || !bankName || !bankNumber || !accountHolder) {
      SimpleNotification({
        title: "",
        message: "거래 정보 입력해주세요.",
      });
      return;
    }

    if (!image) {
      SimpleNotification({
        title: "",
        message: "상점 이미지를 등록해주세요.",
      });
      return;
    }

    storeCreate({
      variables: {
        place: `${place && place.value}`,
        name,
        tel,
        post: `${post}`,
        address,
        detailAddress,
        intro: "",
        onLine,
        offLine,
        category: "",
        keyword: `[${tagTest && tagTest.map((m) => `"${m.name}"`)}]`,
        site,
        blog,
        star,
        storeRegister: "승인",
        storeGroupId: id,
        businessNumber,
        bankName: `${bankName && bankName.value}`,
        bankNumber,
        accountHolder,
        image: `[${image && image.map((m) => `"${m}"`)}]`,
        gpsLatitude,
        gpsLongitude,
        bankImage: bankImage ? `${bankImage}` : "",
        businessNumberImage: businessNumberImage ? `${businessNumberImage}` : "",
      },
    });
  };

  // 주소 값 저장
  const handlePostComplete = (data) => {
    console.log(data);
    let fullAddress = data.address;
    let extraAddress = "";
    let zonecode = "";

    if (data.addressType === "R") {
      if (data.bname !== "") {
        extraAddress += data.bname;
      }
      if (data.buildingName !== "") {
        extraAddress += extraAddress !== "" ? `, ${data.buildingName}` : data.buildingName;
      }
      fullAddress += extraAddress !== "" ? ` (${extraAddress})` : "";
    }

    zonecode = data.zonecode;

    setMdShow(false);
    setPost(zonecode);
    setAddress(fullAddress);

    // 다음 API 호출 ,
    const encodedFullAddress = encodeURIComponent(fullAddress);
    const URL = `https://dapi.kakao.com/v2/local/search/address.json?query=${encodedFullAddress}`;
    fetch(URL, {
      headers: { Authorization: "KakaoAK 27c0f7d907c519c3195db1360600eb68" },
    })
      .then((res) => {
        res.json().then((json) => {
          if (json && json.documents && json.documents.length > 0) {
            const { x, y } = json.documents[0];
            // DB에 저장 (API 코드 참조할 것)
            setGpsLongitude(x);
            setgpsLatitude(y);
          }
        });
      })
      .catch((err) => {
        console.log("error : ", err);
      });
  };

  // -------------------------------------------------- 이미지관련
  // 통장 & 사업자 등록증 사진 변경
  const [businessNumberImageUpload, { data: businessNumberImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (businessNumberImageData !== undefined) {
      const result = `${businessNumberImageData.multiUpload[0].filename}`;
      setBusinessNumberImage(result);
    }
  }, [businessNumberImageData]);

  // 통장 & 사업자 등록증 사진 변경
  const [bankImageUpload, { data: bankImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (bankImageData !== undefined) {
      const result = `${bankImageData.multiUpload[0].filename}`;
      setBankImage(result);
    }
  }, [bankImageData]);

  // 통장사본 변경
  const handleBankImageChange = async (e) => {
    const file = e.target.files[0];

    // 이미지 저장
    await bankImageUpload({ variables: { files: file, size: `[${file.size}]` } });
  };

  // 사업자등록증 변경
  const handlebusinessNumberImageChange = async (e) => {
    const file = e.target.files[0];

    // 이미지 저장
    await businessNumberImageUpload({ variables: { files: file, size: `[${file.size}]` } });
  };

  //  상점 이미지 저장
  const [storeImageUpload, { data: storeImageData }] = useMutation(MULTI_FILE_RESIZE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (storeImageData !== undefined) {
      const tempDetailImage = [];
      image.map((m) => tempDetailImage.push(m));
      storeImageData.multiUploadResize.map((m) => tempDetailImage.push(m.filename));

      setImage(tempDetailImage);
    }
  }, [storeImageData]);

  // 상점 이미지 추가
  const handleDetailImageChange = async (e) => {
    const { files } = e.target;

    if (image.length > 5) {
      SimpleNotification({
        title: "",
        message: "상점 이미지는 5개까지만 등록이 가능합니다.",
      });
      return;
    }

    if (files.length > 5) {
      SimpleNotification({
        title: "",
        message: "상점 이미지는 5개까지만 등록이 가능합니다.",
      });
      return;
    }

    const fileArray = [];
    if (files) {
      for (let i = 0; i < files.length; i += 1) {
        fileArray.push(files[i]);
      }
    }
    const filesSize = `[${fileArray.map((m) => m.size)}]`;
    // 바로 추가 보내기
    await storeImageUpload({ variables: { files: fileArray, size: filesSize } });
  };

  // 상점 이미지 삭제
  const handleImageDelete = async (value) => {
    const tempImage = image.map((m) => m);
    const idx = tempImage.indexOf(value);
    if (idx > -1) tempImage.splice(idx, 1);

    await setImage(tempImage);
  };

  // 배열값 이동
  const arrayMove = (arr, old_index, new_index) => {
    while (old_index < 0) {
      old_index += arr.length;
    }
    while (new_index < 0) {
      new_index += arr.length;
    }
    if (new_index >= arr.length) {
      let k = new_index - arr.length + 1;
      while ((k -= 1)) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing purposes
  };

  // 상점 이미지 왼쪽으로 이동
  const handleImageLeftMove = async (value) => {
    let tempImage = image.map((m) => m);
    const idx = tempImage.indexOf(value);

    if (idx !== 0) {
      tempImage = arrayMove(tempImage, idx, idx - 1);
    }

    await setImage(tempImage);
  };

  // 상세 이미지 오른쪽으로 이동
  const handleImageRightMove = async (value) => {
    let tempImage = image.map((m) => m);
    const idx = tempImage.indexOf(value);

    if (idx !== tempImage.length - 1) {
      tempImage = arrayMove(tempImage, idx, idx + 1);
    }

    await setImage(tempImage);
  };

  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">사용자 관리</li>
            <Link to="/app-users/expert-table" className="breadcrumb-item active mt-1" aria-current="page">
              전문가 목록
            </Link>
            <Link to="r/app-users/expert-create" className="breadcrumb-item active mt-1" aria-current="page">
              전문가 등록
            </Link>
          </ol>
        </nav>
      </div>
      {/* 상점 기본 정보 */}
      <Tabs />
      <div className="row">
        <div className="col-md-12">
          <div className="card">
            <div className="card-body">
              <div className="card-title-wrap">
                <h4 className="card-title">전문가 등록</h4>
              </div>
              <div className="mt-6 card-wrap">
                <table className="app-table w-full">
                  <tr>
                    <td className="th">분류</td>
                    <td colSpan="3">
                      <Select
                        isOptionSelected
                        value={place}
                        onChange={(e) => setPlace(e)}
                        options={[
                          { value: "안경원", label: "안경원" },
                          {
                            value: "시기능연구소",
                            label: "시기능연구소",
                          },
                          { value: "렌즈샵", label: "렌즈샵" },
                          { value: "안과", label: "안과" },
                        ]}
                      />
                    </td>
                  </tr>
                  <tr>
                    <td className="th w-2/12">전문가명</td>
                    <td className="w-4/12">
                      <Form.Control type="text" value={name} onChange={(e) => setName(e.target.value)} />
                    </td>
                    <td className="th w-2/12">연락처</td>
                    <td className="w-4/12">
                      <Form.Control type="text" value={tel} onChange={(e) => setTel(e.target.value)} />
                    </td>
                  </tr>

                  <tr>
                    <td className="th">주소</td>
                    <td colSpan="3">
                      <div className="flex mb-2">
                        <Form.Control className="w-2/12" type="text" value={post} readOnly placeholder="우편번호" />
                        <button onClick={(e) => setMdShow(true)} type="button" className="btn btn-outline-primary btn-sm ml-2">
                          주소찾기
                        </button>
                      </div>
                      <Form.Control className="mb-2" type="text" readOnly value={address} placeholder="주소" />
                      <Form.Control
                        type="text"
                        value={detailAddress}
                        placeholder="상세주소"
                        onChange={(e) => setDetailAddress(e.target.value)}
                      />

                      {/* Modal Start */}
                      <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
                        <Modal.Header closeButton>
                          <DaumPostcode
                            className="h-full"
                            // style={{ height: "500px" }}
                            onComplete={handlePostComplete}
                            {...props}
                          />
                        </Modal.Header>
                      </Modal>
                      {/* Modal Ends */}
                    </td>
                  </tr>
                  <tr>
                    <td className="th w-2/12">업체명</td>
                    <td className="w-4/12">
                      <Form.Control type="text" placeholder="업체명을 입력하세요." />
                    </td>
                    <td className="th w-2/12">영업시간</td>
                    <td className="w-4/12">
                      <Form.Control type="text" placeholder="영업시간을 입력하세요." />
                    </td>
                  </tr>
                  <tr>
                    <td colSpan="3">
                      <Form.Control type="text" value={site} onChange={(e) => setSite(e.target.value)} placeholder="www.example.com" />
                    </td>
                  </tr>
                  <tr>
                    <td className="th">입력 태그</td>
                    <td colSpan="3">
                      <ul className="tag-wrap select-mode">
                        <li className="tag-item select">자가테스트</li>
                        <li className="tag-item">노안</li>
                        <li className="tag-item">눈건강</li>
                        <li className="tag-item select">눈운동</li>
                        <li className="tag-item">컨텐트렌즈</li>
                        <li className="tag-item">시력</li>
                        <li className="tag-item">라섹</li>
                        <li className="tag-item">빛번짐</li>
                        <li className="tag-item">색약</li>
                        <li className="tag-item">색맹</li>
                        <li className="tag-item">아이웨어</li>
                        <li className="tag-item">황반변성</li>
                        <li className="tag-item">잘모르겠어요</li>
                      </ul>
                    </td>
                  </tr>
                  <tr>
                    <td className="th">프로필 사진</td>
                    <td colSpan="3">
                      <div className="form-file">
                        <Form.Control type="file" placeholder="file" />
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td className="th">소개</td>
                    <td colSpan="3">
                      <textarea value="" onChange="" className="expert-text" />
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 btnbar">
        <div className="row foot-edit">
          <button className="btn btn-warning" onClick={goBack}>
            취소하기
          </button>
          <button type="button" className="btn btn-primary" onClick={handleCreate}>
            저장하기
          </button>
        </div>
      </div>
    </>
  );
};

//탭 영역
const Tabs = () => {
  return (
    <div className="tab-wrap">
      <div className="tab-item active">
        <Link to={`#tab1`}>한국어</Link>
      </div>
      <div className="tab-item">
        <Link to={`#tab2`}>영어</Link>
      </div>
      <div className="tab-item">
        <Link to={`#tab3`}>일본어</Link>
      </div>
      <div className="tab-item">
        <Link to={`#tab4`}>중국어</Link>
      </div>
    </div>
  );
};

// 상점 이미지 보여주기
const MultiImages = ({ images, handleImageDelete, handleImageLeftMove, handleImageRightMove }) => {
  // 이미지 삭제
  const handleDelete = (e, index) => {
    handleImageDelete(e, index);
  };

  const handleLeftMove = (e, index) => {
    handleImageLeftMove(e, index);
  };

  const handleRightMove = (e, index) => {
    handleImageRightMove(e, index);
  };

  if (!images || images === "") {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
  try {
    const detailImages = images;
    if (!detailImages || detailImages.length === 0) {
      return <img src={null} className="w-12 h-12" alt="" />;
    }
    const multiImages = detailImages.map((image, index) => {
      return (
        <div>
          <img src={image} className="w-12 h-12 mr-2" alt="" />
          <button onClick={(e) => handleDelete(image)}>x</button>
          <div className={`flex ${index === 0 ? "justify-end" : index === detailImages.length - 1 ? "justify-start" : "justify-between"} `}>
            {index !== 0 && (
              <button onClick={(e) => handleLeftMove(image)} className="bg-blue-700 px-2 text-white ml-1">
                {"< "}
              </button>
            )}

            {index !== detailImages.length - 1 && (
              <button onClick={(e) => handleRightMove(image)} className="bg-blue-700 px-2 text-white mr-1">
                {" >"}
              </button>
            )}
          </div>
        </div>
      );
    });
    return multiImages;
  } catch (e) {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
};

CreatePage.propTypes = {
  id: PropTypes.string.isRequired,
  memberId: PropTypes.string.isRequired,
  defaultStoreId: PropTypes.string.isRequired,
};
export default CreatePage;
