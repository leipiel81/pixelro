/* eslint-disable no-use-before-define */
import React, { useState, useEffect } from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { Button, Modal } from "react-bootstrap";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { ALL_STORES } from "graphql/gql/select";
import { UPDATE_STORE_REGISTER_REPLY, UPDATE_MEMBER_DEFAULT_STORE } from "graphql/gql/update";
import { FormatDateShort } from "components/FormatDate";
import { PhoneFormatter } from "components/PhoneFormatter";
import { SimpleNotification } from "components/SimpleNotification";

const ReactContentPage = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const [acceptShow, setAcceptShow] = useState(false);
  const [refuseShow, setRefuseShow] = useState(false);
  const [clickId, setClickId] = useState();
  const [clickMemberId, setClickMemberId] = useState();

  // member defaultStoreId 변경
  const [memberUpdate, { data: defaultStoreUpdateData }] = useMutation(UPDATE_MEMBER_DEFAULT_STORE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (defaultStoreUpdateData !== undefined) {
      refetch();
    }
  }, [defaultStoreUpdateData]);

  // 답변 값 변경
  const [replyUpdate, { data: updateData }] = useMutation(UPDATE_STORE_REGISTER_REPLY, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      const storeId = updateData.updateStore.id;
      // defaultStoreId 등록
      memberUpdate({ variables: { id: clickMemberId, defaultStoreId: storeId } });
    }
  }, [updateData]);

  const { loading, error, data, refetch } = useQuery(ALL_STORES, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return;
  }

  // store 정보
  let items;
  if (data) {
    items = data.allStores;
    // 대기중 임시로 추가하는 상점 테이블에서 빼기
    items = items.filter((f) => f.id !== "1");
    items = items.filter((f) => f.storeRegister !== "승인");
    // 최근 등록날짜 순으로 나타내기
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  // 화면에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleAcceptButtonClick = (id, array) => {
    // 승인버튼 클릭시 상점 관리자 id 가져오기
    const member = array.storeGroup.members.filter((f) => f.admin === true);
    setClickMemberId(member[0].id);
    setClickId(id);
    setAcceptShow(true);
  };

  const handleRefuseButtonClick = (id) => {
    setClickId(id);
    setRefuseShow(true);
  };

  // 모달에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleModalAccept = () => {
    const value = "승인";
    replyUpdate({ variables: { id: clickId, storeRegister: value } });

    setAcceptShow(false);
  };

  const handleModalRefuse = () => {
    const value = "거절";
    replyUpdate({ variables: { id: clickId, storeRegister: value } });

    setRefuseShow(false);
  };

  const handleTelFormat = (data) => {
    const tel = PhoneFormatter(data.value);
    return <div>{tel}</div>;
  };
  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">미등록 관리</li>
            <Link to="/unregistered-store/request-table" className="breadcrumb-item active mt-1" aria-current="page">
              신청 접수 목록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between">
                <h4 className="card-title">상점 신청 접수 목록</h4>
                <h4 className="card-title">{`Total : ${items.length}`}</h4>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      filterable
                      defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "분류",
                          id: "place",
                          accessor: (d) => d.place,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["place"],
                            }),
                          filterAll: true,
                        },
                        {
                          Header: "이름",
                          id: "name",
                          accessor: (d) => d.name,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["name"] }),
                          filterAll: true,
                          Cell: (row) => (
                            <Link to={`/unregistered-store/request-detail/${row.original.id}`} className="text-blue-700 font-medium">
                              {row.value}
                            </Link>
                          ),
                        },
                        {
                          Header: "전화번호",
                          id: "tel",
                          accessor: (d) => d.tel,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["tel"] }),
                          filterAll: true,
                          Cell: handleTelFormat,
                        },
                        {
                          Header: "주소",
                          id: "address",
                          accessor: (d) => d.address,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["address"],
                            }),
                          filterAll: true,
                        },
                        {
                          Header: "답변",
                          id: "reply",
                          accessor: (d) => d.reply,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["reply"],
                            }),
                          filterAll: true,
                        },
                        {
                          Header: "신청날짜",
                          accessor: "createAt",
                          Filter: () => false,
                          Cell: (row) => <div>{`${FormatDateShort(new Date(parseInt(row.value, 10)))}`}</div>,
                        },
                        {
                          Header: "",
                          accessor: "",
                          Filter: () => false,
                          Cell: (row) => (
                            <>
                              {row.original.storeRegister === "대기" ? (
                                <div>
                                  <button
                                    onClick={(e) => handleAcceptButtonClick(row.original.id, row.original)}
                                    type="button"
                                    className="btn btn-outline-success btn-sm mr-1"
                                  >
                                    승인
                                  </button>
                                  <button
                                    onClick={(e) => handleRefuseButtonClick(row.original.id)}
                                    type="button"
                                    className="btn btn-outline-danger btn-sm"
                                  >
                                    거절
                                  </button>
                                </div>
                              ) : (
                                <div>거절</div>
                              )}
                            </>
                          ),
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
              {/* 승인 Modal Start */}
              <Modal size="sm" show={acceptShow} onHide={() => setAcceptShow(false)} aria-labelledby="example-modal-sizes-title-sm">
                <Modal.Header closeButton>
                  <Modal.Title>상품 등록 요청 승인</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <p>요청을 승인하시겠습니까?</p>
                </Modal.Body>

                <Modal.Footer className="flex-wrap">
                  <Button variant="success btn-sm m-2" onClick={handleModalAccept}>
                    승인
                  </Button>
                  <Button variant="light btn-sm m-2" onClick={() => setAcceptShow(false)}>
                    취소
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Modal Ends */}
              {/* 거절 Modal Start */}
              <Modal size="sm" show={refuseShow} onHide={() => setRefuseShow(false)} aria-labelledby="example-modal-sizes-title-sm">
                <Modal.Header closeButton>
                  <Modal.Title>상품 등록 요청 거절</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <p>요청을 거절하시겠습니까?</p>
                </Modal.Body>

                <Modal.Footer className="flex-wrap">
                  <Button variant="danger btn-sm m-2" onClick={handleModalRefuse}>
                    거절
                  </Button>
                  <Button variant="light btn-sm m-2" onClick={() => setRefuseShow(false)}>
                    취소
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Modal Ends */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReactContentPage;
