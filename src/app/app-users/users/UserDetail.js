import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import ReactTable from "react-table";
import { User } from "graphql/query/select";
import { useHistory, Link } from "react-router-dom";
import { FormatDate } from "components/FormatDate";
import { PhoneFormatter } from "components/PhoneFormatter";
import { observer } from "mobx-react-lite";
import useSettings from "stores/settings";
import { useMutation } from "@apollo/react-hooks";
import { CREATE_LOG } from "graphql/gql/insert";
import { UPDATE_USER } from "graphql/gql/update";
import { Modal, Button } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import publicIp from "public-ip";

const DetailPage = observer(({ match }) => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const { id } = match.params;
  const [mdShow, setMdShow] = useState(false);

  const [logCreate, { data: createLogData }] = useMutation(CREATE_LOG, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (createLogData !== undefined) {
      console.log("ok");
    }
  }, [createLogData]);

  useEffect(() => {
    async function logData() {
      const adminEmail = settings.email;
      const ip = await publicIp.v4();
      const url = `/app-users/user-detail/${id}`;
      const target = "user";
      const detail = "열람";

      logCreate({ variables: { adminEmail, ip, url, target, detail } });
    }

    logData();
  }, []);

  const [withdrawUser, { data: updateData }] = useMutation(UPDATE_USER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });
  useEffect(() => {
    if (updateData !== undefined) {
      history.goBack();
    }
  }, [updateData]);

  const handleWithdrawModal = () => {
    setMdShow(false);
    withdrawUser({
      variables: { id, withdraw: true },
    });
  };

  const { result } = User(id);
  const items = result;

  return (
    <>
      {items && (
        <div>
          {/* breadcrumb */}
          <div>
            <nav aria-label="breadcrumb" role="navigation">
              <ol className="breadcrumb">
                <li className="breadcrumb-item active">사용자 관리</li>
                <Link to="/app-users/user-table" className="breadcrumb-item active mt-1" aria-current="page">
                  회원 목록
                </Link>
                <Link to={`/app-users/user-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
                  회원 정보
                </Link>
              </ol>
            </nav>
          </div>
          <Tabs />
          <InfoForm items={items} />
          <TestHistory />
          <WorkoutHistory />
          <div className="col-12 btnbar">
            <div className="row justify-between">
              <button className="btn btn-danger" onClick={() => setMdShow(true)}>
                삭제하기
              </button>
              <Link className="btn btn-primary" to={`/app-users/user-edit/${items.id}`}>
                수정하기
              </Link>
            </div>
          </div>
          {/* Modal Starts  */}
          <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
            <Modal.Header closeButton>
              <Modal.Title>사용자 삭제</Modal.Title>
            </Modal.Header>

            <Modal.Body>
              <p>정말 삭제하시겠습니까?</p>
            </Modal.Body>

            <Modal.Footer className="flex-wrap">
              <Button variant="btn btn-primary m-2" onClick={handleWithdrawModal}>
                확인
              </Button>
              <Button variant="btn btn-light m-2" onClick={() => setMdShow(false)}>
                취소
              </Button>
            </Modal.Footer>
          </Modal>
          {/* Modal Ends */}
        </div>
      )}
    </>
  );
});

//tab01
const InfoForm = ({ items }) => {
  return (
    <div className="row tab1">
      <div className="col-md-12">
        <div className="card">
          <div className="card-body">
            <div className="flex justify-between card-title-wrap">
              <h4 className="card-title">기본 정보</h4>
            </div>
            <div className="mt-6 card-wrap">
              <table className="app-table bg-white w-full">
                <tr>
                  <td className="th">이메일</td>
                  <td colSpan="3" className="">
                    {items.email}
                  </td>
                </tr>
                <tr>
                  <td className="th">이름</td>
                  <td colSpan="3" className="">
                    {items.name}
                  </td>
                </tr>
                <tr>
                  <td className="th">전화번호</td>
                  <td colSpan="3" className="">
                    {PhoneFormatter(items.tel)}
                  </td>
                </tr>
                <tr>
                  <td className="th">주소</td>
                  <td colSpan="3" className="">
                    <div className="mb-1">{items.post}</div>
                    <div>
                      {items.address} {items.detailAddress}
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="th">생년월일</td>
                  <td colSpan="3" className="">
                    {items.birthday}
                  </td>
                </tr>
                <tr>
                  <td className="th">직업</td>
                  <td colSpan="3" className="">
                    {items.job}
                  </td>
                </tr>
                <tr>
                  <td className="th">성별</td>
                  <td colSpan="3" className="">
                    {items.gender}
                  </td>
                </tr>
                <tr>
                  <td className="th">가입경로</td>
                  <td colSpan="3" className="">
                    {items.sns}
                  </td>
                </tr>
                <tr className="">
                  <td className="th w-2/12">가입 날짜</td>
                  <td className="w-4/12">{FormatDate(new Date(parseInt(items.createAt, 10)))}</td>
                  <td className="th w-2/12">최근 접속일</td>
                  <td className="w-4/12">{FormatDate(new Date(parseInt(items.signedAt, 10)))}</td>
                </tr>
                <tr>
                  <td className="th">입력 태그</td>
                  <td colSpan="3">
                    <ul className="tag-wrap">
                      <li className="tag-item select">자가테스트</li>
                      <li className="tag-item">노안</li>
                      <li className="tag-item">눈건강</li>
                      <li className="tag-item select">눈운동</li>
                      <li className="tag-item">컨텐트렌즈</li>
                      <li className="tag-item">시력</li>
                      <li className="tag-item">라섹</li>
                      <li className="tag-item">빛번짐</li>
                      <li className="tag-item">색약</li>
                      <li className="tag-item">색맹</li>
                      <li className="tag-item">아이웨어</li>
                      <li className="tag-item">황반변성</li>
                      <li className="tag-item">잘모르겠어요</li>
                    </ul>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

//tab02: react-table로 구성된다는 전제로 스타일링
const TestHistory = () => {
  return (
    <div className="row tab2">
      <div className="col-12">
        <div className="card">
          <div className="card-body">
            <div className="flex justify-between card-title-wrap">
              <h4 className="card-title">테스트 이력</h4>
              <span className="card-sub">Total : 1000</span>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="ReactTable card-wrap">
                  <div className="rt-table">
                    <div className="rt-thead -header">
                      <div className="rt-tr">
                        <div className="rt-th rt-resizable-header -cursor-pointer">
                          <div className="rt-resizable-header-content">테스트 유형</div>
                          <div className="rt-resizer"></div>
                        </div>
                        <div className="rt-th rt-resizable-header -cursor-pointer">
                          <div className="rt-resizable-header-content">결과</div>
                          <div className="rt-resizer"></div>
                        </div>
                        <div className="rt-th rt-resizable-header -cursor-pointer">
                          <div className="rt-resizable-header-content">테스트 일시</div>
                          <div className="rt-resizer"></div>
                        </div>
                      </div>
                    </div>
                      
                    <div className="rt-tbody">
                      <div className="rt-tr-group">
                        <div className="rt-tr -odd">
                          <div className="rt-td">노안 테스트</div>
                          <div className="rt-td">평균 5.2 디옵터, 노안 나이 43세</div>
                          <div className="rt-td">2020.01.21</div>
                        </div>
                      </div>

                      <div className="rt-tr-group">
                        <div className="rt-tr -odd">
                          <div className="rt-td">시력 테스트</div>
                          <div className="rt-td">좌안 시력:1.1, 우안 시력:1.2, 각막 나이 20세</div>
                          <div className="rt-td">2020.01.21</div>
                        </div>
                      </div>

                      <div className="rt-tr-group">
                        <div className="rt-tr -odd">
                          <div className="rt-td">황반변성 테스트</div>
                          <div className="rt-td">정상</div>
                          <div className="rt-td">2020.01.21</div>
                        </div>
                      </div>

                      <div className="rt-tr-group">
                        <div className="rt-tr -odd">
                          <div className="rt-td">빛번짐 테스트</div>
                          <div className="rt-td">이상</div>
                          <div className="rt-td">2020.01.21</div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

//tab03: react-table로 구성된다는 전제로 스타일링
const WorkoutHistory = () => {
  return (
    <div className="row tab3">
      <div className="col-12">
        <div className="card">
          <div className="card-body">
            <div className="flex justify-between card-title-wrap">
              <h4 className="card-title">눈운동 이력</h4>
              <span className="card-sub">Total : 1000</span>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="ReactTable card-wrap">
                  <div className="rt-table">
                    <div className="rt-thead -header">
                      <div className="rt-tr">
                        <div className="rt-th rt-resizable-header -cursor-pointer">
                          <div className="rt-resizable-header-content">운동 유형</div>
                          <div className="rt-resizer"></div>
                        </div>
                        <div className="rt-th rt-resizable-header -cursor-pointer">
                          <div className="rt-resizable-header-content">운동명</div>
                          <div className="rt-resizer"></div>
                        </div>
                        <div className="rt-th rt-resizable-header -cursor-pointer">
                          <div className="rt-resizable-header-content">결과</div>
                          <div className="rt-resizer"></div>
                        </div>
                        <div className="rt-th rt-resizable-header -cursor-pointer">
                          <div className="rt-resizable-header-content">테스트 일시</div>
                          <div className="rt-resizer"></div>
                        </div>
                      </div>
                    </div>
                      
                    <div className="rt-tbody">
                      <div className="rt-tr-group">
                        <div className="rt-tr -odd">
                          <div className="rt-td">노안 테스트</div>
                          <div className="rt-td">노안 테스트</div>
                          <div className="rt-td">평균 5.2 디옵터, 노안 나이 43세</div>
                          <div className="rt-td">2020.01.21</div>
                        </div>
                      </div>

                      <div className="rt-tr-group">
                        <div className="rt-tr -odd">
                          <div className="rt-td">시력 테스트</div>
                          <div className="rt-td">시력 테스트</div>
                          <div className="rt-td">좌안 시력:1.1, 우안 시력:1.2, 각막 나이 20세</div>
                          <div className="rt-td">2020.01.21</div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

//탭 영역
const Tabs = () => {
  return (
    <div className="tab-wrap">
      <div className="tab-item active">
        <Link to={`#tab1`}>기본정보</Link>
      </div>
      <div className="tab-item">
        <Link to={`#tab2`}>테스트 이력</Link>
      </div>
      <div className="tab-item">
      <Link to={`#tab3`}>눈운동 이력</Link>
      </div>
    </div>
  );
};


DetailPage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default DetailPage;
