import React, { useState, useEffect } from "react";
import { useHistory, Link } from "react-router-dom";
import { ALL_USERS, ALL_PRODUCTS, ALL_NOTICE_BOARDS, ALL_SERVICE_CENTERS } from "graphql/gql/select";
import { SEND_MANY_USER_FCM } from "graphql/gql/update";
import Paginator from "react-hooks-paginator";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Form, Modal, Button } from "react-bootstrap";
import { PhoneFormatter } from "components/PhoneFormatter";
import useSettings from "stores/settings";
import { SimpleNotification } from "components/SimpleNotification";
import { observer } from "mobx-react-lite";

const AppPushMessage = observer(() => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">사용자 관리</li>
            <Link to="/app-users/app-push-message" className="breadcrumb-item active mt-1" aria-current="page">
                푸시 메시지 발송
            </Link>
          </ol>
        </nav>
      </div>
      <MesssageForm />
    </div>
  );
});

const MesssageForm = () => {
  const [users, setUsers] = useState([]);
  const [title, setTitle] = useState();
  const [message, setMessage] = useState();
  const [type, setType] = useState("매거진");
  const [targetUrl, setTargetUrl] = useState();

  const [magazineShow, setMagazineShow] = useState(false);
  const [eventShow, setEventShow] = useState(false);
  const [productShow, setProductShow] = useState(false);
  const [noticeShow, setNoticeShow] = useState(false);

  const [sendShow, setSendShow] = useState(false);

  const handleTargetChoice = () => {
    if (type === "매거진") setMagazineShow(true);
    if (type === "이벤트") setEventShow(true);
    if (type === "상품") setProductShow(true);
    if (type === "공지") setNoticeShow(true);
  };

  const handleMagazineValue = (targetId) => {
    const url = `https://www.nenoons.com/app-magazine-detail/${targetId}`;
    setTargetUrl(url);
    setMagazineShow(false);
  };

  const handleEventValue = (targetId) => {
    const url = `https://www.nenoons.com/app-event-detail/${targetId}`;
    setTargetUrl(url);
    setEventShow(false);
  };

  const handleProductValue = (targetId) => {
    const url = `https://www.nenoons.com/app-product-detail/${targetId}`;
    setTargetUrl(url);
    setProductShow(false);
  };

  const handleNoticeValue = (targetId) => {
    const url = `https://www.nenoons.com/app-notice-detail/${targetId}`;
    setTargetUrl(url);
    setNoticeShow(false);
  };

  // 체크한 사용자 저장
  const handleUsers = (checkUser) => {
    setUsers(checkUser);

    console.log("checkUser :: ", checkUser);
  };

  const [sendManyUserFcm, { data: sendManyUserFcmData }] = useMutation(SEND_MANY_USER_FCM, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (sendManyUserFcmData !== undefined) {
      console.log("sendManyUserFcmData :: ", sendManyUserFcmData);
      if (sendManyUserFcmData.sendManyUserFcm.result === "ok") window.location.reload();
    }
  }, [sendManyUserFcmData]);

  //   푸쉬메세지 보내기
  const handleSendModal = async () => {
    setSendShow(false);

    await sendManyUserFcm({
      variables: {
        users: `[${users.map((m) => `"${m}"`)}]`,
        title,
        message,
        link: `${targetUrl}`,
        action: "",
        value: "",
      },
    });
  };

  const handleSend = () => {
    if (users.length === 0) {
      SimpleNotification({
        message: "푸쉬메세지를 전송할 사용자를 선택해주세요.",
      });
      return "";
    }

    if (!targetUrl) {
      SimpleNotification({
        message: "이동 타켓을 선택해주세요.",
      });
      return "";
    }

    if (!title || !message) {
      SimpleNotification({
        message: "내용을 입력해주세요.",
      });
      return "";
    }

    setSendShow(true);
  };
  return (
    <div className="wrap">
      <div className="row">
        <div className="col-md-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between card-title-wrap">
                <h4 className="card-title">타겟 선택</h4>
                <div className="radiobox">
                  <label htmlFor="push_magazine" className="radio-box">
                    <input
                      id="push_magazine"
                      name="push_notifications"
                      type="radio"
                      className="form-radio transition duration-150 ease-in-out"
                      defaultChecked
                      onChange={() => setType("매거진")}
                    />
                    <span>매거진</span>
                  </label>

                  <label htmlFor="push_notice" className="radio-box">
                  <input
                    id="push_notice"
                    name="push_notifications"
                    type="radio"
                    className="form-radio transition duration-150 ease-in-out"
                    onChange={() => setType("공지")}
                  />
                  <span>공지사항</span>
                  </label>
                </div>
              </div>
              {/* 바디영역 */}
              <div className="wrap">
                <div className="flex items-center justify-between">
                  <div className="w-2/12">
                    <button className="btn btn-default p-2" onClick={handleTargetChoice}>
                      이동 타겟 선택
                    </button>
                  </div>
                  {/* 매거진 모달 */}
                  <Modal show={magazineShow} onHide={() => setMagazineShow(false)} aria-labelledby="example-modal-sizes-title-md">
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="push">
                      <MagazineModal handleValue={handleMagazineValue} />
                    </Modal.Body>
                  </Modal>
              
                  {/* 공지 모달 */}
                  <Modal show={noticeShow} onHide={() => setNoticeShow(false)} aria-labelledby="example-modal-sizes-title-md">
                    <Modal.Header closeButton></Modal.Header>
                    <Modal.Body className="push">
                      <NoticeModal handleValue={handleNoticeValue} />
                    </Modal.Body>
                  </Modal>
                  <div className="w-10/12">
                    <Form.Control type="text" className="form-control" readOnly value={targetUrl} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* 유저 리스트 */}
      <UserForm handleValue={handleUsers} />
      <div className="row mt-4">
        <div className="col-md-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between card-title-wrap">
                <h4 className="card-title">메시지 작성</h4>
              </div>
              {/* 제목 & 메세지 */}
              <div className="card-wrap">
                <table className="app-table w-full">
                  <tr>
                    <td className="th">제목</td>
                    <td colSpan="3" className="">
                      <Form.Control type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
                    </td>
                  </tr>
                  <tr>
                    <td className="th">내용</td>
                    <td colSpan="3" className="">
                      <textarea
                        id="about"
                        rows="5"
                        className="orm-control expert-text form-textarea block w-full transition duration-150 ease-in-out"
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}
                      />
                    </td>
                  </tr>
                </table>
              </div>
              {/* Modal Starts  */}
              <Modal show={sendShow} onHide={() => setSendShow(false)} aria-labelledby="example-modal-sizes-title-md">
                <Modal.Header closeButton>
                  <Modal.Title>푸쉬 메세지 전송</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <p>메세지를 전송하시겠습니까?</p>
                </Modal.Body>

                <Modal.Footer className="flex-wrap">
                  <Button variant="btn btn-primary m-2" onClick={handleSendModal}>
                    확인
                  </Button>
                  <Button variant="btn btn-light m-2" onClick={() => setSendShow(false)}>
                    취소
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Modal Ends */}
            </div>
          </div>
        </div>
      </div>
      <SendReservation />
      {/* 보내기 */}
      <div className="flex w-full items-center justify-center mt-8">
        <button className="btn btn-primary" onClick={handleSend}>
          보내기
        </button>
      </div>
    </div>
  );
};

const UserForm = ({ handleValue }) => {
  const [allUserCheck, setAllUserCheck] = useState(false);
  const [userCheckList, setUserCheckList] = useState([]);

  useEffect(() => {
    handleValue(userCheckList);
  }, [userCheckList]);
  // 구매 데이터 가져오기
  const { loading, error, data } = useQuery(ALL_USERS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <div />;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }
  let items;
  if (data && data.allUsers) {
    // 탈퇴되지않고 fcmToken이 있는 사용자
    items = data.allUsers;
    items = items.filter((f) => f.withdraw === false);
    items = items.filter((f) => f.fcmToken !== "");
  }

  //   사용자 체크
  const handleUserCheck = (id) => {
    const tempUserCheckList = userCheckList.map((m) => m);
    const idx = userCheckList.indexOf(id);

    if (idx > -1) tempUserCheckList.splice(idx, 1);
    if (idx === -1) tempUserCheckList.push(id);

    setUserCheckList(tempUserCheckList);
  };

  //   전체선택 & 전체선택해제
  const handleAllUserCheck = () => {
    if (allUserCheck) {
      setUserCheckList([]);
      setAllUserCheck(false);
    }

    if (!allUserCheck) {
      const tempUserCheckList = items.map((m) => m.id);
      setUserCheckList(tempUserCheckList);
      setAllUserCheck(true);
    }
  };

  if (!items) return <div />;
  return (
    <div className="row mt-4">
      <div className="col-md-12">
        <div className="card overflow-hidden">
          <div className="card-body">
            <div className="flex justify-between card-title-wrap">
              <h4 className="card-title">사용자 목록</h4>
              <div className="radiobox">
                <label htmlFor="choice_all" className="radio-box">
                  <input
                    id="choice_all"
                    name="choice_platform"
                    type="radio"
                    className="form-radio transition duration-150 ease-in-out"
                    defaultChecked
                  />
                  <span>전체</span>
                </label>

                <label htmlFor="choice_ios" className="radio-box">
                <input
                  id="choice_ios"
                  name="choice_platform"
                  type="radio"
                  className="form-radio transition duration-150 ease-in-out"
                />
                <span>아이폰</span>
                </label>

                <label htmlFor="choice_android" className="radio-box">
                  <input
                    id="choice_android"
                    name="choice_platform"
                    type="radio"
                    className="form-radio transition duration-150 ease-in-out"
                  />
                  <span>안드로이드</span>
                </label>
              </div>
            </div>
            <div className="list-wrap">
              <div className="list-contents">
                {items.map((m) => (
                  <div className="mail-list" key={m.id}>
                    <div className="checkbox">
                      <label className="checkbox-label">
                        <input
                          type="checkbox"
                          checked={userCheckList.indexOf(m.id) > -1 || false}
                          onChange={(e) => handleUserCheck(m.id)}
                          className="checkbox-input"
                        />
                        <span></span>
                      </label>
                    </div>
                    <div className="content">
                      <div>
                        <p className="sender-email">{m.email}</p>
                      </div>
                      <div className="flex">
                        {m.name ? (
                          <>
                            <div>
                              <p className="sender-name">{m.name}</p>
                            </div>
                          </>
                        ) : (
                          ""
                        )}
                        <p className="sender-phone">{PhoneFormatter(m.tel) || ""}</p>
                      </div>
                    </div>
                  </div>
                ))}
              </div>

              <div className="put-wrap">
                  <button className="btn btn-sm btn-info put-one">
                    <i className="ti-angle-right"></i>
                    선택 담기
                  </button>
                  <button className="btn btn-sm btn-info put-all">
                    <i className="ti-angle-double-right"></i>
                    모두 담기
                  </button>
                  <button className="btn btn-sm btn-danger out-one">
                    <i className="ti-angle-left"></i>
                    선택 제외
                  </button>
                  <button className="btn btn-sm btn-danger out-all">
                    <i className="ti-angle-double-left"></i>
                    모두 제외
                  </button>
              </div>

              <div className="list-contents">
                {/*담기는 리스트 영역 */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const SendReservation = () => {
  return (
    <div className="row mt-4">
      <div className="col-md-12">
        <div className="card">
          <div className="card-body">
            <div className="flex justify-between card-title-wrap">
              <h4 className="card-title">예약설정</h4>
              <div className="checkbox">
                <label className="checkbox-label">
                  <input
                    name=""
                    type="checkbox"
                    className="checkbox-input"
                  />
                  <span>예약 발송하기</span>
                </label>
              </div>
            </div>

            <div className="reserv-wrap row">
              <div className="col-md-6 col-xs-12">
                <input type="date" className="form-control" placeholder="원하는 날짜를 입력하세요."></input>
              </div>
              <div className="col-md-6 col-xs-12">
                <input type="time" className="form-control" placeholder="원하는 시간을 입력하세요."></input>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

// 매거진 모달
const MagazineModal = ({ handleValue }) => {
  // 검색
  const [searchItem, setSearchItem] = useState();
  // 상품 관련 데이터

  const [items, setItems] = useState(null);
  const [products, setProducts] = useState();

  // 페이지네이션 적용
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentItem, setCurrentItem] = useState([]);

  const pageLimit = 7;

  // 페이지네이션 초기화
  useEffect(() => {
    if (products) {
      setCurrentItem(products.slice(offset, offset + pageLimit));
    }
  }, [offset, products]);

  //   배너 클릭시 이동할 상품 선택
  const handleProductChoice = (productId) => {
    handleValue(productId);
    setSearchItem();
    setProducts(items);
  };

  // 검색
  const handleSearch = () => {
    setProducts(items);

    if (searchItem) {
      setProducts(
        items.filter((f) => {
          return f.title.indexOf(searchItem) > -1;
        })
      );
    }
  };

  //   상품정보
  const { loading, error, data } = useQuery(ALL_NOTICE_BOARDS, {
    fetchPolicy: "cache-and-network",
    skip: !!items,
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  if (data && data.allNoticeBoards) {
    const resultData = data.allNoticeBoards.filter((f) => f.category === "매거진");
    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });

    setItems(resultData);
    setProducts(resultData);
  }

  if (!items) return <div />;

  return (
    <div className="w-full">
      <div className="sidebar-widget">
        <h4 className="pro-sidebar-title">Search </h4>
        <div className="pro-sidebar-search mt-15">
          <div className="pro-sidebar-search-form" action="#">
            <input type="text" className="form-control" placeholder="Search here..." value={searchItem} onChange={(e) => setSearchItem(e.target.value)} />
            <button onClick={handleSearch}>
              <i className="ti-search" />
            </button>
          </div>
        </div>
      </div>
      <div className="flex mx-auto">
        <div>
          {currentItem &&
            currentItem.map((m) => (
              <div className="px-3 pt-1 mt-1 flex justify-between border-b">
                <div className="justify-start w-3/4">
                  <img src={m.image} className="h-12 rounded-md w-full" alt="dp" />
                  <div className="flex justify-start flex-wrap pb-3 mt-2 w-full">
                    <div className="flex justify-start font-bold">{m.title}</div>
                  </div>
                </div>
                <div className="flex items-center justify-end text-black w-1/4">
                  <button type="button" className="btn btn-warning btn-rounded py-1 px-3" onClick={(e) => handleProductChoice(m.id)}>
                    선택
                  </button>
                </div>
              </div>
            ))}
        </div>
      </div>
      <div className="pro-pagination-style text-center mt-30">
        <Paginator
          totalRecords={products.length}
          pageLimit={pageLimit}
          pageNeighbours={1} // 0, 1, 2
          setOffset={setOffset}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          pageContainerClass="mb-0 mt-0"
          pagePrevText="«"
          pageNextText="»"
        />
      </div>
    </div>
  );
};

// 이벤트 모달
const EventModal = ({ handleValue }) => {
  // 검색
  const [searchItem, setSearchItem] = useState();
  // 상품 관련 데이터

  const [items, setItems] = useState(null);
  const [products, setProducts] = useState();

  // 페이지네이션 적용
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentItem, setCurrentItem] = useState([]);

  const pageLimit = 7;

  // 페이지네이션 초기화
  useEffect(() => {
    if (products) {
      setCurrentItem(products.slice(offset, offset + pageLimit));
    }
  }, [offset, products]);

  //   배너 클릭시 이동할 상품 선택
  const handleProductChoice = (productId) => {
    handleValue(productId);
    setSearchItem();
    setProducts(items);
  };

  // 검색
  const handleSearch = () => {
    setProducts(items);

    if (searchItem) {
      setProducts(
        items.filter((f) => {
          return f.title.indexOf(searchItem) > -1;
        })
      );
    }
  };

  //   상품정보
  const { loading, error, data } = useQuery(ALL_NOTICE_BOARDS, {
    fetchPolicy: "cache-and-network",
    skip: !!items,
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  if (data && data.allNoticeBoards) {
    const resultData = data.allNoticeBoards.filter((f) => f.category === "이벤트");
    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });

    setItems(resultData);
    setProducts(resultData);
  }

  if (!items) return <div />;

  return (
    <div className="w-full">
      <div className="sidebar-widget">
        <h4 className="pro-sidebar-title">Search </h4>
        <div className="pro-sidebar-search mt-15">
          <div className="pro-sidebar-search-form" action="#">
            <input type="text" className="form-control" placeholder="Search here..." value={searchItem} onChange={(e) => setSearchItem(e.target.value)} />
            <button onClick={handleSearch}>
              <i className="ti-search" />
            </button>
          </div>
        </div>
      </div>
      <div className="flex mx-auto">
        <div>
          {currentItem &&
            currentItem.map((m) => (
              <div className="px-3 pt-1 mt-1 flex justify-between border-b">
                <div className="justify-start w-3/4">
                  <img src={m.image} className="h-12 rounded-md w-full" alt="dp" />
                  <div className="flex justify-start flex-wrap pb-3 mt-2 w-full">
                    <div className="flex justify-start font-bold">{m.title}</div>
                  </div>
                </div>
                <div className="flex items-center justify-end text-black w-1/4">
                  <button type="button" className="btn btn-warning btn-rounded py-1 px-3" onClick={(e) => handleProductChoice(m.id)}>
                    선택
                  </button>
                </div>
              </div>
            ))}
        </div>
      </div>
      <div className="pro-pagination-style text-center mt-30">
        <Paginator
          totalRecords={products.length}
          pageLimit={pageLimit}
          pageNeighbours={1} // 0, 1, 2
          setOffset={setOffset}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          pageContainerClass="mb-0 mt-0"
          pagePrevText="«"
          pageNextText="»"
        />
      </div>
    </div>
  );
};

// 상품 모달
const ProductModal = ({ handleValue }) => {
  // 검색
  const [searchItem, setSearchItem] = useState();
  // 상품 관련 데이터

  const [items, setItems] = useState(null);
  const [products, setProducts] = useState();

  // 페이지네이션 적용
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentItem, setCurrentItem] = useState([]);

  const pageLimit = 7;

  // 페이지네이션 초기화
  useEffect(() => {
    if (products) {
      setCurrentItem(products.slice(offset, offset + pageLimit));
    }
  }, [offset, products]);

  //   배너 클릭시 이동할 상품 선택
  const handleProductChoice = (productId) => {
    handleValue(productId);
    setSearchItem();
    setProducts(items);
  };

  // 검색
  const handleSearch = () => {
    setProducts(items);

    if (searchItem) {
      setProducts(
        items.filter((f) => {
          return f.name.indexOf(searchItem) > -1;
        })
      );
    }
  };

  //   상품정보
  const { loading, error, data } = useQuery(ALL_PRODUCTS, {
    fetchPolicy: "cache-and-network",
    skip: !!items,
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  if (data) {
    const resultData = data.allProducts;
    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });

    setItems(resultData);
    setProducts(resultData);
  }

  if (!items) return <div />;

  return (
    <div className="w-full">
      <div className="sidebar-widget">
        <h4 className="pro-sidebar-title">Search </h4>
        <div className="pro-sidebar-search mt-15">
          <div className="pro-sidebar-search-form" action="#">
            <input type="text" placeholder="Search here..." value={searchItem} onChange={(e) => setSearchItem(e.target.value)} />
            <button onClick={handleSearch}>
              <i className="ti-search" />
            </button>
          </div>
        </div>
      </div>
      <div className="flex mx-auto">
        <div>
          {currentItem &&
            currentItem.map((m) => (
              <div className="px-3 pt-1 mt-1 flex justify-between border-b">
                <div className="flex justify-start w-3/4">
                  <img src={m.mainImage} className="w-12 h-12 rounded-full" alt="dp" />
                  <div className="flex justify-start flex-wrap ml-4 pb-3 mt-2 ">
                    <div className="flex justify-start font-bold">{m.name}</div>
                    <div className="inline-flex w-full text-sm text-gray-500">
                      <span className="pr-2">{`${m.brand} | ${m.model}`}</span>
                    </div>
                  </div>
                </div>
                <div className="flex items-center text-black w-1/4">
                  <button type="button" className="btn btn-warning btn-rounded py-1 px-3" onClick={(e) => handleProductChoice(m.id)}>
                    선택
                  </button>
                </div>
              </div>
            ))}
        </div>
      </div>
      <div className="pro-pagination-style text-center mt-30">
        <Paginator
          totalRecords={products.length}
          pageLimit={pageLimit}
          pageNeighbours={1} // 0, 1, 2
          setOffset={setOffset}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          pageContainerClass="mb-0 mt-0"
          pagePrevText="«"
          pageNextText="»"
        />
      </div>
    </div>
  );
};

// 공지 모달
const NoticeModal = ({ handleValue }) => {
  // 검색
  const [searchItem, setSearchItem] = useState();
  // 상품 관련 데이터

  const [items, setItems] = useState(null);
  const [products, setProducts] = useState();

  // 페이지네이션 적용
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentItem, setCurrentItem] = useState([]);

  const pageLimit = 7;

  // 페이지네이션 초기화
  useEffect(() => {
    if (products) {
      setCurrentItem(products.slice(offset, offset + pageLimit));
    }
  }, [offset, products]);

  //   배너 클릭시 이동할 상품 선택
  const handleProductChoice = (productId) => {
    handleValue(productId);
    setSearchItem();
    setProducts(items);
  };

  // 검색
  const handleSearch = () => {
    setProducts(items);

    if (searchItem) {
      setProducts(
        items.filter((f) => {
          return f.title.indexOf(searchItem) > -1;
        })
      );
    }
  };

  //   상품정보
  const { loading, error, data } = useQuery(ALL_SERVICE_CENTERS, {
    fetchPolicy: "cache-and-network",
    skip: !!items,
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  if (data && data.allServiceCenters) {
    const resultData = data.allServiceCenters.filter((f) => f.type === "notice");
    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });

    setItems(resultData);
    setProducts(resultData);
  }

  if (!items) return <div />;

  return (
    <div className="w-full">
      <div className="sidebar-widget">
        <h4 className="pro-sidebar-title">Search </h4>
        <div className="pro-sidebar-search mt-15">
          <div className="pro-sidebar-search-form" action="#">
            <input type="text" placeholder="Search here..." value={searchItem} onChange={(e) => setSearchItem(e.target.value)} />
            <button onClick={handleSearch}>
              <i className="ti-search" />
            </button>
          </div>
        </div>
      </div>
      <div className="flex mx-auto">
        <div className="w-full">
          {currentItem &&
            currentItem.map((m) => (
              <div className="px-3 pt-1 mt-1 flex justify-between border-b">
                <div className="justify-start w-3/4">
                  <div className="flex justify-start flex-wrap pb-3 mt-2 w-full">
                    <div className="flex justify-start font-bold">{m.title}</div>
                  </div>
                </div>
                <div className="flex items-center justify-end text-black w-1/4">
                  <button type="button" className="btn btn-warning btn-rounded py-1 px-3" onClick={(e) => handleProductChoice(m.id)}>
                    선택
                  </button>
                </div>
              </div>
            ))}
        </div>
      </div>
      <div className="pro-pagination-style text-center mt-30">
        <Paginator
          totalRecords={products.length}
          pageLimit={pageLimit}
          pageNeighbours={1} // 0, 1, 2
          setOffset={setOffset}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          pageContainerClass="mb-0 mt-0"
          pagePrevText="«"
          pageNextText="»"
        />
      </div>
    </div>
  );
};

export default AppPushMessage;
