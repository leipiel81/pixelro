import React, { useState } from "react";
import PropTypes from "prop-types";
import { ServiceCenter } from "graphql/query/select";
import { Link, useHistory } from "react-router-dom";
import { FormatDate } from "components/FormatDate";
import useSettings from "stores/settings";
import { Button, Modal } from "react-bootstrap";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_SERVICE_CENTER } from "graphql/gql/delete";

const DetailPage = () => {

  const [mdShow, setMdShow] = useState(false);
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  return (
    <>
      <div>
        <div>
          <nav aria-label="breadcrumb" role="navigation">
            <ol className="breadcrumb">
              <li className="breadcrumb-item active">사용자 관리</li>
              <Link to="/app-user/search-history" className="breadcrumb-item active mt-1" aria-current="page">
                검색기록 내역
              </Link>
            </ol>
          </nav>    
        </div>
        <InfoForm />  
      </div>
    </>
  );
};

const InfoForm = () => {
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="card">
          <div className="card-body">
            <div className="flex justify-between card-title-wrap">
              <h4 className="card-title">검색기록 내역</h4>
            </div>
            <div className="card-wrap">
              <div className="overflow-hidden">
                <>
                  <table className="app-table w-full">
                    <thead>
                      <tr>
                        <td className="th">
                          검색일자
                        </td>
                        <td className="th">
                          검색 키워드
                        </td>
                        <td className="th">
                          선택 조건
                        </td>
                        <td className="th">
                          선택 태그
                        </td>

                      </tr>
                    </thead>
                    <tbody className="bg-white">
                      <tr>
                        <td>
                          2020-07-05 01:25:03
                        </td>
                        <td>
                          노안
                        </td>
                        <td>
                          안경원, 안과, 시기능
                        </td>
                        <td>
                          시력, 황반변성, 빛번짐, 라섹, 눈운동, 아이웨어
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  {/* Modal Starts  */}
                 
                  {/* Modal Ends */}
                </>

                <Page />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

//다른 곳에서 쓰는 페이지 그대로 쓰면 됨
const Page = () => {
  return (
    <div className="bg-white flex items-center justify-between board-pagination">
      <div className="hidden sm:block">
        <p className="text-sm leading-5 text-gray-700">
          <span className="font-medium mx-1">1</span>
          {" ~  "}
          <span className="font-medium mx-1">10</span>
          {" / 총 "}
          <span className="font-medium mx-1">20</span>
          {" 건"}
        </p>
      </div>
      <div className="flex-1 flex justify-between sm:justify-end"></div>
    </div>
  );
};

export default DetailPage;
