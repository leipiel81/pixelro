/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import PropTypes from "prop-types";
import { AllNoticeBoards } from "graphql/query/select";
import { FormatDate } from "components/FormatDate";
import Pagination from "components/Pagination";

const Tables = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }
  const { result } = AllNoticeBoards();
  const items = result && result.filter((f) => f.category === "매거진");

  const [pageOfItems, setPageOfItems] = useState([]);
  const [page, setPage] = useState(1);

  function onChangePage(pageOfItems, pageNumber) {
    setPageOfItems(pageOfItems);
    setPage(pageNumber);
    // 페이지 이동시 맨 위로 스크롤 이동
    window.scrollTo(0, 0);
  }

  return (
    <>
      <div>
        <TableHeader />
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card-body">
                <div className="flex justify-between card-title-wrap">
                  <h4 className="card-title">매거진 목록</h4>
                  <Link className="btn btn-primary" to="/notice-board/magazine-create">
                    등록하기
                  </Link>
                </div>
                <div className="card-wrap">
                  <div className="overflow-hidden">
                    <TableForm items={items} pageOfItems={pageOfItems} />
                    <Page items={items} page={page} onChangePage={onChangePage} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

// /* breadcrumb */
const TableHeader = () => {
  return (
    <div>
      <nav aria-label="breadcrumb" role="navigation">
        <ol className="breadcrumb">
          <li className="breadcrumb-item active">게시판 관리</li>
          <Link to="/notice-board/magazine-table" className="breadcrumb-item active mt-1" aria-current="page">
            매거진 목록
          </Link>
        </ol>
      </nav>
    </div>
  );
};

const TableForm = ({ items, pageOfItems }) => {
  return (
    <table className="app-table min-w-full">
      <thead className="">
        <tr>
          <td className="th">
            대표 이미지
          </td>
          <td className="th">
            제목
          </td>

          <td className="th">
            등록날짜
          </td>
          <td className="th"></td>
          <td className="th"></td>
        </tr>
      </thead>
      {items && items.length === 0 ? (
        <tr>
          <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
            <div>등록된 내용이 없습니다.</div>
          </td>
        </tr>
      ) : (
        pageOfItems && (
          <tbody className="bg-white">
            {pageOfItems.map((m) => (
              <tr>
                <td className="">
                  <img src={m.image} alt="" className="mgz-thumb" />
                </td>
                <td className="">
                  {m.title}
                </td>
                <td className="">
                  {FormatDate(new Date(parseInt(m.createAt, 10)))}
                </td>
                <td className="">
                  <Link className="ti-pencil-alt" to={`/notice-board/magazine-detail/${m.id}`}>
                    상세보기
                  </Link>
                </td>
                <td className="">
                  <Link className="ti-pencil-alt" to={`/notice-board/magazine-edit/${m.id}`}>
                    수정하기
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        )
      )}
    </table>
  );
};

Tables.propTypes = {
  pageOfItems: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
};

const Page = (props) => {
  const PAGE_SIZE = 10;
  const { items, page, onChangePage } = props;
  return (
    <div className="bg-white flex items-center justify-between board-pagination">
      <div className="hidden sm:block">
        <p className="text-sm leading-5 text-gray-700">
          <span className="font-medium mx-1">{(page - 1) * PAGE_SIZE + 1}</span>
          {" ~  "}
          <span className="font-medium mx-1">{items && items.length < page * PAGE_SIZE ? items.length : page * PAGE_SIZE}</span>
          {" / 총 "}
          <span className="font-medium mx-1">{items && items.length}</span>
          {" 건"}
        </p>
      </div>
      <div className="flex-1 flex justify-between sm:justify-end">{items && <Pagination items={items} onChangePage={onChangePage} />}</div>
    </div>
  );
};

Page.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  page: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
};

export default Tables;
