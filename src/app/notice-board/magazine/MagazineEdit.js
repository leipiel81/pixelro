/* eslint-disable no-nested-ternary */
/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useQuery, useMutation } from "@apollo/react-hooks";
import Paginator from "react-hooks-paginator";
import { Form, Modal } from "react-bootstrap";
import { MULTI_FILE } from "graphql/gql/insert";
import { UPDATE_NOTICE_BOARD, UPDATE_NOTICE_BOARD_CONTENT } from "graphql/gql/update";
import { NOTICE_BOARD, ALL_PRODUCTS } from "graphql/gql/select";
import { SimpleNotification } from "components/SimpleNotification";
import { Editor } from "@tinymce/tinymce-react";

const EditPage = ({ match }) => {
  const { id } = match.params;

  //  매거진
  const { loading, error, data } = useQuery(NOTICE_BOARD, {
    fetchPolicy: "cache-and-network",
    variables: { id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    items = data.noticeBoard;
  }

  if (!items) return <div />;

  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">게시판 관리</li>
            <Link to="/notice-board" className="breadcrumb-item active mt-1" aria-current="page">
              매거진
            </Link>
            <Link to="/notice-board/magazine-create" className="breadcrumb-item active mt-1" aria-current="page">
              매거진 수정
            </Link>
          </ol>
        </nav>
      </div>
      <Content items={items} />
    </>
  );
};

EditPage.propTypes = {
  id: PropTypes.string.isRequired,
};

const Content = ({ items }) => {
  const history = useHistory();
  const goBack = () => {
    history.push("/notice-board/magazine-table");
  };

  // --------------------------
  // -- 데이터 값 저장 --

  const { id, category } = items;
  const [image, setImage] = useState(items.image);
  const [title, setTitle] = useState(items.title);

  const [content, setContent] = useState("");
  const [contentImages, setContentImages] = useState([]);
  useEffect(() => {
    fetch(items.content)
      .then((res) => res.text())
      .then((body) => {
        setContent(body);
      });
  }, []);

  const [visible, setVisible] = useState(items.visible);
  const [link1, setLink1] = useState(items.link1);
  const [linkImage1, setLinkImage1] = useState(items.linkImage1);
  const [link2, setLink2] = useState(items.link2);
  const [linkImage2, setLinkImage2] = useState(items.linkImage2);
  const [link3, setLink3] = useState(items.link3);
  const [linkImage3, setLinkImage3] = useState(items.linkImage3);

  // url 직접입력
  const [urlOpen1, setUrlOpen1] = useState(false);
  const [urlOpen2, setUrlOpen2] = useState(false);
  const [urlOpen3, setUrlOpen3] = useState(false);

  // 상품 관련
  // 검색
  const [searchItem, setSearchItem] = useState();

  // 상품 관련 데이터
  const [mdShow1, setMdShow1] = useState(false);
  const [mdShow2, setMdShow2] = useState(false);
  const [mdShow3, setMdShow3] = useState(false);
  const [productItems, setProductItems] = useState(null);
  const [products, setProducts] = useState();

  // 페이지네이션 적용
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentItem, setCurrentItem] = useState([]);

  const pageLimit = 7;

  // 페이지네이션 초기화
  useEffect(() => {
    if (products) {
      setCurrentItem(products.slice(offset, offset + pageLimit));
    }
  }, [offset, products]);

  //---------------------------

  // 내용 등록
  const [contentUpdate, { data: updateIntroData }] = useMutation(UPDATE_NOTICE_BOARD_CONTENT, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateIntroData !== undefined) {
      goBack();
    }
  }, [updateIntroData]);

  // 정보 업데이트
  const [magazineUpdate, { data: updateData }] = useMutation(UPDATE_NOTICE_BOARD, {
    onError(err) {
      console.log("updateAdmin: err=", err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });
  useEffect(() => {
    if (updateData !== undefined) {
      contentUpdate({ variables: { id, content: `${content}` } });
    }
  }, [updateData]);

  const handleUpdate = () => {
    if (!title) {
      SimpleNotification({
        title: "",
        message: "필수 정보를 입력해주세요.",
      });
      return;
    }
    magazineUpdate({
      variables: {
        id,
        category,
        image,
        title,
        link1,
        linkImage1,
        link2,
        linkImage2,
        link3,
        linkImage3,
        visible,
        no: "",
        startAt: "",
        endAt: "",
      },
    });
  };

  // 이미지 관련 함수들

  //  url 이미지 저장
  const [url1ImageUpload, { data: url1ImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (url1ImageData !== undefined) {
      const result = `${url1ImageData.multiUpload[0].filename}`;
      setLinkImage1(result);
    }
  }, [url1ImageData]);

  //  url2 이미지 저장
  const [url2ImageUpload, { data: url2ImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (url2ImageData !== undefined) {
      const result = `${url2ImageData.multiUpload[0].filename}`;
      setLinkImage2(result);
    }
  }, [url2ImageData]);

  //  url3 이미지 저장
  const [url3ImageUpload, { data: url3ImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (url3ImageData !== undefined) {
      const result = `${url3ImageData.multiUpload[0].filename}`;
      setLinkImage3(result);
    }
  }, [url3ImageData]);
  //  메인 이미지 저장
  const [mainImageUpload, { data: mainImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (mainImageData !== undefined) {
      const result = `${mainImageData.multiUpload[0].filename}`;
      setImage(result);
    }
  }, [mainImageData]);

  const introUpdateFunction = async (tempDetailImage) => {
    let tempIntro = content;
    // 이미지 저장과 동시에 editor에 표현
    for (let i = 0; i < tempDetailImage.length; i += 1) {
      tempIntro += `<p><img style="width : 100%;" src="${tempDetailImage[i]}" alt=""></p>`;
    }

    await setContent(tempIntro);
  };

  //  상세 이미지 저장
  // 이미지 저장
  const [contentImageUpload, { data: contentData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (contentData !== undefined) {
      const tempDetailImage = [];
      contentImages.map((m) => tempDetailImage.push(m));
      contentData.multiUpload.map((m) => tempDetailImage.push(m.filename));

      // 이미지 저장과 동시에 editor에 표현
      introUpdateFunction(tempDetailImage);

      setContentImages(tempDetailImage);
    }
  }, [contentData]);

  // 이미지 추가
  const handleContentImageChange = async (e) => {
    const { files } = e.target;

    const fileArray = [];
    if (files) {
      for (let i = 0; i < files.length; i += 1) {
        fileArray.push(files[i]);
      }
    }
    const filesSize = `[${fileArray.map((m) => m.size)}]`;
    // 바로 추가 보내기
    await contentImageUpload({ variables: { files: fileArray, size: filesSize } });
  };
  // 이미지 선택
  const handleImageClick = async (image) => {
    const tempIntro = `${content}<p><img style="width : 100%;" src="${image}" alt=""></p>`;
    setContent(tempIntro);
  };
  const handleEditorChange = (value) => {
    setContent(value);
  };

  // url 이미지 변경
  const handleUrlImageChange = async (e, type) => {
    const file = e.target.files[0];

    if (type === "1") {
      // 이미지 저장
      await url1ImageUpload({ variables: { files: file, size: `[${file.size}]` } });
    }

    if (type === "2") {
      // 이미지 저장
      await url2ImageUpload({ variables: { files: file, size: `[${file.size}]` } });
    }

    if (type === "3") {
      // 이미지 저장
      await url3ImageUpload({ variables: { files: file, size: `[${file.size}]` } });
    }
  };

  // url 이미지 삭제
  const handleUrlDelete = async (type) => {
    if (type === "1") {
      // 이미지 삭제
      setLinkImage1("");
    }

    if (type === "2") {
      // 이미지 삭제
      setLinkImage2("");
    }

    if (type === "3") {
      // 이미지 삭제
      setLinkImage3("");
    }
  };

  // 메인 이미지 변경
  const handleImageChange = async (e) => {
    const file = e.target.files[0];

    // 이미지 저장
    await mainImageUpload({ variables: { files: file, size: `[${file.size}]` } });
  };

  //   링크1 클릭시 이동할 상품 선택
  const handleProductChoice1 = (productId) => {
    setUrlOpen1(true);
    const url = `/app-product-detail/${productId}`;

    setLink1(url);
    setMdShow1(false);
    setSearchItem();
    setProducts(productItems);
  };

  //   링크2 클릭시 이동할 상품 선택
  const handleProductChoice2 = (productId) => {
    setUrlOpen2(true);
    const url = `/app-product-detail/${productId}`;

    setLink2(url);
    setMdShow2(false);
    setSearchItem();
    setProducts(items);
  };

  //   링크3 클릭시 이동할 상품 선택
  const handleProductChoice3 = (productId) => {
    setUrlOpen3(true);
    const url = `/app-product-detail/${productId}`;

    setLink3(url);
    setMdShow3(false);
    setSearchItem();
    setProducts(items);
  };
  //   상품정보
  const { loading, error, data } = useQuery(ALL_PRODUCTS, {
    fetchPolicy: "cache-and-network",
    skip: !!productItems,
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  if (data) {
    const resultData = data.allProducts;
    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });

    setProductItems(resultData);
    setProducts(resultData);
  }

  // 검색
  const handleSearch = () => {
    setProducts(productItems);

    if (searchItem) {
      setProducts(
        productItems.filter((f) => {
          return f.name.indexOf(searchItem) > -1;
        })
      );
    }
  };

  if (!productItems) return <div />;

  const handleUrlWrite = (type) => {
    if (type === "1") {
      setLink1("");
      setUrlOpen1(false);
    }

    if (type === "2") {
      setLink2("");
      setUrlOpen2(false);
    }

    if (type === "3") {
      setLink3("");
      setUrlOpen3(false);
    }
  };
  return (
    <>
      {/* 매거진 정보 */}
      <div className="row">
        <div className="col-md-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between card-title-wrap">
                <h4 className="card-title">매거진 수정</h4>
              </div>
              <div className="mt-6">
                {/* 배너 이미지 등록 */}
                <div className="card-wrap">
                  <table className="app-table bg-white w-full">
                    <tr>
                      <td className="th w-1/6">대표 이미지</td>
                      <td>
                        <div className="">
                          <div className="w-full">
                            <div className="form-file">
                              <form
                                onSubmit={() => {
                                  console.log("Submitted");
                                }}
                                encType="multipart/form-data"
                                className="w-full"
                              >
                                <label className="" for="img_primary" className="mb-0">파일을 선택하세요.<i className="ti-upload"></i></label>
                                <input name="document" id="img_primary" type="file" accept="image/*" method="POST" onChange={handleImageChange} />
                              </form>
                            </div>
                          </div>
                          <img src={image && image} className="mgz-upload-thumb" />
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
                {/* url 등록 */}
                <div className="card-wrap">
                  <table className="app-table bg-white w-full">
                    {/* url1 */}
                    <tr>
                      <td className="th w-1/6">동영상 링크</td>
                      <td>
                          <Form.Control
                            type="text"
                            className="form-control"
                            value={link1}
                            onChange={(e) => setLink1(e.target.value)}
                            placeholder="Youtube 공유 링크를 넣어주세요."
                          />
                      </td>
                    </tr>
                  </table>
                </div>

                {/* 내용 등록 */}
                <div className="card-wrap">
                  <table className="app-table bg-white w-full">
                    <tr>
                      <td className="th w-1/6">제목</td>
                      <td>
                        <Form.Control type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
                      </td>
                    </tr>
                    <tr>
                      <td className="th w-1/6">입력태그</td>
                      <td>
                        <ul className="tag-wrap select-mode">
                          <li className="tag-item select">자가테스트</li>
                          <li className="tag-item">노안</li>
                          <li className="tag-item">눈건강</li>
                          <li className="tag-item select">눈운동</li>
                          <li className="tag-item">컨텐트렌즈</li>
                          <li className="tag-item">시력</li>
                          <li className="tag-item">라섹</li>
                          <li className="tag-item">빛번짐</li>
                          <li className="tag-item">색약</li>
                          <li className="tag-item">색맹</li>
                          <li className="tag-item">아이웨어</li>
                          <li className="tag-item">황반변성</li>
                          <li className="tag-item">잘모르겠어요</li>
                        </ul>
                      </td>
                    </tr>
                  </table>
                </div>
                {/* 내용 소개 */}
                <div className="row mt-4">
                  <div className="col-md-12">
                    <div className="editor-container">
                      네이버 스마트 에디터 영역
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 btnbar">
        <div className="row foot-edit">
          <button className="btn btn-warning" onClick={goBack}>
            취소하기
          </button>
          <button type="button" className="btn btn-primary" onClick={handleUpdate}>
            저장하기
          </button>
        </div>
      </div>
    </>
  );
};

// 내용에 들어가는 이미지
const MultiContentImages = ({ images, handleImageClick }) => {
  // 이미지 삭제
  const handleClickImage = (e, index) => {
    handleImageClick(e, index);
  };

  if (!images || images === "") {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
  try {
    const detailImages = images;
    if (!detailImages || detailImages.length === 0) {
      return <img src={null} className="w-12 h-12" alt="" />;
    }
    const multiImages = detailImages.map((image, index) => {
      return (
        <div className="items-center">
          <img src={image} className="w-12 h-12 mr-2" alt="" />
          <button onClick={(e) => handleClickImage(image)} className="border border-blue-500 rounded-md text-blue-700 px-1">
            선택
          </button>
        </div>
      );
    });
    return multiImages;
  } catch (e) {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
};

export default EditPage;
