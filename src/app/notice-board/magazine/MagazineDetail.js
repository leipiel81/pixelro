/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Button, Modal } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import { NOTICE_BOARD } from "graphql/gql/select";
import { DELETE_NOTICE_BOARD } from "graphql/gql/delete";

const DetailPage = ({ match }) => {
  const { id } = match.params;

  //  매거진
  const { loading, error, data } = useQuery(NOTICE_BOARD, {
    fetchPolicy: "cache-and-network",
    variables: { id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    items = data.noticeBoard;
  }

  if (!items) return <div />;
  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">게시판 관리</li>
            <Link to="/notice-board/magazine-table" className="breadcrumb-item active mt-1" aria-current="page">
              매거진 목록
            </Link>
            <Link to={`/notice-board/magazine-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              매거진 정보
            </Link>
          </ol>
        </nav>
      </div>
      <Content items={items} />
    </>
  );
};

DetailPage.propTypes = {
  id: PropTypes.string.isRequired,
};

const Content = ({ items }) => {
  const [mdShow, setMdShow] = useState(false);

  const history = useHistory();
  const goBack = () => {
    history.push("/notice-board/magazine-table");
  };

  const [noticeBoardDelete, { data: deleteData }] = useMutation(DELETE_NOTICE_BOARD, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (deleteData !== undefined) {
      goBack();
    }
  }, [deleteData]);

  const handleDelete = () => {
    setMdShow(false);
    noticeBoardDelete({ variables: { id: items.id } });
  };

  return (
    <>
      <div className="row">
        <div className="col-md-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between card-title-wrap">
                <h4 className="card-title">매거진 정보</h4>
              </div>
              {/* 대표 이미지 */}
              <div className="card-wrap">
                <img className="mgz-detail-img" src={items.image} alt="" />
              </div>

              {/* 영상 영역 :: 입력 없으면 display: none 필요 */}
              <div className="card-wrap">
                <YoutubeLink />
              </div>
              {/* 내용 */}
              <div className="card-wrap">
                <table className="app-table bg-white w-full">
                  <tr>
                    <td className="th w-1/6">제목</td>
                    <td colSpan="3">
                      {items.title}
                    </td>
                  </tr>
                  <tr>
                    <td className="th w-1/6">입력태그</td>
                    <td colSpan="3">
                      <ul className="tag-wrap">
                        <li className="tag-item select">자가테스트</li>
                        <li className="tag-item">노안</li>
                        <li className="tag-item">눈건강</li>
                        <li className="tag-item select">눈운동</li>
                        <li className="tag-item">컨텐트렌즈</li>
                        <li className="tag-item">시력</li>
                        <li className="tag-item">라섹</li>
                        <li className="tag-item">빛번짐</li>
                        <li className="tag-item">색약</li>
                        <li className="tag-item">색맹</li>
                        <li className="tag-item">아이웨어</li>
                        <li className="tag-item">황반변성</li>
                        <li className="tag-item">잘모르겠어요</li>
                      </ul>
                    </td>
                  </tr>
                  <tr>
                    <td colSpan="4">
                      <ContentForm items={items} />
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 grid-margin mt-4">
        <div className="row justify-between">
          <button className="btn btn-danger" onClick={() => setMdShow(true)}>
            삭제하기
          </button>
          <Link className="btn btn-primary" to={`/notice-board/magazine-edit/${items.id}`}>
            수정하기
          </Link>
        </div>
        <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
          <Modal.Header closeButton>
            <Modal.Title>해당 내용을 삭제하시겠습니까?</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <p>삭제를 원하시면 삭제 버튼을 눌러주세요.</p>
          </Modal.Body>

          <Modal.Footer className="fleex-wrap">
            <Button variant="danger m-2" onClick={handleDelete}>
              삭제
            </Button>
            <Button variant="primary m-2" onClick={() => setMdShow(false)}>
              취소
            </Button>
          </Modal.Footer>
        </Modal>
        {/* Modal Ends */}
      </div>
    </>
  );
};

const ContentForm = ({ items }) => {
  const [content, setContent] = useState("");

  fetch(items.content)
    .then((res) => res.text())
    .then((body) => {
      setContent(body);
      console.log("body :: ", body);
    });
  return (
    <div className="w-full">
      <div
        className="max-w-sm table-intro table-responsive  cart-table-intro w-full bg-white html-content"
        introeditable="true"
        dangerouslySetInnerHTML={{ __html: content }}
      />
    </div>
  );
};

/*유투브 링크*/
const YoutubeLink = () => {
  let link = 'https://www.youtube.com/watch?v=Vp9I_m6znMM&ab_channel=STUDIOCHOOM%5B%EC%8A%A4%ED%8A%9C%EB%94%94%EC%98%A4%EC%B6%A4%5D'; //입력 링크 담기

  function youtubeId(url) {
    let tag = "";
    if (url) {
      let regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
      const matchs = url.match(regExp);
      if (matchs) {
        tag +='https://www.youtube.com/embed/' + matchs[7];
      }
      return tag;
    }
  }

  return (
    <div className="movie-container">
      <iframe src={youtubeId(link)} frameborder='0' allowfullscreen></iframe>
    </div>
  );
};

export default DetailPage;
