import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { observer } from "mobx-react-lite";
import { ALL_ORDERS, ALL_RESERVATIONS, ALL_LENS_RESERVATIONS, ALL_USERS, ALL_STORES, ALL_PRODUCTS } from "graphql/gql/select";
import { useHistory } from "react-router-dom";
import { FormatDate } from "components/FormatDate";
import { SimpleNotification } from "components/SimpleNotification";
import useSettings from "stores/settings";
import HomeMemberQna from "./HomeMemberQna";
import HomeUserQna from "./HomeUserQna";

const DashBoard = observer(() => {
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  // 오늘 날짜 가져오기 YYYY-MM-DD
  const date = new Date();

  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();

  let filterDate = `${year}-${month}-${day}`;
  if (month < 10) {
    filterDate = `${year}-0${month}-${day}`;
  }

  if (day < 10) {
    filterDate = `${year}-${month}-0${day}`;
  }

  if (month < 10 && day < 10) {
    filterDate = `${year}-0${month}-0${day}`;
  }

  return (
    <div>
      <div>
        {/* Top */}
        <Top />
        {/* Middle */}
        <Middle filterDate={filterDate} />
        <div className="row">
          <HomeMemberQna />
          <HomeUserQna />
        </div>
      </div>
    </div>
  );
});

const Top = () => {
  return (
    <div className="row">
      <div className="col-12 content-title">
        <h2 className="hello">어서오세요</h2>
        <h3 className="title">pixelro 관리자 화면입니다.</h3>
      </div>
    </div>
  );
};

const Middle = ({ filterDate }) => {
  return (
    <div className="row">
      <div className="col-md-3 grid-margin stretch-card">
        <div className="card">
          <div className="card-body">
            <p className="card-title text-md-center text-xl-left">누적 회원수<span>iOS</span></p>
            <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
              <OrderSaleData filterDate={filterDate} />
              <i className="ti-stats-up icon-md text-muted mb-0 mb-md-3 mb-xl-0" />
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-3 grid-margin stretch-card">
        <div className="card">
          <div className="card-body">
            <p className="card-title text-md-center text-xl-left">누적 회원수<span>Android</span></p>
            <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
              <OrderCountData filterDate={filterDate} />
              <i className="ti-package icon-md text-muted mb-0 mb-md-3 mb-xl-0" />
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-3 grid-margin stretch-card">
        <div className="card">
          <div className="card-body">
            <p className="card-title text-md-center text-xl-left">일 픽업예약</p>
            <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
              <PickUpReservationData filterDate={filterDate} />
              <i className="ti-agenda icon-md text-muted mb-0 mb-md-3 mb-xl-0" />
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-3 grid-margin stretch-card">
        <div className="card">
          <div className="card-body">
            <p className="card-title text-md-center text-xl-left">일 렌즈예약</p>
            <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
              <LensReservationData filterDate={filterDate} />
              <i className="ti-calendar icon-md text-muted mb-0 mb-md-3 mb-xl-0" />
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-3 grid-margin stretch-card">
        <div className="card">
          <div className="card-body">
            <p className="card-title text-md-center text-xl-left">앱 신규고객</p>
            <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
              <UserData filterDate={filterDate} />
              <i className="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0" />
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-3 grid-margin stretch-card">
        <div className="card">
          <div className="card-body">
            <p className="card-title text-md-center text-xl-left">상점 등록 수</p>
            <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
              <StoreData filterDate={filterDate} />
              <i className="ti-home icon-md text-muted mb-0 mb-md-3 mb-xl-0" />
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-3 grid-margin stretch-card">
        <div className="card">
          <div className="card-body">
            <p className="card-title text-md-center text-xl-left">상품 등록 수</p>
            <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
              <ProductData filterDate={filterDate} />
              <i className="ti-gallery icon-md text-muted mb-0 mb-md-3 mb-xl-0" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

// 일 매출
const OrderSaleData = ({ filterDate }) => {
  // 구매 데이터 가져오기
  const { loading, error, data } = useQuery(ALL_ORDERS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <div />;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }
  let items;
  let orderSale = 0;
  if (data) {
    items = data.allOrders;
    // 결제 수
    items = items.filter((f) => f.process1.substring(0, 10) === filterDate);
    // 취소 주문 빼기
    items = items.filter((f) => f.process5 === "");

    if (items) {
      items.map((m) => (orderSale += Number(m.productTotal)));
    }
  }

  if (!items) return <div />;
  return <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{`${orderSale.toLocaleString()}원`}</h3>;
};

// 일 주문
const OrderCountData = ({ filterDate }) => {
  // 구매 데이터 가져오기
  const { loading, error, data } = useQuery(ALL_ORDERS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <div />;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }
  let items;
  let orderCount = 0;
  if (data) {
    items = data.allOrders;
    // 결제 수
    items = items.filter((f) => f.process1.substring(0, 10) === filterDate);
    // 취소 주문 빼기
    items = items.filter((f) => f.process5 === "");

    if (items) {
      orderCount = items.length;
    }
  }

  if (!items) return <div />;
  return <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{`${orderCount.toLocaleString()}`}</h3>;
};

// 일 픽업에약
const PickUpReservationData = ({ filterDate }) => {
  // 구매 데이터 가져오기
  const { loading, error, data } = useQuery(ALL_RESERVATIONS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <div />;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }
  let items;
  let reservationCount = 0;
  if (data) {
    items = data.allReservations;

    // FILTER STEP 1 : 예약하다가 취소한거 제거
    // FILTER STEP 2 : 오늘 신청
    // FILTER STEP 3 : 픽업 매장일 경우
    items = items.filter((f) => f.status !== "주문중");
    items = items.filter((f) => FormatDate(new Date(parseInt(f.createAt, 10))).substring(0, 10) === filterDate);

    reservationCount = items.length;
  }

  if (!items) return <div />;
  return <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{`${reservationCount.toLocaleString()}`}</h3>;
};

// 일 렌즈에약
const LensReservationData = ({ filterDate }) => {
  // 구매 데이터 가져오기
  const { loading, error, data } = useQuery(ALL_LENS_RESERVATIONS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <div />;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }
  let items;
  let reservationCount = 0;
  if (data) {
    items = data.allLensReservations;

    // FILTER STEP 1 : 예약취소 빼기
    // FILTER STEP 2 : 오늘 신청
    items = items.filter((f) => f.status !== "예약취소");
    items = items.filter((f) => FormatDate(new Date(parseInt(f.createAt, 10))).substring(0, 10) === filterDate);

    reservationCount = items.length;
  }

  if (!items) return <div />;
  return <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{`${reservationCount.toLocaleString()}`}</h3>;
};

// 일 사용자 등록 수
const UserData = ({ filterDate }) => {
  // 구매 데이터 가져오기
  const { loading, error, data } = useQuery(ALL_USERS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <div />;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }
  let items;
  let userCount = 0;
  if (data) {
    items = data.allUsers;

    // FILTER STEP 1 : 오늘 등록
    items = items.filter((f) => FormatDate(new Date(parseInt(f.createAt, 10))).substring(0, 10) === filterDate);
    userCount = items.length;
  }

  if (!items) return <div />;
  return <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{`${userCount.toLocaleString()}`}</h3>;
};

// 일 상점 등록 수
const StoreData = ({ filterDate }) => {
  // 구매 데이터 가져오기
  const { loading, error, data } = useQuery(ALL_STORES, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <div />;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }
  let items;
  let storeCount = 0;
  if (data) {
    items = data.allStores;

    // FILTER STEP 1 : 오늘 등록
    items = items.filter((f) => FormatDate(new Date(parseInt(f.createAt, 10))).substring(0, 10) === filterDate);
    storeCount = items.length;
  }

  if (!items) return <div />;
  return <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{`${storeCount.toLocaleString()}`}</h3>;
};

// 일 상품 등록 수
const ProductData = ({ filterDate }) => {
  // 구매 데이터 가져오기
  const { loading, error, data } = useQuery(ALL_PRODUCTS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <div />;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }
  let items;
  let productCount = 0;
  if (data) {
    items = data.allProducts;

    // FILTER STEP 1 : 오늘 등록
    items = items.filter((f) => FormatDate(new Date(parseInt(f.createAt, 10))).substring(0, 10) === filterDate);
    productCount = items.length;
  }

  if (!items) return <div />;
  return <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{`${productCount.toLocaleString()}`}</h3>;
};

export default DashBoard;
