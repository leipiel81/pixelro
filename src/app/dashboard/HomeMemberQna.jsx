import React from "react";
import { Link } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import { ALL_QNAS } from "graphql/gql/select";
import { FormatDateShort } from "components/FormatDate";
import { SimpleNotification } from "components/SimpleNotification";

const HomeQna = () => {
  // 상점 데이터 가져오기
  const { loading, error, data } = useQuery(ALL_QNAS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p />;
  if (error) {
    console.log("message : ", error.message);
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    items = data.allQnas;

    if (items) {
      items.sort((a, b) => {
        return b.createAt - a.createAt;
      });

      if (items.length > 5) {
        items = items.slice(0, 5);
      }
    }
  }

  return (
    <div className="col-md-6 grid-margin stretch-card">
      <div className="card">
        <div className="card-body">
          <p className="card-title">회원사 문의</p>
          <div className="table-responsive">
            <table className="table">
              <thead>
                <tr>
                  <td className="text-black font-medium">#</td>
                  <td className="text-black font-medium">카테고리</td>
                  <td className="text-black font-medium">작성자</td>
                  <td className="text-black font-medium">등록날짜</td>
                </tr>
              </thead>
              {items && items.length > 0
                ? items.map((m, index) => (
                    <tr>
                      <td>{index + 1}</td>
                      <td>
                        <Link to={`/service-center/member-qna-detail/${m.id}`}>
                          <div className="text-sm leading-5 text-blue-700"> {m.category}</div>
                        </Link>
                      </td>
                      <td>{m.member.name}</td>
                      <td>{FormatDateShort(new Date(parseInt(m.createAt, 10)))}</td>
                    </tr>
                  ))
                : "등록된 문의가 없습니다."}
            </table>
          </div>
        </div>
        <div className="card border-right-0 border-left-0 border-bottom-0">
          <div className="d-flex justify-content-center justify-content-md-end">
            <button className="btn btn-lg btn-outline-light text-primary rounded-0 border-0 d-none d-md-block" type="button">
              <Link to="/service-center/member-qna-table">
                <p className="card-title">더보기</p>
              </Link>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomeQna;
