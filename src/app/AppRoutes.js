/* eslint-disable import/no-named-as-default */
import React, { Component, Suspense } from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import Spinner from "./shared/Spinner";

import Dashboard from "./dashboard/Dashboard";

//  ------------------ 추가
// <<<<<<<<<<< 홈 >>>>>>>>>>>>
import PixelroIn from "./login/PixelroIn";

// <<<<<<<<<<< 앱멤버관리 >>>>>>>>>>>>
// 앱멤버관리 - 멤버목록
import UserTable from "./app-users/users/UserTable";
import UserDetail from "./app-users/users/UserDetail";
import UserEdit from "./app-users/users/UserEdit";
// 앱멤버관리 - 전문가 목록
import ExpertTable from "./app-users/expert/ExpertTable";
import ExpertDetail from "./app-users/expert/ExpertDetail";
import ExpertEdit from "./app-users/expert/ExpertEdit";
import ExpertCreate from "./app-users/expert/ExpertCreate";

//앱멤버 관리 - 푸시 메시지 발송
import NewAppPushMessage from "./app-users/appPushMessage/NewAppPushMessage";

//앱멤버 관리 - 검색 히스토리
import SearchHistoryTable from "./app-users/searchHistory/SearchHistoryTable";

// <<<<<<<<<<< 게시판관리 >>>>>>>>>>>>
// 게시판관리 - 매거진
import MagazineTable from "./notice-board/magazine/MagazineTable";
import MagazineDetail from "./notice-board/magazine/MagazineDetail";
import MagazineEdit from "./notice-board/magazine/MagazineEdit";
import MagazineCreate from "./notice-board/magazine/MagazineCreate";

// <<<<<<<<<<< 고객센터 >>>>>>>>>>>>
// 고객센터 - 공지사항
import NoticeTable from "./service-center/notice/NoticeTable";
import NoticeDetail from "./service-center/notice/NoticeDetail";
import NoticeEdit from "./service-center/notice/NoticeEdit";
import NoticeCreate from "./service-center/notice/NoticeCreate";

// 고객센터 - 멤버FAQ
import AppUserFAQTable from "./service-center/app-user-faq/AppUserFAQTable";
import AppUserFAQDetail from "./service-center/app-user-faq/AppUserFAQDetail";
import AppUserFAQEdit from "./service-center/app-user-faq/AppUserFAQEdit";
import AppUserFAQCreate from "./service-center/app-user-faq/AppUserFAQCreate";

// 고객센터 - 멤버QNA
import AppUserQnaTable from "./service-center/app-user-qna/AppUserQnaTable";
import AppUserQnaDetail from "./service-center/app-user-qna/AppUserQnaDetail";

// <<<<<<<<<<< 기업정보 >>>>>>>>>>>>
// 기업정보 - 이용약관
import ProvisionTable from "./company/provision/ProvisionTable";
import ProvisionDetail from "./company/provision/ProvisionDetail";
import ProvisionEdit from "./company/provision/ProvisionEdit";
import ProvisionCreate from "./company/provision/ProvisionCreate";

// 기업정보 - 기업정보
import CompanyInfoDetail from "./company/company-info/CompanyInfoDetail";
import CompanyInfoEdit from "./company/company-info/CompanyInfoEdit";

// 기업정보 - 협력사
import PartnerInfoTable from "./company/partner-info/PartnerInfoTable";
import PartnerInfoDetail from "./company/partner-info/PartnerInfoDetail";
import PartnerInfoEdit from "./company/partner-info/PartnerInfoEdit";
import PartnerInfoCreate from "./company/partner-info/PartnerInfoCreate";


// <<<<<<<<<<< 관리자 >>>>>>>>>>>>
// 관리자 - 관리자목록
import PixelroTable from "./pixelro/PixelroTable";
import PixelroEdit from "./pixelro/PixelroEdit";
import PixelroCreate from "./pixelro/PixelroCreate";

/*
==========================================================================
    안쓰는 페이지 :: 차후 필요할지 모름
==========================================================================
*/

// 고객센터 - 운영정책
import OperationTable from "./service-center/operation/OperationTable";
import OperationDetail from "./service-center/operation/OperationDetail";
import OperationEdit from "./service-center/operation/OperationEdit";
import OperationCreate from "./service-center/operation/OperationCreate";

// <<<<<<<<<<< 실적관리 >>>>>>>>>>>>
// 실적관리 - 기간별매출조회
import DateSales from "./sales/date/DateSales";
// 실적관리 - 상점별매출조회
import StoreSales from "./sales/store/StoreSales";
import StoreSalesDetail from "./sales/store/StoreSalesDetail";
// 실적관리 - 베스트 셀러
import BestSales from "./sales/best/BestSales";
// 실적관리 - 쿠폰
import CouponTable from "./sales/coupon/CouponTable";
import CouponDetail from "./sales/coupon/CouponDetail";
import CouponCreate from "./sales/coupon/CouponCreate";
import CouponEdit from "./sales/coupon/CouponEdit";

// <<<<<<<<<<< 주문관리 >>>>>>>>>>>>
// 주문관리 - 배송완료처리
import DeliveryCheckTable from "./orders/delivery-check/DeliveryCheckTable";
// 주문관리 - 구매확정처리
import ConfirmCheckTable from "./orders/confirm-check/ConfirmCheckTable";
// 주문관리 - 교환완료처리
import ChangeCheckTable from "./orders/change-check/ChangeCheckTable";

// <<<<<<<<<<< 사이트꾸미기 >>>>>>>>>>>>
// 사이트꾸미기 - 배너
import Banner from "./banners/Banner";
import BannerCreate from "./banners/BannerCreate";
import BannerDetail from "./banners/BannerDetail";
import BannerEdit from "./banners/BannerEdit";
// 사이트꾸미기 - 추천상품
import RecommendProduct from "./banners/RecommendProduct";
import RecommendProductCreate from "./banners/RecommendProductCreate";
import RecommendProductDetail from "./banners/RecommendProductDetail";
import RecommendProductEdit from "./banners/RecommendProductEdit";

// 백업 확인용
// <<<<<<<<<<< 앱 푸쉬 메세지 >>>>>>>>>>>>
import AppPushMessage from "./old/app-push-message/AppPushMessage";

// <<<<<<<<<<< 등록관리 >>>>>>>>>>>>
// 등록관리 - 상점목록
import StoreTable from "./old/registered-store/stores/StoreTable";
import StoreDetail from "./old/registered-store/stores/StoreDetail";
import StoreEdit from "./old/registered-store/stores/StoreEdit";
import StoreCreate from "./old/registered-store/stores/StoreCreate";
// 등록관리 - 상품목록
import ProductTable from "./old/registered-store/products/ProductTable";
import ProductDetail from "./old/registered-store/products/ProductDetail";
import ProductEdit from "./old/registered-store/products/ProductEdit";
import ProductCreate from "./old/registered-store/products/ProductCreate";
// 등록관리 - 상품등록요청
import ProductRegisterTable from "./old/registered-store/register/ProductRegisterTable";
import ProductRegisterDetail from "./old/registered-store/register/ProductRegisterDetail";
// <<<<<<<<<<< 미등록관리 >>>>>>>>>>>>
// 미등록관리 - 신청접수목록
import RequestTable from "./old/unregistered-store/request/RequestTable";
import RequestDetail from "./old/unregistered-store/request/RequestDetail";

// 앱멤버관리 - 픽업예약
import PickupReservationTable from "./old/app-users/pickupReservation/PickupReservationTable";
import PickupReservationDetail from "./old/app-users/pickupReservation/PickupReservationDetail";
// 앱멤버관리 - 렌즈예약
import LensReservationTable from "./old/app-users/lensReservation/LensReservationTable";
import LensReservationDetail from "./old/app-users/lensReservation/LensReservationDetail";

// <<<<<<<<<<< 실적관리 >>>>>>>>>>>>
// 회원사관리 - 회원사목록
import MemberTable from "./old/members/MemberTable";
import MemberDetail from "./old/members/MemberDetail";
import MemberEdit from "./old/members/MemberEdit";
import MemberCreate from "./old/members/MemberCreate";
import EmployeeCreate from "./old/members/EmployeeCreate";
// 회원사관리 - 회원사수익신청목록
import SettlemenRequestTable from "./old/members/settlementRequest/SettlementRequestTable";
import SettlementRequestDetail from "./old/members/settlementRequest/SettlementRequestDetail";
import SettlementProductList from "./old/members/settlementRequest/SettlementProductList";
// 회원사관리 - 회원사세금계산서현황
import SettlemenTable from "./old/members/settlement/SettlementTable";
import SettlementDetail from "./old/members/settlement/SettlementDetail";
// 회원사관리 - 가맹취소내역
import StoreWithdrawTable from "./old/members/withdraw/StoreWithdrawTable";
// 회원사관리 - 메세지 충전 신청
import MessageTable from "./old/members/message/MessageTable";

// 게시판관리 - 이벤트
import EventTable from "./old/notice-board/event/EventTable";
import EventDetail from "./old/notice-board/event/EventDetail";
import EventEdit from "./old/notice-board/event/EventEdit";
import EventCreate from "./old/notice-board/event/EventCreate";

// 고객센터 - 회원사FAQ
import MemberFAQTable from "./old/service-center/member-faq/MemberFAQTable";
import MemberFAQDetail from "./old/service-center/member-faq/MemberFAQDetail";
import MemberFAQEdit from "./old/service-center/member-faq/MemberFAQEdit";
import MemberFAQCreate from "./old/service-center/member-faq/MemberFAQCreate";
// 고객센터 - 회원사QNA
import MemberQnaTable from "./old/service-center/member-qna/MemberQnaTable";
import MemberQnaDetail from "./old/service-center/member-qna/MemberQnaDetail";



class AppRoutes extends Component {
  render() {
    return (
      <Suspense fallback={<Spinner />}>
        <Switch>
          <Route exact path="/" component={Dashboard} />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route exact path="/adminIn" component={PixelroIn} />
          
          {/* 회원 관리 */}
          <Route path="/app-users/user-table" component={UserTable} />
          <Route path="/app-users/user-detail/:id" component={UserDetail} />
          <Route path="/app-users/user-edit/:id" component={UserEdit} />
          {/* 전문가 관리 */}
          <Route path="/app-users/expert-table" component={ExpertTable} />
          <Route path="/app-users/expert-detail/:id" component={ExpertDetail} />
          <Route path="/app-users/expert-edit/:id" component={ExpertEdit} />
          <Route path="/app-users/expert-create" component={ExpertCreate} />

          {/* 푸시 메시지 */}
          <Route path="/app-users/app-push-message" component={NewAppPushMessage} />
          {/* 푸시 메시지 */}
          <Route path="/app-users/search-history" component={SearchHistoryTable} />

          {/* 공지사항 */}
          <Route path="/service-center/notice-table" component={NoticeTable} />
          <Route path="/service-center/notice-detail/:id" component={NoticeDetail} />
          <Route path="/service-center/notice-edit/:id" component={NoticeEdit} />
          <Route path="/service-center/notice-create" component={NoticeCreate} />
          
          {/* FAQ */}
          <Route path="/service-center/app-user-faq-table" component={AppUserFAQTable} />
          <Route path="/service-center/app-user-faq-detail/:id" component={AppUserFAQDetail} />
          <Route path="/service-center/app-user-faq-edit/:id" component={AppUserFAQEdit} />
          <Route path="/service-center/app-user-faq-create" component={AppUserFAQCreate} />
          
          {/* 의견보내기 */}
          <Route path="/service-center/member-qna-table" component={MemberQnaTable} />
          <Route path="/service-center/member-qna-detail/:id" component={MemberQnaDetail} />
          <Route path="/service-center/app-user-qna-table" component={AppUserQnaTable} />
          <Route path="/service-center/app-user-qna-detail/:id" component={AppUserQnaDetail} />
          
          {/* 매거진 */}
          <Route path="/notice-board/magazine-table" component={MagazineTable} />
          <Route path="/notice-board/magazine-detail/:id" component={MagazineDetail} />
          <Route path="/notice-board/magazine-edit/:id" component={MagazineEdit} />
          <Route path="/notice-board/magazine-create" component={MagazineCreate} />

          {/* 이용약관 */}
          <Route path="/company/provision-table" component={ProvisionTable} />
          <Route path="/company/provision-detail/:id" component={ProvisionDetail} />
          <Route path="/company/provision-edit/:id" component={ProvisionEdit} />
          <Route path="/company/provision-create" component={ProvisionCreate} />

          {/* 기업정보 */}
          <Route path="/company/company-info-detail" component={CompanyInfoDetail} />
          <Route path="/company/company-info-edit" component={CompanyInfoEdit} />

          {/* 협력사 정보 :: 디테일 에디트는 아이디 :id 붙여야 함 개발 시.. */}
          <Route path="/company/partner-table" component={PartnerInfoTable} />
          <Route path="/company/partner-detail" component={PartnerInfoDetail} />
          <Route path="/company/partner-edit" component={PartnerInfoEdit} />
          <Route path="/company/partner-create" component={PartnerInfoCreate} />

          {/* 픽셀로 관리자 */}
          <Route path="/pixelro/pixelro-table" component={PixelroTable} />
          <Route path="/pixelro/pixelro-edit/:id" component={PixelroEdit} />
          <Route path="/pixelro/pixelro-create" component={PixelroCreate} />

          {/*
          
          안쓰는 루트
          
          */}

          {/* 운영정책 */}
          <Route path="/service-center/operation-table" component={OperationTable} />
          <Route path="/service-center/operation-detail/:id" component={OperationDetail} />
          <Route path="/service-center/operation-edit/:id" component={OperationEdit} />
          <Route path="/service-center/operation-create" component={OperationCreate} />

          {/* 실적 관리 */}
          <Route path="/sales/date-sales" component={DateSales} />
          <Route path="/sales/store-sales" component={StoreSales} />
          <Route path="/sales/store-sales-detail/:id" component={StoreSalesDetail} />
          <Route path="/sales/best-sales" component={BestSales} />
          <Route path="/sales/coupon" component={CouponTable} />
          <Route path="/sales/coupon-detail/:id" component={CouponDetail} />
          <Route path="/sales/coupon-create" component={CouponCreate} />
          <Route path="/sales/coupon-edit/:id" component={CouponEdit} />
          {/* 주문 관리 */}
          <Route path="/orders/delivery-check" component={DeliveryCheckTable} />
          <Route path="/orders/confirm-check" component={ConfirmCheckTable} />
          <Route path="/orders/change-check" component={ChangeCheckTable} />
          {/* 배너 */}
          <Route path="/banners/banner" component={Banner} />
          <Route path="/banners/banner-create" component={BannerCreate} />
          <Route path="/banners/banner-detail/:id" component={BannerDetail} />
          <Route path="/banners/banner-edit/:id" component={BannerEdit} />
          {/* 추천 상품 & 장소 */}
          <Route path="/banners/recommend-product" component={RecommendProduct} />
          <Route path="/banners/recommend-product-create" component={RecommendProductCreate} />
          <Route path="/banners/recommend-product-detail/:id" component={RecommendProductDetail} />
          <Route path="/banners/recommend-product-edit/:id" component={RecommendProductEdit} />

          {/* Old */}
          {/* 푸시 메시지 발송 */}
          <Route path="/old/app-push-message" component={AppPushMessage} />
          {/* 등록관리 -  */}
          {/* 상점 목록 */}
          <Route path="/old/registered-store/store-table" component={StoreTable} />
          <Route path="/old/registered-store/store-detail/:id" component={StoreDetail} />
          <Route path="/old/registered-store/store-edit/:id" component={StoreEdit} />
          <Route path="/old/registered-store/store-create/:id/:memberId/:defaultStoreId" component={StoreCreate} />
          {/* 상품 목록 */}
          <Route path="/old/registered-store/products-table" component={ProductTable} />
          <Route path="/old/registered-store/product-detail/:id" component={ProductDetail} />
          <Route path="/old/registered-store/product-edit/:id" component={ProductEdit} />
          <Route path="/old/registered-store/product-create/:id" component={ProductCreate} />
          {/* 상품 등록 요청 */}
          <Route path="/old/registered-store/product-register-table" component={ProductRegisterTable} />
          <Route path="/old/registered-store/product-register-detail/:id" component={ProductRegisterDetail} />
          {/* - 등록관리 */}
          <Route path="/old/unregistered-store/request-table" component={RequestTable} />
          <Route path="/old/unregistered-store/request-detail/:id" component={RequestDetail} />
          {/* 렌즈 예약 */}
          <Route path="/old/app-users/lens-reservation-table" component={LensReservationTable} />
          <Route path="/old/app-users/lens-reservation-detail/:id" component={LensReservationDetail} />
          {/* 픽업 예약 */}
          <Route path="/old/app-users/pickup-reservation-table" component={PickupReservationTable} />
          <Route path="/old/app-users/pickup-reservation-detail/:id" component={PickupReservationDetail} />
          {/* 회원사 관리 */}
          <Route path="/old/members/member-table" component={MemberTable} />
          <Route path="/old/members/member-detail/:id" component={MemberDetail} />
          <Route path="/old/members/member-edit/:id" component={MemberEdit} />
          <Route path="/old/members/member-create" component={MemberCreate} />
          <Route path="/old/members/employee-create/:id" component={EmployeeCreate} />
          {/* 수익 신청 목록 */}
          <Route path="/old/members/settlement-request-table" component={SettlemenRequestTable} />
          <Route path="/old/members/settlement-request-detail/:id" component={SettlementRequestDetail} />
          <Route path="/old/members/settlement-product-list/:id" component={SettlementProductList} />
          <Route path="/old/members/settlement-table" component={SettlemenTable} />
          <Route path="/old/members/settlement-detail/:id" component={SettlementDetail} />
          <Route path="/old/members/store-withdraw-table" component={StoreWithdrawTable} />
          <Route path="/old/members/message-table" component={MessageTable} />

          {/* 이벤트 */}
          <Route path="/old/notice-board/event-table" component={EventTable} />
          <Route path="/old/notice-board/event-detail/:id" component={EventDetail} />
          <Route path="/old/notice-board/event-edit/:id" component={EventEdit} />
          <Route path="/old/notice-board/event-create" component={EventCreate} />

          {/* 회원사 FAQ */}
          <Route path="/old/service-center/member-faq-table" component={MemberFAQTable} />
          <Route path="/old/service-center/member-faq-detail/:id" component={MemberFAQDetail} />
          <Route path="/old/service-center/member-faq-edit/:id" component={MemberFAQEdit} />
          <Route path="/old/service-center/member-faq-create" component={MemberFAQCreate} />

          <Redirect to="/dashboard" />
        </Switch>
      </Suspense>
    );
  }
}

export default AppRoutes;
