import React, { useState } from "react";
import PropTypes from "prop-types";
import { useQuery } from "@apollo/react-hooks";
import { Link } from "react-router-dom";
import Pagination from "components/HalfSizePagination";
import { SimpleNotification } from "components/SimpleNotification";
import { FILTER_BANNER } from "graphql/gql/select";

const GlassesLens = () => {
  // 페이지네이션 적용
  const [pageOfItems, setPageOfItems] = useState([]);
  const [page, setPage] = useState(1);

  function onChangePage(pageOfItems, pageNumber) {
    setPageOfItems(pageOfItems);
    setPage(pageNumber);
  }

  //   배너 전체 데이터
  const { loading, error, data } = useQuery(FILTER_BANNER, {
    fetchPolicy: "cache-and-network",
    variables: { value: "안경렌즈_배너" },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    const resultData = data.filterBanner;

    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });

    items = resultData;
  }

  if (!items) return <div />;

  return (
    <div className="max-w-full w-full overflow-auto">
      <table className="w-full">
        <thead>
          <tr>
            <th className="px-3 py-3 border-b border-black bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider w-2/12" />
            <th className="px-3 py-3 border-b border-black bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider w-1/12">
              항목명
            </th>
            <th className="px-3 py-3 border-b border-black bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider w-1/12">
              번호
            </th>
            <th className="px-3 py-3 border-b border-black bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider w-1/12">
              이동URL
            </th>

            <th className="px-3 py-3 border-b border-black bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider w-2/12">
              타이틀
            </th>
            <th className="px-3 py-3 border-b border-black bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider w-3/12">
              내용
            </th>
            <th className="px-3 py-3 border-b border-black bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider w-2/12">
              보이기
            </th>
            <th className="px-3 py-3 border-b border-black bg-gray-50 w-1/12" />
          </tr>
        </thead>
        {items.length === 0 ? (
          <tr>
            <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
              <div>등록된 배너가 없습니다.</div>
            </td>
          </tr>
        ) : (
          pageOfItems && (
            <tbody className="bg-white">
              {pageOfItems.map((m) => (
                <tr>
                  <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                    <img src={m.image} alt="" />
                  </td>
                  <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-black">{m.category}</td>
                  <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-black">{m.no}</td>
                  <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-black">{m.targetUrl}</td>

                  <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-black">{m.title}</td>
                  <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-black">{m.content}</td>
                  <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-black">
                    {m.visible ? "true" : "false"}
                  </td>
                  <td className="px-3 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium">
                    <Link to={`/banners/banner-detail/${m.id}`}>
                      <i className="ti-pencil-alt ml-3" />
                      상세보기
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          )
        )}
      </table>
      <Page items={items} page={page} onChangePage={onChangePage} />
    </div>
  );
};

const Page = (props) => {
  const PAGE_SIZE = 5;
  const { items, page, onChangePage } = props;
  return (
    <div className="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">
      <div className="hidden sm:block">
        <p className="text-sm leading-5 text-gray-700">
          <span className="font-medium mx-1">{(page - 1) * PAGE_SIZE + 1}</span>
          {" ~  "}
          <span className="font-medium mx-1">{items && items.length < page * PAGE_SIZE ? items.length : page * PAGE_SIZE}</span>
          {" / 총 "}
          <span className="font-medium mx-1">{items && items.length}</span>
          {" 건"}
        </p>
      </div>
      <div className="flex-1 flex justify-between sm:justify-end">{items && <Pagination items={items} onChangePage={onChangePage} />}</div>
    </div>
  );
};

Page.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  page: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
};

export default GlassesLens;
