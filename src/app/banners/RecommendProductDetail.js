/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Button, Modal } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import { BANNER } from "graphql/gql/select";
import { DELETE_BANNER } from "graphql/gql/delete";
import { Product } from "graphql/query/select";
import { FormatDate } from "components/FormatDate";

const DetailPage = ({ match }) => {
  const { id } = match.params;
  const [mdShow, setMdShow] = useState(false);

  const history = useHistory();
  const goBack = () => {
    history.push("/banners/recommend-product");
  };

  const [bannerDelete, { data: deleteData }] = useMutation(DELETE_BANNER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (deleteData !== undefined) {
      goBack();
    }
  }, [deleteData]);

  const handleDelete = () => {
    setMdShow(false);
    bannerDelete({ variables: { id: id } });
  };

  //   배너정보
  const { loading, error, data } = useQuery(BANNER, {
    fetchPolicy: "cache-and-network",
    variables: { id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    items = data.banner;
    console.log("banner items :: ", items);
  }

  if (!items) return <div />;
  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">사이트 꾸미기</li>
            <Link to="/banners/recommend-product" className="breadcrumb-item active mt-1" aria-current="page">
              추천 상품
            </Link>
            <Link to={`/banners/recommend-product-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              추천 상품 상세 정보
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 추천 상품 상세 정보</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">항목명</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    {items.category}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">target url</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.targetUrl}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">no</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.no}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">보이기</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <div className="form-check">
                      <label className="form-check-label">
                        <input type="checkbox" checked={items.visible} className="form-check-input" />
                        <i className="input-helper" />
                      </label>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <InfoForm targetId={items.targetId} />

      <div className="col-12 grid-margin mt-3">
        <div className="row justify-between">
          <button className="btn btn-danger mt-2" onClick={() => setMdShow(true)}>
            삭제하기
          </button>
          <Link className="btn btn-primary mt-2 " to={`/banners/recommend-product-edit/${id}`}>
            수정하기
          </Link>
        </div>
        {/* Modal Starts  */}
        <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
          <Modal.Header closeButton>
            <Modal.Title>해당 내용을 삭제하시겠습니까?</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <p>삭제를 원하시면 삭제 버튼을 눌러주세요.</p>
          </Modal.Body>

          <Modal.Footer className="fleex-wrap">
            <Button variant="danger m-2" onClick={handleDelete}>
              삭제
            </Button>
            <Button variant="primary m-2" onClick={() => setMdShow(false)}>
              취소
            </Button>
          </Modal.Footer>
        </Modal>
        {/* Modal Ends */}
      </div>
    </>
  );
};

// 상품 기본 정보
const InfoForm = ({ targetId }) => {
  const { result } = Product(targetId);

  let items;
  if (result) {
    items = result;
  }

  if (!items) return <div />;

  let keywordCheck = false;
  if (items.keyword) {
    keywordCheck = JSON.parse(items.keyword).length !== 0;
  }

  let optionCheck = false;
  if (items.option) {
    optionCheck = JSON.parse(items.option).length !== 0;
  }

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상품 기본 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tbody>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">상품명</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.name}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">요약 설명</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.description}
                  </td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">가격</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{items.price}</td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">할인율</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{items.discount}%</td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">브랜드</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{items.brand}</td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">모델명</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{items.model}</td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">옵션</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    <div className="flex">
                      {optionCheck &&
                        JSON.parse(items.option).map((m) => (
                          <div key={m.code}>
                            <div className="flex py-2">
                              <span className="border p-2 mr-1">
                                상품코드 : {m.code} , 수량 : {m.stock} , 추가금액 : {m.addPrice} , 판매수량 : {m.sale}
                              </span>
                            </div>
                          </div>
                        ))}
                    </div>
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">카테고리</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    {items.category}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">키워드</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    <div className="flex">
                      {keywordCheck &&
                        JSON.parse(items.keyword).map((m) => (
                          <div className="flex" key={m}>
                            <span className="border p-2 mr-1">{m}</span>
                          </div>
                        ))}
                    </div>
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">등록날짜</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    {FormatDate(new Date(parseInt(items.createAt, 10)))}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

// 상품 상세 정보
const DetailForm = ({ targetId }) => {
  const { result } = Product(targetId);

  let items;
  if (result) {
    items = result;
  }

  if (!items) return <div />;

  let productInfo;
  if (items.productInfo) {
    productInfo = JSON.parse(items.productInfo);
  }

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상품 상세 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tbody>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">종류</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[0]}</td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">소재</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[1]}</td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">치수</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[2]}</td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">제조년월</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[3]}</td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">제조사</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[4]}</td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">제조국</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[5]}</td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">취급시 주의사항</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[6]}</td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">품질보증기준</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[7]}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

// 상품 이미지 정보
const SubInfoForm = ({ targetId }) => {
  const [previewShow, setPreviewShow] = useState(false);

  const { result } = Product(targetId);

  let items;
  if (result) {
    items = result;
  }

  if (!items) return <div />;

  let detailImages;
  if (items.detailImage) {
    detailImages = JSON.parse(items.detailImage);
  }
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상품 이미지</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tbody>
                <tr>
                  <td colSpan="6" className="bg-gray-200 border px-8 py-2 text-sm font-bold">
                    메인 이미지
                  </td>
                </tr>

                <tr>
                  <td colSpan="6" className="flex border px-2 py-2 text-sm">
                    {items.mainImage && <img className="h-24 w-24 m-3 border" alt="" src={items.mainImage} />}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tbody>
                <tr>
                  <td colSpan="6" className="bg-gray-200 border px-8 py-2 text-sm font-bold">
                    <div className="flex justify-between">
                      <div>상세 이미지</div>
                      <div>
                        <button onClick={(e) => setPreviewShow(true)}>preview</button>
                      </div>
                    </div>
                  </td>
                </tr>

                <tr>
                  <td colSpan="6" className="flex border px-2 py-2 text-sm">
                    {detailImages && detailImages.map((m) => <img className="h-24 w-24 m-3 border" alt="" src={`${m}`} key={m} />)}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      {/* Modal Start */}
      <Modal show={previewShow} onHide={() => setPreviewShow(false)} aria-labelledby="example-modal-sizes-title-lg">
        <Modal.Header closeButton>
          <div>{detailImages && detailImages.map((m) => <img className="w-full" alt="" src={`${m}`} key={m} />)}</div>
        </Modal.Header>
      </Modal>
      {/* Modal Ends */}
    </div>
  );
};

DetailPage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default DetailPage;
