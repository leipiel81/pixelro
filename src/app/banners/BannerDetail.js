/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Button, Modal } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import { BANNER } from "graphql/gql/select";
import { DELETE_BANNER } from "graphql/gql/delete";

const DetailPage = ({ match }) => {
  const { id } = match.params;
  const [mdShow, setMdShow] = useState(false);

  const history = useHistory();
  const goBack = () => {
    history.push("/banners/banner");
  };

  const [bannerDelete, { data: deleteData }] = useMutation(DELETE_BANNER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (deleteData !== undefined) {
      goBack();
    }
  }, [deleteData]);

  const handleDelete = () => {
    setMdShow(false);
    bannerDelete({ variables: { id: id } });
  };

  //   배너정보
  const { loading, error, data } = useQuery(BANNER, {
    fetchPolicy: "cache-and-network",
    variables: { id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    items = data.banner;
  }

  if (!items) return <div />;
  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">사이트 꾸미기</li>
            <Link to="/banners/banner" className="breadcrumb-item active mt-1" aria-current="page">
              배너 관리
            </Link>
            <Link to={`/banners/banner-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              배너 상세 정보
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 배너 상세 정보</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">항목명</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    {items.category}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">제목</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.title}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">내용</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.content}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">target url</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.targetUrl}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">이미지</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <div className="flex my-3 ml-10">
                      <img src={items.image} className="w-64 h-24" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">no</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.no}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">보이기</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <div className="form-check">
                      <label className="form-check-label">
                        <input type="checkbox" checked={items.visible} className="form-check-input" />
                        <i className="input-helper" />
                      </label>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 grid-margin mt-3">
        <div className="row justify-between">
          <button className="btn btn-danger mt-2" onClick={() => setMdShow(true)}>
            삭제하기
          </button>
          <Link className="btn btn-primary mt-2 " to={`/banners/banner-edit/${id}`}>
            수정하기
          </Link>
        </div>

        {/* Modal Starts  */}
        <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
          <Modal.Header closeButton>
            <Modal.Title>해당 내용을 삭제하시겠습니까?</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <p>삭제를 원하시면 삭제 버튼을 눌러주세요.</p>
          </Modal.Body>

          <Modal.Footer className="fleex-wrap">
            <Button variant="danger m-2" onClick={handleDelete}>
              삭제
            </Button>
            <Button variant="primary m-2" onClick={() => setMdShow(false)}>
              취소
            </Button>
          </Modal.Footer>
        </Modal>
        {/* Modal Ends */}
      </div>
    </>
  );
};

DetailPage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default DetailPage;
