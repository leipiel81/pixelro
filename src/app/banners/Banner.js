import React from "react";
import { Col, Row, Nav, Tab } from "react-bootstrap";
import { useHistory, Link } from "react-router-dom";
import { observer } from "mobx-react-lite";
import useSettings from "stores/settings";
import Main from "./bannerTab/Main";
import Place from "./bannerTab/Place";
import Glasses from "./bannerTab/Glasses";
import SunGlasses from "./bannerTab/SunGlasses";
import ContactLens from "./bannerTab/ContactLens";
import Pixelro from "./bannerTab/Pixelro";
import GlassesLens from "./bannerTab/GlassesLens";
import Accessory from "./bannerTab/Accessory";
import Food from "./bannerTab/Food";
import Etc from "./bannerTab/Etc";

const Banner = observer(() => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const { bannerTab } = settings;

  const handleTabClick = (tabId) => {
    settings.bannerTab = tabId;
  };
  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">사이트 꾸미기</li>
            <Link to="/banners/banner" className="breadcrumb-item active mt-1" aria-current="page">
              배너
            </Link>
          </ol>
        </nav>
      </div>
      {/* /banners/create */}
      <div className="row">
        <div className="col-md-12 stretch-card">
          <div className="card">
            <div className="card-body">
              <div className="tab-custom-pills-horizontal">
                <Tab.Container id="left-tabs-example" defaultActiveKey="1">
                  <Row>
                    <Col xs={12}>
                      <Nav variant="pills" className="flex-column">
                        <Nav.Item>
                          <Nav.Link
                            eventKey="1"
                            className="d-flex align-items-center"
                            active={bannerTab === "1"}
                            onClick={(e) => handleTabClick("1")}
                          >
                            메인
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="2"
                            className="d-flex align-items-center"
                            active={bannerTab === "2"}
                            onClick={(e) => handleTabClick("2")}
                          >
                            장소
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="3"
                            className="d-flex align-items-center"
                            active={bannerTab === "3"}
                            onClick={(e) => handleTabClick("3")}
                          >
                            안경
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="4"
                            className="d-flex align-items-center"
                            active={bannerTab === "4"}
                            onClick={(e) => handleTabClick("4")}
                          >
                            선글라스
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="5"
                            className="d-flex align-items-center"
                            active={bannerTab === "5"}
                            onClick={(e) => handleTabClick("5")}
                          >
                            콘택트렌즈
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="6"
                            className="d-flex align-items-center"
                            active={bannerTab === "6"}
                            onClick={(e) => handleTabClick("6")}
                          >
                            픽셀로제품
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="7"
                            className="d-flex align-items-center"
                            active={bannerTab === "7"}
                            onClick={(e) => handleTabClick("7")}
                          >
                            안경렌즈
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="8"
                            className="d-flex align-items-center"
                            active={bannerTab === "8"}
                            onClick={(e) => handleTabClick("8")}
                          >
                            악세사리
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="9"
                            className="d-flex align-items-center"
                            active={bannerTab === "9"}
                            onClick={(e) => handleTabClick("9")}
                          >
                            식품
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="10"
                            className="d-flex align-items-center"
                            active={bannerTab === "10"}
                            onClick={(e) => handleTabClick("10")}
                          >
                            기타
                          </Nav.Link>
                        </Nav.Item>
                      </Nav>
                    </Col>
                    <Col xs={12}>
                      <Tab.Content>
                        <Tab.Pane eventKey="1" active={bannerTab === "1"}>
                          <Main />
                        </Tab.Pane>
                        <Tab.Pane eventKey="2" active={bannerTab === "2"}>
                          <Place />
                        </Tab.Pane>
                        <Tab.Pane eventKey="3" active={bannerTab === "3"}>
                          <Glasses />
                        </Tab.Pane>
                        <Tab.Pane eventKey="4" active={bannerTab === "4"}>
                          <SunGlasses />
                        </Tab.Pane>
                        <Tab.Pane eventKey="5" active={bannerTab === "5"}>
                          <ContactLens />
                        </Tab.Pane>
                        <Tab.Pane eventKey="6" active={bannerTab === "6"}>
                          <Pixelro />
                        </Tab.Pane>
                        <Tab.Pane eventKey="7" active={bannerTab === "7"}>
                          <GlassesLens />
                        </Tab.Pane>
                        <Tab.Pane eventKey="8" active={bannerTab === "8"}>
                          <Accessory />
                        </Tab.Pane>
                        <Tab.Pane eventKey="9" active={bannerTab === "9"}>
                          <Food />
                        </Tab.Pane>
                        <Tab.Pane eventKey="10" active={bannerTab === "10"}>
                          <Etc />
                        </Tab.Pane>
                      </Tab.Content>
                    </Col>
                  </Row>
                </Tab.Container>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="flex justify-end mt-6">
        <Link className="btn btn-warning" to="/banners/banner-create">
          배너 등록
        </Link>
      </div>
    </div>
  );
});

export default Banner;
