import React from "react";
import { Col, Row, Nav, Tab } from "react-bootstrap";
import { useHistory, Link } from "react-router-dom";
import { observer } from "mobx-react-lite";
import useSettings from "stores/settings";
import Main from "./recommendProductTab/Main";
import Glasses from "./recommendProductTab/Glasses";
import SunGlasses from "./recommendProductTab/SunGlasses";
import ContactLens from "./recommendProductTab/ContactLens";
import Pixelro from "./recommendProductTab/Pixelro";
import GlassesLens from "./recommendProductTab/GlassesLens";
import Accessory from "./recommendProductTab/Accessory";
import Food from "./recommendProductTab/Food";
import Etc from "./recommendProductTab/Etc";

const RecommendProduct = observer(() => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }
  const { recommendProductTab } = settings;

  const handleTabClick = (tabId) => {
    settings.recommendProductTab = tabId;
  };
  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">사이트 꾸미기</li>
            <Link to="/banners/recommend-product" className="breadcrumb-item active mt-1" aria-current="page">
              추천 상품
            </Link>
          </ol>
        </nav>
      </div>
      {/* /banners/create */}
      <div className="row">
        <div className="col-md-12 stretch-card">
          <div className="card">
            <div className="card-body">
              <div className="tab-custom-pills-horizontal">
                <Tab.Container id="left-tabs-example" defaultActiveKey="1">
                  <Row>
                    <Col xs={12}>
                      <Nav variant="pills" className="flex-column">
                        <Nav.Item>
                          <Nav.Link
                            eventKey="1"
                            className="d-flex align-items-center"
                            active={recommendProductTab === "1"}
                            onClick={(e) => handleTabClick("1")}
                          >
                            메인
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="2"
                            className="d-flex align-items-center"
                            active={recommendProductTab === "2"}
                            onClick={(e) => handleTabClick("2")}
                          >
                            안경
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="3"
                            className="d-flex align-items-center"
                            active={recommendProductTab === "3"}
                            onClick={(e) => handleTabClick("3")}
                          >
                            선글라스
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="4"
                            className="d-flex align-items-center"
                            active={recommendProductTab === "4"}
                            onClick={(e) => handleTabClick("4")}
                          >
                            콘택트렌즈
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="5"
                            className="d-flex align-items-center"
                            active={recommendProductTab === "5"}
                            onClick={(e) => handleTabClick("5")}
                          >
                            픽셀로제품
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="6"
                            className="d-flex align-items-center"
                            active={recommendProductTab === "6"}
                            onClick={(e) => handleTabClick("6")}
                          >
                            안경렌즈
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="7"
                            className="d-flex align-items-center"
                            active={recommendProductTab === "7"}
                            onClick={(e) => handleTabClick("7")}
                          >
                            악세사리
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="8"
                            className="d-flex align-items-center"
                            active={recommendProductTab === "8"}
                            onClick={(e) => handleTabClick("8")}
                          >
                            식품
                          </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link
                            eventKey="8"
                            className="d-flex align-items-center"
                            active={recommendProductTab === "9"}
                            onClick={(e) => handleTabClick("9")}
                          >
                            기타
                          </Nav.Link>
                        </Nav.Item>
                      </Nav>
                    </Col>
                    <Col xs={12}>
                      <Tab.Content>
                        <Tab.Pane eventKey="1" active={recommendProductTab === "1"}>
                          <Main />
                        </Tab.Pane>
                        <Tab.Pane eventKey="2" active={recommendProductTab === "2"}>
                          <Glasses />
                        </Tab.Pane>
                        <Tab.Pane eventKey="3" active={recommendProductTab === "3"}>
                          <SunGlasses />
                        </Tab.Pane>
                        <Tab.Pane eventKey="4" active={recommendProductTab === "4"}>
                          <ContactLens />
                        </Tab.Pane>
                        <Tab.Pane eventKey="5" active={recommendProductTab === "5"}>
                          <Pixelro />
                        </Tab.Pane>
                        <Tab.Pane eventKey="6" active={recommendProductTab === "6"}>
                          <GlassesLens />
                        </Tab.Pane>
                        <Tab.Pane eventKey="7" active={recommendProductTab === "7"}>
                          <Accessory />
                        </Tab.Pane>
                        <Tab.Pane eventKey="8" active={recommendProductTab === "8"}>
                          <Food />
                        </Tab.Pane>
                        <Tab.Pane eventKey="9" active={recommendProductTab === "9"}>
                          <Etc />
                        </Tab.Pane>
                      </Tab.Content>
                    </Col>
                  </Row>
                </Tab.Container>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="flex justify-end mt-6">
        <Link className="btn btn-warning" to="/banners/recommend-product-create">
          추천 상품 등록
        </Link>
      </div>
    </div>
  );
});

export default RecommendProduct;
