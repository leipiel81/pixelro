/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import Paginator from "react-hooks-paginator";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Form, Modal } from "react-bootstrap";
import Select from "react-select";
import useSettings from "stores/settings";
import { SimpleNotification } from "components/SimpleNotification";
import { ALL_PRODUCTS, BANNER } from "graphql/gql/select";
import { MULTI_FILE } from "graphql/gql/insert";
import { UPDATE_BANNER } from "graphql/gql/update";

const EditPage = ({ match }) => {
  const { id } = match.params;

  //   배너정보
  const { loading, error, data } = useQuery(BANNER, {
    fetchPolicy: "cache-and-network",
    variables: { id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    items = data.banner;
  }

  if (!items) return <div />;

  return <div>{items && <EditForm items={items} />}</div>;
};

const EditForm = ({ items }) => {
  const settings = useSettings();
  const { id } = items;

  const goBack = () => {
    window.location.replace("/banners/banner");
  };

  // --------------------------
  // -- 데이터 값 저장 --

  const [category, setCategory] = useState({
    label: items.category,
    value: items.category,
  });
  const [title, setTitle] = useState(items.title);
  const [content, setContent] = useState(items.content);
  const [targetId, setTargetId] = useState(items.targetId);
  const [targetUrl, setTargetUrl] = useState(items.targetUrl);
  const [image, setImage] = useState(items.image);
  const [no, setNo] = useState(Number(items.no));
  const [visible, setVisible] = useState(items.visible);

  // ----------------------------

  // url 직접입력
  const [urlOpen, setUrlOpen] = useState(false);

  // 검색
  const [searchItem, setSearchItem] = useState();

  // 상품 관련 데이터
  const [mdShow, setMdShow] = useState(false);
  const [products, setProducts] = useState(null);
  const [productItems, setProductItems] = useState();

  // 페이지네이션 적용
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentItem, setCurrentItem] = useState([]);

  const pageLimit = 7;

  // 페이지네이션 초기화
  useEffect(() => {
    if (productItems) {
      setCurrentItem(productItems.slice(offset, offset + pageLimit));
    }
  }, [offset, productItems]);

  //  3. 데이터 저장
  const [updateBanner, { data: updateData }] = useMutation(UPDATE_BANNER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      goBack();
    }
  }, [updateData]);

  // 이미지 변경
  const [imageUpload, { data: imageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (imageData !== undefined) {
      const result = `${imageData.multiUpload[0].filename}`;
      setImage(result);
    }
  }, [imageData]);

  // 배너 이미지 변경
  const handleImageChange = async (e) => {
    const file = e.target.files[0];

    // 이미지 저장
    await imageUpload({ variables: { files: file, size: `[${file.size}]` } });
  };

  // 1. 저장 버튼 클릭
  const handleBannerCreate = () => {
    if (!category || !title || !content) {
      SimpleNotification({
        title: "",
        message: "정보를 입력해주세요.",
      });
      return "";
    }

    updateBanner({
      variables: { id: id, category: `${category && category.value}`, title, content, image, targetId, targetUrl, no: `${no}`, visible },
    });
  };

  //   배너 클릭시 이동할 상품 선택
  const handleProductChoice = (productId) => {
    setUrlOpen(true);
    setTargetId(productId);
    const url = `/app-product-detail/${productId}`;

    setTargetUrl(url);
    setMdShow(false);
    setSearchItem();
    setProductItems(products);
  };

  //   상품정보
  const { loading, error, data } = useQuery(ALL_PRODUCTS, {
    fetchPolicy: "cache-and-network",
    skip: !!products,
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  if (data) {
    const resultData = data.allProducts;
    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });

    setProducts(resultData);
    setProductItems(resultData);
  }

  // 검색
  const handleSearch = () => {
    setProductItems(products);

    if (searchItem) {
      setProductItems(
        products.filter((f) => {
          return f.name.indexOf(searchItem) > -1;
        })
      );
    }
  };

  if (!products) return <div />;

  const handleUrlWrite = () => {
    setTargetUrl("");
    setTargetId("");
    setUrlOpen(false);
  };
  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">사이트 꾸미기</li>
            <Link to="/banners/banner" className="breadcrumb-item active mt-1" aria-current="page">
              배너 관리
            </Link>
            <Link to="/banners/banner-create" className="breadcrumb-item active mt-1" aria-current="page">
              배너 등록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 배너 등록</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">항목명</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    <Select
                      isOptionSelected
                      value={category}
                      onChange={(e) => setCategory(e)}
                      options={[
                        { value: "메인_배너", label: "메인_배너" },
                        { value: "장소_배너", label: "장소_배너" },
                        { value: "안경_배너", label: "안경_배너" },
                        { value: "선글라스_배너", label: "선글라스_배너" },
                        { value: "콘택트렌즈_배너", label: "콘택트렌즈_배너" },
                        { value: "픽셀로제품_배너", label: "픽셀로제품_배너" },
                        { value: "안경렌즈_배너", label: "안경렌즈_배너" },
                        { value: "악세사리_배너", label: "악세사리_배너" },
                        { value: "식품_배너", label: "식품_배너" },
                        { value: "기타_배너", label: "기타_배너" },
                      ]}
                    />
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">제목</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">내용</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <textarea
                      id="about"
                      rows="5"
                      className="p-4 text-gray-700 form-control form-textarea block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                      value={content}
                      onChange={(e) => setContent(e.target.value)}
                    />
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">target id</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <div className="flex">
                      <button onClick={(e) => setMdShow(true)} type="button" className="btn btn-outline-primary btn-sm ml-2">
                        상품 찾기
                      </button>
                      <button onClick={handleUrlWrite} type="button" className="btn btn-primary btn-sm ml-2">
                        직접 입력
                      </button>
                      <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
                        <Modal.Header closeButton>
                          <div className="w-full">
                            <div className="sidebar-widget">
                              <h4 className="pro-sidebar-title">Search </h4>
                              <div className="pro-sidebar-search mt-15">
                                <div className="pro-sidebar-search-form" action="#">
                                  <input
                                    type="text"
                                    placeholder="Search here..."
                                    value={searchItem}
                                    onChange={(e) => setSearchItem(e.target.value)}
                                  />
                                  <button onClick={handleSearch}>
                                    <i className="ti-search" />
                                  </button>
                                </div>
                              </div>
                            </div>
                            <div className="flex mx-auto">
                              <div>
                                {currentItem &&
                                  currentItem.map((m) => (
                                    <div className="px-3 pt-1 mt-1 flex justify-between border-b">
                                      <div className="flex justify-start w-3/4">
                                        <img src={m.mainImage} className="w-12 h-12 rounded-full" alt="dp" />
                                        <div className="flex justify-start flex-wrap ml-4 pb-3 mt-2 ">
                                          <div className="flex justify-start font-bold">{m.name}</div>
                                          <div className="inline-flex w-full text-sm text-gray-500">
                                            <span className="pr-2">{`${m.brand} | ${m.model}`}</span>
                                          </div>
                                        </div>
                                      </div>
                                      <div>
                                        <div className="flex products-center text-black ">
                                          <button
                                            type="button"
                                            className="btn btn-warning btn-rounded py-1 px-3"
                                            onClick={(e) => handleProductChoice(m.id)}
                                          >
                                            선택
                                          </button>
                                        </div>
                                      </div>
                                    </div>
                                  ))}
                              </div>
                            </div>
                            <div className="pro-pagination-style text-center mt-30">
                              <Paginator
                                totalRecords={productItems.length}
                                pageLimit={pageLimit}
                                pageNeighbours={1} // 0, 1, 2
                                setOffset={setOffset}
                                currentPage={currentPage}
                                setCurrentPage={setCurrentPage}
                                pageContainerClass="mb-0 mt-0"
                                pagePrevText="«"
                                pageNextText="»"
                              />
                            </div>
                          </div>
                        </Modal.Header>
                      </Modal>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">target url</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={targetUrl} readOnly={urlOpen} onChange={(e) => setTargetUrl(e.target.value)} />
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">이미지</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <div className="flex my-3 ml-10">
                      <img src={image && image} className="w-64 h-24" />
                      <div className="ml-12 flex-row-left">
                        <div className="w-16 font-bold">첨부파일</div>
                        <div className="mt-3 h-8 flex flex-row table-text relative">
                          <form
                            onSubmit={() => {
                              console.log("Submitted");
                            }}
                            encType="multipart/form-data"
                          >
                            <input name="document" type="file" accept="image/*" method="POST" onChange={handleImageChange} />
                          </form>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">no</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control
                      type="number"
                      value={no}
                      min="0"
                      max="100"
                      step="1"
                      onChange={(e) => setNo(e.target.value)}
                      className="w-24"
                    />
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">보이기</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <div className="form-check">
                      <label className="form-check-label">
                        <input type="checkbox" checked={visible} onChange={(e) => setVisible(!visible)} className="form-check-input" />
                        <i className="input-helper" />
                      </label>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 grid-margin mt-3">
        <div className="row justify-end mr-1">
          <button type="button" className="btn btn-primary mr-2" onClick={handleBannerCreate}>
            저장
          </button>
          <button className="btn btn-light" onClick={goBack}>
            취소
          </button>
        </div>
      </div>
    </>
  );
};

EditPage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default EditPage;
