/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import Paginator from "react-hooks-paginator";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Form, Modal } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import { ALL_PRODUCTS, BANNER } from "graphql/gql/select";
import { UPDATE_BANNER } from "graphql/gql/update";

const EditPage = ({ match }) => {
  const { id } = match.params;

  //   배너정보
  const { loading, error, data } = useQuery(BANNER, {
    fetchPolicy: "cache-and-network",
    variables: { id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    items = data.banner;
  }

  if (!items) return <div />;

  return <div>{items && <EditForm items={items} />}</div>;
};

const EditForm = ({ items }) => {
  const history = useHistory();
  const goBack = () => {
    history.push("/banners/recommend-product");
  };

  // --------------------------
  // -- 데이터 값 저장 --
  const { id, image } = items;
  const [category, setCategory] = useState(items.category);
  const [targetId, setTargetId] = useState(items.targetId);
  const [targetUrl, setTargetUrl] = useState(items.targetUrl);
  const [no, setNo] = useState(Number(items.no));
  const [visible, setVisible] = useState(items.visible);

  // ----------------------------

  // 검색
  const [searchItem, setSearchItem] = useState();

  // 상품 관련 데이터
  const [mdShow, setMdShow] = useState(false);
  const [products, setProducts] = useState(null);
  const [productItems, setProductItems] = useState();

  // 페이지네이션 적용
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentItem, setCurrentItem] = useState([]);

  const pageLimit = 7;

  // 페이지네이션 초기화
  useEffect(() => {
    if (productItems) {
      setCurrentItem(productItems.slice(offset, offset + pageLimit));
    }
  }, [offset, productItems]);

  //  3. 데이터 저장
  const [updateBanner, { data: updateData }] = useMutation(UPDATE_BANNER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      goBack();
    }
  }, [updateData]);

  // 1. 저장 버튼 클릭
  const handleBannerCreate = () => {
    if (!category || !targetUrl) {
      SimpleNotification({
        title: "",
        message: "정보를 입력해주세요.",
      });
      return "";
    }

    updateBanner({
      variables: { id: id, category: `${category && category.value}`, image, targetId, targetUrl, no: `${no}`, visible },
    });
  };

  //   배너 클릭시 이동할 상품 선택
  const handleProductChoice = (productId) => {
    setTargetId(productId);
    const url = `/app-product-detail/${productId}`;

    setTargetUrl(url);
    setMdShow(false);
    setSearchItem();
    setProductItems(products);
  };

  //   상품정보
  const { loading, error, data } = useQuery(ALL_PRODUCTS, {
    fetchPolicy: "cache-and-network",
    skip: !!products,
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  if (data) {
    const resultData = data.allProducts;
    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });

    setProducts(resultData);
    setProductItems(resultData);
  }

  // 검색
  const handleSearch = () => {
    setProductItems(products);

    if (searchItem) {
      setProductItems(
        products.filter((f) => {
          return f.name.indexOf(searchItem) > -1;
        })
      );
    }
  };

  if (!products) return <div />;

  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">사이트 꾸미기</li>
            <Link to="/banners/recommend-product" className="breadcrumb-item active mt-1" aria-current="page">
              추천 상품
            </Link>
            <Link to={`/banners/recommend-product-edit/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              추천 상품 수정
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 추천 상품 수정</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">항목명</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    <select
                      className="border border-black p-3 text-black w-1/4"
                      value={category}
                      onChange={(e) => setCategory(e.target.value)}
                    >
                      <option value="메인_추천">메인_추천</option>
                      <option value="장소_추천">장소_추천</option>
                      <option value="안경_추천">안경_추천</option>
                      <option value="선글라스_추천">선글라스_추천</option>
                      <option value="콘택트렌즈_추천">콘택트렌즈_추천</option>
                      <option value="픽셀로제품_추천">픽셀로제품_추천</option>
                      <option value="안경렌즈_추천">안경렌즈_추천</option>
                      <option value="악세사리_추천">악세사리_추천</option>
                      <option value="식품_추천">식품_추천</option>
                      <option value="기타_추천">기타_추천</option>
                    </select>
                  </td>
                </tr>

                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">URL</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <div className="flex">
                      <button onClick={(e) => setMdShow(true)} type="button" className="btn btn-outline-primary btn-sm ml-2">
                        상품 찾기
                      </button>

                      <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
                        <Modal.Header closeButton>
                          <div className="w-full">
                            <div className="sidebar-widget">
                              <h4 className="pro-sidebar-title">Search </h4>
                              <div className="pro-sidebar-search mt-15">
                                <div className="pro-sidebar-search-form" action="#">
                                  <input
                                    type="text"
                                    placeholder="Search here..."
                                    value={searchItem}
                                    onChange={(e) => setSearchItem(e.target.value)}
                                  />
                                  <button onClick={handleSearch}>
                                    <i className="ti-search" />
                                  </button>
                                </div>
                              </div>
                            </div>
                            <div className="flex mx-auto">
                              <div>
                                {currentItem &&
                                  currentItem.map((m) => (
                                    <div className="px-3 pt-1 mt-1 flex justify-between border-b">
                                      <div className="flex justify-start w-3/4">
                                        <img src={m.mainImage} className="w-12 h-12 rounded-full" alt="dp" />
                                        <div className="flex justify-start flex-wrap ml-4 pb-3 mt-2 ">
                                          <div className="flex justify-start font-bold">{m.name}</div>
                                          <div className="inline-flex w-full text-sm text-gray-500">
                                            <span className="pr-2">{`${m.brand} | ${m.model}`}</span>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="flex products-center text-black w-1/4">
                                        <button
                                          type="button"
                                          className="btn btn-warning btn-rounded py-1 px-3"
                                          onClick={(e) => handleProductChoice(m.id)}
                                        >
                                          선택
                                        </button>
                                      </div>
                                    </div>
                                  ))}
                              </div>
                            </div>
                            <div className="pro-pagination-style text-center mt-30">
                              <Paginator
                                totalRecords={productItems.length}
                                pageLimit={pageLimit}
                                pageNeighbours={1} // 0, 1, 2
                                setOffset={setOffset}
                                currentPage={currentPage}
                                setCurrentPage={setCurrentPage}
                                pageContainerClass="mb-0 mt-0"
                                pagePrevText="«"
                                pageNextText="»"
                              />
                            </div>
                          </div>
                        </Modal.Header>
                      </Modal>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">target url</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={targetUrl} readOnly />
                  </td>
                </tr>

                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">no</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control
                      type="number"
                      value={no}
                      min="0"
                      max="100"
                      step="1"
                      onChange={(e) => setNo(e.target.value)}
                      className="w-24"
                    />
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">보이기</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <div className="form-check">
                      <label className="form-check-label">
                        <input type="checkbox" checked={visible} onChange={(e) => setVisible(!visible)} className="form-check-input" />
                        <i className="input-helper" />
                      </label>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 grid-margin mt-3">
        <div className="row justify-end mr-1">
          <button type="button" className="btn btn-primary mr-2" onClick={handleBannerCreate}>
            저장
          </button>
          <button className="btn btn-light" onClick={goBack}>
            취소
          </button>
        </div>
      </div>
    </>
  );
};

EditPage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default EditPage;
