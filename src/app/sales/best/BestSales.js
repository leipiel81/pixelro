import React, { Component } from "react";

export class ProductCatalogue extends Component {
  render() {
    return (
      <div>
        {/* breadcrumb */}
        <div>
          <nav aria-label="breadcrumb" role="navigation">
            <ol className="breadcrumb">
              <li className="breadcrumb-item active">베스트 상품 목록</li>
            </ol>
          </nav>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-body">베스트 상품 준비중</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductCatalogue;
