import React, { useState } from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useQuery } from "@apollo/react-hooks";
import { Link } from "react-router-dom";
import { FormatDateShort } from "components/FormatDate";
import { ALL_SETTLE_REPORTS } from "graphql/gql/select";
import { Form } from "react-bootstrap";
import DatePicker from "react-datepicker";
import { SimpleNotification } from "components/SimpleNotification";

const Content = () => {
  const tempStartDate = new Date();
  tempStartDate.setMonth(tempStartDate.getMonth() - 1);

  const [pickMonth, setPickMonth] = useState(tempStartDate);

  //   정산
  const { loading, error, data, refetch } = useQuery(ALL_SETTLE_REPORTS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  let productTotal = 0;
  let pixelroTotal = 0;
  let storeTotal = 0;
  let pointTotal = 0;
  let salesCommissionTotal = 0;
  let finalTotal = 0;
  if (data && data.allSettleReports) {
    items = data.allSettleReports;
    if (pickMonth) {
      const year = pickMonth.getFullYear();
      const month = pickMonth.getMonth() + 1;

      items = items.filter((f) => f.year === year).filter((f2) => f2.month === month);

      items.map((m) => (productTotal += Number(m.productTotal)));
      items.map((m) => (pixelroTotal += Number(m.pixelroTotal)));
      items.map((m) => (storeTotal += Number(m.storeTotal)));
      items.map((m) => (pointTotal += Number(m.pointTotal)));
      items.map((m) => (salesCommissionTotal += Number(m.salesCommission)));
      items.map((m) => (finalTotal += Number(m.settlementPrice)));
    }
  }

  if (!items) return <div />;

  const handleSearch = (pt) => {
    setPickMonth(pt);
    refetch();
  };

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">실적 관리</li>
            <li className="breadcrumb-item active">상점별 매출 조회</li>
          </ol>
        </nav>
      </div>
      {/* 검색 */}

      <SearchBar mainMonth={pickMonth} handleSearch={handleSearch} />

      <TotalPrice
        productTotal={productTotal}
        pixelroTotal={pixelroTotal}
        storeTotal={storeTotal}
        pointTotal={pointTotal}
        salesCommissionTotal={salesCommissionTotal}
        finalTotal={finalTotal}
      />
      <Table items={items} />
    </div>
  );
};

// 검색 Bar
const SearchBar = ({ handleSearch, mainMonth }) => {
  const [pickMonth, setPickMonth] = useState(mainMonth);

  const handleStartDateChange = (e) => {
    setPickMonth(e);
    handleSearch(e);
    console.log(e);
  };

  return (
    <div className="flex">
      <div className="col-md-10">
        <div className="flex w-full items-center">
          <div className="w-7/12">
            <div className="flex justify-between items-center mt-2">
              <div>
                <Form.Group className="row">
                  <DatePicker
                    className="text-center items-center justify-center border py-2 px-3 bg-gray-100 font-bold text-lg"
                    selected={pickMonth}
                    onChange={handleStartDateChange}
                    dateFormat="yyyy-MM"
                    showMonthYearPicker
                    showFullMonthYearPicker
                    showTwoColumnMonthYearPicker
                  />
                </Form.Group>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const TotalPrice = ({ productTotal, pixelroTotal, storeTotal, pointTotal, salesCommissionTotal, finalTotal }) => {
  return (
    <div className="row">
      <div className="col-md-3 grid-margin stretch-card">
        <div className="card">
          <div className="p-1">
            <p className="text-muted">판매금액</p>
            <h4 className="m-2 font-weight-bold text-lg text-center">{Number(productTotal).toLocaleString()}</h4>
          </div>
        </div>
      </div>
      <div className="col-md-2 grid-margin stretch-card">
        <div className="card">
          <div className="card-body p-1">
            <p className="text-muted">픽셀로쿠폰</p>
            <h4 className="m-2 font-weight-bold text-lg text-center">{Number(pixelroTotal).toLocaleString()}</h4>
          </div>
        </div>
      </div>
      <div className="col-md-2 grid-margin stretch-card">
        <div className="card">
          <div className="card-body p-1">
            <p className="text-muted">상점쿠폰</p>
            <h4 className="m-2 font-weight-bold text-lg text-center">{Number(storeTotal).toLocaleString()}</h4>
          </div>
        </div>
      </div>
      <div className="col-md-2 grid-margin stretch-card">
        <div className="card">
          <div className="card-body p-1">
            <p className="text-muted">포인트</p>
            <h4 className="m-2 font-weight-bold text-lg  text-center">{Number(pointTotal).toLocaleString()}</h4>
          </div>
        </div>
      </div>
      <div className="col-md-2 grid-margin stretch-card">
        <div className="card">
          <div className="card-body p-1">
            <p className="text-muted">판매수수료</p>
            <h4 className="m-2 font-weight-bold text-lg  text-center">{Number(salesCommissionTotal).toLocaleString()}</h4>
          </div>
        </div>
      </div>
      <div className="col-md-3 grid-margin stretch-card">
        <div className="card">
          <div className="card-body p-1">
            <p className="text-muted">정산금액</p>
            <h4 className="m-2 font-weight-bold text-lg text-center">{Number(finalTotal).toLocaleString()}</h4>
          </div>
        </div>
      </div>
    </div>
  );
};

const Table = ({ items }) => {
  return (
    <div className="row">
      <div className="col-12">
        <div className="card">
          <div className="card-body">
            <div className="flex justify-between">
              <h4 className="card-title">상점별 매출 목록</h4>
              <h4 className="card-title">{`Total : ${items.length}`}</h4>
            </div>
            <div className="row">
              <div className="col-12">
                <div>
                  <ReactTable
                    data={items}
                    filterable
                    defaultPageSize={10}
                    columns={[
                      {
                        Header: "상점",
                        id: "storeName",
                        accessor: (d) => d.store.name,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["storeName"] }),
                        filterAll: true,
                        Cell: (row) => (
                          <Link to={`/sales/store-sales-detail/${row.original.id}`} className="text-blue-700 font-medium">
                            {row.original.store.name}
                          </Link>
                        ),
                      },
                      {
                        Header: "정산월",
                        id: "year",
                        accessor: (d) => `${d.year}-${d.month}`,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["year"] }),
                        filterAll: true,
                        Cell: (row) => <div>{`${row.original.year}-${row.original.month}`}</div>,
                      },

                      {
                        Header: "판매금액",
                        id: "productTotal",
                        accessor: (d) => d.productTotal,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["productTotal"] }),
                        filterAll: true,
                        Cell: (row) => row.value.toLocaleString(),
                      },
                      {
                        Header: "픽셀로쿠폰",
                        id: "pixelroTotal",
                        accessor: (d) => d.pixelroTotal,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["pixelroTotal"] }),
                        filterAll: true,
                        Cell: (row) => row.value.toLocaleString(),
                      },
                      {
                        Header: "상점쿠폰",
                        id: "storeTotal",
                        accessor: (d) => d.storeTotal,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["storeTotal"] }),
                        filterAll: true,
                        Cell: (row) => row.value.toLocaleString(),
                      },
                      {
                        Header: "포인트",
                        id: "pointTotal",
                        accessor: (d) => d.pointTotal,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["pointTotal"] }),
                        filterAll: true,
                        Cell: (row) => row.value.toLocaleString(),
                      },
                      {
                        Header: "판매수수료",
                        id: "salesCommission",
                        accessor: (d) => d.salesCommission,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["salesCommission"] }),
                        filterAll: true,
                        Cell: (row) => <div className="text-red-500">{row.value.toLocaleString()}</div>,
                      },
                      {
                        Header: "정산금액",
                        id: "settlementPrice",
                        accessor: (d) => d.settlementPrice,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["settlementPrice"] }),
                        filterAll: true,
                        Cell: (row) => <div className="text-red-500">{row.value.toLocaleString()}</div>,
                      },
                      {
                        Header: "신청날짜",
                        id: "createAt",
                        accessor: (d) => FormatDateShort(new Date(parseInt(d.createAt, 10))),
                        filterMethod: (filter, rows) =>
                          matchSorter(rows, filter.value, {
                            keys: ["createAt"],
                          }),
                        filterAll: true,
                        Cell: (row) => <div>{row.value}</div>,
                      },
                      {
                        Header: "정산날짜",
                        id: "settlementAt",
                        accessor: (d) => d.settlementAt,
                        filterMethod: (filter, rows) =>
                          matchSorter(rows, filter.value, {
                            keys: ["settlementAt"],
                          }),
                        filterAll: true,
                        Cell: (row) => <div>{row.value}</div>,
                      },
                    ]}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Content;
