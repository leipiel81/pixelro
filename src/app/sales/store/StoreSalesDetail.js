import React from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useQuery } from "@apollo/react-hooks";
import { SETTLEMENT_ORDER_DATA } from "graphql/gql/select";
import { Product, User } from "graphql/query/select";
import { SimpleNotification } from "components/SimpleNotification";

const Content = ({ match }) => {
  const { id } = match.params;

  // 정산 데이터 가져오기
  const { loading, error, data } = useQuery(SETTLEMENT_ORDER_DATA, {
    fetchPolicy: "cache-and-network",
    variables: { id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    const resultData = data.settlementOrderData.result;

    if (resultData) {
      items = JSON.parse(resultData).orderListValue;
    }
  }

  if (!items) return <div />;

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">실적 관리</li>
            <li className="breadcrumb-item active">상점별 매출 조회</li>
          </ol>
        </nav>
      </div>
      {/* 검색 */}

      <Table items={items} />
    </div>
  );
};

const Table = ({ items }) => {
  return (
    <div className="row">
      <div className="col-12">
        <div className="card">
          <div className="card-body">
            <h4 className="card-title">상점별 매출 목록</h4>
            <div className="row">
              <div className="col-12">
                <div>
                  <ReactTable
                    data={items}
                    filterable
                    defaultPageSize={10}
                    columns={[
                      {
                        Header: "상품명",
                        id: "productId",
                        accessor: (d) => d.productId,
                        filterMethod: (filter, rows) =>
                          matchSorter(rows, <ProductName productId={filter.value} />, {
                            keys: ["productId"],
                          }),
                        filterAll: true,
                        Cell: (row) => <ProductName productId={row.value} />,
                      },
                      {
                        Header: "옵션/수량",
                        id: "options",
                        accessor: (d) => `${d.productOption}/${d.productCount}`,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["options"] }),
                        filterAll: true,
                        Cell: (row) => <div>{`${row.original.productOption}/${row.original.productCount}`}</div>,
                      },

                      {
                        Header: "판매금액",
                        id: "productTotal",
                        accessor: (d) => d.productTotal,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["productTotal"] }),
                        filterAll: true,
                        Cell: (row) => (row.value ? row.value.toLocaleString() : 0),
                      },
                      {
                        Header: "픽셀로쿠폰",
                        id: "pixelroCouponDiscount",
                        accessor: (d) => d.pixelroCouponDiscount,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["pixelroCouponDiscount"] }),
                        filterAll: true,
                        Cell: (row) => (row.value ? row.value.toLocaleString() : 0),
                      },
                      {
                        Header: "상점쿠폰",
                        id: "storeCouponDiscount",
                        accessor: (d) => d.storeCouponDiscount,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["storeCouponDiscount"] }),
                        filterAll: true,
                        Cell: (row) => (row.value ? row.value.toLocaleString() : 0),
                      },
                      {
                        Header: "포인트",
                        id: "point",
                        accessor: (d) => d.point,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["point"] }),
                        filterAll: true,
                        Cell: (row) => (row.value ? row.value.toLocaleString() : 0),
                      },
                      {
                        Header: "정산금액",
                        id: "productTotal",
                        accessor: (d) => d.productTotal,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["productTotal"] }),
                        filterAll: true,
                        Cell: (row) => <div className="text-red-500">{row.value ? row.value.toLocaleString() : 0}</div>,
                      },
                      {
                        Header: "구매자",
                        id: "userId",
                        accessor: (d) => d.userId,
                        filterMethod: (filter, rows) =>
                          matchSorter(rows, filter.value, {
                            keys: ["userId"],
                          }),
                        filterAll: true,
                        Cell: (row) => <UserName userId={row.value} />,
                      },
                      {
                        Header: "구매확정일자",
                        id: "fixAt",
                        accessor: (d) => d.fixAt,
                        filterMethod: (filter, rows) =>
                          matchSorter(rows, filter.value, {
                            keys: ["fixAt"],
                          }),
                        filterAll: true,
                        Cell: (row) => <div>{row.value}</div>,
                      },
                    ]}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const ProductName = ({ productId }) => {
  const { result } = Product(productId);

  if (!result) return <div />;

  return <div>{result.name}</div>;
};

const UserName = ({ userId }) => {
  const { result } = User(userId);

  if (!result) return <div />;

  return <div>{result.name}</div>;
};

export default Content;
