/* eslint-disable no-plusplus */
/* eslint-disable no-return-assign */
import React, { useState } from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useQuery } from "@apollo/react-hooks";
import { Bar } from "react-chartjs-2";
import { ALL_SETTLE_REPORTS } from "graphql/gql/select";
import { Form } from "react-bootstrap";
import DatePicker from "react-datepicker";
import { SimpleNotification } from "components/SimpleNotification";

const Content = () => {
  const tempStartDate = new Date();

  const [pickYear, setPickYear] = useState(tempStartDate);

  //   정산
  const { loading, error, data, refetch } = useQuery(ALL_SETTLE_REPORTS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  let productTotal = 0;
  let pixelroTotal = 0;
  let storeTotal = 0;
  let pointTotal = 0;
  let salesCommissionTotal = 0;
  let finalTotal = 0;
  const monthList = [];
  if (data && data.allSettleReports) {
    items = data.allSettleReports;
    if (pickYear) {
      const year = pickYear.getFullYear();

      items = items.filter((f) => f.year === year);

      items.map((m) => (productTotal += Number(m.productTotal)));
      items.map((m) => (pixelroTotal += Number(m.pixelroTotal)));
      items.map((m) => (storeTotal += Number(m.storeTotal)));
      items.map((m) => (pointTotal += Number(m.pointTotal)));
      items.map((m) => (salesCommissionTotal += Number(m.salesCommission)));
      items.map((m) => (finalTotal += Number(m.settlementPrice)));

      // 정산월에 맞춰서 값 저장
      for (let i = 1; i < 13; i += 1) {
        const tempItems = items;

        let tempProductTotal = 0;
        let tempPixelroTotal = 0;
        let tempStoreTotal = 0;
        let tempPointTotal = 0;
        let tempSalesCommissionTotal = 0;
        let tempFinalTotal = 0;

        tempItems
          .filter((f) => f.month === i)
          .map(
            (m) => (
              (tempProductTotal += Number(m.productTotal)),
              (tempPixelroTotal += Number(m.pixelroTotal)),
              (tempStoreTotal += Number(m.storeTotal)),
              (tempPointTotal += Number(m.pointTotal)),
              (tempSalesCommissionTotal += Number(m.salesCommission)),
              (tempFinalTotal += Number(m.settlementPrice))
            )
          );

        if (tempProductTotal !== 0) {
          monthList.push({
            year: pickYear.getFullYear(),
            month: i,
            productTotal: tempProductTotal,
            pixelroCouponDiscount: tempPixelroTotal,
            storeCouponDiscount: tempStoreTotal,
            point: tempPointTotal,
            salesCommission: tempSalesCommissionTotal,
            finalPrice: tempFinalTotal,
          });
        }
      }
    }
  }

  if (!items) return <div />;

  const handleSearch = (pt) => {
    setPickYear(pt);
    refetch();
  };

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">실적 관리</li>
            <li className="breadcrumb-item active">기간별 매출 조회</li>
          </ol>
        </nav>
      </div>
      {/* 검색 */}
      <SearchBar mainYear={pickYear} handleSearch={handleSearch} />
      {/* 그래프 */}
      <Chart items={items} />

      <TotalPrice
        productTotal={productTotal}
        pixelroTotal={pixelroTotal}
        storeTotal={storeTotal}
        pointTotal={pointTotal}
        salesCommissionTotal={salesCommissionTotal}
        finalTotal={finalTotal}
      />
      <Table monthList={monthList} />
    </div>
  );
};

// 검색 Bar
const SearchBar = ({ handleSearch, mainYear }) => {
  const [pickYear, setPickYear] = useState(mainYear);

  const handleStartDateChange = (e) => {
    setPickYear(e);
    handleSearch(e);
    console.log(e);
  };

  return (
    <div className="flex">
      <div className="col-md-10">
        <div className="flex w-full items-center">
          <div className="w-7/12">
            <div className="flex justify-between items-center mt-2">
              <div>
                <Form.Group className="row">
                  <DatePicker
                    className="text-center items-center justify-center border py-2 px-3 bg-gray-100 font-bold text-lg"
                    selected={pickYear}
                    onChange={handleStartDateChange}
                    showYearPicker
                    dateFormat="yyyy"
                  />
                </Form.Group>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const Chart = ({ items }) => {
  const year = new Date().getUTCFullYear();

  const months = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  const yearItems = items.filter((f) => f.year === year);
  for (let i = 1; i < 13; i += 1) {
    yearItems.map((m) => (m.month === i ? (months[i - 1] = m.settlementPrice) : ""));
  }

  const state = {
    labels: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
    datasets: [
      {
        label: `${year}년 월별 수익 조회`,
        backgroundColor: "rgba(255,99,132,0.4)",
        borderColor: "rgba(255,99,132,1)",
        borderWidth: 1,
        hoverBackgroundColor: "rgba(255,99,132,0.6)",
        hoverBorderColor: "rgba(255,99,132,1)",
        data: months,
      },
    ],
  };

  return <Bar data={state} height={100} />;
};

const TotalPrice = ({ productTotal, pixelroTotal, storeTotal, pointTotal, salesCommissionTotal, finalTotal }) => {
  return (
    <div className="row mt-4">
      <div className="col-md-3 grid-margin stretch-card">
        <div className="card">
          <div className="p-1">
            <p className="text-muted">판매금액</p>
            <h4 className="m-2 font-weight-bold text-lg text-center">{Number(productTotal).toLocaleString()}</h4>
          </div>
        </div>
      </div>
      <div className="col-md-2 grid-margin stretch-card">
        <div className="card">
          <div className="card-body p-1">
            <p className="text-muted">픽셀로쿠폰</p>
            <h4 className="m-2 font-weight-bold text-lg text-center">{Number(pixelroTotal).toLocaleString()}</h4>
          </div>
        </div>
      </div>
      <div className="col-md-2 grid-margin stretch-card">
        <div className="card">
          <div className="card-body p-1">
            <p className="text-muted">상점쿠폰</p>
            <h4 className="m-2 font-weight-bold text-lg text-center">{Number(storeTotal).toLocaleString()}</h4>
          </div>
        </div>
      </div>
      <div className="col-md-2 grid-margin stretch-card">
        <div className="card">
          <div className="card-body p-1">
            <p className="text-muted">포인트</p>
            <h4 className="m-2 font-weight-bold text-lg  text-center">{Number(pointTotal).toLocaleString()}</h4>
          </div>
        </div>
      </div>
      <div className="col-md-2 grid-margin stretch-card">
        <div className="card">
          <div className="card-body p-1">
            <p className="text-muted">판매 수수료</p>
            <h4 className="m-2 font-weight-bold text-lg  text-center">{Number(salesCommissionTotal).toLocaleString()}</h4>
          </div>
        </div>
      </div>
      <div className="col-md-3 grid-margin stretch-card">
        <div className="card">
          <div className="card-body p-1">
            <p className="text-muted">정산금액</p>
            <h4 className="m-2 font-weight-bold text-lg text-center">{Number(finalTotal).toLocaleString()}</h4>
          </div>
        </div>
      </div>
    </div>
  );
};

const Table = ({ monthList }) => {
  return (
    <div className="row">
      <div className="col-12">
        <div className="card">
          <div className="card-body">
            <div className="flex justify-between">
              <h4 className="card-title">기간별 매출 목록</h4>
              <h4 className="card-title">{`Total : ${monthList.length}`}</h4>
            </div>
            <div className="row">
              <div className="col-12">
                <div>
                  <ReactTable
                    data={monthList}
                    filterable
                    defaultPageSize={10}
                    columns={[
                      {
                        Header: "정산월",
                        id: "year",
                        accessor: (d) => `${d.year}-${d.month}`,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["year"] }),
                        filterAll: true,
                        Cell: (row) => <div>{`${row.original.year}-${row.original.month}`}</div>,
                      },

                      {
                        Header: "판매금액",
                        id: "productTotal",
                        accessor: (d) => d.productTotal,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["productTotal"] }),
                        filterAll: true,
                        Cell: (row) => row.value.toLocaleString(),
                      },
                      {
                        Header: "픽셀로쿠폰",
                        id: "pixelroCouponDiscount",
                        accessor: (d) => d.pixelroCouponDiscount,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["pixelroCouponDiscount"] }),
                        filterAll: true,
                        Cell: (row) => row.value.toLocaleString(),
                      },
                      {
                        Header: "상점쿠폰",
                        id: "storeCouponDiscount",
                        accessor: (d) => d.storeCouponDiscount,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["storeCouponDiscount"] }),
                        filterAll: true,
                        Cell: (row) => row.value.toLocaleString(),
                      },
                      {
                        Header: "포인트",
                        id: "point",
                        accessor: (d) => d.point,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["point"] }),
                        filterAll: true,
                        Cell: (row) => row.value.toLocaleString(),
                      },
                      {
                        Header: "정산금액",
                        id: "finalPrice",
                        accessor: (d) => d.finalPrice,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["finalPrice"] }),
                        filterAll: true,
                        Cell: (row) => <div className="text-red-500">{row.value.toLocaleString()}</div>,
                      },
                    ]}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Content;
