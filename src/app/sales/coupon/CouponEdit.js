/* eslint-disable no-shadow */
/* eslint-disable react/self-closing-comp */
/* eslint-disable no-lone-blocks */
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { observer } from "mobx-react-lite";
import useSettings from "stores/settings";
import { FormatDate } from "components/FormatDate";
import DatePicker from "react-datepicker";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { COUPON } from "graphql/gql/select";
import { UPDATE_COUPON } from "graphql/gql/update";
import { SimpleNotification } from "components/SimpleNotification";

const EditPage = observer(({ match }) => {
  const { id } = match.params;
  const settings = useSettings();
  const history = useHistory();

  if (!settings.isLogin) {
    history.push("/signin");
  }

  const { loading, error, data } = useQuery(COUPON, {
    fetchPolicy: "cache-and-network",
    variables: { id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    console.log(error);
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (!error && data) {
    items = data.coupon;
  }

  if (!items) return <div />;

  return (
    <>
      <TopBar />
      <EditForm items={items} />
    </>
  );
});

const TopBar = () => {
  return (
    <div>
      <div className="shop-top-bar mb-35">
        <div className="select-shopping-wrap">
          <div className="shop-select">
            <h3 className="cart-page-title mb-0">쿠폰 수정</h3>
          </div>
        </div>
      </div>
    </div>
  );
};

const EditForm = ({ items }) => {
  const history = useHistory();
  const goBack = () => {
    history.push("/sales/coupon");
  };

  // -- 데이터 값 저장 --
  const [startAt, setStartAt] = useState(new Date(items.startAt.slice(0, 10)));
  const [endAt, setEndAt] = useState(new Date(items.endAt.slice(0, 10)));
  const [storeActive, setStoreActive] = useState(String(items.storeActive));
  const [min, setMin] = useState(String(items.min));
  const [cashDiscount, setCashDiscount] = useState(String(items.cashDiscount));
  const [percentDiscount, setPercentDiscount] = useState(String(items.percentDiscount));
  const [downloadLimit, setDownloadLimit] = useState(String(items.downloadLimit));
  const [visitName, setVisitName] = useState(items.visitName);
  const [couponName, setCouponName] = useState(items.couponName);

  const [couponUpdate, { data: updateData }] = useMutation(UPDATE_COUPON, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });
  useEffect(() => {
    if (updateData !== undefined) {
      goBack();
    }
  }, [updateData]);

  const handleUpdate = () => {
    if (!couponName || couponName === "") {
      SimpleNotification({
        message: "쿠폰 이름을 설정해주세요.",
      });
      return;
    }

    // endAt 변경
    const tempStartAt = startAt;
    tempStartAt.setHours(0, 0, 0, 0);
    const tempEndAt = endAt;
    tempEndAt.setHours(23, 59, 59, 999);

    couponUpdate({
      variables: {
        id: `${items.id}`,
        startAt: `${FormatDate(tempStartAt)}`,
        endAt: `${FormatDate(tempEndAt)}`,
        storeActive: storeActive === "true",
        min: Number(min),
        cashDiscount: Number(cashDiscount),
        percentDiscount: Number(percentDiscount),
        downloadLimit: Number(downloadLimit),
        visitName: `${visitName}`,
        couponName,
      },
    });
  };

  return (
    <>
      <div>
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-lg leading-6 font-medium text-black">▶︎ 기간 설정</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">시작일</th>
                    <td className="border px-8 py-2 text-sm w-2/6">
                      <DatePicker className="form-control" selected={startAt} onChange={(e) => setStartAt(e)} />
                    </td>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">종료일</th>
                    <td className="border px-8 py-2 text-sm w-2/6">
                      <DatePicker className="form-control" selected={endAt} onChange={(e) => setEndAt(e)} />
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* 쿠폰 이름 설정 */}
      <div>
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-lg leading-6 font-medium text-black">▶︎ 쿠폰 이름 설정</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">쿠폰 이름</th>
                    <td className="flex border px-8 py-2 text-sm ">
                      <input
                        type="text"
                        className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 pl-2"
                        value={couponName}
                        onChange={(e) => setCouponName(e.target.value)}
                      />
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* 쿠폰 개수 설정 */}
      <div>
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-lg leading-6 font-medium text-black">▶︎ 쿠폰 개수 설정</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">쿠폰 개수</th>
                    <td className="flex border px-8 py-2 text-sm ">
                      <div className="w-1/6">
                        <input
                          type="number"
                          className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 pl-2"
                          value={downloadLimit}
                          onChange={(e) => setDownloadLimit(e.target.value)}
                        />
                      </div>
                      <div className="w-1/6 flex items-center ml-2">개</div>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-lg leading-6 font-medium text-black">▶︎ 쿠폰 종류 수정</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  {items.min !== 0 ? (
                    <tr>
                      <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">조건금액</th>
                      <td colSpan="3" className=" flex border px-8 py-2 text-sm">
                        <div className="w-1/6">
                          <input
                            type="number"
                            className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 pl-2"
                            value={min}
                            onChange={(e) => setMin(e.target.value)}
                          />
                        </div>
                        <div className="w-2/6 flex items-center ml-2">{`원 이상 구매  ${items.feeFree ? "무료배송" : ""}`}</div>
                      </td>
                    </tr>
                  ) : (
                    ""
                  )}
                  {items.cashDiscount !== 0 ? (
                    <tr>
                      <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">할인금액</th>
                      <td colSpan="3" className=" flex border px-8 py-2 text-sm">
                        <div className="w-1/6">
                          <input
                            type="number"
                            className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 pl-2"
                            value={cashDiscount}
                            onChange={(e) => setCashDiscount(e.target.value)}
                          />
                        </div>
                        <div className="w-1/6 flex items-center ml-2">원 할인</div>
                      </td>
                    </tr>
                  ) : (
                    ""
                  )}

                  {items.percentDiscount !== 0 ? (
                    <tr>
                      <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">할인 %</th>

                      <td colSpan="3" className=" flex border px-8 py-2 text-sm">
                        <div className="w-1/6">
                          <input
                            type="number"
                            className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 pl-2"
                            value={percentDiscount}
                            onChange={(e) => setPercentDiscount(e.target.value)}
                          />
                        </div>
                        <div className="w-1/6 flex items-center ml-2"> % 할인</div>
                      </td>
                    </tr>
                  ) : (
                    ""
                  )}

                  {items.visit ? (
                    <tr>
                      <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">방문쿠폰</th>

                      <td colSpan="3" className=" flex border px-8 py-2 text-sm">
                        <div className="w-full">
                          <input
                            type="text"
                            className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 pl-2"
                            value={visitName}
                            onChange={(e) => setVisitName(e.target.value)}
                          />
                        </div>
                      </td>
                    </tr>
                  ) : (
                    ""
                  )}
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-lg leading-6 font-medium text-black">▶︎ 쿠폰 상태</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">쿠폰 상태</th>
                    <td className="flex border px-8 py-2 text-sm ">
                      <select className="form-control" value={storeActive} onChange={(e) => setStoreActive(e.target.value)}>
                        <option value>활성</option>
                        <option value={false}>비활성</option>
                      </select>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 grid-margin mt-3">
        <div className="row justify-end mr-1">
          <button type="button" className="btn btn-primary mr-2" onClick={handleUpdate}>
            수정
          </button>
          <button className="btn btn-light" onClick={goBack}>
            취소
          </button>
        </div>
      </div>
    </>
  );
};

export default EditPage;
