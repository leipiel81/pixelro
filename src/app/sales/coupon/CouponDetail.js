/* eslint-disable prefer-destructuring */
import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import { COUPON } from "graphql/gql/select";
import { SimpleNotification } from "components/SimpleNotification";

const DetailPage = ({ match }) => {
  const { id } = match.params;

  const { loading, error, data } = useQuery(COUPON, {
    fetchPolicy: "cache-and-network",
    variables: { id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  // 쿠폰 정보
  let items;
  if (data) {
    items = data.coupon;
  }

  if (!items) return <div />;
  return (
    <div className="shop-bottom-area">
      <div className="row">
        <div className="w-full">
          <Topbar />
          <InfoForm items={items} />
          <div className="col-12 grid-margin mt-3">
            <div className="row justify-end mr-1">
              <Link className="btn btn-primary mr-2 mt-2 " to={`/sales/coupon-edit/${id}`}>
                수정하기
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
const Topbar = () => {
  return (
    <div>
      <div className="shop-top-bar ">
        <div className="select-shopping-wrap">
          <div className="shop-select">
            <h3 className="cart-page-title mb-0">쿠폰 상세보기</h3>
          </div>
        </div>
      </div>
    </div>
  );
};

// 상품 기본 정보
const InfoForm = ({ items }) => {
  return (
    <>
      <div>
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-lg leading-6 font-medium text-black">▶︎ 등록 정보</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">쿠폰 이름</th>
                    <td className=" border px-8 py-2 text-sm ">{items.couponName || ""}</td>
                  </tr>
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">쿠폰 사용기간</th>
                    <td className=" border px-8 py-2 text-sm ">{`${items.startAt} ~ ${items.endAt}`}</td>
                  </tr>
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">쿠폰 개수</th>
                    <td className=" border px-8 py-2 text-sm ">{`${items.downloadLimit} 개`}</td>
                  </tr>
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">쿠폰종류</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm">
                      {items.visit ? `${items.visitName}` : ""}
                      {items.min !== 0 ? `${items.min}원 이상 구매시 ` : ""}
                      {items.feeFree ? ` 무료배송` : ""}
                      {items.cashDiscount !== 0 ? ` ${items.cashDiscount} 원 할인` : ""}
                      {items.percentDiscount !== 0 ? ` ${items.cashDiscount} % 할인` : ""}
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-lg leading-6 font-medium text-black">▶︎ 쿠폰 상태</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">승인</th>
                    <td className="border px-8 py-2 text-sm ">{`${items.adminActive ? "승인" : "미승인"}`}</td>
                  </tr>
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">쿠폰상태</th>
                    <td className="border px-8 py-2 text-sm ">{`${items.storeActive ? "활성" : "비활성"}`}</td>
                  </tr>
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">쿠폰 다운로드 수</th>
                    <td className="border px-8 py-2 text-sm ">{`${items.downloadCount}`}</td>
                  </tr>
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">쿠폰 사용한 횟수</th>
                    <UseCouponCount couponRecodes={items.couponRecodes} />
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const UseCouponCount = ({ couponRecodes }) => {
  const tempCouponRecodes = couponRecodes.filter((f) => f.payId !== "");

  return <td className="flex border px-8 py-2 text-sm ">{`${tempCouponRecodes.length}`}</td>;
};

DetailPage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default DetailPage;
