import React, { useState, useEffect } from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import { Button, Modal } from "react-bootstrap";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Store } from "graphql/query/select";
import { ALL_COUPONS } from "graphql/gql/select";
import { DELETE_COUPON } from "graphql/gql/delete";
import { UPDATE_COUPON } from "graphql/gql/update";
import { FormatDateShort } from "components/FormatDate";
import { SimpleNotification } from "components/SimpleNotification";

const DetailPage = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const [acceptShow, setAcceptShow] = useState(false);
  const [deleteShow, setDeleteShow] = useState(false);
  const [clickId, setClickId] = useState();

  // 승인
  const [couponDelete, { data: deleteData }] = useMutation(DELETE_COUPON, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (deleteData !== undefined) {
      refetch();
    }
  }, [deleteData]);

  // 쿠폰 삭제
  const handleDeleteButton = (id) => {
    setClickId(id);
    setDeleteShow(true);
  };

  const handleModalDelete = () => {
    setDeleteShow(false);
    couponDelete({ variables: { id: clickId } });
  };

  // 승인
  const [couponUpdate, { data: updateData }] = useMutation(UPDATE_COUPON, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      refetch();
    }
  }, [updateData]);

  // 화면에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleAcceptButtonClick = (id) => {
    setClickId(id);
    setAcceptShow(true);
  };

  // 모달에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleModalAccept = () => {
    couponUpdate({ variables: { id: clickId, adminActive: true } });

    setAcceptShow(false);
  };

  const { loading, error, data, refetch } = useQuery(ALL_COUPONS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return;
  }

  // store 정보
  let items;
  if (data) {
    items = data.allCoupons;
    // 최근 등록날짜 순으로 나타내기
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">실적 관리</li>
            <Link to="/sales/coupon" className="breadcrumb-item active mt-1" aria-current="page">
              쿠폰
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between">
                <h4 className="card-title">쿠폰</h4>
                <h4 className="card-title">{`Total : ${items.length}`}</h4>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      filterable
                      defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "상점이름",
                          id: "storeId",
                          accessor: (d) => d.storeId,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["storeId"] }),
                          filterAll: true,
                          Cell: (row) => (
                            <Link to={`/sales/coupon-detail/${row.original.id}`} className="text-blue-700 font-medium">
                              {row.value ? <StoreName storeId={row.value} /> : "내눈"}
                            </Link>
                          ),
                        },
                        {
                          Header: "쿠폰이름",
                          id: "couponName",
                          accessor: (d) => d.couponName,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["couponName"],
                            }),
                          filterAll: true,
                        },
                        {
                          Header: "쿠폰종류",
                          Filter: () => false,
                          Cell: (row) => (
                            <div>
                              <div>{`${row.original.visit ? `${row.original.visitName}` : ""}`}</div>
                              <div>{`${row.original.min !== 0 ? `${row.original.min}원 이상 구매시` : ""}`}</div>
                              <div>{`${row.original.feeFree ? "무료배송" : ""}`}</div>
                              <div>{`${row.original.cashDiscount !== 0 ? `${row.original.cashDiscount}원 할인` : ""}`}</div>
                              <div>{`${row.original.percentDiscount !== 0 ? `${row.original.percentDiscount} % 할인` : ""}`}</div>
                            </div>
                          ),
                        },
                        {
                          Header: "쿠폰기간",
                          Filter: () => false,
                          Cell: (row) => <div>{`${row.original.startAt} ~ ${row.original.endAt}`}</div>,
                        },

                        {
                          Header: "등록날짜",
                          accessor: "createAt",
                          Filter: () => false,
                          Cell: (row) => <div>{`${FormatDateShort(new Date(parseInt(row.value, 10)))}`}</div>,
                        },
                        {
                          Header: "개수",
                          id: "downloadLimit",
                          accessor: (d) => d.downloadLimit,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["downloadLimit"],
                            }),
                          filterAll: true,
                        },
                        {
                          Header: "승인",
                          id: "",
                          Filter: () => false,
                          Cell: (row) => (
                            <>
                              {!row.original.adminActive ? (
                                <div>
                                  <button
                                    onClick={(e) => handleAcceptButtonClick(row.original.id)}
                                    type="button"
                                    className="btn btn-outline-success btn-sm mr-1"
                                  >
                                    승인
                                  </button>
                                </div>
                              ) : (
                                <div>승인</div>
                              )}
                            </>
                          ),
                        },
                        {
                          Header: "",
                          accessor: "edit",
                          Filter: () => false,
                          Cell: (row) => (
                            <div>
                              <Link className="ti-pencil-alt" to={`/sales/coupon-edit/${row.original.id}`}>
                                수정하기
                              </Link>
                            </div>
                          ),
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
              {/* 승인 Modal Start */}
              <Modal size="sm" show={acceptShow} onHide={() => setAcceptShow(false)} aria-labelledby="example-modal-sizes-title-sm">
                <Modal.Header closeButton>
                  <Modal.Title>쿠폰 등록 요청 승인</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <p>요청을 승인하시겠습니까?</p>
                </Modal.Body>

                <Modal.Footer className="flex-wrap">
                  <Button variant="success btn-sm m-2" onClick={handleModalAccept}>
                    승인
                  </Button>
                  <Button variant="light btn-sm m-2" onClick={() => setAcceptShow(false)}>
                    취소
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Modal Ends */}
              {/* 승인 Modal Start */}
              <Modal size="sm" show={deleteShow} onHide={() => setDeleteShow(false)} aria-labelledby="example-modal-sizes-title-sm">
                <Modal.Header closeButton>
                  <Modal.Title>쿠폰 삭제</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <p>쿠픈을 삭제하시겠습니까?</p>
                </Modal.Body>

                <Modal.Footer className="flex-wrap">
                  <Button variant="success btn-sm m-2" onClick={handleModalDelete}>
                    삭제
                  </Button>
                  <Button variant="light btn-sm m-2" onClick={() => setDeleteShow(false)}>
                    취소
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Modal Ends */}
            </div>
          </div>
        </div>
      </div>
      <div className="flex justify-end m-4">
        <Link className="btn btn-warning" to="/sales/coupon-create">
          등록하기
        </Link>
      </div>
    </div>
  );
};

const StoreName = ({ storeId }) => {
  const { result } = Store(storeId);

  if (!result) return <div />;

  return <div>{result.name}</div>;
};

export default DetailPage;
