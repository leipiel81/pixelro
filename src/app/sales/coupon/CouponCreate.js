/* eslint-disable no-shadow */
/* eslint-disable react/self-closing-comp */
/* eslint-disable no-lone-blocks */
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { observer } from "mobx-react-lite";
import useSettings from "stores/settings";
import { FormatDate } from "components/FormatDate";
import { useMutation } from "@apollo/react-hooks";
import { CREATE_COUPON } from "graphql/gql/insert";
import DatePicker from "react-datepicker";
import { SimpleNotification } from "components/SimpleNotification";

const CreatePage = observer(() => {
  const settings = useSettings();
  const history = useHistory();

  if (!settings.isLogin) {
    history.push("/signin");
  }

  return (
    <>
      <TopBar />
      <CreateForm />
    </>
  );
});

const TopBar = () => {
  return (
    <div>
      <div className="shop-top-bar mb-35">
        <div className="select-shopping-wrap">
          <div className="shop-select">
            <h3 className="cart-page-title mb-0">쿠폰 등록</h3>
          </div>
        </div>
      </div>
    </div>
  );
};

const CreateForm = (props) => {
  const history = useHistory();
  const goBack = () => {
    history.push("/sales/coupon");
  };

  // -- 데이터 값 저장 --
  const [couponType, setCouponType] = useState("");
  const [startAt, setStartAt] = useState();
  // 주소
  const [endAt, setEndAt] = useState();
  const [min, setMin] = useState(0);
  const [feeFree, setFeeFree] = useState(false);
  const [cashDiscount, setCashDiscount] = useState(0);
  const [percentDiscount, setPercentDiscount] = useState(0);
  const [downloadLimit, setDownloadLimit] = useState(0);
  const [visit, setVisit] = useState(false);
  const [visitName, setVisitName] = useState();
  const [couponName, setCouponName] = useState();

  const [couponCreate, { data: createData }] = useMutation(CREATE_COUPON, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });
  useEffect(() => {
    if (createData !== undefined) {
      goBack();
    }
  }, [createData]);

  const handleCreate = () => {
    if (!startAt || !endAt) {
      SimpleNotification({
        title: "",
        message: "쿠폰 사용 기간을 설정해주세요.",
      });
      return;
    }

    if (!couponName) {
      SimpleNotification({
        title: "",
        message: "쿠폰 이름을 설정해주세요.",
      });
      return;
    }

    // endAt 변경
    const tempEndAt = endAt;
    tempEndAt.setHours(23, 59, 59, 999);
    couponCreate({
      variables: {
        type: "pixelro",
        startAt: `${FormatDate(startAt)}`,
        endAt: `${FormatDate(tempEndAt)}`,
        adminActive: true,
        storeActive: true,
        min: Number(min),
        feeFree: Boolean(feeFree),
        cashDiscount: Number(cashDiscount),
        percentDiscount: Number(percentDiscount),
        downloadLimit: Number(downloadLimit),
        visit: Boolean(visit),
        visitName: `${visitName && visitName}`,
        couponName,
      },
    });
  };

  const handleCouponTypeChange = (value) => {
    setCouponType(value);
    setMin(0);
    setFeeFree(false);
    setCashDiscount(0);
    setPercentDiscount(0);
    setVisit(false);
    setVisitName();

    if (value === "4") {
      setFeeFree(true);
    }
    if (value === "5") {
      setVisit(true);
    }
  };

  return (
    <>
      <div>
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-lg leading-6 font-medium text-black">▶︎ 기간 설정</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">시작일</th>
                    <td className="border px-8 py-2 text-sm w-4/12">
                      <DatePicker className="form-control" selected={startAt} onChange={(e) => setStartAt(e)} />
                    </td>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">종료일</th>
                    <td className="border px-8 py-2 text-sm w-4/12">
                      <DatePicker className="form-control" selected={endAt} onChange={(e) => setEndAt(e)} />
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* 쿠폰 이름 설정 */}
      <div>
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-lg leading-6 font-medium text-black">▶︎ 쿠폰 이름 설정</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">쿠폰 이름</th>
                    <td className="border px-8 py-2 text-sm ">
                      <input
                        type="text"
                        className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 pl-2"
                        value={couponName}
                        onChange={(e) => setCouponName(e.target.value)}
                      />
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* 쿠폰 개수 설정 */}
      <div>
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-lg leading-6 font-medium text-black">▶︎ 쿠폰 개수 설정</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">쿠폰 개수</th>
                    <td className="border px-8 py-2 text-sm ">
                      <div className="w-1/6">
                        <input
                          type="number"
                          className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 pl-2"
                          value={downloadLimit}
                          onChange={(e) => setDownloadLimit(e.target.value)}
                        />
                      </div>
                      <div className="w-1/6 flex items-center ml-2">개</div>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-lg leading-6 font-medium text-black">▶︎ 쿠폰 종류 선택</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">쿠폰종류</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm">
                      <select className="form-control" value={couponType} onChange={(e) => handleCouponTypeChange(e.target.value)}>
                        <option selected value="">
                          쿠폰종류선택
                        </option>
                        <option value="1">00원 할인</option>
                        <option value="2">00% 할인</option>
                        <option value="3">00원 이상 00원 할인</option>
                        <option value="4">00원 이상 무료배송</option>
                      </select>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      {couponType && couponType !== "" ? (
        <>
          <div>
            <div className="row">
              <div className="col-md-12">
                <div className="mt-6 border-gray-200">
                  <div>
                    <h3 className="text-lg leading-6 font-medium text-black">▶︎ 쿠폰 등록</h3>
                  </div>
                  <div className="mt-6 sm:mt-5">
                    <table className="shadow-sm bg-white w-full">
                      {couponType === "3" || couponType === "4" ? (
                        <tr>
                          <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">조건금액</th>
                          <td colSpan="3" className=" flex border px-8 py-2 text-sm">
                            <div className="w-1/6">
                              <input
                                type="number"
                                className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 pl-2"
                                value={min}
                                onChange={(e) => setMin(e.target.value)}
                              />
                            </div>
                            <div className="w-2/6 flex items-center ml-2">{`원 이상 구매  ${couponType === "4" ? "무료배송" : ""}`}</div>
                          </td>
                        </tr>
                      ) : (
                        ""
                      )}
                      {couponType === "1" || couponType === "3" ? (
                        <tr>
                          <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">할인금액</th>
                          <td colSpan="3" className=" flex border px-8 py-2 text-sm">
                            <div className="w-1/6">
                              <input
                                type="number"
                                className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 pl-2"
                                value={cashDiscount}
                                onChange={(e) => setCashDiscount(e.target.value)}
                              />
                            </div>
                            <div className="w-1/6 flex items-center ml-2">원 할인</div>
                          </td>
                        </tr>
                      ) : (
                        ""
                      )}

                      {couponType === "2" ? (
                        <tr>
                          <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">할인 %</th>

                          <td colSpan="3" className=" flex border px-8 py-2 text-sm">
                            <div className="w-1/6">
                              <input
                                type="number"
                                className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 pl-2"
                                value={percentDiscount}
                                onChange={(e) => setPercentDiscount(e.target.value)}
                              />
                            </div>
                            <div className="w-1/6 flex items-center ml-2"> % 할인</div>
                          </td>
                        </tr>
                      ) : (
                        ""
                      )}

                      {couponType === "5" ? (
                        <tr>
                          <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">방문쿠폰 명칭</th>

                          <td colSpan="3" className="border px-8 py-2 text-sm">
                            <input
                              type="text"
                              className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 pl-2"
                              value={visitName}
                              onChange={(e) => setVisitName(e.target.value)}
                            />
                          </td>
                        </tr>
                      ) : (
                        ""
                      )}
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 grid-margin mt-3">
            <div className="row justify-end mr-1">
              <button type="button" className="btn btn-primary mr-2" onClick={handleCreate}>
                등록
              </button>
              <button className="btn btn-light" onClick={goBack}>
                취소
              </button>
            </div>
          </div>
        </>
      ) : (
        <div className="col-12 grid-margin mt-3">
          <div className="row justify-end mr-1">
            <button className="btn btn-light" onClick={goBack}>
              취소
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default CreatePage;
