import React, { useState, useEffect } from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import { Button, Modal } from "react-bootstrap";
import { ALL_PIXELROS } from "graphql/gql/select";
import { DELETE_PIXELRO } from "graphql/gql/delete";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { FormatDateShort } from "components/FormatDate";
import { PhoneFormatter } from "components/PhoneFormatter";
import { SimpleNotification } from "components/SimpleNotification";

const ReactContentPage = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const [clickId, setClickId] = useState();
  const [mdShow, setMdShow] = useState(false);

  const [pixelroDelete, { data: deleteData }] = useMutation(DELETE_PIXELRO, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (deleteData !== undefined) {
      refetch();
    }
  }, [deleteData]);

  //   상품정보
  const { loading, error, data, refetch } = useQuery(ALL_PIXELROS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    const resultData = data.allPixelros;
    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });
    items = resultData;
  }

  if (!items) return <div />;

  const handlePixelroDelete = (id) => {
    setClickId(id);
    setMdShow(true);
  };
  const handlePixelroDeleteModal = () => {
    setMdShow(false);
    pixelroDelete({ variables: { id: clickId } });
  };
  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">관리자</li>
            <Link to="/pixelro/pixelro-table" className="breadcrumb-item active mt-1" aria-current="page">
              관리자 목록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between card-title-wrap">
                <h4 className="card-title">관리자 목록</h4>
                <span className="card-sub">{`Total : ${items.length}`}</span>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      className="card-wrap"
                      filterable
                      defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "이메일",
                          id: "email",
                          accessor: (d) => d.email,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["email"] }),
                          filterAll: true,
                          Cell: (row) => row.value,
                        },
                        {
                          Header: "이름",
                          id: "name",
                          accessor: (d) => d.name,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["name"],
                            }),
                          filterAll: true,
                          Cell: (row) => row.value,
                        },
                        {
                          Header: "전화번호",
                          id: "tel",
                          accessor: (d) => d.tel,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["tel"],
                            }),
                          filterAll: true,
                          Cell: (row) => PhoneFormatter(row.value),
                        },

                        {
                          Header: "등록날짜",
                          accessor: "createAt",
                          Filter: () => false,
                          Cell: (row) => <div>{`${FormatDateShort(new Date(parseInt(row.value, 10)))}`}</div>,
                        },
                        {
                          Header: "",
                          accessor: "edit",
                          Filter: () => false,
                          Cell: (row) => (
                            <div className="flex">
                              <div>
                                {settings.email === "pixelro" || settings.email === row.original.email ? (
                                  <div>
                                    <Link className="ti-pencil-alt" to={`/pixelro/pixelro-edit/${row.original.id}`}>
                                      수정
                                    </Link>
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                              <div>
                                {settings.email === "pixelro" && row.original.email !== "pixelro" ? (
                                  <div>
                                    <button className="ti-pencil-alt ml-4" onClick={(e) => handlePixelroDelete(row.original.id)}>
                                      삭제하기
                                    </button>
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                              {/* Modal Starts  */}
                              <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
                                <Modal.Header closeButton>
                                  <Modal.Title>관리자 삭제</Modal.Title>
                                </Modal.Header>

                                <Modal.Body>
                                  <p>해당 관리자를 삭제하시겠습니까?</p>
                                </Modal.Body>

                                <Modal.Footer className="flex-wrap">
                                  <Button variant="btn btn-primary m-2" onClick={handlePixelroDeleteModal}>
                                    확인
                                  </Button>
                                  <Button variant="btn btn-light m-2" onClick={() => setMdShow(false)}>
                                    취소
                                  </Button>
                                </Modal.Footer>
                              </Modal>
                              {/* Modal Ends */}
                            </div>
                          ),
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {settings.email === "pixelro" ? (
        <div className="flex justify-end btnbar">
          <Link className="btn btn-primary" to="/pixelro/pixelro-create">
            등록하기
          </Link>
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

export default ReactContentPage;
