/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Form } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import { Pixelro } from "graphql/query/select";
import { UPDATE_PIXELRO } from "graphql/gql/update";

const EditPage = ({ match }) => {
  const { id } = match.params;
  const { result } = Pixelro(id);
  const items = result;

  return <div>{items && <EditForm items={items} />}</div>;
};

const EditForm = ({ items }) => {
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  // -- 데이터 값 저장 --
  const { id, email } = items;
  const [name, setName] = useState(items.name);
  const [tel, setTel] = useState(items.tel);
  const [password, setPassword] = useState();
  const [newPassword, setNewPassword] = useState();

  // 관리자 정보 업데이트
  const [pixelroUpdate, { data }] = useMutation(UPDATE_PIXELRO, {
    onError(err) {
      console.log("updateAdmin: err=", err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });
  useEffect(() => {
    if (data !== undefined) {
      goBack();
    }
  }, [data]);

  const handleUpdate = () => {
    if (!name || !tel) {
      SimpleNotification({
        title: "",
        message: "필수항목을 모두 입력해주세요.",
      });
      return;
    }

    if (password && newPassword) {
      if (password !== newPassword) {
        SimpleNotification({
          title: "",
          message: "비밀번호가 일치하지 않습니다.",
        });
        return;
      }
    }

    pixelroUpdate({
      variables: {
        id,
        name,
        tel,
        email,
        password: newPassword || "",
      },
    });
  };

  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">관리자</li>
            <Link to="/pixelro/pixelro-table" className="breadcrumb-item active mt-1" aria-current="page">
              관리자 목록
            </Link>
            <Link to={`/pixelro/pixelro-edit/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              관리자 정보 수정
            </Link>
          </ol>
        </nav>
      </div>
      {/* 관리자 정보 */}
      <div className="row">
        <div className="col-md-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between card-title-wrap">
                <h4 className="card-title">관리자 정보 수정</h4>
              </div>
              <div className="mt-6 card-wrap">
                <table className="app-table bg-white w-full">
                  <tr>
                    <td className="th">이메일</td>
                    <td colSpan="3">
                      {email}
                    </td>
                  </tr>
                  <tr>
                    <td className="th">비밀번호</td>
                    <td colSpan="3">
                      <Form.Control type="text" value={password} onChange={(e) => setPassword(e.target.value)} />
                    </td>
                  </tr>
                  <tr>
                    <td className="th">비밀번호 확인</td>
                    <td colSpan="3">
                      <Form.Control type="text" value={newPassword} onChange={(e) => setNewPassword(e.target.value)} />
                    </td>
                  </tr>
                  <tr>
                    <td className="th">이름</td>
                    <td colSpan="3">
                      <Form.Control type="text" value={name} onChange={(e) => setName(e.target.value)} />
                    </td>
                  </tr>
                  <tr>
                    <td className="th">전화번호</td>
                    <td colSpan="3">
                      <Form.Control type="text" value={tel} onChange={(e) => setTel(e.target.value)} />
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 btnbar">
        <div className="row foot-edit">
          <button className="btn btn-warning" onClick={goBack}>
            취소하기
          </button>
          <button type="button" className="btn btn-primary" onClick={handleUpdate}>
            저장하기
          </button>
        </div>
      </div>
    </>
  );
};
EditPage.prototype = {
  id: PropTypes.string.isRequired,
};

export default EditPage;
