/* eslint-disable no-await-in-loop */
/* eslint-disable no-inner-declarations */
/* eslint-disable no-use-before-define */
import React, { useState, useEffect } from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Store } from "graphql/query/select";
import { ALL_ORDERS } from "graphql/gql/select";
import { UPDATE_ORDER, UPDATE_PRODUCT_SALE_COUNT } from "graphql/gql/update";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { FormatDate } from "components/FormatDate";
import { SimpleNotification } from "components/SimpleNotification";

const DetailPage = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const { loading, error, data, refetch } = useQuery(ALL_ORDERS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return;
  }

  // store 정보
  let items;
  if (data && data.allOrders) {
    let tempItems = data.allOrders.filter((f) => f.status === "배송완료");

    const tempStartDate = new Date();
    tempStartDate.setDate(tempStartDate.getDate() - 7);

    tempItems = tempItems.filter((f) => new Date(f.process4) <= tempStartDate);

    // 최근 등록날짜 순으로 나타내기
    tempItems.sort((a, b) => {
      return new Date(a.process4) - new Date(b.process4);
    });

    items = tempItems;
  }

  if (!items) return <div />;
  return <Content items={items} refetch={refetch} />;
};

const Content = ({ items, refetch }) => {
  const [updateCount, setUpdateCount] = useState(0);
  const [clickId, setClickId] = useState();

  const [orderAllUpdate, { data: updateAllData }] = useMutation(UPDATE_ORDER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateAllData !== undefined) {
      if (updateCount === items.length - 1) {
        for (let i = 0; i < items.length; i += 1) {
          async function productCountUpdate() {
            await countUpdate({ variables: { id: items[i].id } });
          }
          productCountUpdate();
        }
      }
      setUpdateCount(updateCount + 1);
    }
  }, [updateAllData]);

  const [countUpdate, { data: countUpdateData }] = useMutation(UPDATE_PRODUCT_SALE_COUNT, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (countUpdateData !== undefined) {
      refetch();
    }
  }, [countUpdateData]);

  // 승인
  const [orderUpdate, { data: updateData }] = useMutation(UPDATE_ORDER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      countUpdate({ variables: { id: clickId } });
    }
  }, [updateData]);

  // 화면에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleConfirmCheckButton = (id) => {
    setClickId(id);
    const date = FormatDate(new Date());
    orderUpdate({ variables: { id, fixAt: `${date}`, status: "구매확정" } });
  };

  const handleAllConfirmCheck = async () => {
    const date = new Date();
    for (let i = 0; i < items.length; i += 1) {
      await orderAllUpdate({
        variables: { id: items[i].id, fixAt: `${date}`, status: "구매확정" },
      });
    }
  };

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">주문 관리</li>
            <Link to="/orders/delivery-check" className="breadcrumb-item active mt-1" aria-current="page">
              구매확정처리
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between">
                <div>
                  <h4 className="card-title">구매확정처리</h4>
                </div>
                <div className="flex items-center -mt-4">
                  <h4 className="card-title items-center mt-3 mr-4">{`Total : ${items.length}`}</h4>
                  <button type="button" className="btn btn-outline-danger btn-sm mr-1" onClick={handleAllConfirmCheck}>
                    전체 구매확정
                  </button>
                </div>
              </div>

              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "상품이름",
                          id: "product.name",
                          accessor: (d) => d.product.name,
                          Filter: () => false,
                        },
                        {
                          Header: "구매자명",
                          id: "user",
                          accessor: (d) => d.user.name,
                          Filter: () => false,
                        },

                        {
                          Header: "상점이름",
                          id: "storeId",
                          accessor: (d) => <StoreName storeId={d.storeId} />,
                          Filter: () => false,
                          Cell: (row) => <div>{row.value}</div>,
                        },

                        {
                          Header: "배송상태",
                          id: "status",
                          accessor: (d) => d.status,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["status"],
                            }),
                          filterAll: true,
                        },
                        {
                          Header: "배송완료날짜",
                          id: "process4",
                          accessor: (d) => d.process4,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["process4"],
                            }),
                          filterAll: true,
                        },

                        {
                          Header: "택배사",
                          id: "delivery",
                          accessor: (d) => d.delivery,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["delivery"],
                            }),
                          filterAll: true,
                        },
                        {
                          Header: "운송장번호",
                          id: "trackingNumber",
                          accessor: (d) => d.trackingNumber,
                          Cell: (row) => (
                            <CopyToClipboard
                              text={row.value}
                              onCopy={() =>
                                window.open(
                                  "https://search.naver.com/search.naver?sm=mtp_hty.top&where=m&query=%ED%83%9D%EB%B0%B0%EC%A1%B0%ED%9A%8C"
                                )
                              }
                            >
                              <div className="flex justify-between border border-gray-300 rounded-md p-1">
                                <div className="m-1 text-sm">
                                  <div>{row.value}</div>
                                </div>
                                <div className="flex items-center">
                                  <i className="ti-arrow-right" />
                                </div>
                              </div>
                            </CopyToClipboard>
                          ),
                        },
                        {
                          Header: "",
                          id: "",
                          Filter: () => false,
                          Cell: (row) => (
                            <div>
                              <button
                                onClick={(e) => handleConfirmCheckButton(row.original.id)}
                                type="button"
                                className="btn btn-outline-danger btn-sm mr-1"
                              >
                                구매확정
                              </button>
                            </div>
                          ),
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const StoreName = ({ storeId }) => {
  const { result } = Store(storeId);

  if (!result) return <div />;

  return <div>{result.name}</div>;
};

export default DetailPage;
