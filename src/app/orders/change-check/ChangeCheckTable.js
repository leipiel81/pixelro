import React, { useState, useEffect } from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import { Button, Modal } from "react-bootstrap";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Store } from "graphql/query/select";
import { ALL_ORDERS } from "graphql/gql/select";
import { UPDATE_ORDER, UPDATE_PRODUCT_SALE_COUNT, SEND_USER_FCM } from "graphql/gql/update";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { FormatDate } from "components/FormatDate";
import { SimpleNotification } from "components/SimpleNotification";

const DetailPage = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const [acceptShow, setAcceptShow] = useState(false);
  const [clickId, setClickId] = useState();
  const [userId, setUserId] = useState();
  const [payId, setPayId] = useState();

  // 알람
  const [sendUserFcm, { data: sendUserFcmData }] = useMutation(SEND_USER_FCM, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (sendUserFcmData !== undefined) {
      refetch();
    }
  }, [sendUserFcmData]);

  const [countUpdate, { data: countUpdateData }] = useMutation(UPDATE_PRODUCT_SALE_COUNT, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (countUpdateData !== undefined) {
      sendUserFcm({
        variables: {
          id: userId,
          title: `[nenoons] 교환완료`,
          message: `교환상품의 배송이 완료되었습니다.`,
          link: `https://www.nenoons.com/app-order-detail/${payId}`,
          action: "",
          value: "",
        },
      });
    }
  }, [countUpdateData]);

  // 승인
  const [orderUpdate, { data: updateData }] = useMutation(UPDATE_ORDER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      countUpdate({ variables: { id: clickId } });
    }
  }, [updateData]);

  // 화면에 있는 승인 버튼 클릭시 실행 함수
  const handleDeliveryCheckButtonClick = (id, id2, id3) => {
    setClickId(id);
    setUserId(id2);
    setPayId(id3);
    setAcceptShow(true);
  };

  // 모달에 있는 승인 버튼 클릭시 실행 함수
  const handleModalAccept = () => {
    const date = FormatDate(new Date());
    orderUpdate({
      variables: { id: clickId, fixAt: `${date}`, status: "교환완료", changeOkAt: `${date}` },
    });

    setAcceptShow(false);
  };

  const { loading, error, data, refetch } = useQuery(ALL_ORDERS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return;
  }

  // store 정보
  let items;
  if (data && data.allOrders) {
    items = data.allOrders.filter((f) => f.changeCheckAt !== "").filter((f2) => f2.status === "배송중");

    // 최근 등록날짜 순으로 나타내기
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">주문 관리</li>
            <Link to="/orders/delivery-check" className="breadcrumb-item active mt-1" aria-current="page">
              교환완료처리
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between">
                <h4 className="card-title">교환완료처리</h4>
                <h4 className="card-title">{`Total : ${items.length}`}</h4>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "상품이름",
                          id: "product.name",
                          accessor: (d) => d.product.name,
                          Filter: () => false,
                        },
                        {
                          Header: "구매자명",
                          id: "user",
                          accessor: (d) => d.user.name,
                          Filter: () => false,
                        },

                        {
                          Header: "상점이름",
                          id: "storeId",
                          accessor: (d) => d.storeId,
                          Filter: () => false,
                          Cell: (row) => <StoreName storeId={row.value} />,
                        },

                        {
                          Header: "배송상태",
                          id: "status",
                          accessor: (d) => d.status,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["status"],
                            }),
                          filterAll: true,
                        },

                        {
                          Header: "택배사",
                          id: "changeDelivery",
                          accessor: (d) => d.changeDelivery,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["changeDelivery"],
                            }),
                          filterAll: true,
                        },
                        {
                          Header: "운송장번호",
                          id: "changeTrackingNumber",
                          accessor: (d) => d.changeTrackingNumber,
                          Cell: (row) => (
                            <CopyToClipboard
                              text={row.value}
                              onCopy={() =>
                                window.open(
                                  "https://search.naver.com/search.naver?sm=mtp_hty.top&where=m&query=%ED%83%9D%EB%B0%B0%EC%A1%B0%ED%9A%8C"
                                )
                              }
                            >
                              <div className="flex justify-between border border-gray-300 rounded-md p-1">
                                <div className="m-1 text-sm">
                                  <div>{row.value}</div>
                                </div>
                                <div className="flex items-center">
                                  <i className="ti-arrow-right" />
                                </div>
                              </div>
                            </CopyToClipboard>
                          ),
                        },
                        {
                          Header: "",
                          id: "",
                          Filter: () => false,
                          Cell: (row) => (
                            <div>
                              <button
                                onClick={(e) => handleDeliveryCheckButtonClick(row.original.id, row.original.user.id, row.original.payId)}
                                type="button"
                                className="btn btn-outline-success btn-sm mr-1"
                              >
                                교환완료
                              </button>
                            </div>
                          ),
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
              {/* 승인 Modal Start */}
              <Modal size="sm" show={acceptShow} onHide={() => setAcceptShow(false)} aria-labelledby="example-modal-sizes-title-sm">
                <Modal.Header closeButton>
                  <Modal.Title>교환완료처리</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <p>배송이 완료되었는지 확인하셨나요?</p>
                </Modal.Body>

                <Modal.Footer className="flex-wrap">
                  <Button variant="success btn-sm m-2" onClick={handleModalAccept}>
                    확인
                  </Button>
                  <Button variant="light btn-sm m-2" onClick={() => setAcceptShow(false)}>
                    취소
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Modal Ends */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const StoreName = ({ storeId }) => {
  const { result } = Store(storeId);

  if (!result) return <div />;

  return <div>{result.name}</div>;
};

export default DetailPage;
