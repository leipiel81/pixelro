/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import PropTypes from "prop-types";
import { AllAppQnas } from "graphql/query/select";
import { FormatDate } from "components/FormatDate";
import Pagination from "components/Pagination";

const Tables = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const { result } = AllAppQnas();

  let items;
  if (result) {
    items = result;

    // 최근 등록날짜 순으로 나타내기
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  const [pageOfItems, setPageOfItems] = useState([]);
  const [page, setPage] = useState(1);

  function onChangePage(pageOfItems, pageNumber) {
    setPageOfItems(pageOfItems);
    setPage(pageNumber);
    // 페이지 이동시 맨 위로 스크롤 이동
    window.scrollTo(0, 0);
  }

  return (
    <>
      <div>
        <TableHeader />
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card-body">
                <div className="flex justify-between card-title-wrap">
                  <h4 className="card-title">의견보내기 목록</h4>
                </div>
                <div className="card-wrap">     
                  <div className="overflow-hidden">
                    <TableForm pageOfItems={pageOfItems} />
                    <Page items={items} page={page} onChangePage={onChangePage} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

// /* breadcrumb */
const TableHeader = () => {
  return (
    <div>
      <nav aria-label="breadcrumb" role="navigation">
        <ol className="breadcrumb">
          <li className="breadcrumb-item active">고객센터</li>
          <Link to="/service-center/app-user-qna-table" className="breadcrumb-item active mt-1" aria-current="page">
            의견보내기 목록
          </Link>
        </ol>
      </nav>
    </div>
  );
};

const TableForm = ({ pageOfItems }) => {
  return (
    <table className="app-table w-full">
      <thead>
        <tr>
          <td className="th">
            구분
          </td>
          <td className="th">
            제목
          </td>
          <td className="th">
            등록날짜
          </td>
          <td className="th">
            상태
          </td>
          <td className="th" />
        </tr>
      </thead>
      {pageOfItems && (
        <tbody className="bg-white">
          {pageOfItems.map((m) => (
            <tr>
              <td>
                {m.category}
              </td>
              <td>{m.title}</td>
              <td>
                {FormatDate(new Date(parseInt(m.createAt, 10)))}
              </td>
              <td>
                답변완료
              </td>
              <td>
                <Link className="ti-pencil-alt" to={`/service-center/app-user-qna-detail/${m.id}`}>
                  상세보기
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      )}
    </table>
  );
};

Tables.propTypes = {
  pageOfItems: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
};

const Page = (props) => {
  const PAGE_SIZE = 10;
  const { items, page, onChangePage } = props;
  return (
    <div className="bg-white flex items-center justify-between board-pagination">
      <div className="hidden sm:block">
        <p className="text-sm leading-5 text-gray-700">
          <span className="font-medium mx-1">{(page - 1) * PAGE_SIZE + 1}</span>
          {" ~  "}
          <span className="font-medium mx-1">{items && items.length < page * PAGE_SIZE ? items.length : page * PAGE_SIZE}</span>
          {" / 총 "}
          <span className="font-medium mx-1">{items && items.length}</span>
          {" 건"}
        </p>
      </div>
      <div className="flex-1 flex justify-between sm:justify-end">{items && <Pagination items={items} onChangePage={onChangePage} />}</div>
    </div>
  );
};

Page.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  page: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
};

export default Tables;
