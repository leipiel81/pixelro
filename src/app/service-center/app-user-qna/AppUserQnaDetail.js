import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { AppQna } from "graphql/query/select";
import { Link, useHistory } from "react-router-dom";
import { FormatDate } from "components/FormatDate";
import { Button, Modal } from "react-bootstrap";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_APP_QNA } from "graphql/gql/delete";
import { UPDATE_APP_QNA, SEND_USER_FCM } from "graphql/gql/update";
import { SimpleNotification } from "components/SimpleNotification";

const DetailPage = ({ match }) => {
  const { id } = match.params;

  let items;
  if (id !== undefined && id !== "undefined") {
    const { result } = AppQna(id);
    items = result;
  }

  return (
    <>
      {items && (
        <div>
          {/* breadcrumb */}
          <div>
            <nav aria-label="breadcrumb" role="navigation">
              <ol className="breadcrumb">
                <li className="breadcrumb-item active">고객센터</li>
                <Link to="/service-center/app-user-qna-table" className="breadcrumb-item active mt-1" aria-current="page">
                  의견보내기
                </Link>
                <Link to={`/service-center/app-user-qna-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
                  의견내용
                </Link>
              </ol>
            </nav>
          </div>
          <div className="row">
            <InfoForm items={items} />
          </div>
        </div>
      )}
    </>
  );
};

const InfoForm = ({ items }) => {
  const [comment, setComment] = useState(items.comment);

  // 답변 수정
  const { id } = items;

  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  // 알람
  const [sendUserFcm, { data: sendUserFcmData }] = useMutation(SEND_USER_FCM, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (sendUserFcmData !== undefined) {
      goBack();
    }
  }, [sendUserFcmData]);

  //  3. 데이터 저장
  const [qnaUpdate, { data: updateData }] = useMutation(UPDATE_APP_QNA, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      sendUserFcm({
        variables: {
          id: items.user.id,
          title: `[nenoons] 앱 문의 답변`,
          message: `앱 문의 답변이 작성되었습니다.`,
          link: `https://www.nenoons.com/app-qna`,
          action: "",
          value: "",
        },
      });
    }
  }, [updateData]);

  const handleUpdate = () => {
    qnaUpdate({
      variables: {
        id,
        comment,
        reply: `답변완료`,
      },
    });
  };

  const [mdShow, setMdShow] = useState(false);

  const [noticeDelete] = useMutation(DELETE_APP_QNA, {
    update: (proxy, result) => {
      console.log(result);
      goBack();
    },
    variables: {
      id,
    },
  });

  const handleDelete = () => {
    setMdShow(false);
    noticeDelete();
  };

  return (
    <>
      <div className="col-12">
        <div className="card">
          <div className="card-body">
            <form className="form-sample">
              <div className="mt-2 border-gray-200">
                <div className="lex justify-between card-title-wrap">
                  <h4 className="card-title">의견 내용</h4>
                </div>
                <div className="card-wrap">
                  <table className="app-table bg-white w-full">
                    <tr>
                      <td className="th">문의 유형</td>
                      <td colSpan="3">
                        {items.category}
                      </td>
                    </tr>
                    <tr className="m-0 p-0">
                      <td className="th w-2/12">등록자</td>
                      <td className="w-4/12">{items.user.name}</td>
                      <td className="th w-2/12">등록 일시</td>
                      <td className="w-4/12">{FormatDate(new Date(parseInt(items.createAt, 10)))}</td>
                    </tr>
                    <tr>
                      <td className="th">제목</td>
                      <td colSpan="3">
                        {items.title}
                      </td>
                    </tr>
                    <tr>
                      <td className="th">문의 내용</td>
                      <td colSpan="3">
                        {items.content}
                      </td>
                    </tr>
                    <tr>
                      <td className="th">첨부파일</td>
                      <td colSpan="3">
                        <div className="attach-wrap">
                          <Link to='#' className="attach-item" aria-current="page">
                            img_001.png
                          </Link>
                          <Link to='#' className="attach-item" aria-current="page">
                            img_001.png
                          </Link>
                          <Link to='#' className="attach-item" aria-current="page">
                            img_001.png
                          </Link>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
                {/* 답변 */}
                <div className="card-body-split">
                  <div className="flex justify-between card-title-wrap">
                    <h4 className="card-title">답변</h4>
                  </div>
                  <div className="card-wrap">
                    <table className="app-table bg-white w-full">
                      <tr>
                        <td className="th w-1/6">답변 내용</td>
                        <td colSpan="3">
                          {items.reply === "대기" ? (
                            <textarea
                              id="about"
                              rows="10"
                              className="form-control block w-full transition duration-150 ease-in-out sm:leading-5"
                              value={comment}
                              onChange={(e) => setComment(e.target.value)}
                            />
                          ) : (
                            items.comment
                          )}
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="col-12 btnbar">
        <div className="foot-edit">
          <button className="btn btn-danger" onClick={() => setMdShow(true)}>
            삭제하기
          </button>
          {items.reply === "대기" ? (
            <button className="btn btn-primary" onClick={handleUpdate}>
              답변하기
            </button>
          ) : (
            <div />
          )}
        </div>
        {/* Modal Starts  */}
        <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
          <Modal.Header closeButton>
            <Modal.Title>해당 내용을 삭제하시겠습니까?</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <p>삭제를 원하시면 삭제 버튼을 눌러주세요.</p>
          </Modal.Body>

          <Modal.Footer className="fleex-wrap">
            <Button variant="danger m-2" onClick={handleDelete}>
              삭제
            </Button>
            <Button variant="primary m-2" onClick={() => setMdShow(false)}>
              취소
            </Button>
          </Modal.Footer>
        </Modal>
        {/* Modal Ends */}
      </div>
    </>
  );
};

DetailPage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default DetailPage;
