import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Form } from "react-bootstrap";
import { observer } from "mobx-react-lite";
import useSettings from "stores/settings";
import { Editor } from "@tinymce/tinymce-react";
import { SERVICE_CENTER } from "graphql/gql/select";
import { SimpleNotification } from "components/SimpleNotification";
import { UPDATE_SERVICE_CENTER } from "graphql/gql/update";

const EditPage = ({ match }) => {
  const { id } = match.params;

  // 사용자 FAQ
  const { loading, error, data } = useQuery(SERVICE_CENTER, {
    fetchPolicy: "cache-and-network",
    variables: { id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data && data.serviceCenter) {
    items = data.serviceCenter;
  }

  if (!items) return <div />;

  return <EditForm items={items} />;
};

const EditForm = observer(({ items }) => {
  const settings = useSettings();
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  // --------------------------
  // -- 데이터 값 저장 --

  const [category, setCategory] = useState(items.category);
  const [title, setTitle] = useState(items.title);
  const { serviceCenterContent } = settings;
  const [content, setContent] = useState(serviceCenterContent);

  const { id } = items;
  const { type } = items;

  const [noticeUpdate, { data: noticeUpdateData }] = useMutation(UPDATE_SERVICE_CENTER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (noticeUpdateData !== undefined) {
      goBack();
    }
  }, [noticeUpdateData]);

  const handleUpdate = () => {
    if (!title || !content) {
      SimpleNotification({
        title: "",
        message: "내용을 입력해주세요.",
      });
      return;
    }

    noticeUpdate({
      variables: {
        id,
        type,
        category,
        title,
        content,
      },
    });
  };

  const handleEditorChange = (e) => {
    setContent(e.target.getContent());
  };

  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">고객센터</li>
            <Link to="/service-center/app-user-faq-table" className="breadcrumb-item active mt-1" aria-current="page">
              FAQ
            </Link>
            <Link to={`/service-center/app-user-faq-edit/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              FAQ 수정
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between card-title-wrap">
                <h4 className="card-title">FAQ 수정</h4>
              </div>
              <div className="mt-6">
                <div className="card-wrap">
                  <table className="app-table bg-white w-full">
                    <tr>
                      <td className="th">구분</td>
                      <td colSpan="3">
                        <select
                          className="form-control w-1/4"
                          value={category}
                          onChange={(e) => setCategory(e.target.value)}
                        >
                          <option value="회원">회원</option>
                          <option value="이용문의">이용문의</option>
                          <option value="결제">기타</option>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td className="th">제목</td>
                      <td colSpan="3">
                        <Form.Control type="text" className="form-control" value={title} onChange={(e) => setTitle(e.target.value)} />
                      </td>
                    </tr>
                  </table>
                </div>
              
                <div className="row mt-4">
                  <div className="col-md-12">
                    <div className="editor-container">
                      네이버 스마트 에디터 영역
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 btnbar">
        <div className="row foot-edit">
          <button className="btn btn-warning" onClick={goBack}>
            취소하기
          </button>
          <button type="button" className="btn btn-primary" onClick={handleUpdate}>
            저장하기
          </button>
        </div>
      </div>
    </>
  );
});
EditPage.prototype = {
  id: PropTypes.string.isRequired,
};

export default EditPage;
