import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Form } from "react-bootstrap";
import { Editor } from "@tinymce/tinymce-react";
import { SimpleNotification } from "components/SimpleNotification";
import { CREATE_SERVICE_CENTER } from "graphql/gql/insert";

const CreatePage = () => {
  const history = useHistory();

  const goBack = () => {
    history.push("/service-center/app-user-faq-table");
  };

  const [category, setCategory] = useState("회원");
  const [title, setTitle] = useState();
  const [content, setContent] = useState();

  const type = "user";

  const [noticeCreate, { data: createData }] = useMutation(CREATE_SERVICE_CENTER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (createData !== undefined) {
      goBack();
    }
  }, [createData]);

  const handleUpdate = () => {
    if (!title || !content) {
      SimpleNotification({
        title: "",
        message: "내용을 입력해주세요.",
      });
      return;
    }

    noticeCreate({
      variables: {
        type,
        category,
        title,
        content,
      },
    });
  };

  const handleEditorChange = (e) => {
    setContent(e.target.getContent());
  };
  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">고객센터</li>
            <Link
              to="/service-center/app-user-faq-table"
              className="breadcrumb-item active mt-1"
              aria-current="page"
            >
              FAQ
            </Link>
            <Link
              to="/service-center/app-user-faq-create"
              className="breadcrumb-item active mt-1"
              aria-current="page"
            >
              FAQ등록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between card-title-wrap">
                <h4 className="card-title">FAQ 수정</h4>
              </div>
              <div className="mt-6">
                <div className="card-wrap">
                  <table className="app-table bg-white w-full">
                    <tr>
                      <td className="th">구분</td>
                      <td colSpan="3">
                        <select
                          className="form-control w-1/4"
                          value={category}
                          onChange={(e) => setCategory(e.target.value)}
                        >
                          <option value="회원">회원</option>
                          <option value="이용문의">이용문의</option>
                          <option value="결제">기타</option>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td className="th">제목</td>
                      <td colSpan="3">
                        <Form.Control type="text" className="form-control" value={title} onChange={(e) => setTitle(e.target.value)} />
                      </td>
                    </tr>
                  </table>
                </div>
              
                <div className="row mt-4">
                  <div className="col-md-12">
                    <div className="editor-container">
                      네이버 스마트 에디터 영역
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 btnbar">
        <div className="row foot-edit">
          <button className="btn btn-warning" onClick={goBack}>
            취소하기
          </button>
          <button type="button" className="btn btn-primary" onClick={handleUpdate}>
            저장하기
          </button>
        </div>
      </div>
    </>
  );
};

CreatePage.prototype = {
  id: PropTypes.string.isRequired,
};

export default CreatePage;
