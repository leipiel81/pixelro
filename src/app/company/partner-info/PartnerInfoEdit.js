import React, { useState } from "react";
import PropTypes from "prop-types";
import { ServiceCenter } from "graphql/query/select";
import { Link, useHistory } from "react-router-dom";
import { Form } from "react-bootstrap";
import { FormatDate } from "components/FormatDate";
import useSettings from "stores/settings";
import { Button, Modal } from "react-bootstrap";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_SERVICE_CENTER } from "graphql/gql/delete";

const DetailPage = () => {

  const [mdShow, setMdShow] = useState(false);
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  return (
    <>
      <div>
        <div>
          <nav aria-label="breadcrumb" role="navigation">
            <ol className="breadcrumb">
              <li className="breadcrumb-item active">회사정보 관리</li>
                <Link to="/company/partner-table" className="breadcrumb-item active mt-1" aria-current="page">
                  협력사 목록
                </Link>
                <Link to="/company/partner-detail" className="breadcrumb-item active mt-1" aria-current="page">
                  협력사 상세보기
                </Link>
                <Link to="/company/partner-edit" className="breadcrumb-item active mt-1" aria-current="page">
                  협력사 수정
                </Link>
            </ol>
          </nav>
        </div>
        <InfoForm />
        <div className="col-12 btnbar">
          <div className="row foot-edit">
            <button className="btn btn-warning" onClick={goBack}>
              취소하기
            </button>
            <button type="button" className="btn btn-primary" onClick="">
              저장하기
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

const InfoForm = () => {
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="card">
          <div className="card-body">
            <div className="flex justify-between card-title-wrap">
              <h4 className="card-title">협력사 수정</h4>
            </div>
            <div className="card-wrap">
              <table className="app-table bg-white w-full">
                <tr>
                  <td className="th w-1/6">로고 이미지</td>
                  <td>
                    <div className="form-file">
                      <img src="/static/images/temp/img_expert_profile.jpg" className="profile_preview" />
                      <label className="" for="input_file">파일을 선택하세요.<i className="ti-upload"></i></label>
                      <Form.Control 
                        type="file" 
                        accept='image/jpg,impge/png,image/jpeg,image/gif' 
                        name='profile_img' 
                        onChange=""
                        id="input_file"
                      />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="th w-1/6">협력사명</td>
                  <td><Form.Control type="text" className="form-control" value="" /></td>
                </tr>
                <tr>
                  <td className="th w-1/6">주소</td>
                  <td><Form.Control type="text" className="form-control" value="" /></td>
                </tr>
                <tr>
                  <td className="th w-1/6">담당자명</td>
                  <td><Form.Control type="text" className="form-control" value="" /></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailPage;
