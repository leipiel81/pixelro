import React, { useState } from "react";
import PropTypes from "prop-types";
import { ServiceCenter } from "graphql/query/select";
import { Link, useHistory } from "react-router-dom";
import { FormatDate } from "components/FormatDate";
import useSettings from "stores/settings";
import { Button, Modal } from "react-bootstrap";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_SERVICE_CENTER } from "graphql/gql/delete";

const DetailPage = () => {

  const [mdShow, setMdShow] = useState(false);
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  return (
    <>
      <div>
        <div>
          <nav aria-label="breadcrumb" role="navigation">
            <ol className="breadcrumb">
              <li className="breadcrumb-item active">회사정보 관리</li>
              <Link to="/company/parter-table" className="breadcrumb-item active mt-1" aria-current="page">
                협력사 목록
              </Link>
            </ol>
          </nav>
        </div>
        <InfoForm />
      </div>
    </>
  );
};

const InfoForm = () => {
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="card">
          <div className="card-body">
            <div className="flex justify-between card-title-wrap">
              <h4 className="card-title">협력사 목록</h4>
              <Link className="btn btn-primary" to="/company/partner-create">
                등록하기
              </Link>
            </div>
            <div className="card-wrap">
              <div className="overflow-hidden">
                <>
                  <table className="app-table w-full">
                    <thead>
                      <tr>
                        <td className="th">
                          로고 이미지
                        </td>
                        <td className="th w-3/12">
                          업체명
                        </td>
                        <td className="th">
                          담당자
                        </td>
                        <td className="th">
                          등록날짜
                        </td>
                        <td className="th" />
                        <td className="th" />
                      </tr>
                    </thead>
                    <tbody className="bg-white">
                      <tr>
                        <td>
                          <img src='/static/images/temp/logo.png' alt="" className="partner-logo-thumb" />
                        </td>
                        <td>
                          인피니텀
                        </td>
                        <td>
                          홍길동
                        </td>
                        <td>
                          2020.00.00
                        </td>
                        <td>
                          <Link className="ti-pencil-alt" to={`/company/partner-detail`}>
                            상세보기
                          </Link>
                        </td>
                        <td>
                          <button className="ti-trash btn btn-sm btn-danger" onClick="">
                            삭제하기
                          </button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  {/* Modal Starts  */}
                 
                  {/* Modal Ends */}
                </>

                <Page />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

//다른 곳에서 쓰는 페이지 그대로 쓰면 됨
const Page = () => {
  return (
    <div className="bg-white flex items-center justify-between board-pagination">
      <div className="hidden sm:block">
        <p className="text-sm leading-5 text-gray-700">
          <span className="font-medium mx-1">1</span>
          {" ~  "}
          <span className="font-medium mx-1">10</span>
          {" / 총 "}
          <span className="font-medium mx-1">20</span>
          {" 건"}
        </p>
      </div>
      <div className="flex-1 flex justify-between sm:justify-end"></div>
    </div>
  );
};

export default DetailPage;
