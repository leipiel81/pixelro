import React, { useState } from "react";
import PropTypes from "prop-types";
import { ServiceCenter } from "graphql/query/select";
import { Link, useHistory } from "react-router-dom";
import { FormatDate } from "components/FormatDate";
import useSettings from "stores/settings";
import { Button, Modal } from "react-bootstrap";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_SERVICE_CENTER } from "graphql/gql/delete";

const DetailPage = () => {

  const [mdShow, setMdShow] = useState(false);
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  return (
    <>
      <div>
        <div>
          <nav aria-label="breadcrumb" role="navigation">
            <ol className="breadcrumb">
              <li className="breadcrumb-item active">회사정보 관리</li>
              <Link to="/company/partner-table" className="breadcrumb-item active mt-1" aria-current="page">
                협력사 목록
              </Link>
              <Link to="/company/partner-detail" className="breadcrumb-item active mt-1" aria-current="page">
                협력사 상세보기
              </Link>
            </ol>
          </nav>
        </div>
        <InfoForm />
        <div className="col-12 grid-margin mt-4">
          <div className="row justify-between">
            <button className="btn btn-danger" onClick="">
              삭제하기
            </button>
            <Link className="btn btn-primary" to={`/company/partner-edit`}>
              수정하기
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

const InfoForm = () => {
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="card">
          <div className="card-body">
            <div className="flex justify-between card-title-wrap">
              <h4 className="card-title">협력사 상세보기</h4>
            </div>
            <div className="card-wrap">
              <table className="app-table bg-white w-full">
                <tr>
                  <td className="th w-1/6">로고 이미지</td>
                  <td><img src='/static/images/temp/logo.png' alt="" className="partner-logo-detail" /></td>
                </tr>
                <tr>
                  <td className="th w-1/6">협력사명</td>
                  <td>인피니텀</td>
                </tr>
                <tr>
                  <td className="th w-1/6">주소</td>
                  <td>경기도 성남시 대왕판교로 ㅇㅇㅇ</td>
                </tr>
                <tr>
                  <td className="th w-1/6">담당자명</td>
                  <td>홍길동, 홍길순</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailPage;
