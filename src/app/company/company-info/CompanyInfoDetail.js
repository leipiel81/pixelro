import React, { useState } from "react";
import PropTypes from "prop-types";
import { ServiceCenter } from "graphql/query/select";
import { Link, useHistory } from "react-router-dom";
import { FormatDate } from "components/FormatDate";
import useSettings from "stores/settings";
import { Button, Modal } from "react-bootstrap";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_SERVICE_CENTER } from "graphql/gql/delete";

const DetailPage = () => {

  const [mdShow, setMdShow] = useState(false);
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  return (
    <>
      <div>
        <div>
          <nav aria-label="breadcrumb" role="navigation">
            <ol className="breadcrumb">
              <li className="breadcrumb-item active">회사정보 관리</li>
              <Link to="/company/company-info-detail" className="breadcrumb-item active mt-1" aria-current="page">
                기업정보
              </Link>
            </ol>
          </nav>
        </div>
        <InfoForm />
        <div className="col-12 grid-margin mt-4">
          <div className="row justify-between">
            <Link className="btn btn-primary ml-auto" to={`/company/company-info-edit`}>
              수정하기
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

const InfoForm = () => {
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="card">
          <div className="card-body">
            <div className="flex justify-between card-title-wrap">
              <h4 className="card-title">기업 정보</h4>
            </div>
            <div className="card-wrap">
              <table className="app-table bg-white w-full">
                <tr>
                  <td className="th">상호</td>
                  <td>주식회사 픽셀로</td>
                  <td className="th">사업자등록번호</td>
                  <td>000-00-0000</td>
                </tr>
                <tr>
                  <td className="th">대표자명</td>
                  <td>강석명</td>
                  <td className="th">대표전화번호</td>
                  <td>000-000-0000</td>
                </tr>
                <tr>
                  <td className="th">담당자</td>
                  <td colspan="3">홍길동, 홍길순</td>
                </tr>
                <tr>
                  <td className="th">사업소재지</td>
                  <td colspan="3">경기도 성남시 대왕판교로 ㅇㅇㅇ</td>
                </tr>
                <tr>
                  <td className="th">제휴문의</td>
                  <td colspan="3">help@pixelro.com</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailPage;
