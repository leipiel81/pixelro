import React, { useState } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Form } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import { Store } from "graphql/query/select";
import { CREATE_MEMBER } from "graphql/gql/insert";

const CreatePage = ({ match }) => {
  const { id } = match.params;

  const { result } = Store(id);
  const items = result;

  const history = useHistory();
  const goBack = () => {
    history.push("/members/member-table");
  };

  // --------------------------
  // -- 데이터 값 저장 --
  const [email, setEmail] = useState();
  const [name, setName] = useState();
  const [tel, setTel] = useState();
  const [password, setPassword] = useState();
  const storeGroupId = items && items.storeGroup.id;
  const defaultStoreId = items && items.id;

  // ----------------------------

  const [memberCreate] = useMutation(CREATE_MEMBER, {
    update: (proxy, result) => {
      goBack();
    },
    variables: {
      email,
      name,
      tel,
      password,
      admin: false,
      memberOpen: true,
      storeGroupId,
      defaultStoreId,
    },
  });

  const handleStoreGroupCreate = () => {
    if (!name || !email || !tel) {
      SimpleNotification({
        title: "",
        message: "정보를 입력해주세요.",
      });
      return;
    }

    memberCreate();
  };

  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">회원사 관리</li>
            <Link to="/members/member-table" className="breadcrumb-item active mt-1" aria-current="page">
              회원사 목록
            </Link>
            <Link to={`/members/employee-create/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              직원 등록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 직원 기본 정보</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">이름</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={name} onChange={(e) => setName(e.target.value)} />
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">전화번호</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={tel} onChange={(e) => setTel(e.target.value)} />
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 로그인 정보</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">이메일</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={email} onChange={(e) => setEmail(e.target.value)} />
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">비밀번호</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={password} onChange={(e) => setPassword(e.target.value)} />
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 grid-margin mt-3">
        <div className="row justify-end mr-1">
          <button type="button" className="btn btn-primary mr-2" onClick={handleStoreGroupCreate}>
            저장
          </button>
          <button className="btn btn-light" onClick={goBack}>
            취소
          </button>
        </div>
      </div>
    </>
  );
};

CreatePage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default CreatePage;
