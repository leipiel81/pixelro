import React, { useState, useEffect } from "react";
import ReactTable from "react-table";
import { useHistory, Link } from "react-router-dom";
import matchSorter from "match-sorter";
import { useQuery, useMutation } from "@apollo/react-hooks";
import useSettings from "stores/settings";
import { Button, Modal } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import { ALL_SETTLE_REPORTS } from "graphql/gql/select";
import { UPDATE_SETTLE_REPORT } from "graphql/gql/update";
import { FormatDateShort, FormatDate } from "components/FormatDate";

const ReactContentPage = () => {
  const [acceptShow, setAcceptShow] = useState(false);
  const [clickId, setClickId] = useState();
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  // 답변 값 변경
  const [settleReportUpdate, { data: updateData }] = useMutation(UPDATE_SETTLE_REPORT, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      refetch();
    }
  }, [updateData]);

  const { loading, error, data, refetch } = useQuery(ALL_SETTLE_REPORTS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data && data.allSettleReports) {
    items = data.allSettleReports;
    // 최근 등록날짜 순으로 나타내기
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  // 모달에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleModalAccept = () => {
    const date = FormatDate(new Date());
    settleReportUpdate({
      variables: { id: clickId, settlementAt: `${date}`, status: "정산완료" },
    });

    setAcceptShow(false);
  };

  // 화면에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleAcceptButtonClick = (id) => {
    setClickId(id);
    setAcceptShow(true);
  };

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">회원사 관리</li>
            <Link to="/members/settlement-request-table" className="breadcrumb-item active mt-1" aria-current="page">
              회원사 수익 신청 목록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between">
                <h4 className="card-title">회원사 수익 신청 목록</h4>
                <h4 className="card-title">{`Total : ${items.length}`}</h4>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      filterable
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "정산월",
                          id: "year",
                          accessor: (d) => `${d.year}-${d.month}`,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["year"] }),
                          filterAll: true,
                          Cell: (row) => (
                            <Link to={`/members/settlement-product-list/${row.original.id}`} className="text-blue-700 font-medium">
                              {row.value}
                            </Link>
                          ),
                        },
                        {
                          Header: "상점",
                          id: "storeName",
                          accessor: (d) => d.store.name,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["storeName"] }),
                          filterAll: true,
                          Cell: (row) => (
                            <Link to={`/members/settlement-request-detail/${row.original.id}`} className="text-blue-700 font-medium">
                              {row.value}
                            </Link>
                          ),
                        },
                        {
                          Header: "판매금액",
                          id: "productTotal",
                          accessor: (d) => d.productTotal,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["productTotal"] }),
                          filterAll: true,
                          Cell: (row) => row.value.toLocaleString(),
                        },
                        {
                          Header: "픽셀로쿠폰",
                          id: "pixelroTotal",
                          accessor: (d) => d.pixelroTotal,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["pixelroTotal"] }),
                          filterAll: true,
                          Cell: (row) => row.value.toLocaleString(),
                        },
                        {
                          Header: "상점쿠폰",
                          id: "storeTotal",
                          accessor: (d) => d.storeTotal,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["storeTotal"] }),
                          filterAll: true,
                          Cell: (row) => row.value.toLocaleString(),
                        },
                        {
                          Header: "포인트",
                          id: "pointTotal",
                          accessor: (d) => d.pointTotal,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["pointTotal"] }),
                          filterAll: true,
                          Cell: (row) => row.value.toLocaleString(),
                        },
                        {
                          Header: "판매수수료",
                          id: "salesCommission",
                          accessor: (d) => d.salesCommission,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["salesCommission"] }),
                          filterAll: true,
                          Cell: (row) => <div className="text-red-500">{row.value.toLocaleString()}</div>,
                        },
                        {
                          Header: "정산금액",
                          id: "settlementPrice",
                          accessor: (d) => d.settlementPrice,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["settlementPrice"] }),
                          filterAll: true,
                          Cell: (row) => <div className="text-red-500">{row.value.toLocaleString()}</div>,
                        },
                        {
                          Header: "신청날짜",
                          id: "createAt",
                          accessor: (d) => FormatDateShort(new Date(parseInt(d.createAt, 10))),
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["createAt"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                        {
                          Header: "신청확인 및 정산일",
                          accessor: "",
                          Filter: () => false,
                          Cell: (row) => (
                            <>
                              {row.original.settlementAt === "" ? (
                                <div>
                                  <button
                                    onClick={(e) => handleAcceptButtonClick(row.original.id)}
                                    type="button"
                                    className="btn btn-outline-success btn-sm mr-1"
                                  >
                                    신청확인 및 정산하기
                                  </button>
                                </div>
                              ) : (
                                <div>{row.original.settlementAt}</div>
                              )}
                            </>
                          ),
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
              {/* 승인 Modal Start */}
              <Modal size="md" show={acceptShow} onHide={() => setAcceptShow(false)} aria-labelledby="example-modal-sizes-title-sm">
                <Modal.Header closeButton>
                  <Modal.Title>정산 신청 확인 및 송금 완료</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <div>신청 내용을 확인하셨습니까?</div>
                  <div>
                    <p>최종금액을 확인하고 입금이 완료되었습니까?</p>
                  </div>
                </Modal.Body>
                <Modal.Footer className="flex-wrap">
                  <Button variant="success btn-sm m-2" onClick={handleModalAccept}>
                    확인
                  </Button>
                  <Button variant="light btn-sm m-2" onClick={() => setAcceptShow(false)}>
                    취소
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Modal Ends */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReactContentPage;
