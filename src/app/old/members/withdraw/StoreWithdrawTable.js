import React, { useState, useEffect } from "react";
import ReactTable from "react-table";
import { useHistory, Link } from "react-router-dom";
import matchSorter from "match-sorter";
import { useQuery, useMutation } from "@apollo/react-hooks";
import useSettings from "stores/settings";
import { Button, Modal } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import { ALL_STORES, FILTER_SETTLE_REPORT } from "graphql/gql/select";
import { UPDATE_STORE } from "graphql/gql/update";
import { PhoneFormatter } from "components/PhoneFormatter";

const ReactContentPage = () => {
  const [acceptShow, setAcceptShow] = useState(false);
  const [clickId, setClickId] = useState();
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  // 답변 값 변경
  const [storeUpdate, { data: updateData }] = useMutation(UPDATE_STORE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      refetch();
    }
  }, [updateData]);

  const { loading, error, data, refetch } = useQuery(ALL_STORES, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data && data.allStores) {
    items = data.allStores.filter((f) => f.withdrawStatus !== "");

    items.sort((a, b) => {
      const aDate = a.withdrawCreateAt;
      const ad = new Date(aDate.replace(/-/g, "/"));
      const bDate = b.withdrawCreateAt;
      const bd = new Date(bDate.replace(/-/g, "/"));

      return bd - ad;
    });
  }

  // 모달에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleModalAccept = () => {
    storeUpdate({ variables: { id: clickId, withdraw: true, withdrawStatus: "승인" } });

    setAcceptShow(false);
  };

  // 화면에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleAcceptButtonClick = (id) => {
    setClickId(id);
    setAcceptShow(true);
  };

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">회원사 관리</li>
            <Link to="/members/store-withdraw-table" className="breadcrumb-item active mt-1" aria-current="page">
              가맹취소내역
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between">
                <h4 className="card-title">가맹취소내역</h4>
                <h4 className="card-title">{`Total : ${items.length}`}</h4>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      filterable
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "상점이름",
                          id: "name",
                          accessor: (d) => d.name,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["name"] }),
                          filterAll: true,
                          Cell: (row) => row.value,
                        },
                        {
                          Header: "장소",
                          id: "place",
                          accessor: (d) => d.place,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["place"] }),
                          filterAll: true,
                          Cell: (row) => row.value,
                        },
                        {
                          Header: "전화번호",
                          id: "tel",
                          accessor: (d) => PhoneFormatter(d.tel),
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["tel"] }),
                          filterAll: true,
                          Cell: (row) => row.value,
                        },
                        {
                          Header: "정산상태",
                          id: "id",
                          accessor: (d) => <SettleMentCheck id={d.id} />,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["id"] }),
                          filterAll: true,
                          Cell: (row) => row.value,
                        },
                        {
                          Header: "가맹취소신청날짜",
                          id: "withdrawCreateAt",
                          accessor: (d) => d.withdrawCreateAt,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["withdrawCreateAt"],
                            }),
                          filterAll: true,
                          Cell: (row) => row.value,
                        },
                        {
                          Header: "",
                          accessor: "",
                          Filter: () => false,
                          Cell: (row) => (
                            <>
                              {row.original.withdrawStatus === "가맹취소신청" ? (
                                <div>
                                  <button
                                    onClick={(e) => handleAcceptButtonClick(row.original.id)}
                                    type="button"
                                    className="btn btn-outline-success btn-sm mr-1"
                                  >
                                    승인
                                  </button>
                                </div>
                              ) : (
                                <div>승인</div>
                              )}
                            </>
                          ),
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
              {/* 승인 Modal Start */}
              <Modal size="sm" show={acceptShow} onHide={() => setAcceptShow(false)} aria-labelledby="example-modal-sizes-title-sm">
                <Modal.Header closeButton>
                  <Modal.Title>가맹 취소 승인</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <p>승인하시겠습니까?</p>
                </Modal.Body>

                <Modal.Footer className="flex-wrap">
                  <Button variant="success btn-sm m-2" onClick={handleModalAccept}>
                    확인
                  </Button>
                  <Button variant="light btn-sm m-2" onClick={() => setAcceptShow(false)}>
                    취소
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Modal Ends */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const SettleMentCheck = ({ id }) => {
  const { loading, error, data } = useQuery(FILTER_SETTLE_REPORT, {
    fetchPolicy: "cache-and-network",
    variables: { value: id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    items = data.filterSettleReport.filter((f) => f.settlementAt === "");
  }

  if (items && items.length > 0) {
    return "정산금액이 남아있습니다.";
  }
  return "정산완료";
};

export default ReactContentPage;
