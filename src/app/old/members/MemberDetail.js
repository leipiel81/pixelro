import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Button, Modal } from "react-bootstrap";
import { MEMBER } from "graphql/gql/select";
import { Link, useHistory } from "react-router-dom";
import { FormatDate } from "components/FormatDate";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { PhoneFormatter } from "components/PhoneFormatter";
import { WITHDRAW_MEMBER } from "graphql/gql/update";
import { observer } from "mobx-react-lite";
import useSettings from "stores/settings";
import { CREATE_LOG } from "graphql/gql/insert";
import { SimpleNotification } from "components/SimpleNotification";
import publicIp from "public-ip";

const MemberDetail = observer(({ match }) => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }
  const { id } = match.params;

  const [mdShow, setMdShow] = useState(false);

  const [logCreate, { data: createLogData }] = useMutation(CREATE_LOG, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (createLogData !== undefined) {
      console.log("ok");
    }
  }, [createLogData]);

  useEffect(() => {
    async function logData() {
      const adminEmail = settings.email;
      const ip = await publicIp.v4();
      const url = `/members/member-detail/${id}`;
      const target = "member";
      const detail = "열람";

      logCreate({ variables: { adminEmail, ip, url, target, detail } });
    }

    logData();
  }, []);

  const goBack = () => {
    history.goBack();
  };

  const [withdrawMember, { data: updateData }] = useMutation(WITHDRAW_MEMBER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });
  useEffect(() => {
    if (updateData !== undefined) {
      if (updateData.withdrawMember.result === "cancel") {
        SimpleNotification({
          message: updateData.withdrawMember.error || "Error",
        });
        return;
      }
      if (updateData.withdrawMember.result === "ok") {
        goBack();
      }
    }
  }, [updateData]);

  const { loading, error, data } = useQuery(MEMBER, {
    fetchPolicy: "cache-and-network",
    variables: { id: id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return;
  }

  let items;
  if (data) {
    items = data.member;
  }

  const handleDelete = () => {
    setMdShow(false);
    withdrawMember({
      variables: { memberId: `${items.id}` },
    });
  };

  return (
    <>
      {items && (
        <div>
          {/* breadcrumb */}
          <div>
            <nav aria-label="breadcrumb" role="navigation">
              <ol className="breadcrumb">
                <li className="breadcrumb-item active">회원사 관리</li>
                <Link to="/members/member-table" className="breadcrumb-item active mt-1" aria-current="page">
                  회원사 목록
                </Link>
                <Link to={`/members/member-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
                  회원사 정보
                </Link>
              </ol>
            </nav>
          </div>
          {items.admin && (
            <div className="row">
              <Link
                className="btn btn-primary ml-4 mb-6 "
                to={`/registered-store/store-create/${items.storeGroup.id}/${items.id}/${items.defaultStoreId}`}
              >
                상점 등록 하기
              </Link>
            </div>
          )}
          <MemberInfo items={items} />
          <div className="col-12 grid-margin mt-3">
            <div className="row justify-between mr-1">
              <button className="btn btn-danger mt-2" onClick={() => setMdShow(true)}>
                삭제하기
              </button>
              <Link className="btn btn-primary mr-2 mt-2 " to={`/members/member-edit/${items.id}`}>
                수정하기
              </Link>
            </div>
            {/* Modal Starts  */}
            <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
              <Modal.Header closeButton>
                <Modal.Title>회원 탈퇴</Modal.Title>
              </Modal.Header>

              <Modal.Body>
                <p>해당 회원사를 삭제하시겠습니까?</p>
              </Modal.Body>

              <Modal.Footer className="fleex-wrap">
                <Button variant="danger m-2" onClick={handleDelete}>
                  삭제
                </Button>
                <Button variant="primary m-2" onClick={() => setMdShow(false)}>
                  취소
                </Button>
              </Modal.Footer>
            </Modal>
            {/* Modal Ends */}
          </div>
        </div>
      )}
    </>
  );
});

const MemberInfo = ({ items }) => {
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 회원사 기본 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">이메일</th>
                <td colSpan="3" className="border px-8 py-2 text-sm">
                  {items.email}
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">이름</th>
                <td colSpan="3" className="border px-8 py-2 text-sm">
                  {items.name}
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">전화번호</th>
                <td colSpan="3" className="border px-8 py-2 text-sm">
                  {PhoneFormatter(items.tel)}
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">관리자</th>
                <td colSpan="3" className="border px-8 py-2 text-sm">
                  {items.admin ? "Y" : "N"}
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">권한</th>
                <td colSpan="3" className="border px-8 py-2 text-sm">
                  {items.memberOpen ? "Y" : "N"}
                </td>
              </tr>
              <tr className="m-0 p-0">
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">가입 날짜</th>
                <td className="border px-8 py-2 text-sm w-4/12">{FormatDate(new Date(parseInt(items.createAt, 10)))}</td>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">최근 접속일</th>
                <td className="border px-8 py-2 text-sm w-4/12">{FormatDate(new Date(parseInt(items.signedAt, 10)))}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

MemberDetail.propTypes = {
  id: PropTypes.string.isRequired,
};

export default MemberDetail;
