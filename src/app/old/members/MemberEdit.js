/* eslint-disable no-inner-declarations */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Form } from "react-bootstrap";
import useSettings from "stores/settings";
import { CREATE_LOG } from "graphql/gql/insert";
import { SimpleNotification } from "components/SimpleNotification";
import publicIp from "public-ip";
import { Member } from "graphql/query/select";
import { UPDATE_MEMBER } from "graphql/gql/update";

const MemberEdit = ({ match }) => {
  const { id } = match.params;

  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  let items;
  if (id !== undefined && id !== "undefined") {
    const { result } = Member(id);
    items = result;
  }

  return <div>{items && <EditForm items={items} />}</div>;
};

const EditForm = ({ items }) => {
  const history = useHistory();
  const settings = useSettings();
  const goBack = () => {
    history.goBack();
  };

  // --------------------------
  // -- 데이터 값 저장 --
  const [email, setEmail] = useState(items.email);
  const [name, setName] = useState(items.name);
  const [tel, setTel] = useState(items.tel);
  const [password, setPassword] = useState("");
  const [passwordCheck, setPasswordCheck] = useState("");

  // 수정 안하는 값들.
  const { id } = items;

  // ----------------------------

  // Log
  const [logCreate, { data: createLogData }] = useMutation(CREATE_LOG, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (createLogData !== undefined) {
      goBack();
    }
  }, [createLogData]);

  // 회원사 정보 업데이트
  const [memberUpdate, { data: updateData }] = useMutation(UPDATE_MEMBER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      async function logData() {
        const adminEmail = settings.email;
        const ip = await publicIp.v4();
        const url = `/members/member-edit/${id}`;
        const target = "member";
        const detail = "수정";

        logCreate({ variables: { adminEmail, ip, url, target, detail } });
      }

      logData();
    }
  }, [updateData]);

  const handleUpdate = () => {
    if (password !== passwordCheck) {
      SimpleNotification({
        title: "",
        message: "비밀번호가 일치하지 않습니다.",
      });
      return;
    }

    if (!email || !name) {
      SimpleNotification({
        title: "",
        message: "내용을 입력해주세요.",
      });
      return;
    }

    let pwd;
    if (password && passwordCheck && password !== "" && passwordCheck !== "") {
      if (password === passwordCheck) {
        pwd = password;
      }
    }

    memberUpdate({
      variables: {
        id,
        email,
        name,
        tel,
        password: items.password,
        newPassword: pwd,
      },
    });
  };

  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">회원사 관리</li>
            <Link to="/members/member-table" className="breadcrumb-item active mt-1" aria-current="page">
              회원사 목록
            </Link>
            <Link to={`/members/member-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              회원사 정보
            </Link>
            <Link to={`/members/member-edit/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              회원사 정보 수정
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 회원사 정보 수정</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">이메일</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={email} onChange={(e) => setEmail(e.target.value)} />
                  </td>
                </tr>

                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm ">비밀번호</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm ">
                    <Form.Control
                      className="mb-2 w-3/12"
                      type="password"
                      value={password}
                      placeholder="비밀번호"
                      onChange={(e) => setPassword(e.target.value)}
                    />
                    <div className="flex mb-2">
                      <Form.Control
                        className="w-3/12"
                        type="password"
                        value={passwordCheck}
                        placeholder="비밀번호 확인"
                        onChange={(e) => setPasswordCheck(e.target.value)}
                      />
                    </div>
                  </td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">이름</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={name} onChange={(e) => setName(e.target.value)} />
                  </td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">전화번호</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={tel} onChange={(e) => setTel(e.target.value)} />
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 grid-margin mt-3">
        <div className="row justify-end mr-1">
          <button type="button" className="btn btn-primary mr-2" onClick={handleUpdate}>
            저장
          </button>
          <button className="btn btn-light" onClick={goBack}>
            취소
          </button>
        </div>
      </div>
    </>
  );
};
MemberEdit.prototype = {
  id: PropTypes.string.isRequired,
};

export default MemberEdit;
