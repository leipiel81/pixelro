import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Form } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import { UPDATE_STORE_GROUP } from "graphql/gql/update";
import { CREATE_STORE_GROUP, CREATE_MEMBER } from "graphql/gql/insert";

const CreatePage = () => {
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  // --------------------------
  // -- 데이터 값 저장 --
  const [adminId, setAdminId] = useState();
  const [email, setEmail] = useState();
  const [name, setName] = useState();
  const [tel, setTel] = useState();
  const [password, setPassword] = useState();
  const [storeGroupId, setStoreGroupId] = useState();

  // ----------------------------

  const [storeGroupUpdate] = useMutation(UPDATE_STORE_GROUP, {
    update: (proxy, result) => {
      goBack();
    },
    variables: {
      id: storeGroupId,
      adminId,
    },
  });

  const [memberCreate] = useMutation(CREATE_MEMBER, {
    update: (proxy, result) => {
      // 회원사 Id 저장
      setAdminId(result.data.createMember.id);
      // store group에 adminId 저장
      storeGroupUpdate();
    },
    variables: {
      email,
      name,
      tel,
      password,
      admin: true,
      memberOpen: true,
      storeGroupId,
      defaultStoreId: "1",
    },
  });

  const [storeGroupCreate] = useMutation(CREATE_STORE_GROUP, {
    update: (proxy, result) => {
      setStoreGroupId(result.data.createStoreGroup.id);
      // 회원사 등록
      memberCreate();
    },
    variables: {},
  });

  const handleStoreGroupCreate = () => {
    if (!name || !email || !tel) {
      SimpleNotification({
        title: "",
        message: "정보를 입력해주세요.",
      });
      return;
    }

    storeGroupCreate();
  };

  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">회원사 관리</li>
            <Link to="/members/member-table" className="breadcrumb-item active mt-1" aria-current="page">
              회원사 목록
            </Link>
            <Link to="/members/member-create" className="breadcrumb-item active mt-1" aria-current="page">
              회원사 등록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 회원사 기본 정보</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">이름</th>
                  <td className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={name} onChange={(e) => setName(e.target.value)} />
                  </td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">전화번호</th>
                  <td className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={tel} onChange={(e) => setTel(e.target.value)} />
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 로그인 정보</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">이메일</th>
                  <td className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={email} onChange={(e) => setEmail(e.target.value)} />
                  </td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">비밀번호</th>
                  <td className="border px-8 py-2 text-sm">
                    <Form.Control type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 grid-margin mt-3">
        <div className="row justify-end mr-1">
          <button type="button" className="btn btn-primary mr-2" onClick={handleStoreGroupCreate}>
            저장
          </button>
          <button className="btn btn-light" onClick={goBack}>
            취소
          </button>
        </div>
      </div>
    </>
  );
};

export default CreatePage;
