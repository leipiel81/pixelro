/* eslint-disable no-use-before-define */
import React, { useEffect } from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import { ALL_MEMBERS } from "graphql/gql/select";
import { UPDATE_MEMBER_BASIC } from "graphql/gql/update";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { FormatDateShort } from "components/FormatDate";
import { PhoneFormatter } from "components/PhoneFormatter";
import { Form } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";

const ReactContentPage = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  // 권한 수정
  const [memberOpenUpdate, { data: updateData }] = useMutation(UPDATE_MEMBER_BASIC, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      refetch();
    }
  }, [updateData]);

  // 회원사 전체 목록 가져오기
  const { loading, error, data, refetch } = useQuery(ALL_MEMBERS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  // 데이터 저장
  let items;
  if (data && data.allMembers) {
    items = data.allMembers;
    // 최근 등록날짜 순으로 나타내기
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  if (!items) return <div />;

  // 전화번호 포맷
  const handleTelFormat = (data) => {
    const tel = PhoneFormatter(data.value);
    return <div>{tel}</div>;
  };

  // 권한 값 포맷
  const handleBooleanCheck = (data) => {
    if (data) {
      return "Y";
    }
    return "N";
  };

  // 권한 변경
  const handleChangeOpen = (e, id) => {
    let memberOpen = false;
    if (e.target.value === "Y") {
      memberOpen = true;
    }
    const idValue = id;

    memberOpenUpdate({ variables: { id: idValue, memberOpen: memberOpen } });
  };
  return (
    <div>
      <div className="">
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">회원사 관리</li>
            <Link to="/members/member-table" className="breadcrumb-item active mt-1" aria-current="page">
              회원사 목록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between">
                <h4 className="card-title">회원사 목록</h4>
                <h4 className="card-title">{`Total : ${items.length}`}</h4>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      filterable
                      defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "이메일",
                          id: "email",
                          accessor: (d) => d.email,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["email"],
                            }),
                          filterAll: true,
                        },
                        {
                          Header: "이름",
                          id: "name",
                          accessor: (d) => d.name,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["name"] }),
                          filterAll: true,
                          Cell: (row) => (
                            <Link to={`/members/member-detail/${row.original.id}`} className="text-blue-700 font-medium">
                              {row.value}
                            </Link>
                          ),
                        },
                        {
                          Header: "전화번호",
                          id: "tel",
                          accessor: (d) => d.tel,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["tel"] }),
                          filterAll: true,
                          Cell: handleTelFormat,
                        },
                        {
                          Header: "관리자",
                          id: "admin",
                          accessor: (d) => handleBooleanCheck(d.admin),
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["admin"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },

                        {
                          Header: "권한",
                          id: "memberOpen",
                          accessor: (d) => handleBooleanCheck(d.memberOpen),
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["memberOpen"],
                            }),
                          filterAll: true,
                          Cell: (row) => (
                            <div className="justify-between">
                              <Form.Group>
                                <select
                                  className="text-black"
                                  id="openSelect"
                                  value={row.value}
                                  onChange={(e) => handleChangeOpen(e, row.original.id)}
                                >
                                  <option value="Y">Y</option>
                                  <option value="N">N</option>
                                </select>
                              </Form.Group>
                            </div>
                          ),
                        },
                        {
                          Header: "가입날짜",
                          id: "createAt",
                          accessor: (d) => FormatDateShort(new Date(parseInt(d.createAt, 10))),
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["createAt"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                        {
                          Header: "최근접속일",
                          id: "signedAt",
                          accessor: (d) => FormatDateShort(new Date(parseInt(d.signedAt, 10))),
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["signedAt"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="flex justify-end m-4">
        <Link className="btn btn-warning" to="/members/member-create">
          회원사 등록하기
        </Link>
      </div>
    </div>
  );
};

export default ReactContentPage;
