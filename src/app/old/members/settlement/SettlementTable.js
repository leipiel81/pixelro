import React, { useState, useEffect } from "react";
import ReactTable from "react-table";
import { useHistory, Link } from "react-router-dom";
import matchSorter from "match-sorter";
import { useQuery, useMutation } from "@apollo/react-hooks";
import useSettings from "stores/settings";
import { Button, Modal } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import { ALL_SETTLE_REPORTS } from "graphql/gql/select";
import { UPDATE_SETTLEMENT_OK } from "graphql/gql/update";
import { FormatDateShort, FormatDate } from "components/FormatDate";

const ReactContentPage = () => {
  const [acceptShow, setAcceptShow] = useState(false);
  const [clickId, setClickId] = useState();
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  // 답변 값 변경
  const [settleReportUpdate, { data: updateData }] = useMutation(UPDATE_SETTLEMENT_OK, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      refetch();
    }
  }, [updateData]);

  const { loading, error, data, refetch } = useQuery(ALL_SETTLE_REPORTS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data && data.allSettleReports) {
    items = data.allSettleReports.filter((f) => f.settlementAt !== "");

    // 최근 등록날짜 순으로 나타내기
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  // 모달에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleModalAccept = () => {
    const date = FormatDate(new Date());
    settleReportUpdate({ variables: { id: clickId, taxBillAt: `${date}` } });

    setAcceptShow(false);
  };

  // 화면에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleAcceptButtonClick = (id) => {
    setClickId(id);
    setAcceptShow(true);
  };

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">회원사 관리</li>
            <Link to="/members/settlement-table" className="breadcrumb-item active mt-1" aria-current="page">
              회원사 세금 계산서 현황
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between">
                <h4 className="card-title">회원사 세금 계산서 현황</h4>
                <h4 className="card-title">{`Total : ${items.length}`}</h4>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      filterable
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "정산월",
                          id: "year",
                          accessor: (d) => `${d.year}-${d.month}`,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["year"] }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                        {
                          Header: "상점",
                          id: "storeName",
                          accessor: (d) => d.store.name,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["storeName"] }),
                          filterAll: true,
                          Cell: (row) => (
                            <Link to={`/members/settlement-detail/${row.original.id}`} className="text-blue-700 font-medium">
                              {row.original.store.name}
                            </Link>
                          ),
                        },
                        {
                          Header: "세금액",
                          id: "tax",
                          accessor: (d) => d.tax,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["tax"] }),
                          filterAll: true,
                          Cell: (row) => <div className="text-red-500">{row.value.toLocaleString()}</div>,
                        },
                        {
                          Header: "공급가액",
                          id: "supplyPrice",
                          accessor: (d) => d.supplyPrice,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["supplyPrice"] }),
                          filterAll: true,
                          Cell: (row) => <div className="text-red-500">{row.value.toLocaleString()}</div>,
                        },
                        {
                          Header: "정산금액",
                          id: "settlementPrice",
                          accessor: (d) => d.settlementPrice,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["settlementPrice"] }),
                          filterAll: true,
                          Cell: (row) => <div className="text-red-500">{row.value.toLocaleString()}</div>,
                        },
                        {
                          Header: "신청날짜",
                          id: "createAt",
                          accessor: (d) => FormatDateShort(new Date(parseInt(d.createAt, 10))),
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["createAt"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                        {
                          Header: "송금날짜",
                          id: "settlementAt",
                          accessor: (d) => d.settlementAt,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["settlementAt"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                        {
                          Header: "세금계산서 발행일",
                          accessor: "",
                          Filter: () => false,
                          Cell: (row) => (
                            <>
                              {row.original.taxBillAt === "" ? (
                                <div>
                                  <button
                                    onClick={(e) => handleAcceptButtonClick(row.original.id)}
                                    type="button"
                                    className="btn btn-outline-success btn-sm mr-1"
                                  >
                                    세금계산서 발행하기
                                  </button>
                                </div>
                              ) : (
                                <div>{row.original.taxBillAt}</div>
                              )}
                            </>
                          ),
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
              {/* 승인 Modal Start */}
              <Modal size="sm" show={acceptShow} onHide={() => setAcceptShow(false)} aria-labelledby="example-modal-sizes-title-sm">
                <Modal.Header closeButton>
                  <Modal.Title>세금계산서 발행 확인</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <p>세금계산서 발행이 완료되었습니까?</p>
                </Modal.Body>

                <Modal.Footer className="flex-wrap">
                  <Button variant="success btn-sm m-2" onClick={handleModalAccept}>
                    확인
                  </Button>
                  <Button variant="light btn-sm m-2" onClick={() => setAcceptShow(false)}>
                    취소
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Modal Ends */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReactContentPage;
