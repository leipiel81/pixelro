import React, { useState, useEffect } from "react";
import ReactTable from "react-table";
import { useHistory, Link } from "react-router-dom";
import matchSorter from "match-sorter";
import { useQuery, useMutation } from "@apollo/react-hooks";
import useSettings from "stores/settings";
import { Button, Modal } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import { ALL_MESSAGES } from "graphql/gql/select";
import { UPDATE_STORE, UPDATE_MESSAGE } from "graphql/gql/update";
import { FormatDate } from "components/FormatDate";

const ReactContentPage = () => {
  const [acceptShow, setAcceptShow] = useState(false);
  const [clickId, setClickId] = useState();
  const [clickStoreId, setClickStoreId] = useState();
  const [clickStoreMessageCount, setClickStoreMessageCount] = useState();
  const [clickStoreChargePrice, setClickStoreChargePrice] = useState();

  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  // 메세지 건수 추가
  const [messageUpdate, { data: updateMessageData }] = useMutation(UPDATE_MESSAGE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateMessageData !== undefined) {
      refetch();
    }
  }, [updateMessageData]);

  // 메세지 건수 추가
  const [storeUpdate, { data: updateData }] = useMutation(UPDATE_STORE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      const date = FormatDate(new Date());
      messageUpdate({ variables: { id: clickId, status: "확인", checkAt: date } });
    }
  }, [updateData]);

  const { loading, error, data, refetch } = useQuery(ALL_MESSAGES, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data && data.allMessages) {
    items = data.allMessages;
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  // 모달에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleModalAccept = () => {
    storeUpdate({ variables: { id: clickStoreId, messageCount: Number(clickStoreMessageCount + clickStoreChargePrice) } });

    setAcceptShow(false);
  };

  // 화면에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleAcceptButtonClick = (id, storeId, storeMessageCount, chargePrice) => {
    setClickId(id);
    setClickStoreId(storeId);
    setClickStoreMessageCount(storeMessageCount);
    setClickStoreChargePrice(chargePrice);
    setAcceptShow(true);
  };

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">회원사 관리</li>
            <Link to="/members/message-table" className="breadcrumb-item active mt-1" aria-current="page">
              메세지 충전 신청
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between">
                <h4 className="card-title">메세지 충전 신청</h4>
                <h4 className="card-title">{`Total : ${items.length}`}</h4>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      filterable
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "상점이름",
                          id: "name",
                          accessor: (d) => d.store.name,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["name"] }),
                          filterAll: true,
                          Cell: (row) => row.value,
                        },
                        {
                          Header: "예금주",
                          id: "depositor",
                          accessor: (d) => d.depositor,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["depositor"] }),
                          filterAll: true,
                          Cell: (row) => row.value,
                        },
                        {
                          Header: "충전금액",
                          id: "chargePrice",
                          accessor: (d) => d.chargePrice,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["chargePrice"],
                            }),
                          filterAll: true,
                          Cell: (row) => (row.value ? row.value.toLocaleString() : 0),
                        },
                        {
                          Header: "실입금액",
                          id: "sendPrice",
                          accessor: (d) => d.sendPrice,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["sendPrice"],
                            }),
                          filterAll: true,
                          Cell: (row) => (row.value ? row.value.toLocaleString() : 0),
                        },
                        {
                          Header: "",
                          accessor: "",
                          Filter: () => false,
                          Cell: (row) => (
                            <>
                              {row.original.status === "신청" ? (
                                <div>
                                  <button
                                    onClick={(e) =>
                                      handleAcceptButtonClick(
                                        row.original.id,
                                        row.original.store.id,
                                        row.original.store.messageCount,
                                        row.original.chargePrice
                                      )
                                    }
                                    type="button"
                                    className="btn btn-outline-success btn-sm mr-1"
                                  >
                                    충전하기
                                  </button>
                                </div>
                              ) : (
                                <div>{row.original.checkAt}</div>
                              )}
                            </>
                          ),
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
              {/* 승인 Modal Start */}
              <Modal size="sm" show={acceptShow} onHide={() => setAcceptShow(false)} aria-labelledby="example-modal-sizes-title-sm">
                <Modal.Header closeButton>
                  <Modal.Title>충전하기</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <p>송금을 확인하였습니까?</p>
                </Modal.Body>

                <Modal.Footer className="flex-wrap">
                  <Button variant="success btn-sm m-2" onClick={handleModalAccept}>
                    확인
                  </Button>
                  <Button variant="light btn-sm m-2" onClick={() => setAcceptShow(false)}>
                    취소
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Modal Ends */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReactContentPage;
