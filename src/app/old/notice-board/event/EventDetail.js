/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Button, Modal } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import { NOTICE_BOARD } from "graphql/gql/select";
import { DELETE_NOTICE_BOARD } from "graphql/gql/delete";
import { FormatDateShort } from "components/FormatDate";

const DetailPage = ({ match }) => {
  const { id } = match.params;

  //  이벤트
  const { loading, error, data } = useQuery(NOTICE_BOARD, {
    fetchPolicy: "cache-and-network",
    variables: { id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    items = data.noticeBoard;
  }

  if (!items) return <div />;
  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">게시판 관리</li>
            <Link to="/notice-board/event-table" className="breadcrumb-item active mt-1" aria-current="page">
              이벤트 목록
            </Link>
            <Link to={`/notice-board/event-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              이벤트 정보
            </Link>
          </ol>
        </nav>
      </div>
      <Content items={items} />
    </>
  );
};

DetailPage.propTypes = {
  id: PropTypes.string.isRequired,
};

const Content = ({ items }) => {
  const [mdShow, setMdShow] = useState(false);

  const history = useHistory();
  const goBack = () => {
    history.push("/notice-board/event-table");
  };

  const [noticeBoardDelete, { data: deleteData }] = useMutation(DELETE_NOTICE_BOARD, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (deleteData !== undefined) {
      goBack();
    }
  }, [deleteData]);

  const handleDelete = () => {
    setMdShow(false);
    noticeBoardDelete({ variables: { id: items.id } });
  };

  return (
    <>
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 이벤트 상세 정보</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              {/* 배너 이미지 */}
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">배너 이미지</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    <img className="h-24 w-64" src={items.image} alt="" />
                  </td>
                </tr>
              </table>
              {/* url */}
              <table className="shadow-sm bg-white w-full mt-4">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm  w-1/6">url1</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.link1}
                    <img className="h-24 w-64" src={items.linkImage1} alt="" />
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm  w-1/6">url2</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.link2}
                    <img className="h-24 w-64" src={items.linkImage2} alt="" />
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm  w-1/6">url3</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.link3}
                    <img className="h-24 w-64" src={items.linkImage3} alt="" />
                  </td>
                </tr>
              </table>
              {/* 기간 */}
              <table className="shadow-sm bg-white w-full mt-4">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">이벤트 기간</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {`${FormatDateShort(new Date(parseInt(items.startAt, 10)))} ~ 
                    ${FormatDateShort(new Date(parseInt(items.endAt, 10)))}`}
                  </td>
                </tr>
              </table>
              {/* 내용 */}
              <table className="shadow-sm bg-white w-full mt-4">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm  w-1/6">제목</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.title}
                  </td>
                </tr>
                <tr>
                  <td colSpan="4" className="border px-8 py-2 text-sm">
                    <ContentForm items={items} />
                  </td>
                </tr>
              </table>

              {/* 홈화면 */}
              <table className="shadow-sm bg-white w-full mt-4">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">홈화면</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <div className="form-check">
                      <label className="form-check-label">
                        <input type="checkbox" checked={items.visible} className="form-check-input" />
                        <i className="input-helper" />
                      </label>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 grid-margin mt-3">
        <div className="row justify-between">
          <button className="btn btn-danger mt-2" onClick={() => setMdShow(true)}>
            삭제하기
          </button>
          <Link className="btn btn-primary mt-2 " to={`/notice-board/event-edit/${items.id}`}>
            수정하기
          </Link>
        </div>
        <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
          <Modal.Header closeButton>
            <Modal.Title>해당 내용을 삭제하시겠습니까?</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <p>삭제를 원하시면 삭제 버튼을 눌러주세요.</p>
          </Modal.Body>

          <Modal.Footer className="fleex-wrap">
            <Button variant="danger m-2" onClick={handleDelete}>
              삭제
            </Button>
            <Button variant="primary m-2" onClick={() => setMdShow(false)}>
              취소
            </Button>
          </Modal.Footer>
        </Modal>
        {/* Modal Ends */}
      </div>
    </>
  );
};

const ContentForm = ({ items }) => {
  const [content, setContent] = useState("");

  fetch(items.content)
    .then((res) => res.text())
    .then((body) => {
      setContent(body);
      console.log("body :: ", body);
    });
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="border-gray-200">
          <div className="w-full p-2">
            <div
              className="max-w-sm table-intro table-responsive  cart-table-intro w-full bg-white p-2"
              introeditable="true"
              dangerouslySetInnerHTML={{ __html: content }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailPage;
