/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import PropTypes from "prop-types";
import { AllNoticeBoards } from "graphql/query/select";
import { FormatDateShort } from "components/FormatDate";
import Pagination from "components/Pagination";

const Tables = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const { result } = AllNoticeBoards();
  const items = result && result.filter((f) => f.category === "이벤트");

  const [pageOfItems, setPageOfItems] = useState([]);
  const [page, setPage] = useState(1);

  function onChangePage(pageOfItems, pageNumber) {
    setPageOfItems(pageOfItems);
    setPage(pageNumber);
    // 페이지 이동시 맨 위로 스크롤 이동
    window.scrollTo(0, 0);
  }

  return (
    <>
      <div>
        <TableHeader />
        <div className="row grid-margin">
          <div className="col-12">
            <div className="flex justify-between mb-4">
              <h4 className="card-title mt-2 pt-1 text-xl">이벤트 목록</h4>
              <Link className="btn btn-warning" to="/notice-board/event-create">
                등록하기
              </Link>
            </div>
            <div className="flex flex-col">
              <div className="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                <div className="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                  <TableForm items={items} pageOfItems={pageOfItems} />
                  <Page items={items} page={page} onChangePage={onChangePage} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

// /* breadcrumb */
const TableHeader = () => {
  return (
    <div>
      <nav aria-label="breadcrumb" role="navigation">
        <ol className="breadcrumb">
          <li className="breadcrumb-item active">게시판 관리</li>
          <Link to="/notice-board/event-table" className="breadcrumb-item active mt-1" aria-current="page">
            이벤트 목록
          </Link>
        </ol>
      </nav>
    </div>
  );
};

const TableForm = ({ items, pageOfItems }) => {
  return (
    <table className="min-w-full">
      <thead className="bg-gray-200">
        <tr>
          <th className="px-3 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider w-2/12">
            배너 이미지
          </th>
          <th className="px-3 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider w-2/12">
            제목
          </th>
          <th className="px-3 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider w-3/12">
            이벤트 기간
          </th>
          <th className="px-3 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider w-2/12">
            등록날짜
          </th>
          <th className="px-3 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider w-1/12">
            홈화면
          </th>
          <th className="px-3 py-3 border-b border-gray-200 bg-gray-50 w-2/12" />
        </tr>
      </thead>
      {items && items.length === 0 ? (
        <tr>
          <td className="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
            <div>등록된 내용이 없습니다.</div>
          </td>
        </tr>
      ) : (
        pageOfItems && (
          <tbody className="bg-white">
            {pageOfItems.map((m) => (
              <tr>
                <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                  <img src={m.image} alt="" />
                </td>
                <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900 ">
                  {m.title}
                </td>
                <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-black ">
                  {`${FormatDateShort(new Date(parseInt(m.startAt, 10)))} ~ 
                    ${FormatDateShort(new Date(parseInt(m.endAt, 10)))}`}
                </td>
                <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-black ">
                  {FormatDateShort(new Date(parseInt(m.createAt, 10)))}
                </td>
                <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900 ">
                  {m.visible ? "true" : "false"}
                </td>
                <td className="px-3 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium ">
                  <Link className="ti-pencil-alt" to={`/notice-board/event-detail/${m.id}`}>
                    상세보기
                  </Link>
                  <Link className="ti-pencil-alt ml-3" to={`/notice-board/event-edit/${m.id}`}>
                    수정하기
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        )
      )}
    </table>
  );
};

const Page = (props) => {
  const PAGE_SIZE = 10;
  const { items, page, onChangePage } = props;
  return (
    <div className="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">
      <div className="hidden sm:block">
        <p className="text-sm leading-5 text-gray-700">
          <span className="font-medium mx-1">{(page - 1) * PAGE_SIZE + 1}</span>
          {" ~  "}
          <span className="font-medium mx-1">{items && items.length < page * PAGE_SIZE ? items.length : page * PAGE_SIZE}</span>
          {" / 총 "}
          <span className="font-medium mx-1">{items && items.length}</span>
          {" 건"}
        </p>
      </div>
      <div className="flex-1 flex justify-between sm:justify-end">{items && <Pagination items={items} onChangePage={onChangePage} />}</div>
    </div>
  );
};

Page.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  page: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
};

export default Tables;
