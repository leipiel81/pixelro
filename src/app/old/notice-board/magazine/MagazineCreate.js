/* eslint-disable no-nested-ternary */
/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useQuery, useMutation } from "@apollo/react-hooks";
import Paginator from "react-hooks-paginator";
import { Form, Modal } from "react-bootstrap";
import { CREATE_NOTICE_BOARD, MULTI_FILE } from "graphql/gql/insert";
import { ALL_PRODUCTS } from "graphql/gql/select";
import { UPDATE_NOTICE_BOARD_CONTENT } from "graphql/gql/update";
import { SimpleNotification } from "components/SimpleNotification";
import { Editor } from "@tinymce/tinymce-react";

const CreatePage = () => {
  const history = useHistory();
  const goBack = () => {
    history.push("/notice-board/magazine-table");
  };

  // --------------------------
  // -- 데이터 값 저장 --
  const category = "매거진";
  const [image, setImage] = useState();
  const [title, setTitle] = useState();

  const [content, setContent] = useState("");
  const [contentImages, setContentImages] = useState([]);

  const [visible, setVisible] = useState(false);
  const [link1, setLink1] = useState();
  const [linkImage1, setLinkImage1] = useState();
  const [link2, setLink2] = useState();
  const [linkImage2, setLinkImage2] = useState();
  const [link3, setLink3] = useState();
  const [linkImage3, setLinkImage3] = useState();

  // url 직접입력
  const [urlOpen1, setUrlOpen1] = useState(false);
  const [urlOpen2, setUrlOpen2] = useState(false);
  const [urlOpen3, setUrlOpen3] = useState(false);

  // 상품 관련
  // 검색
  const [searchItem, setSearchItem] = useState();

  // 상품 관련 데이터
  const [mdShow1, setMdShow1] = useState(false);
  const [mdShow2, setMdShow2] = useState(false);
  const [mdShow3, setMdShow3] = useState(false);
  const [items, setItems] = useState(null);
  const [products, setProducts] = useState();

  // 페이지네이션 적용
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentItem, setCurrentItem] = useState([]);

  const pageLimit = 7;

  // 페이지네이션 초기화
  useEffect(() => {
    if (products) {
      setCurrentItem(products.slice(offset, offset + pageLimit));
    }
  }, [offset, products]);

  //---------------------------

  // 내용 등록
  const [contentUpdate, { data: updateIntroData }] = useMutation(UPDATE_NOTICE_BOARD_CONTENT, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateIntroData !== undefined) {
      goBack();
    }
  }, [updateIntroData]);

  // 데이터 저장
  const [boardCreate, { data: createData }] = useMutation(CREATE_NOTICE_BOARD, {
    onError(err) {
      console.log("updateAdmin: err=", err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });
  useEffect(() => {
    if (createData !== undefined) {
      const data1 = createData.createNoticeBoard;
      contentUpdate({ variables: { id: data1.id, content: `${content}` } });
    }
  }, [createData]);

  // 0. 저장 버튼 클릭
  const handleCreate = async () => {
    if (!title) {
      window.scrollTo(0, 0);
      SimpleNotification({
        title: "Error",
        message: "필수항목을 모두 입력해주세요.",
      });
      return;
    }

    if (!image || !content) {
      SimpleNotification({
        title: "Error",
        message: "이미지를 등록해주세요.",
      });
      return;
    }

    // 데이터 저장
    boardCreate({
      variables: {
        category,
        image,
        title,
        content: "",
        link1,
        linkImage1,
        link2,
        linkImage2,
        link3,
        linkImage3,
        visible,
        no: "",
        startAt: "",
        endAt: "",
      },
    });
  };

  // 이미지 관련 함수들
  //  url 이미지 저장
  const [url1ImageUpload, { data: url1ImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (url1ImageData !== undefined) {
      const result = `${url1ImageData.multiUpload[0].filename}`;
      setLinkImage1(result);
    }
  }, [url1ImageData]);

  //  url2 이미지 저장
  const [url2ImageUpload, { data: url2ImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (url2ImageData !== undefined) {
      const result = `${url2ImageData.multiUpload[0].filename}`;
      setLinkImage2(result);
    }
  }, [url2ImageData]);

  //  url3 이미지 저장
  const [url3ImageUpload, { data: url3ImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (url3ImageData !== undefined) {
      const result = `${url3ImageData.multiUpload[0].filename}`;
      setLinkImage3(result);
    }
  }, [url3ImageData]);

  //  메인 이미지 저장
  const [mainImageUpload, { data: mainImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (mainImageData !== undefined) {
      const result = `${mainImageData.multiUpload[0].filename}`;
      setImage(result);
    }
  }, [mainImageData]);

  // url 이미지 변경
  const handleUrlImageChange = async (e, type) => {
    const file = e.target.files[0];

    if (type === "1") {
      // 이미지 저장
      await url1ImageUpload({ variables: { files: file, size: `[${file.size}]` } });
    }

    if (type === "2") {
      // 이미지 저장
      await url2ImageUpload({ variables: { files: file, size: `[${file.size}]` } });
    }

    if (type === "3") {
      // 이미지 저장
      await url3ImageUpload({ variables: { files: file, size: `[${file.size}]` } });
    }
  };

  // url 이미지 삭제
  const handleUrlDelete = async (type) => {
    if (type === "1") {
      // 이미지 삭제
      setLinkImage1("");
    }

    if (type === "2") {
      // 이미지 삭제
      setLinkImage2("");
    }

    if (type === "3") {
      // 이미지 삭제
      setLinkImage3("");
    }
  };

  // 메인 이미지 변경
  const handleImageChange = async (e) => {
    const file = e.target.files[0];

    // 이미지 저장
    await mainImageUpload({ variables: { files: file, size: `[${file.size}]` } });
  };

  const introUpdateFunction = async (tempDetailImage) => {
    let tempIntro = content;
    // 이미지 저장과 동시에 editor에 표현
    for (let i = 0; i < tempDetailImage.length; i += 1) {
      tempIntro += `<p><img style="width : 100%;" src="${tempDetailImage[i]}" alt=""></p>`;
    }

    await setContent(tempIntro);
  };

  //  상세 이미지 저장
  // 이미지 저장
  const [contentImageUpload, { data: contentData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (contentData !== undefined) {
      const tempDetailImage = [];
      contentImages.map((m) => tempDetailImage.push(m));
      contentData.multiUpload.map((m) => tempDetailImage.push(m.filename));

      // 이미지 저장과 동시에 editor에 표현
      introUpdateFunction(tempDetailImage);

      setContentImages(tempDetailImage);
    }
  }, [contentData]);

  // 이미지 추가
  const handleContentImageChange = async (e) => {
    const { files } = e.target;

    const fileArray = [];
    if (files) {
      for (let i = 0; i < files.length; i += 1) {
        fileArray.push(files[i]);
      }
    }
    const filesSize = `[${fileArray.map((m) => m.size)}]`;
    // 바로 추가 보내기
    await contentImageUpload({ variables: { files: fileArray, size: filesSize } });
  };

  // 이미지 선택
  const handleImageClick = async (image) => {
    const tempIntro = `${content}<p><img style="width : 100%;" src="${image}" alt=""></p>`;
    setContent(tempIntro);
  };
  const handleEditorChange = (value) => {
    setContent(value);
  };

  //   링크1 클릭시 이동할 상품 선택
  const handleProductChoice1 = (productId) => {
    setUrlOpen1(true);
    const url = `/app-product-detail/${productId}`;

    setLink1(url);
    setMdShow1(false);
    setSearchItem();
    setProducts(items);
  };

  //   링크2 클릭시 이동할 상품 선택
  const handleProductChoice2 = (productId) => {
    setUrlOpen2(true);
    const url = `/app-product-detail/${productId}`;

    setLink2(url);
    setMdShow2(false);
    setSearchItem();
    setProducts(items);
  };

  //   링크3 클릭시 이동할 상품 선택
  const handleProductChoice3 = (productId) => {
    setUrlOpen3(true);
    const url = `/app-product-detail/${productId}`;

    setLink3(url);
    setMdShow3(false);
    setSearchItem();
    setProducts(items);
  };

  //   상품정보
  const { loading, error, data } = useQuery(ALL_PRODUCTS, {
    fetchPolicy: "cache-and-network",
    skip: !!items,
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  if (data) {
    const resultData = data.allProducts;
    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });

    setItems(resultData);
    setProducts(resultData);
  }

  // 검색
  const handleSearch = () => {
    setProducts(items);

    if (searchItem) {
      setProducts(
        items.filter((f) => {
          return f.name.indexOf(searchItem) > -1;
        })
      );
    }
  };

  if (!items) return <div />;

  const handleUrlWrite = (type) => {
    if (type === "1") {
      setLink1("");
      setUrlOpen1(false);
    }

    if (type === "2") {
      setLink2("");
      setUrlOpen2(false);
    }

    if (type === "3") {
      setLink3("");
      setUrlOpen3(false);
    }
  };
  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">게시판 관리</li>
            <Link to="/notice-board" className="breadcrumb-item active mt-1" aria-current="page">
              매거진
            </Link>
            <Link to="/notice-board/magazine-create" className="breadcrumb-item active mt-1" aria-current="page">
              매거진 등록
            </Link>
          </ol>
        </nav>
      </div>
      {/* 매거진 정보 */}
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 매거진 정보</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              {/* 배너 이미지 등록 */}
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">배너 이미지</th>
                  <td className="border px-8 py-2 text-sm">
                    <div className="flex my-3 ml-10">
                      <img src={image && image} className="w-64 h-32" />
                      <div className="ml-12 flex-row-left">
                        <div className="w-16 font-bold">첨부파일</div>
                        <div className="mt-3 h-8 flex flex-row table-text relative">
                          <form
                            onSubmit={() => {
                              console.log("Submitted");
                            }}
                            encType="multipart/form-data"
                          >
                            <input name="document" type="file" accept="image/*" method="POST" onChange={handleImageChange} />
                          </form>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
              </table>
              {/* url 등록 */}
              <table className="shadow-sm bg-white w-full mt-4">
                {/* url1 */}
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm  w-1/6">URL1</th>
                  <td className="border px-8 py-2 text-sm">
                    <div className="flex">
                      <Form.Control
                        type="text"
                        className="w-1/2"
                        value={link1}
                        readOnly={urlOpen1}
                        onChange={(e) => setLink1(e.target.value)}
                      />
                      <button onClick={(e) => setMdShow1(true)} type="button" className="btn btn-outline-primary btn-sm ml-2">
                        상품 찾기
                      </button>
                      <button onClick={(e) => handleUrlWrite("1")} type="button" className="btn btn-primary btn-sm ml-2">
                        직접 입력
                      </button>
                      <Modal show={mdShow1} onHide={() => setMdShow1(false)} aria-labelledby="example-modal-sizes-title-md">
                        <Modal.Header closeButton>
                          <div className="w-full">
                            <div className="sidebar-widget">
                              <h4 className="pro-sidebar-title">Search </h4>
                              <div className="pro-sidebar-search mt-15">
                                <div className="pro-sidebar-search-form" action="#">
                                  <input
                                    type="text"
                                    placeholder="Search here..."
                                    value={searchItem}
                                    onChange={(e) => setSearchItem(e.target.value)}
                                  />
                                  <button onClick={handleSearch}>
                                    <i className="ti-search" />
                                  </button>
                                </div>
                              </div>
                            </div>
                            <div className="flex mx-auto">
                              <div>
                                {currentItem &&
                                  currentItem.map((m) => (
                                    <div className="px-3 pt-1 mt-1 flex justify-between border-b">
                                      <div className="flex justify-start w-3/4">
                                        <img src={m.mainImage} className="w-12 h-12 rounded-full" alt="dp" />
                                        <div className="flex justify-start flex-wrap ml-4 pb-3 mt-2 ">
                                          <div className="flex justify-start font-bold">{m.name}</div>
                                          <div className="inline-flex w-full text-sm text-gray-500">
                                            <span className="pr-2">{`${m.brand} | ${m.model}`}</span>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="flex items-center text-black w-1/4">
                                        <button
                                          type="button"
                                          className="btn btn-warning btn-rounded py-1 px-3"
                                          onClick={(e) => handleProductChoice1(m.id)}
                                        >
                                          선택
                                        </button>
                                      </div>
                                    </div>
                                  ))}
                              </div>
                            </div>
                            <div className="pro-pagination-style text-center mt-30">
                              <Paginator
                                totalRecords={products.length}
                                pageLimit={pageLimit}
                                pageNeighbours={1} // 0, 1, 2
                                setOffset={setOffset}
                                currentPage={currentPage}
                                setCurrentPage={setCurrentPage}
                                pageContainerClass="mb-0 mt-0"
                                pagePrevText="«"
                                pageNextText="»"
                              />
                            </div>
                          </div>
                        </Modal.Header>
                      </Modal>
                      {/* 이미지 등록 */}
                    </div>
                    <div className="flex mt-2">
                      <img src={linkImage1 && linkImage1} className="w-64 h-32" />
                      <div className="ml-12 flex-row-left">
                        <div className="w-16 font-bold">첨부파일</div>
                        <div className="mt-3 h-8 flex flex-row table-text relative">
                          <form
                            onSubmit={() => {
                              console.log("Submitted");
                            }}
                            encType="multipart/form-data"
                          >
                            <input
                              name="document"
                              type="file"
                              accept="image/*"
                              method="POST"
                              onChange={(e) => handleUrlImageChange(e, "1")}
                            />
                          </form>
                          {linkImage1 ? (
                            <button
                              className="ml-3 bg-gray-400 hover:bg-gray-500 text-gray-800 font-bold py-0 px-2 rounded"
                              onClick={(e) => handleUrlDelete("1")}
                            >
                              x
                            </button>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>

                {/* url2 */}
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">URL2</th>
                  <td className="border px-8 py-2 text-sm">
                    <div className="flex">
                      <Form.Control
                        type="text"
                        className="w-1/2"
                        value={link2}
                        readOnly={urlOpen2}
                        onChange={(e) => setLink2(e.target.value)}
                      />
                      <button onClick={(e) => setMdShow2(true)} type="button" className="btn btn-outline-primary btn-sm ml-2">
                        상품 찾기
                      </button>
                      <button onClick={(e) => handleUrlWrite("2")} type="button" className="btn btn-primary btn-sm ml-2">
                        직접 입력
                      </button>
                      <Modal show={mdShow2} onHide={() => setMdShow2(false)} aria-labelledby="example-modal-sizes-title-md">
                        <Modal.Header closeButton>
                          <div className="w-full">
                            <div className="sidebar-widget">
                              <h4 className="pro-sidebar-title">Search </h4>
                              <div className="pro-sidebar-search mt-15">
                                <div className="pro-sidebar-search-form" action="#">
                                  <input
                                    type="text"
                                    placeholder="Search here..."
                                    value={searchItem}
                                    onChange={(e) => setSearchItem(e.target.value)}
                                  />
                                  <button onClick={handleSearch}>
                                    <i className="ti-search" />
                                  </button>
                                </div>
                              </div>
                            </div>
                            <div className="flex mx-auto">
                              <div>
                                {currentItem &&
                                  currentItem.map((m) => (
                                    <div className="px-3 pt-1 mt-1 flex justify-between border-b">
                                      <div className="flex justify-start w-3/4">
                                        <img src={m.mainImage} className="w-12 h-12 rounded-full" alt="dp" />
                                        <div className="flex justify-start flex-wrap ml-4 pb-3 mt-2 ">
                                          <div className="flex justify-start font-bold">{m.name}</div>
                                          <div className="inline-flex w-full text-sm text-gray-500">
                                            <span className="pr-2">{`${m.brand} | ${m.model}`}</span>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="flex items-center text-black w-1/4">
                                        <button
                                          type="button"
                                          className="btn btn-warning btn-rounded py-1 px-3"
                                          onClick={(e) => handleProductChoice2(m.id)}
                                        >
                                          선택
                                        </button>
                                      </div>
                                    </div>
                                  ))}
                              </div>
                            </div>
                            <div className="pro-pagination-style text-center mt-30">
                              <Paginator
                                totalRecords={products.length}
                                pageLimit={pageLimit}
                                pageNeighbours={1} // 0, 1, 2
                                setOffset={setOffset}
                                currentPage={currentPage}
                                setCurrentPage={setCurrentPage}
                                pageContainerClass="mb-0 mt-0"
                                pagePrevText="«"
                                pageNextText="»"
                              />
                            </div>
                          </div>
                        </Modal.Header>
                      </Modal>
                    </div>
                    <div className="flex mt-2">
                      <img src={linkImage2 && linkImage2} className="w-64 h-32" />
                      <div className="ml-12 flex-row-left">
                        <div className="w-16 font-bold">첨부파일</div>
                        <div className="mt-3 h-8 flex flex-row table-text relative">
                          <form
                            onSubmit={() => {
                              console.log("Submitted");
                            }}
                            encType="multipart/form-data"
                          >
                            <input
                              name="document"
                              type="file"
                              accept="image/*"
                              method="POST"
                              onChange={(e) => handleUrlImageChange(e, "2")}
                            />
                          </form>
                          {linkImage2 ? (
                            <button
                              className="ml-3 bg-gray-400 hover:bg-gray-500 text-gray-800 font-bold py-0 px-2 rounded"
                              onClick={(e) => handleUrlDelete("2")}
                            >
                              x
                            </button>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>

                {/* url3 */}
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">URL3</th>
                  <td className="border px-8 py-2 text-sm">
                    <div className="flex">
                      <Form.Control
                        type="text"
                        className="w-1/2"
                        value={link3}
                        readOnly={urlOpen3}
                        onChange={(e) => setLink3(e.target.value)}
                      />
                      <button onClick={(e) => setMdShow3(true)} type="button" className="btn btn-outline-primary btn-sm ml-2">
                        상품 찾기
                      </button>
                      <button onClick={(e) => handleUrlWrite("3")} type="button" className="btn btn-primary btn-sm ml-2">
                        직접 입력
                      </button>
                      <Modal show={mdShow3} onHide={() => setMdShow3(false)} aria-labelledby="example-modal-sizes-title-md">
                        <Modal.Header closeButton>
                          <div className="w-full">
                            <div className="sidebar-widget">
                              <h4 className="pro-sidebar-title">Search </h4>
                              <div className="pro-sidebar-search mt-15">
                                <div className="pro-sidebar-search-form" action="#">
                                  <input
                                    type="text"
                                    placeholder="Search here..."
                                    value={searchItem}
                                    onChange={(e) => setSearchItem(e.target.value)}
                                  />
                                  <button onClick={handleSearch}>
                                    <i className="ti-search" />
                                  </button>
                                </div>
                              </div>
                            </div>
                            <div className="flex mx-auto">
                              <div>
                                {currentItem &&
                                  currentItem.map((m) => (
                                    <div className="px-3 pt-1 mt-1 flex justify-between border-b">
                                      <div className="flex justify-start w-3/4">
                                        <img src={m.mainImage} className="w-12 h-12 rounded-full" alt="dp" />
                                        <div className="flex justify-start flex-wrap ml-4 pb-3 mt-2 ">
                                          <div className="flex justify-start font-bold">{m.name}</div>
                                          <div className="inline-flex w-full text-sm text-gray-500">
                                            <span className="pr-2">{`${m.brand} | ${m.model}`}</span>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="flex items-center text-black w-1/4">
                                        <button
                                          type="button"
                                          className="btn btn-warning btn-rounded py-1 px-3"
                                          onClick={(e) => handleProductChoice3(m.id)}
                                        >
                                          선택
                                        </button>
                                      </div>
                                    </div>
                                  ))}
                              </div>
                            </div>
                            <div className="pro-pagination-style text-center mt-30">
                              <Paginator
                                totalRecords={products.length}
                                pageLimit={pageLimit}
                                pageNeighbours={1} // 0, 1, 2
                                setOffset={setOffset}
                                currentPage={currentPage}
                                setCurrentPage={setCurrentPage}
                                pageContainerClass="mb-0 mt-0"
                                pagePrevText="«"
                                pageNextText="»"
                              />
                            </div>
                          </div>
                        </Modal.Header>
                      </Modal>
                    </div>
                    <div className="flex mt-2">
                      <img src={linkImage3 && linkImage3} className="w-64 h-32" />
                      <div className="ml-12 flex-row-left">
                        <div className="w-16 font-bold">첨부파일</div>
                        <div className="mt-3 h-8 flex flex-row table-text relative">
                          <form
                            onSubmit={() => {
                              console.log("Submitted");
                            }}
                            encType="multipart/form-data"
                          >
                            <input
                              name="document"
                              type="file"
                              accept="image/*"
                              method="POST"
                              onChange={(e) => handleUrlImageChange(e, "3")}
                            />
                          </form>
                          {linkImage3 ? (
                            <button
                              className="ml-3 bg-gray-400 hover:bg-gray-500 text-gray-800 font-bold py-0 px-2 rounded"
                              onClick={(e) => handleUrlDelete("3")}
                            >
                              x
                            </button>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
              </table>

              {/* 내용 등록 */}
              <table className="shadow-sm bg-white w-full mt-4">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">제목</th>
                  <td className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
                  </td>
                </tr>
              </table>
              <div>
                <div className="border text-sm bg-white">
                  <div className="flex w-full my-3">
                    <MultiContentImages images={contentImages} handleImageClick={handleImageClick} />
                    <div className="ml-12 flex-row-left">
                      <div className="w-16 font-bold">첨부파일</div>
                      <div className="mt-3 h-8 flex flex-row table-text relative">
                        <form
                          onSubmit={() => {
                            console.log("Submitted");
                          }}
                          encType="multipart/form-data"
                        >
                          <input name="document" multiple type="file" method="POST" onChange={handleContentImageChange} />
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                {/* 내용 소개 */}
                <div className="row">
                  {/* 미리보기 */}
                  <div className="col-md-4 overflow-x-auto">
                    <div className="mt-6 border-gray-200">
                      <div className="w-full">
                        <div
                          className="max-w-sm table-intro table-responsive  cart-table-intro w-full border p-1 bg-white"
                          introeditable="true"
                          dangerouslySetInnerHTML={{ __html: content }}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="col-md-8">
                    <div className="mt-6 border-gray-200">
                      {/* Editor */}
                      {/* menubar: false, */}
                      <div className="mt-6 sm:mt-5">
                        <Editor
                          apiKey="qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc"
                          initialValue={content}
                          value={content}
                          init={{
                            height: 500,
                          }}
                          onEditorChange={handleEditorChange}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* 홈화면 체크 */}
              <table className="shadow-sm bg-white w-full mt-4">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">홈화면</th>
                  <td className="border px-8 py-2 text-sm">
                    <div className="form-check">
                      <label className="form-check-label">
                        <input type="checkbox" checked={visible} onChange={(e) => setVisible(!visible)} className="form-check-input" />
                        <i className="input-helper" />
                      </label>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 grid-margin mt-10">
        <div className="row justify-end mr-1">
          <button type="button" className="btn btn-primary mr-2" onClick={handleCreate}>
            저장
          </button>
          <button className="btn btn-light" onClick={goBack}>
            취소
          </button>
        </div>
      </div>
    </>
  );
};

CreatePage.propTypes = {
  id: PropTypes.string.isRequired,
};

// 내용에 들어가는 이미지
const MultiContentImages = ({ images, handleImageClick }) => {
  // 이미지 삭제
  const handleClickImage = (e, index) => {
    handleImageClick(e, index);
  };

  if (!images || images === "") {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
  try {
    const detailImages = images;
    if (!detailImages || detailImages.length === 0) {
      return <img src={null} className="w-12 h-12" alt="" />;
    }
    const multiImages = detailImages.map((image, index) => {
      return (
        <div className="items-center">
          <img src={image} className="w-12 h-12 mr-2" alt="" />
          <button onClick={(e) => handleClickImage(image)} className="border border-blue-500 rounded-md text-blue-700 px-1">
            선택
          </button>
        </div>
      );
    });
    return multiImages;
  } catch (e) {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
};
export default CreatePage;
