import React from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import { AllLensReservations, Store } from "graphql/query/select";
import { FormatDateShort } from "components/FormatDate";

const ReactContentPage = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const { result } = AllLensReservations();

  let items;
  if (result) {
    items = result;
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  if (!items) return <div />;

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">앱 멤버 관리</li>
            <Link to="/app-users/reservation-table" className="breadcrumb-item active mt-1" aria-current="page">
              렌즈 예약 목록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between">
                <h4 className="card-title">렌즈 예약 목록</h4>
                <h4 className="card-title">{`Total : ${items.length}`}</h4>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      filterable
                      defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "구매매장",
                          id: "storeId",
                          accessor: (d) => d.storeId,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: [`${(<StoreName storeId={filter.value} />)}`],
                            }),
                          filterAll: true,
                          Cell: (row) => (
                            <Link to={`/app-users/lens-reservation-detail/${row.original.id}`} className="text-blue-700 font-medium">
                              <StoreName storeId={row.original.storeId} />
                            </Link>
                          ),
                        },
                        {
                          Header: "예약상품",
                          id: "productName",
                          accessor: (d) => d.product.name,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["productName"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                        {
                          Header: "예약자",
                          id: "userName",
                          accessor: (d) => d.user.name,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["userName"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                        {
                          Header: "예약신청일",
                          id: "createAt",
                          accessor: (d) => d.createAt,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["createAt"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{`${FormatDateShort(new Date(parseInt(row.value, 10)))}`}</div>,
                        },
                        {
                          Header: "방문예정일",
                          id: "reservationAt",
                          accessor: (d) => d.reservationAt,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["reservationAt"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                        {
                          Header: "상태",
                          id: "status",
                          accessor: (d) => d.status,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["status"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const StoreName = ({ storeId }) => {
  const { result, error } = Store(storeId);

  let storeData;
  if (!error && result) {
    storeData = result;
  }

  if (!storeData) return "";
  return storeData.name;
};

export default ReactContentPage;
