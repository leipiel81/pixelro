import React from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import { AllReservations, Store } from "graphql/query/select";
import { FormatDateShort } from "components/FormatDate";

const ReactContentPage = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const { result } = AllReservations();

  let items;
  if (result) {
    items = result;
    items = items.filter((f) => f.status !== "주문중");
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  if (!items) return <div />;

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">앱 멤버 관리</li>
            <Link to="/app-users/reservation-table" className="breadcrumb-item active mt-1" aria-current="page">
              픽업 예약 목록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between">
                <h4 className="card-title">픽업 예약 목록</h4>
                <h4 className="card-title">{`Total : ${items.length}`}</h4>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      filterable
                      defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "구매매장",
                          id: "inStoreId",
                          accessor: (d) => d.inStoreId,
                          filterAll: true,
                          Cell: (row) => (
                            <Link to={`/app-users/pickup-reservation-detail/${row.original.id}`} className="text-blue-700 font-medium">
                              <StoreName storeId={row.original.inStoreId} />
                            </Link>
                          ),
                        },
                        {
                          Header: "픽업매장",
                          id: "outStoreId",
                          accessor: (d) => d.outStoreId,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["outStoreId"],
                            }),
                          filterAll: true,
                          Cell: (row) => <StoreName storeId={row.original.outStoreId} />,
                        },
                        {
                          Header: "주문자",
                          id: "userName",
                          accessor: (d) => d.pay.user.name,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["userName"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                        {
                          Header: "수령자",
                          id: "receiverName",
                          accessor: (d) => d.pay.receiverName,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["receiverName"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                        {
                          Header: "주문상태",
                          id: "status",
                          accessor: (d) => d.status,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["status"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                        {
                          Header: "주문날짜",
                          id: "createAt",
                          accessor: (d) => d.createAt,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["createAt"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{`${FormatDateShort(new Date(parseInt(row.value, 10)))}`}</div>,
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const StoreName = ({ storeId }) => {
  const { result } = Store(storeId);

  let storeData;
  if (result) {
    storeData = result;
  }

  if (!storeData) return "";
  return storeData.name;
};

export default ReactContentPage;
