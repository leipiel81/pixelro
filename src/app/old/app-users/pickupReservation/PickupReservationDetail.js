import React from "react";
import PropTypes from "prop-types";
import { Reservation, Store, Order } from "graphql/query/select";
import { useHistory, Link } from "react-router-dom";
import { FormatDate } from "components/FormatDate";
import { PhoneFormatter } from "components/PhoneFormatter";
import useSettings from "stores/settings";
import { observer } from "mobx-react-lite";

const DetailPage = observer(({ match }) => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const { id } = match.params;

  const { result } = Reservation(id);
  let item;
  let orderList;
  if (result) {
    item = result;
    if (item.pay.orderList) {
      orderList = JSON.parse(item.pay.orderList);
    }
  }

  if (!item) return <div />;

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">앱 멤버 관리</li>
            <Link to="/app-users/pickup-reservation-table" className="breadcrumb-item active mt-1" aria-current="page">
              픽업 예약 목록
            </Link>
            <Link to={`/app-users/reservation-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              픽업 예약 정보
            </Link>
          </ol>
        </nav>
      </div>
      <ReservationInfo item={item} />
      <ReservationProductForm orderList={orderList} />
      <div className="flex">
        <div className="col-6 grid-margin">
          <InStoreInfo storeId={item.inStoreId} />
        </div>
        <div className="col-6 grid-margin">
          <OutStoreInfo storeId={item.outStoreId} />
        </div>
      </div>
    </div>
  );
});

// 주문자 정보
const ReservationInfo = ({ item }) => {
  return (
    <>
      <div className="col-12 grid-margin">
        {/* 구매자 정보 */}
        <div className="row mt-3">
          <div className="col-md-12">
            <div>
              <div className="mt-2 border-gray-200">
                <div>
                  <h3 className="text-lg leading-6 font-medium text-black">▶︎ 주문 정보</h3>
                </div>
                <div className="flex mt-6 sm:mt-5">
                  <table className="shadow-sm bg-white w-full mr-2">
                    <tr>
                      <td className="border px-8 py-2 text-sm bg-gray-200 w-1/3">구매자 정보</td>
                      <td className="border px-8 py-2 text-sm bg-gray-200 w-1/3">예약자 정보</td>
                      <td className="border px-8 py-2 text-sm bg-gray-200 w-1/3">구매 날짜</td>
                    </tr>
                    <tr>
                      <td className="border px-8 py-2 text-sm">
                        <div>{item.pay.name}</div>
                        <div>{PhoneFormatter(item.pay.phone)}</div>
                        <div>{item.pay.email}</div>
                      </td>
                      <td className="border px-8 py-2 text-sm">
                        <div>{item.pay.receiverName}</div>
                        <div>{PhoneFormatter(item.pay.receiverPhone1)}</div>
                      </td>
                      <td className="border px-8 py-2 text-sm">
                        <div> {FormatDate(new Date(parseInt(item.createAt, 10)))}</div>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

// 상품
const ReservationProductForm = ({ orderList }) => {
  const orderDataList = [];
  for (let i = 0; i < orderList.length; i += 1) {
    const { result, error } = Order(orderList[i]);
    if (!error && result) {
      orderDataList.push(result);
    }
  }
  if (orderDataList.length === 0) return <div />;

  return (
    <>
      <div className="col-12 grid-margin mt-4">
        {/* 예약 상품 내용 */}
        <div className="row">
          <div className="col-md-12">
            <div>
              <div className="mt-2 border-gray-200">
                <div>
                  <h3 className="text-lg leading-6 font-medium text-black">▶︎ 예약 상품</h3>
                </div>
                <div className="mt-6 sm:mt-5">
                  <table className="shadow-sm bg-white w-full">
                    <tr>
                      <td className="border px-2 py-2 text-sm">상품 이미지</td>
                      <td className="border px-2 py-2 text-sm">예약 상품 정보</td>
                      <td className="border px-2 py-2 text-sm">구매 상점 정보</td>
                      <td className="border px-2 py-2 text-sm">상품 배송 현황</td>
                    </tr>
                    {orderDataList.map((orderData) => (
                      <tr>
                        <td className="border px-2 py-2 text-sm">
                          <img className="h-24 w-24" src={orderData.product.mainImage} alt="" />
                        </td>
                        <td className="border px-2 py-2 text-sm">
                          <div>{`상품이름 : ${orderData.product.name}`}</div>
                          <div>{`선택옵션 : ${orderData.productOption}`}</div>
                          <div>{`구매수량 : ${orderData.productCount}`}</div>
                        </td>
                        <td className="border px-2 py-2 text-sm">
                          <div>{`상점이름 : ${orderData.product.store.name}`}</div>
                          <div>{`전화번호 : ${PhoneFormatter(orderData.product.store.tel)}`}</div>
                        </td>
                        <td className="border px-2 py-2 text-sm">
                          <div>{`${orderData.status}`}</div>
                          <div>{orderData.delivery ? `택배사 : ${orderData.delivery}` : ""}</div>
                          <div>{orderData.trackingNumber ? `${orderData.trackingNumber}` : ""}</div>
                        </td>
                      </tr>
                    ))}
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
// 구매매장 정보
const InStoreInfo = ({ storeId }) => {
  const { result, error } = Store(storeId);

  let item;
  if (!error && result) {
    item = result;
  }

  if (!item) return "";
  return (
    <>
      <div className="w-full">
        {/* 구매자 정보 */}
        <div className="row mt-3">
          <div className="col-md-12">
            <div>
              <div className="mt-2 border-gray-200">
                <div>
                  <h3 className="text-lg leading-6 font-medium text-black">▶︎ 구매 매장 정보</h3>
                </div>
                <div className="flex mt-6 sm:mt-5">
                  <table className="shadow-sm bg-white w-full mr-2">
                    <tr>
                      <td className="border px-8 py-2 text-sm w-1/3">
                        <div>{item.name}</div>
                        <div>{PhoneFormatter(item.tel)}</div>
                        <div>{`${item.post} ${item.address} ${item.detailAddress}`}</div>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

// 픽업매장 정보
const OutStoreInfo = ({ storeId }) => {
  const { result, error } = Store(storeId);

  let item;
  if (!error && result) {
    item = result;
  }

  if (!item) return "";
  return (
    <>
      <div className="w-full">
        {/* 구매자 정보 */}
        <div className="row mt-3">
          <div className="col-md-12">
            <div>
              <div className="mt-2 border-gray-200">
                <div>
                  <h3 className="text-lg leading-6 font-medium text-black">▶︎ 픽업 매장 정보</h3>
                </div>
                <div className="flex mt-6 sm:mt-5">
                  <table className="shadow-sm bg-white w-full mr-2">
                    <tr>
                      <td className="border px-8 py-2 text-sm w-1/3">
                        <div>{item.name}</div>
                        <div>{PhoneFormatter(item.tel)}</div>
                        <div>{`${item.post} ${item.address} ${item.detailAddress}`}</div>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
DetailPage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default DetailPage;
