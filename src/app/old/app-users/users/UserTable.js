import React from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import { AllUsers } from "graphql/query/select";
import { FormatDateShort } from "components/FormatDate";
import { PhoneFormatter } from "components/PhoneFormatter";

const ReactContentPage = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const { result } = AllUsers();

  let items;
  if (result) {
    items = result;
    items = items.filter((f) => f.withdraw === false);

    // 최근 등록날짜 순으로 나타내기
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  if (!items) return <div />;

  const handleTelFormat = (data) => {
    const tel = PhoneFormatter(data.value);
    return tel;
  };
  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">사용자 관리</li>
            <Link to="/app-users/user-table" className="breadcrumb-item active mt-1" aria-current="page">
              회원 목록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between card-title-wrap">
                <h4 className="card-title">회원 목록</h4>
                <span className="card-sub">{`Total : ${items.length}`}</span>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      className="card-wrap"
                      filterable
                      defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "이메일",
                          id: "email",
                          accessor: (d) => d.email,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["email"],
                            }),
                          filterAll: true,
                          Cell: (row) => (
                            <Link to={`/app-users/user-detail/${row.original.id}`} className="text-blue-700 font-medium">
                              {row.value}
                            </Link>
                          ),
                        },
                        {
                          Header: "이름",
                          id: "name",
                          accessor: (d) => d.name,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["name"] }),
                          filterAll: true,
                        },
                        {
                          Header: "전화번호",
                          id: "tel",
                          accessor: (d) => d.tel,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["tel"],
                            }),
                          filterAll: true,
                          Cell: handleTelFormat,
                        },
                        {
                          Header: "주소",
                          id: "address",
                          accessor: (d) => d.address,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["address"],
                            }),
                          filterAll: true,
                        },
                        {
                          Header: "가입날짜",
                          accessor: "createAt",
                          Filter: () => false,
                          Cell: (row) => `${FormatDateShort(new Date(parseInt(row.value, 10)))}`,
                        },
                        {
                          Header: "가입경로",
                          accessor: "sns",
                          Filter: () => false,
                          Cell: (row) => <div>{`${row.value}`}</div>,
                        },
                        {
                          Header: "최근접속일",
                          accessor: "signedAt",
                          Filter: () => false,
                          Cell: (row) => `${FormatDateShort(new Date(parseInt(row.value, 10)))}`,
                        },
                        {
                          Header: "",
                          accessor: "edit",
                          Filter: () => false,
                          Cell: (row) => (
                            <div className="edit-wrap">
                              <Link className="edit ti-pencil-alt" to={`/app-users/user-edit/${row.original.id}`}>
                                수정
                              </Link>
                            </div>
                          ),
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReactContentPage;
