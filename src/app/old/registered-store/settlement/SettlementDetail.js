/* eslint-disable prefer-destructuring */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { SETTLE_REPORT } from "graphql/gql/select";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Link, useHistory } from "react-router-dom";
import { FormatDate } from "components/FormatDate";
import { UPDATE_SETTLE_REPORT } from "graphql/gql/update";
import { Form, Button, Modal } from "react-bootstrap";
import { PhoneFormatter } from "components/PhoneFormatter";
import { SimpleNotification } from "components/SimpleNotification";

const DetailPage = ({ match }) => {
  const { id } = match.params;

  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  const { loading, error, data, refetch } = useQuery(SETTLE_REPORT, {
    fetchPolicy: "cache-and-network",
    variables: { id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    items = data.settleReport;
  }

  if (!items) return <div />;

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">등록 관리</li>
            <Link to="/registered-store/products-table" className="breadcrumb-item active mt-1" aria-current="page">
              상품 목록
            </Link>
            <Link to={`/registered-store/product-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              상품 정보
            </Link>
          </ol>
        </nav>
      </div>
      <StoreInfoForm items={items} />
      <SettlementForm items={items} refetch={refetch} />
    </div>
  );
};

// 상점 기본 정보
const StoreInfoForm = ({ items }) => {
  const { store } = items;

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상점 정산 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tbody>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">상점 이름</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {store.name}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">전화번호</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {PhoneFormatter(store.tel)}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">사업자 번호</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {store.businessNumber}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">거래은행</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {store.bankName}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">계좌번호</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {store.bankNumber}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">예금주</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {store.accountHolder}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

// 정산 정보
const SettlementForm = ({ items, refetch }) => {
  const [acceptShow, setAcceptShow] = useState(false);

  // 답변 값 변경
  const [settleReportUpdate, { data: updateData }] = useMutation(UPDATE_SETTLE_REPORT, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      console.log(updateData);
      refetch();
    }
  }, [updateData]);

  // 모달에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleModalAccept = () => {
    const date = FormatDate(new Date());
    settleReportUpdate({ variables: { id: items.id, settlementAt: `${date}`, status: "완료" } });

    setAcceptShow(false);
  };
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 정산 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tbody>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">정산월</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm w-3/4">
                    {`${items.year}-${items.month}`}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">판매금액</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm w-3/4">
                    {items.productTotal.toLocaleString()}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">픽셀로쿠폰</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm w-3/4">
                    {items.pixelroCouponDiscount.toLocaleString()}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">상점쿠폰</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm w-3/4">
                    {items.storeCouponDiscount.toLocaleString()}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">포인트</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm w-3/4">
                    {items.point.toLocaleString()}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">정산금액</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm w-3/4">
                    {items.finalPrice.toLocaleString()}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">상태</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm w-3/4">
                    {items.status === "신청" ? (
                      <div>
                        <button onClick={(e) => setAcceptShow(true)} type="button" className="btn btn-outline-success btn-sm mr-1">
                          완료
                        </button>
                      </div>
                    ) : (
                      <div>완료</div>
                    )}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      {/* 승인 Modal Start */}
      <Modal size="sm" show={acceptShow} onHide={() => setAcceptShow(false)} aria-labelledby="example-modal-sizes-title-sm">
        <Modal.Header closeButton>
          <Modal.Title>정산 완료 확인</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <p>정산처리가 확인되었습니까?</p>
        </Modal.Body>

        <Modal.Footer className="flex-wrap">
          <Button variant="success btn-sm m-2" onClick={handleModalAccept}>
            확인
          </Button>
          <Button variant="light btn-sm m-2" onClick={() => setAcceptShow(false)}>
            취소
          </Button>
        </Modal.Footer>
      </Modal>
      {/* Modal Ends */}
    </div>
  );
};

DetailPage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default DetailPage;
