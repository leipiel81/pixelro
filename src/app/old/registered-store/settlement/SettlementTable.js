import React, { useState, useEffect } from "react";
import ReactTable from "react-table";
import { useHistory, Link } from "react-router-dom";
import matchSorter from "match-sorter";
import { useQuery, useMutation } from "@apollo/react-hooks";
import useSettings from "stores/settings";
import { Button, Modal } from "react-bootstrap";
import { SimpleNotification } from "components/SimpleNotification";
import { ALL_SETTLE_REPORTS } from "graphql/gql/select";
import { UPDATE_SETTLE_REPORT } from "graphql/gql/update";
import { FormatDateShort, FormatDate } from "components/FormatDate";

const ReactContentPage = () => {
  const [acceptShow, setAcceptShow] = useState(false);
  const [clickId, setClickId] = useState();
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  // 답변 값 변경
  const [settleReportUpdate, { data: updateData }] = useMutation(UPDATE_SETTLE_REPORT, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      refetch();
    }
  }, [updateData]);

  const { loading, error, data, refetch } = useQuery(ALL_SETTLE_REPORTS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data) {
    items = data.allSettleReports;

    // 최근 등록날짜 순으로 나타내기
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  // 모달에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleModalAccept = () => {
    const date = FormatDate(new Date());
    settleReportUpdate({ variables: { id: clickId, settlementAt: `${date}`, status: "완료" } });

    setAcceptShow(false);
  };

  // 화면에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleAcceptButtonClick = (id) => {
    setClickId(id);
    setAcceptShow(true);
  };

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">등록 관리</li>
            <Link to="/registered-store/settlement-request-table" className="breadcrumb-item active mt-1" aria-current="page">
              수익 신청 목록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">수익 신청 목록</h4>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      filterable
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "정산월",
                          id: "year",
                          accessor: (d) => `${d.year}-${d.month}`,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["year"] }),
                          filterAll: true,
                          Cell: (row) => <div>{`${row.original.year}-${row.original.month}`}</div>,
                        },
                        {
                          Header: "상점",
                          id: "storeName",
                          accessor: (d) => d.store.name,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["storeName"] }),
                          filterAll: true,
                          Cell: (row) => (
                            <Link
                              to={`/registered-store/settlement-request-detail/${row.original.id}`}
                              className="text-blue-700 font-medium"
                            >
                              {row.original.store.name}
                            </Link>
                          ),
                        },
                        {
                          Header: "판매금액",
                          id: "productTotal",
                          accessor: (d) => d.productTotal,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["productTotal"] }),
                          filterAll: true,
                          Cell: (row) => row.value.toLocaleString(),
                        },
                        {
                          Header: "픽셀로쿠폰",
                          id: "pixelroCouponDiscount",
                          accessor: (d) => d.pixelroCouponDiscount,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["pixelroCouponDiscount"] }),
                          filterAll: true,
                          Cell: (row) => row.value.toLocaleString(),
                        },
                        {
                          Header: "상점쿠폰",
                          id: "storeCouponDiscount",
                          accessor: (d) => d.storeCouponDiscount,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["storeCouponDiscount"] }),
                          filterAll: true,
                          Cell: (row) => row.value.toLocaleString(),
                        },
                        {
                          Header: "포인트",
                          id: "point",
                          accessor: (d) => d.point,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["point"] }),
                          filterAll: true,
                          Cell: (row) => row.value.toLocaleString(),
                        },
                        {
                          Header: "정산금액",
                          id: "finalPrice",
                          accessor: (d) => d.finalPrice,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["finalPrice"] }),
                          filterAll: true,
                          Cell: (row) => <div className="text-red-500">{row.value.toLocaleString()}</div>,
                        },
                        {
                          Header: "신청날짜",
                          id: "createAt",
                          accessor: (d) => FormatDateShort(new Date(parseInt(d.createAt, 10))),
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["createAt"],
                            }),
                          filterAll: true,
                          Cell: (row) => <div>{row.value}</div>,
                        },
                        {
                          Header: "",
                          accessor: "",
                          Filter: () => false,
                          Cell: (row) => (
                            <>
                              {row.original.status === "신청" ? (
                                <div>
                                  <button
                                    onClick={(e) => handleAcceptButtonClick(row.original.id)}
                                    type="button"
                                    className="btn btn-outline-success btn-sm mr-1"
                                  >
                                    완료
                                  </button>
                                </div>
                              ) : (
                                <div>완료</div>
                              )}
                            </>
                          ),
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
              {/* 승인 Modal Start */}
              <Modal size="sm" show={acceptShow} onHide={() => setAcceptShow(false)} aria-labelledby="example-modal-sizes-title-sm">
                <Modal.Header closeButton>
                  <Modal.Title>정산 완료 확인</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <p>정산처리가 확인되었습니까?</p>
                </Modal.Body>

                <Modal.Footer className="flex-wrap">
                  <Button variant="success btn-sm m-2" onClick={handleModalAccept}>
                    확인
                  </Button>
                  <Button variant="light btn-sm m-2" onClick={() => setAcceptShow(false)}>
                    취소
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Modal Ends */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReactContentPage;
