import React from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import { AllProducts } from "graphql/query/select";
import { FormatDateShort } from "components/FormatDate";

const ReactContentPage = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const { result } = AllProducts();
  // const items = result;
  let items;

  if (result) {
    items = result.filter((f) => f.visible === true);
    // 최근 등록날짜 순으로 나타내기
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  if (!items) return <div />;

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">등록 관리</li>
            <Link to="/registered-store/products-table" className="breadcrumb-item active mt-1" aria-current="page">
              상품 목록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between">
                <h4 className="card-title">상품 목록</h4>
                <h4 className="card-title">{`Total : ${items.length}`}</h4>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      filterable
                      defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "상품명",
                          id: "name",
                          accessor: (d) => d.name,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["name"] }),
                          filterAll: true,
                          Cell: (row) => (
                            <Link to={`/registered-store/product-detail/${row.original.id}`} className="text-blue-700 font-medium">
                              {row.value}
                            </Link>
                          ),
                        },
                        {
                          Header: "렌즈샵",
                          id: "storeName",
                          accessor: (d) => d.store.name,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["storeName"],
                            }),
                          filterAll: true,
                          Cell: (row) => row.original.store.name,
                        },
                        {
                          Header: "브랜드",
                          id: "brand",
                          accessor: (d) => d.brand,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["brand"],
                            }),
                          filterAll: true,
                        },
                        {
                          Header: "모델명",
                          id: "model",
                          accessor: (d) => d.model,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["model"],
                            }),
                          filterAll: true,
                        },
                        {
                          Header: "등록날짜",
                          accessor: "createAt",
                          Filter: () => false,
                          Cell: (row) => <div>{`${FormatDateShort(new Date(parseInt(row.value, 10)))}`}</div>,
                        },
                        {
                          Header: "",
                          accessor: "edit",
                          Filter: () => false,
                          Cell: (row) => (
                            <div>
                              <Link className="ti-pencil-alt" to={`/registered-store/product-edit/${row.original.id}`}>
                                수정하기
                              </Link>
                            </div>
                          ),
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReactContentPage;
