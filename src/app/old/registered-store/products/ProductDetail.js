/* eslint-disable prefer-destructuring */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Product } from "graphql/query/select";
import { UPDATE_PRODUCT } from "graphql/gql/update";
import { Link, useHistory } from "react-router-dom";
import { FormatDate } from "components/FormatDate";
import { Form, Button, Modal } from "react-bootstrap";
import { useMutation } from "@apollo/react-hooks";
import { PhoneFormatter } from "components/PhoneFormatter";
import { SimpleNotification } from "components/SimpleNotification";

const DetailPage = ({ match }) => {
  const { id } = match.params;

  const { result } = Product(id);

  const items = result;

  const [deleteShow, setDeleteShow] = useState(false);

  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  // 상품 판매종료 & 삭제(숨기기) 업데이트
  const [productUpdate, { data: updateData }] = useMutation(UPDATE_PRODUCT, {
    onError(err) {
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });
  useEffect(() => {
    if (updateData !== undefined) {
      goBack();
    }
  }, [updateData]);

  const handleModalDelete = () => {
    setDeleteShow(false);
    productUpdate({
      variables: {
        id,
        visible: false,
      },
    });
  };

  if (!items) return <div />;
  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">등록 관리</li>
            <Link to="/registered-store/products-table" className="breadcrumb-item active mt-1" aria-current="page">
              상품 목록
            </Link>
            <Link to={`/registered-store/product-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              상품 정보
            </Link>
          </ol>
        </nav>
      </div>
      <InfoForm items={items} />
      <CategoryForm items={items} />
      <DetailForm items={items} />
      <OnSaleForm items={items} />
      <SubInfoForm items={items} />
      <AdminInfoForm items={items} />
      <div className="col-12 grid-margin mt-3">
        <div className="row justify-between mr-1">
          <button className="btn btn-danger mt-2" onClick={(e) => setDeleteShow(true)}>
            삭제하기
          </button>
          <Link className="btn btn-primary mt-2 " to={`/registered-store/product-edit/${id}`}>
            수정하기
          </Link>
        </div>
        {/* Modal Starts  */}
        <Modal show={deleteShow} onHide={() => setDeleteShow(false)} aria-labelledby="example-modal-sizes-title-md">
          <Modal.Header closeButton>
            <Modal.Title>해당 상품을 삭제 하시겠습니까?</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <p>삭제를 원하시면 확인 버튼을 눌러주세요.</p>
          </Modal.Body>

          <Modal.Footer className="flex-wrap">
            <Button variant="btn btn-primary m-2" onClick={handleModalDelete}>
              확인
            </Button>
            <Button variant="btn btn-light m-2" onClick={() => setDeleteShow(false)}>
              취소
            </Button>
          </Modal.Footer>
        </Modal>
        {/* Modal Ends */}
      </div>
    </div>
  );
};

// 상품 기본 정보
const InfoForm = ({ items }) => {
  let keywordCheck = false;
  if (items.keyword) {
    keywordCheck = JSON.parse(items.keyword).length !== 0;
  }

  let optionCheck = false;
  if (items.option) {
    optionCheck = JSON.parse(items.option).length !== 0;
  }

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상품 기본 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tbody>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">상품명</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.name}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">요약 설명</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {items.description}
                  </td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">가격</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{items.price}</td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">할인율</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{items.discount}%</td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">브랜드</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{items.brand}</td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">모델명</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{items.model}</td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">옵션</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    <div className="flex">
                      {optionCheck &&
                        JSON.parse(items.option).map((m) => (
                          <div key={m.code}>
                            <div className="flex py-2">
                              <span className="border p-2 mr-1">
                                상품코드 : {m.code} , 수량 : {m.stock} , 추가금액 : {m.addPrice} , 판매수량 : {m.sale}
                              </span>
                            </div>
                          </div>
                        ))}
                    </div>
                  </td>
                </tr>

                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">키워드</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    <div className="flex">
                      {keywordCheck &&
                        JSON.parse(items.keyword).map((m) => (
                          <div className="flex" key={m}>
                            <span className="border p-2 mr-1">{m}</span>
                          </div>
                        ))}
                    </div>
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">등록날짜</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    {FormatDate(new Date(parseInt(items.createAt, 10)))}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

// 상품 카테고리 설정 정보
const CategoryForm = ({ items }) => {
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상품 카테고리 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tbody>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">카테고리</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm w-3/4">
                    {items.category}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">설정된 카테고리</th>
                  <td colSpan="3" className="w-3/4 border px-8 py-2 text-sm w-1/4">
                    <div className="w-full overflow-auto">
                      {items.categoryDetail &&
                        JSON.parse(items.categoryDetail).map((key) => {
                          return (
                            <div className="w-auto inline-block mr-2 bg-purple-300 rounded-md p-1 my-2 mr-2" key={key}>
                              {key}
                            </div>
                          );
                        })}
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

// 상품 상세 정보
const DetailForm = ({ items }) => {
  let productInfo;
  if (items.productInfo) {
    productInfo = JSON.parse(items.productInfo);
  }

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상품 상세 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tbody>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">종류</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[0]}</td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">소재</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[1]}</td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">치수</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[2]}</td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">제조년월</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[3]}</td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">제조사</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[4]}</td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">제조국</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[5]}</td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">취급시 주의사항</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[6]}</td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">품질보증기준</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{productInfo[7]}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

// 상품 판매 상태
const OnSaleForm = ({ items }) => {
  const [onSale, setOnSale] = useState(items.onSale);

  // 상품 판매종료  업데이트
  const [productUpdate, { data: updateData }] = useMutation(UPDATE_PRODUCT, {
    onError(err) {
      console.log("updateAdmin: err=", err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      console.log("판매종료");
    }
  }, [updateData]);

  const handleChangeOnSale = (value) => {
    setOnSale(value);
    productUpdate({
      variables: {
        id: items.id,
        onSale: value === "true",
      },
    });
  };
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상품 판매 상태</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tbody>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">판매 상태</th>
                  <td className="border px-8 py-2 text-sm">
                    <Form.Group className="mb-0">
                      <select
                        className="flex items-center border text-black w-1/3 "
                        id="openSelect"
                        value={onSale}
                        onChange={(e) => handleChangeOnSale(e.target.value)}
                      >
                        <option value="true">판매중</option>
                        <option value="false">판매종료</option>
                      </select>
                    </Form.Group>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};
// 상품 이미지 정보
const SubInfoForm = ({ items }) => {
  const [previewShow, setPreviewShow] = useState(false);

  let detailImages;
  if (items.detailImage) {
    detailImages = JSON.parse(items.detailImage);
  }
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상품 이미지</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tbody>
                <tr>
                  <td colSpan="6" className="bg-gray-200 border px-8 py-2 text-sm font-bold">
                    메인 이미지
                  </td>
                </tr>

                <tr>
                  <td colSpan="6" className="flex border px-2 py-2 text-sm">
                    {items.mainImage && <img className="h-24 w-24 m-3 border" alt="" src={items.mainImage} />}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tbody>
                <tr>
                  <td colSpan="6" className="bg-gray-200 border px-8 py-2 text-sm font-bold">
                    <div className="flex justify-between">
                      <div>상세 이미지</div>
                      <div>
                        <button onClick={(e) => setPreviewShow(true)}>preview</button>
                      </div>
                    </div>
                  </td>
                </tr>

                <tr>
                  <td colSpan="6" className="flex border px-2 py-2 text-sm">
                    {detailImages && detailImages.map((m) => <img className="h-24 w-24 m-3 border" alt="" src={`${m}`} key={m} />)}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      {/* Modal Start */}
      <Modal show={previewShow} onHide={() => setPreviewShow(false)} aria-labelledby="example-modal-sizes-title-lg">
        <Modal.Header closeButton>
          <div>{detailImages && detailImages.map((m) => <img className="w-full" alt="" src={`${m}`} key={m} />)}</div>
        </Modal.Header>
      </Modal>
      {/* Modal Ends */}
    </div>
  );
};

const AdminInfoForm = ({ items }) => {
  const itemList = [];
  itemList.push({ label: "상점 이름", item: items.store.name });
  itemList.push({ label: "등록자 이름", item: items.member.name });
  itemList.push({ label: "등록자 이메일", item: items.member.email });
  itemList.push({
    label: "등록자 전화번호",
    item: PhoneFormatter(items.member.tel),
  });

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 등록자 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tbody>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">상점 이름</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    {items.store.name}
                  </td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">등록자 이름</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{items.member.name}</td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">등록자 이메일</th>
                  <td className="border px-8 py-2 text-sm w-4/12">{items.member.email}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

DetailPage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default DetailPage;
