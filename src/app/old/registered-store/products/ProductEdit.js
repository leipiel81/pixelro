/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Form } from "react-bootstrap";
import ReactTags from "react-tag-autocomplete";
import { SimpleNotification } from "components/SimpleNotification";
import { Product, FilterSettingKeyValue } from "graphql/query/select";
import { UPDATE_PRODUCT } from "graphql/gql/update";
import { MULTI_FILE, MULTI_FILE_RESIZE } from "graphql/gql/insert";

const EditPage = ({ match }) => {
  const { id } = match.params;
  const { result } = Product(id);
  const items = result;

  // 상품 카테고리 셋팅 값 가져오기
  let settingCategory = {};
  const filterSettingKeyvalue = FilterSettingKeyValue("settingCategory");
  if (filterSettingKeyvalue.result && filterSettingKeyvalue.result.length > 0) {
    settingCategory = filterSettingKeyvalue.result[0].value;
    settingCategory = JSON.parse(settingCategory);
  }

  return <div>{items && <EditForm items={items} settingCategory={settingCategory} />}</div>;
};

const EditForm = ({ items, settingCategory }) => {
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  // -- 데이터 값 저장 --
  const { id, option } = items;
  const [name, setName] = useState(items.name);
  const [price, setPrice] = useState(items.price);
  const [discount, setDiscount] = useState(items.discount);
  const [description, setDescription] = useState(items.description);
  const [brand, setBrand] = useState(items.brand);
  const [model, setModel] = useState(items.model);
  const [mainImage, setMainImage] = useState(items.mainImage);
  const [detailImage, setDetailImage] = useState(items.detailImage && JSON.parse(items.detailImage));

  const parseInfo = JSON.parse(items.productInfo);
  const [productInfo1, setProductInfo1] = useState(parseInfo[0]);
  const [productInfo2, setProductInfo2] = useState(parseInfo[1]);
  const [productInfo3, setProductInfo3] = useState(parseInfo[2]);
  const [productInfo4, setProductInfo4] = useState(parseInfo[3]);
  const [productInfo5, setProductInfo5] = useState(parseInfo[4]);
  const [productInfo6, setProductInfo6] = useState(parseInfo[5]);
  const [productInfo7, setProductInfo7] = useState(parseInfo[6]);
  const [productInfo8, setProductInfo8] = useState(parseInfo[7]);

  // <<<<<<<<<<<<<<< 카테고리
  const [category, setCategory] = useState(items.category);
  const tempC = items.categoryDetail ? JSON.parse(items.categoryDetail) : [];
  const [categoryDetail, setCategoryDetail] = useState(tempC);

  const keys = Object.keys(settingCategory);
  const [key1, setKey1] = useState("");
  const keys1 = Object.keys((settingCategory && settingCategory[category]) || []);
  const keys2 = Object.values((settingCategory && settingCategory[category] && settingCategory[category][key1]) || []);

  // 대 카테고리 변경
  const handleCategory = (value) => {
    if (value !== "") {
      setCategory(value);
      setKey1("");
      setCategoryDetail([]);
    }
  };
  // 설정 카테고리 추가
  const handleCategoryDetail = (value) => {
    setKey1("");

    if (categoryDetail) {
      const idxCateegory = categoryDetail.indexOf(value);

      if (idxCateegory === -1) {
        const tempCategoryDetail = categoryDetail;
        tempCategoryDetail.push(value);
        setCategoryDetail(tempCategoryDetail);
      }
    }
  };

  // 설정한 카테고리 삭제
  const handleCategoryDetailDelete = async (value) => {
    const tempCategoryDetail = categoryDetail.map((m) => m);
    const idx = categoryDetail.indexOf(value);

    if (idx > -1) tempCategoryDetail.splice(idx, 1);

    await setCategoryDetail(tempCategoryDetail);
  };

  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  // ----------옵션 값 저장

  // 옵션 값 저장
  let options = [];
  if (items.option && items.option !== []) {
    options = JSON.parse(items.option).map((m, index) => ({
      id: index + 1,
      code: m.code,
      stock: m.stock,
      addPrice: m.addPrice,
      sale: m.sale,
    }));
  }

  const [rows, setRows] = useState(options);

  const changeText = (id, key) => (e) => {
    const {
      target: { value },
    } = e;

    const tempRows = rows.map((row) => {
      if (row.id === id + 1) {
        if (key === "code") row.code = value;
        if (key === "stock") row.stock = value;
        if (key === "addPrice") row.addPrice = value;
      }
      return row;
    });

    setRows(tempRows);
  };

  const addRow = () => {
    const data = {
      id: rows.length + 1,
      code: "",
      stock: "",
      addPrice: "",
      sale: "0",
    };
    setRows([...rows, data]);
  };

  const allDeleteRow = () => {
    setRows([]);
  };

  const deleteRow = (id) => () => {
    const tempRows = rows.filter((row) => {
      return row.id !== id + 1;
    });

    const sortRows = tempRows.map((m, index) => ({
      id: index + 1,
      code: m.code,
      stock: m.stock,
      addPrice: m.addPrice,
      sale: m.sale,
    }));

    setRows(sortRows);
  };

  // ----------옵션 값 저장

  // 키워드 값 저장
  let tags = [];
  if (items.keyword && items.keyword !== []) {
    tags = JSON.parse(items.keyword).map((m) => ({ name: m }));
  }

  const [tagTest, setTagTest] = useState(tags);

  // 키워드 추가 삭제 기능 함수
  const handleDelete = (i) => {
    const tags = tagTest.slice(0);
    tags.splice(i, 1);
    setTagTest(tags);
  };

  const handleAddition = (tag) => {
    const tags = [].concat(tagTest, tag);
    setTagTest(tags);
  };

  // 상품 정보 업데이트
  const [productUpdate, { data }] = useMutation(UPDATE_PRODUCT, {
    onError(err) {
      console.log("updateAdmin: err=", err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });
  useEffect(() => {
    if (data !== undefined) {
      goBack();
    }
  }, [data]);

  const handleUpdate = () => {
    if (!name || rows.length === 0) {
      SimpleNotification({
        title: "",
        message: "필수항목을 모두 입력해주세요.",
      });
      return;
    }

    // 옵션값 있는지 확인
    for (let i = 0; i < rows.length; i += 1) {
      if (rows[i].stock === "" || rows[i].code === "") {
        SimpleNotification({
          title: "",
          message: "옵션을 입력해주세요.",
        });
        return;
      }
    }

    const infoAdd = `["${productInfo1}", "${productInfo2}", "${productInfo3}", "${productInfo4}", "${productInfo5}", "${productInfo6}", "${productInfo7}", "${productInfo8}"]`;

    // 옵션 정보 저장
    const optionAdd = `[${
      rows && rows.map((m) => `{"code" : "${m.code}", "stock" : "${m.stock}", "addPrice" : "${m.addPrice}", "sale" : "${m.sale}"}`)
    }]`;

    productUpdate({
      variables: {
        id,
        name,
        price,
        discount: Number(discount),
        description,
        productInfo: infoAdd,
        brand,
        model,
        option: optionAdd,
        category,
        categoryDetail: `[${categoryDetail.map((m) => `"${m}"`)}]`,
        mainImage,
        detailImage: `[${detailImage && detailImage.map((m) => `"${m}"`)}]`,
        keyword: `[${tagTest && tagTest.map((m) => `"${m.name}"`)}]`,
      },
    });
  };

  // 이미지 관련 함수들
  //  메인 이미지 저장
  const [mainImageUpload, { data: mainImageData }] = useMutation(MULTI_FILE_RESIZE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (mainImageData !== undefined) {
      const result = `${mainImageData.multiUploadResize[0].filename}`;
      setMainImage(result);
    }
  }, [mainImageData]);

  //  상세 이미지 저장
  const [detailImageUpload, { data: detailImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (detailImageData !== undefined) {
      const tempDetailImage = [];
      detailImage.map((m) => tempDetailImage.push(m));
      detailImageData.multiUpload.map((m) => tempDetailImage.push(m.filename));

      setDetailImage(tempDetailImage);
    }
  }, [detailImageData]);

  // 메인 이미지 변경
  const handleMainImageChange = async (e) => {
    const file = e.target.files[0];

    // 이미지 저장
    await mainImageUpload({ variables: { files: [file], size: `[${file.size}]` } });
  };

  // 상세이미지 추가
  const handleDetailImageChange = async (e) => {
    const { files } = e.target;

    const fileArray = [];
    if (files) {
      for (let i = 0; i < files.length; i += 1) {
        fileArray.push(files[i]);
      }
    }
    const filesSize = `[${fileArray.map((m) => m.size)}]`;
    // 바로 추가 보내기
    await detailImageUpload({ variables: { files: fileArray, size: filesSize } });
  };

  // 상세 이미지 삭제
  const handleImageDelete = async (image) => {
    const tempImage = detailImage.map((m) => m);
    const idx = tempImage.indexOf(image);
    if (idx > -1) tempImage.splice(idx, 1);

    await setDetailImage(tempImage);
  };

  // 배열값 이동
  const arrayMove = (arr, old_index, new_index) => {
    while (old_index < 0) {
      old_index += arr.length;
    }
    while (new_index < 0) {
      new_index += arr.length;
    }
    if (new_index >= arr.length) {
      let k = new_index - arr.length + 1;
      while ((k -= 1)) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing purposes
  };

  // 상세 이미지 왼쪽으로 이동
  const handleImageLeftMove = async (image) => {
    let tempImage = detailImage.map((m) => m);
    const idx = tempImage.indexOf(image);

    if (idx !== 0) {
      tempImage = arrayMove(tempImage, idx, idx - 1);
    }

    await setDetailImage(tempImage);
  };

  // 상세 이미지 오른쪽으로 이동
  const handleImageRightMove = async (image) => {
    let tempImage = detailImage.map((m) => m);
    const idx = tempImage.indexOf(image);

    if (idx !== tempImage.length - 1) {
      tempImage = arrayMove(tempImage, idx, idx + 1);
    }

    await setDetailImage(tempImage);
  };

  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">등록 관리</li>
            <Link to="/registered-store/products-table" className="breadcrumb-item active mt-1" aria-current="page">
              상품 목록
            </Link>
            <Link to={`/registered-store/product-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              상품 정보
            </Link>
            <Link to={`/registered-store/product-edit/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              상품 정보 수정
            </Link>
          </ol>
        </nav>
      </div>
      {/* 상품 기본 정보 */}
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상품 기본 정보</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">상품명</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={name} onChange={(e) => setName(e.target.value)} />
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">요약 설명</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={description} onChange={(e) => setDescription(e.target.value)} />
                  </td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">가격</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={price} onChange={(e) => setPrice(e.target.value)} />
                  </td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">할인율</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={discount} onChange={(e) => setDiscount(e.target.value)} />
                  </td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">브랜드</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={brand} onChange={(e) => setBrand(e.target.value)} />
                  </td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">모델명</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={model} onChange={(e) => setModel(e.target.value)} />
                  </td>
                </tr>

                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">옵션</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    <div className="flex mt-1 sm:mt-0 sm:col-span-1">
                      <div className="rounded-md shadow-sm">
                        <table>
                          <tbody>
                            {rows.map((d, i) => (
                              <tr key={i} className="border border-gray-500">
                                <td className="px-3 py-2">{i + 1}</td>
                                <div className="flex">
                                  <div>
                                    <td>상품코드 : </td>
                                    <td>
                                      <input type="text" className="px-1 py-2" onChange={changeText(i, "code")} value={d.code} />
                                    </td>
                                  </div>
                                </div>
                                <div className="flex">
                                  <div>
                                    <td>추가금액 : </td>
                                    <td>
                                      <input type="text" className="px-1 py-2" onChange={changeText(i, "addPrice")} value={d.addPrice} />
                                    </td>
                                  </div>
                                  <div>
                                    <td>수량 : </td>
                                    <td>
                                      <input type="text" className="px-1 py-2" onChange={changeText(i, "stock")} value={d.stock} />
                                    </td>
                                  </div>
                                </div>

                                <td className="px-3">
                                  <button onClick={deleteRow(i)}>삭제</button>
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="flex mt-3 sm:mt-2 sm:col-span-1">
                      <div>
                        <button onClick={addRow} className="btn btn-primary mr-2">
                          추가
                        </button>
                        <button onClick={allDeleteRow} className="btn btn-primary mr-2">
                          초기화
                        </button>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">키워드</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm  ">
                    <ReactTags tags={tagTest} allowNew handleDelete={handleDelete} handleAddition={handleAddition} />
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      {/* 상품 카테고리 설정 */}
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상품 카테고리 설정</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">대 카테고리</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm w-3/4">
                    <select value={category} onChange={(e) => handleCategory(e.target.value)} className="w-full">
                      <option value="">카테고리를 선택해주세요.</option>
                      <option value="안경">안경</option>
                      <option value="선글라스">선글라스</option>
                      <option value="콘택트렌즈">콘택트렌즈</option>
                      <option value="픽셀로제품">픽셀로제품</option>
                      <option value="안경렌즈">안경렌즈</option>
                      <option value="악세사리">악세사리</option>
                      <option value="식품">식품</option>
                      <option value="기타">기타</option>
                    </select>
                  </td>
                </tr>
                {category && (
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">중 카테고리</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm w-3/4">
                      <div className="w-full overflow-auto">
                        {keys1 &&
                          keys1.map((key) => {
                            return (
                              <div
                                className="w-auto inline-block mr-2 border border-purple-600 rounded-md p-1 my-2 mr-2"
                                onClick={() => setKey1(key)}
                              >
                                {key}
                              </div>
                            );
                          })}
                      </div>
                    </td>
                  </tr>
                )}
                {key1 && key1 !== "" && (
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">소 카테고리</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm w-3/4">
                      <div className="w-full overflow-auto">
                        {keys2 &&
                          keys2.map((key) => {
                            return (
                              <div
                                className="w-auto inline-block mr-2 border border-purple-600 rounded-md p-1 my-2 mr-2"
                                onClick={(e) => handleCategoryDetail(key)}
                              >
                                {key}
                              </div>
                            );
                          })}
                      </div>
                    </td>
                  </tr>
                )}
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/4">설정된 카테고리</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm w-3/4 break-words">
                    <div className="w-full overflow-auto">
                      {categoryDetail &&
                        categoryDetail.map((key) => {
                          return (
                            <div className="w-auto inline-block mr-2 bg-purple-300 rounded-md p-1 my-2 mr-2">
                              {key}
                              <button className="p-1" onClick={(e) => handleCategoryDetailDelete(key)}>
                                x
                              </button>
                            </div>
                          );
                        })}
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      {/* 상품 상세 정보 */}
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상품 상세 정보</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">종류</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={productInfo1} onChange={(e) => setProductInfo1(e.target.value)} />
                  </td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">소재</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={productInfo2} onChange={(e) => setProductInfo2(e.target.value)} />
                  </td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">치수</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={productInfo3} onChange={(e) => setProductInfo3(e.target.value)} />
                  </td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">제조년월</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={productInfo4} onChange={(e) => setProductInfo4(e.target.value)} />
                  </td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">제조사</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={productInfo5} onChange={(e) => setProductInfo5(e.target.value)} />
                  </td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">제조국</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={productInfo6} onChange={(e) => setProductInfo6(e.target.value)} />
                  </td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">취급시 주의사항</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={productInfo7} onChange={(e) => setProductInfo7(e.target.value)} />
                  </td>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">품질보증기준</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control type="text" value={productInfo8} onChange={(e) => setProductInfo8(e.target.value)} />
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      {/* 상품 이미지 등록 */}
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상품 이미지</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <td colSpan="6" className="bg-gray-200 border px-8 py-2 text-sm font-bold">
                    메인 이미지*
                  </td>
                </tr>
                <tr>
                  <td className="border text-sm">
                    <div className="flex my-3 ml-10">
                      <img src={mainImage && mainImage} className="w-64 h-64" />
                      <div className="ml-12 flex-row-left">
                        <div className="w-16 font-bold">첨부파일</div>
                        <div className="mt-3 h-8 flex flex-row table-text relative">
                          <form
                            onSubmit={() => {
                              console.log("Submitted");
                            }}
                            encType="multipart/form-data"
                          >
                            <input name="document" type="file" accept="image/*" method="POST" onChange={handleMainImageChange} />
                          </form>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <td colSpan="6" className="bg-gray-200 border px-8 py-2 text-sm font-bold">
                    상세 이미지
                  </td>
                </tr>
                <tr>
                  <td className="border text-sm">
                    <div className="flex my-3 ml-10">
                      <MultiImages
                        images={detailImage}
                        handleImageDelete={handleImageDelete}
                        handleImageLeftMove={handleImageLeftMove}
                        handleImageRightMove={handleImageRightMove}
                      />
                      <div className="ml-12 flex-row-left">
                        <div className="w-16 font-bold">첨부파일</div>
                        <div className="mt-3 h-8 flex flex-row table-text relative">
                          <form
                            onSubmit={() => {
                              console.log("Submitted");
                            }}
                            encType="multipart/form-data"
                          >
                            <input name="document" multiple type="file" method="POST" onChange={handleDetailImageChange} />
                          </form>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 grid-margin mt-3">
        <div className="row justify-end mr-1">
          <button type="button" className="btn btn-primary mr-2" onClick={handleUpdate}>
            저장
          </button>
          <button className="btn btn-light" onClick={goBack}>
            취소
          </button>
        </div>
      </div>
    </>
  );
};
EditPage.prototype = {
  id: PropTypes.string.isRequired,
};

// 상세 이미지 보여주기
const MultiImages = ({ images, handleImageDelete, handleImageLeftMove, handleImageRightMove }) => {
  // 이미지 삭제
  const handleDelete = (e, index) => {
    handleImageDelete(e, index);
  };
  const handleLeftMove = (e, index) => {
    handleImageLeftMove(e, index);
  };

  const handleRightMove = (e, index) => {
    handleImageRightMove(e, index);
  };

  if (!images || images === "") {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
  try {
    const detailImages = images;
    if (!detailImages || detailImages.length === 0) {
      return <img src={null} className="w-12 h-12" alt="" />;
    }
    const MultiImages = detailImages.map((image, index) => {
      return (
        <div>
          <img src={image} className="w-12 h-12 mr-2" alt="" />
          <button onClick={(e) => handleDelete(image)}>x</button>
          <div className={`flex ${index === 0 ? "justify-end" : index === detailImages.length - 1 ? "justify-start" : "justify-between"} `}>
            {index !== 0 && (
              <button onClick={(e) => handleLeftMove(image)} className="bg-blue-700 px-2 text-white ml-1">
                {"< "}
              </button>
            )}

            {index !== detailImages.length - 1 && (
              <button onClick={(e) => handleRightMove(image)} className="bg-blue-700 px-2 text-white mr-1">
                {" >"}
              </button>
            )}
          </div>
        </div>
      );
    });
    return MultiImages;
  } catch (e) {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
};
export default EditPage;
