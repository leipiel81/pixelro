/* eslint-disable no-use-before-define */
import React, { useState, useEffect } from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useHistory, Link } from "react-router-dom";
import { Button, Modal } from "react-bootstrap";
import { ALL_PRODUCT_REGISTERS } from "graphql/gql/select";
import { UPDATE_PRODUCT_REGISTER_REPLY } from "graphql/gql/update";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { FormatDateShort } from "components/FormatDate";
import { CREATE_PRODUCT_ADMIN } from "graphql/gql/insert";
import useSettings from "stores/settings";
import { SimpleNotification } from "components/SimpleNotification";

const ReactContentPage = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const [acceptShow, setAcceptShow] = useState(false);
  const [refuseShow, setRefuseShow] = useState(false);
  const [clickId, setClickId] = useState();

  const [replyValue, setReplyValue] = useState("");
  // 상품 등록
  const [productCreate, { data: createData }] = useMutation(CREATE_PRODUCT_ADMIN, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (createData !== undefined) {
      refetch();
    }
  }, [createData]);

  // 답변 값 변경
  const [replyUpdate, { data: updateData }] = useMutation(UPDATE_PRODUCT_REGISTER_REPLY, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      console.log(updateData);

      if (replyValue === "승인") {
        // 요청 상품 등록
        const product = items.filter((f) => f.id === clickId)[0];
        productCreate({
          variables: {
            name: product.name,
            price: product.price,
            discount: product.discount,
            description: product.description,
            productInfo: product.productInfo,
            brand: product.brand,
            model: product.model,
            option: product.option,
            mainImage: product.mainImage,
            detailImage: product.detailImage,
            category: product.category,
            categoryDetail: product.categoryDetail,
            keyword: product.keyword,
            storeId: product.store.id,
            memberId: product.member.id,
          },
        });
      }
      // 답변 값 수정 후에 useQuery 다시 호출
      refetch();
    }
  }, [updateData]);

  const { loading, error, data, refetch } = useQuery(ALL_PRODUCT_REGISTERS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return;
  }

  // 상품 요청 전체 데이터 저장
  const items = data.allProductRegisters;

  if (items) {
    // 최근 등록날짜 순으로 나타내기
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  // 화면에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleAcceptButtonClick = (id) => {
    setClickId(id);
    setAcceptShow(true);
  };

  const handleRefuseButtonClick = (id) => {
    setClickId(id);
    setRefuseShow(true);
  };

  // 모달에 있는 승인, 거절 버튼 클릭시 실행 함수
  const handleModalAccept = () => {
    const value = "승인";
    replyUpdate({ variables: { id: clickId, reply: value } });
    setReplyValue(value);

    setAcceptShow(false);
  };

  const handleModalRefuse = () => {
    const value = "거절";
    replyUpdate({ variables: { id: clickId, reply: value } });
    setReplyValue(value);

    setRefuseShow(false);
  };

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">등록 관리</li>
            <Link to="/registered-store/product-register-table" className="breadcrumb-item active mt-1" aria-current="page">
              상품 등록 요청 목록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <div className="flex justify-between">
                <h4 className="card-title">상품 등록 요청 목록</h4>
                <h4 className="card-title">{`Total : ${items.length}`}</h4>
              </div>
              <div className="row">
                <div className="col-12">
                  <div>
                    <ReactTable
                      data={items}
                      filterable
                      defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                      defaultPageSize={10}
                      columns={[
                        {
                          Header: "분류",
                          id: "place",
                          accessor: (d) => d.store.place,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["place"],
                            }),
                          filterAll: true,
                          Cell: (row) => row.original.store.place,
                        },
                        {
                          Header: "상품명",
                          id: "name",
                          accessor: (d) => d.name,
                          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["name"] }),
                          filterAll: true,
                          Cell: (row) => (
                            <Link to={`/registered-store/product-register-detail/${row.original.id}`} className="text-blue-700 font-medium">
                              {row.value}
                            </Link>
                          ),
                        },
                        {
                          Header: "가격",
                          id: "price",
                          accessor: (d) => d.price,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["price"],
                            }),
                          filterAll: true,
                          // Cell: handleTelFormat,
                        },
                        {
                          Header: "답변",
                          id: "reply",
                          accessor: (d) => d.reply,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["reply"],
                            }),
                          filterAll: true,
                        },
                        {
                          Header: "상점",
                          id: "storeName",
                          accessor: (d) => d.store.name,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["storeName"],
                            }),
                          filterAll: true,
                          Cell: (row) => row.original.store.name,
                        },
                        {
                          Header: "회원사",
                          id: "adminName",
                          accessor: (d) => d.member.name,
                          filterMethod: (filter, rows) =>
                            matchSorter(rows, filter.value, {
                              keys: ["adminName"],
                            }),
                          filterAll: true,
                          Cell: (row) => row.original.member.name,
                        },
                        {
                          Header: "요청날짜",
                          accessor: "createAt",
                          Filter: () => false,
                          Cell: (row) => <div>{`${FormatDateShort(new Date(parseInt(row.value, 10)))}`}</div>,
                        },
                        {
                          Header: "",
                          accessor: "",
                          Filter: () => false,
                          Cell: (row) => (
                            <>
                              {row.original.reply === "대기" && (
                                <div>
                                  <button
                                    onClick={(e) => handleAcceptButtonClick(row.original.id)}
                                    type="button"
                                    className="btn btn-outline-success btn-sm mr-1"
                                  >
                                    승인
                                  </button>
                                  <button
                                    onClick={(e) => handleRefuseButtonClick(row.original.id)}
                                    type="button"
                                    className="btn btn-outline-danger btn-sm"
                                  >
                                    거절
                                  </button>
                                </div>
                              )}
                            </>
                          ),
                        },
                      ]}
                      //
                    />
                  </div>
                </div>
              </div>
              {/* 승인 Modal Start */}
              <Modal size="sm" show={acceptShow} onHide={() => setAcceptShow(false)} aria-labelledby="example-modal-sizes-title-sm">
                <Modal.Header closeButton>
                  <Modal.Title>상품 등록 요청 승인</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <p>요청을 승인하시겠습니까?</p>
                </Modal.Body>

                <Modal.Footer className="flex-wrap">
                  <Button variant="success btn-sm m-2" onClick={handleModalAccept}>
                    승인
                  </Button>
                  <Button variant="light btn-sm m-2" onClick={() => setAcceptShow(false)}>
                    취소
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Modal Ends */}
              {/* 거절 Modal Start */}
              <Modal size="sm" show={refuseShow} onHide={() => setRefuseShow(false)} aria-labelledby="example-modal-sizes-title-sm">
                <Modal.Header closeButton>
                  <Modal.Title>상품 등록 요청 거절</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                  <p>요청을 거절하시겠습니까?</p>
                </Modal.Body>

                <Modal.Footer className="flex-wrap">
                  <Button variant="danger btn-sm m-2" onClick={handleModalRefuse}>
                    거절
                  </Button>
                  <Button variant="light btn-sm m-2" onClick={() => setRefuseShow(false)}>
                    취소
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Modal Ends */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReactContentPage;
