import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Form, Modal } from "react-bootstrap";
import ReactTags from "react-tag-autocomplete";
import Select from "react-select";
import { SimpleNotification } from "components/SimpleNotification";
import { Store } from "graphql/query/select";
import { UPDATE_STORE, UPDATE_STORE_INTRO } from "graphql/gql/update";
import { MULTI_FILE, MULTI_FILE_RESIZE } from "graphql/gql/insert";
import DaumPostcode from "react-daum-postcode";
import { Editor } from "@tinymce/tinymce-react";

const EditPage = ({ match }) => {
  const { id } = match.params;

  let items;
  if (id !== undefined && id !== "undefined") {
    const { result } = Store(id);
    items = result;
  }

  return <div>{items && <EditForm items={items} />}</div>;
};

const EditForm = ({ items }, props) => {
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  // --------------------------

  const [mdShow, setMdShow] = useState(false);

  // -- 데이터 값 저장 --
  const [name, setName] = useState(items.name);
  const [tel, setTel] = useState(items.tel);
  const [site, setSite] = useState(items.site);
  const [blog, setBlog] = useState(items.blog);
  const [onLine, setOnLine] = useState(items.onLine);
  const [offLine, setOffLine] = useState(items.offLine);
  const [image, setImage] = useState(items.image ? JSON.parse(items.image) : []);

  // 거래정보
  const [businessNumber, setBusinessNumber] = useState(items.businessNumber);
  const [bankNumber, setBankNumber] = useState(items.bankNumber);
  const [accountHolder, setAccountHolder] = useState(items.accountHolder);

  const [bankImage, setBankImage] = useState(items.bankImage);
  const [businessNumberImage, setBusinessNumberImage] = useState(items.bankImage);

  // 소개 정보
  const [intro, setIntro] = useState("");
  const [introImages, setIntroImages] = useState([]);
  useEffect(() => {
    fetch(items.intro)
      .then((res) => res.text())
      .then((body) => {
        setIntro(body);
      });
  }, []);

  let tempBankName;
  if (items.bankName) {
    tempBankName = { value: `${items.bankName}`, label: `${items.bankName}` };
  }

  const [bankName, setBankName] = useState(tempBankName);

  // 주소
  const [post, setPost] = useState(items.post);
  const [address, setAddress] = useState(items.address);
  const [detailAddress, setDetailAddress] = useState(items.detailAddress);
  const [gpsLatitude, setgpsLatitude] = useState(items.gpsLatitude);
  const [gpsLongitude, setGpsLongitude] = useState(items.gpsLongitude);

  // 키워드 값 저장
  let tags = [];
  if (items.keyword && items.keyword !== []) {
    tags = JSON.parse(items.keyword).map((m) => ({ name: m }));
  }

  const [tagTest, setTagTest] = useState(tags);

  // 수정 안하는 값들.
  const { id, star } = items;

  // ----------------------------

  // 키워드 추가 삭제 기능 함수
  const handleDelete = (i) => {
    const tags = tagTest.slice(0);
    tags.splice(i, 1);
    setTagTest(tags);
  };

  const handleAddition = (tag) => {
    const tags = [].concat(tagTest, tag);
    setTagTest(tags);
  };

  // 소개 글 등록
  const [storeIntroUpdate, { data: updateIntroData }] = useMutation(UPDATE_STORE_INTRO, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateIntroData !== undefined) {
      goBack();
    }
  }, [updateIntroData]);

  const [storeUpdate, { data: updateData }] = useMutation(UPDATE_STORE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (updateData !== undefined) {
      storeIntroUpdate({ variables: { id: items.id, intro: `${intro}` } });
    }
  }, [updateData]);

  const handleUpdate = () => {
    if (!name || !tel || !post || !address) {
      SimpleNotification({
        title: "",
        message: "기본 정보 입력해주세요.",
      });
      return;
    }

    if (!businessNumber || !bankName || !bankNumber || !accountHolder) {
      SimpleNotification({
        title: "",
        message: "거래 정보 입력해주세요.",
      });
      return;
    }

    storeUpdate({
      variables: {
        id,
        name,
        tel,
        post,
        address,
        detailAddress,
        onLine,
        offLine,
        category: "",
        keyword: `[${tagTest && tagTest.map((m) => `"${m.name}"`)}]`,
        site,
        blog,
        star,
        businessNumber,
        bankName: `${bankName ? bankName.value : ""}`,
        bankNumber,
        accountHolder,
        image: `[${image && image.map((m) => `"${m}"`)}]`,
        bankImage: bankImage ? `${bankImage}` : "",
        businessNumberImage: businessNumberImage ? `${businessNumberImage}` : "",
        gpsLatitude,
        gpsLongitude,
      },
    });
  };

  // 주소 값 저장
  const handlePostComplete = (data) => {
    console.log(data);
    let fullAddress = data.address;
    let extraAddress = "";
    let zonecode = "";

    if (data.addressType === "R") {
      if (data.bname !== "") {
        extraAddress += data.bname;
      }
      if (data.buildingName !== "") {
        extraAddress += extraAddress !== "" ? `, ${data.buildingName}` : data.buildingName;
      }
      fullAddress += extraAddress !== "" ? ` (${extraAddress})` : "";
    }

    zonecode = data.zonecode;

    setMdShow(false);
    setPost(zonecode);
    setAddress(fullAddress);

    // 다음 API 호출 ,
    const encodedFullAddress = encodeURIComponent(fullAddress);
    const URL = `https://dapi.kakao.com/v2/local/search/address.json?query=${encodedFullAddress}`;
    fetch(URL, {
      headers: { Authorization: "KakaoAK 27c0f7d907c519c3195db1360600eb68" },
    })
      .then((res) => {
        res.json().then((json) => {
          if (json && json.documents && json.documents.length > 0) {
            const { x, y } = json.documents[0];
            // DB에 저장 (API 코드 참조할 것)
            setGpsLongitude(x);
            setgpsLatitude(y);
          }
        });
      })
      .catch((err) => {
        console.log("error : ", err);
      });
  };

  // -------------------------------------------------- 이미지관련
  // 통장 & 사업자 등록증 사진 변경
  const [businessNumberImageUpload, { data: businessNumberImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (businessNumberImageData !== undefined) {
      const result = `${businessNumberImageData.multiUpload[0].filename}`;
      setBusinessNumberImage(result);
    }
  }, [businessNumberImageData]);

  // 통장 & 사업자 등록증 사진 변경
  const [bankImageUpload, { data: bankImageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (bankImageData !== undefined) {
      const result = `${bankImageData.multiUpload[0].filename}`;
      setBankImage(result);
    }
  }, [bankImageData]);

  // 통장사본 변경
  const handleBankImageChange = async (e) => {
    const file = e.target.files[0];

    // 이미지 저장
    await bankImageUpload({ variables: { files: file, size: `[${file.size}]` } });
  };

  // 사업자등록증 변경
  const handlebusinessNumberImageChange = async (e) => {
    const file = e.target.files[0];

    // 이미지 저장
    await businessNumberImageUpload({ variables: { files: file, size: `[${file.size}]` } });
  };
  //  상점 이미지 저장
  const [storeImageUpload, { data: storeImageData }] = useMutation(MULTI_FILE_RESIZE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (storeImageData !== undefined) {
      const tempDetailImage = [];
      image.map((m) => tempDetailImage.push(m));
      storeImageData.multiUploadResize.map((m) => tempDetailImage.push(m.filename));

      setImage(tempDetailImage);
    }
  }, [storeImageData]);

  // 상점 이미지 추가
  const handleDetailImageChange = async (e) => {
    const { files } = e.target;

    if (image.length >= 5) {
      SimpleNotification({
        title: "",
        message: "상점 이미지는 5개까지만 등록이 가능합니다.",
      });
      return;
    }

    if (files.length > 5) {
      SimpleNotification({
        title: "",
        message: "상점 이미지는 5개까지만 등록이 가능합니다.",
      });
      return;
    }

    const fileArray = [];
    if (files) {
      for (let i = 0; i < files.length; i += 1) {
        fileArray.push(files[i]);
      }
    }
    const filesSize = `[${fileArray.map((m) => m.size)}]`;
    // 바로 추가 보내기
    await storeImageUpload({ variables: { files: fileArray, size: filesSize } });
  };

  // 상점 이미지 삭제
  const handleImageDelete = async (value) => {
    const tempImage = image.map((m) => m);
    const idx = tempImage.indexOf(value);
    if (idx > -1) tempImage.splice(idx, 1);

    await setImage(tempImage);
  };

  // 배열값 이동
  const arrayMove = (arr, old_index, new_index) => {
    while (old_index < 0) {
      old_index += arr.length;
    }
    while (new_index < 0) {
      new_index += arr.length;
    }
    if (new_index >= arr.length) {
      let k = new_index - arr.length + 1;
      while ((k -= 1)) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing purposes
  };

  // 상점 이미지 왼쪽으로 이동
  const handleImageLeftMove = async (value) => {
    let tempImage = image.map((m) => m);
    const idx = tempImage.indexOf(value);

    if (idx !== 0) {
      tempImage = arrayMove(tempImage, idx, idx - 1);
    }

    await setImage(tempImage);
  };

  // 상세 이미지 오른쪽으로 이동
  const handleImageRightMove = async (value) => {
    let tempImage = image.map((m) => m);
    const idx = tempImage.indexOf(value);

    if (idx !== tempImage.length - 1) {
      tempImage = arrayMove(tempImage, idx, idx + 1);
    }

    await setImage(tempImage);
  };

  // 소개 관련

  const introUpdateFunction = async (tempDetailImage) => {
    let tempIntro = intro;
    // 이미지 저장과 동시에 editor에 표현
    for (let i = 0; i < tempDetailImage.length; i += 1) {
      tempIntro += `<p><img style="width : 100%;" src="${tempDetailImage[i]}" alt=""></p>`;
    }

    await setIntro(tempIntro);
  };

  // 이미지 저장
  const [introImageUpload, { data: imageData }] = useMutation(MULTI_FILE, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (imageData !== undefined) {
      const tempDetailImage = [];
      introImages.map((m) => tempDetailImage.push(m));
      imageData.multiUpload.map((m) => tempDetailImage.push(m.filename));

      // 이미지 저장과 동시에 editor에 표현
      introUpdateFunction(tempDetailImage);

      setIntroImages(tempDetailImage);
    }
  }, [imageData]);

  // 이미지 추가
  const handleImageChange = async (e) => {
    const { files } = e.target;

    const fileArray = [];
    if (files) {
      for (let i = 0; i < files.length; i += 1) {
        fileArray.push(files[i]);
      }
    }
    const filesSize = `[${fileArray.map((m) => m.size)}]`;
    // 바로 추가 보내기
    await introImageUpload({ variables: { files: fileArray, size: filesSize } });
  };
  // 이미지 선택
  const handleImageClick = async (image) => {
    const tempIntro = `${intro}<p><img style="width : 100%;" src="${image}" alt=""></p>`;
    setIntro(tempIntro);
  };
  const handleEditorChange = (value) => {
    setIntro(value);
  };
  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">등록 관리</li>
            <Link to="/old/registered-store/store-table" className="breadcrumb-item active mt-1" aria-current="page">
              상점 목록
            </Link>
            <Link to={`/old/registered-store/store-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              상점 정보
            </Link>
            <Link to={`/old/registered-store/store-edit/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              상점 정보 수정
            </Link>
          </ol>
        </nav>
      </div>
      <div>
        {/* 상점 기본 정보 */}
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상점 기본 정보</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm">분류</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm">
                      {items.place}
                    </td>
                  </tr>
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">상점 이름</th>
                    <td className="border px-8 py-2 text-sm w-4/12">
                      <Form.Control type="text" value={name} onChange={(e) => setName(e.target.value)} />
                    </td>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">전화번호</th>
                    <td className="border px-8 py-2 text-sm w-4/12">
                      <Form.Control type="text" value={tel} onChange={(e) => setTel(e.target.value)} />
                    </td>
                  </tr>
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm">주소</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm ">
                      <div className="flex mb-2">
                        <Form.Control className="w-2/12" type="text" value={post} readOnly placeholder="우편번호" />
                        <button onClick={(e) => setMdShow(true)} type="button" className="btn btn-outline-primary btn-sm ml-2">
                          주소찾기
                        </button>
                      </div>
                      <Form.Control className="mb-2" type="text" readOnly value={address} placeholder="주소" />
                      <Form.Control
                        type="text"
                        value={detailAddress}
                        placeholder="상세주소"
                        onChange={(e) => setDetailAddress(e.target.value)}
                      />

                      <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
                        <Modal.Header closeButton>
                          <DaumPostcode className="h-full" onComplete={handlePostComplete} {...props} />
                        </Modal.Header>
                      </Modal>
                      {/* Modal Ends */}
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>

        {/* 상점 부가 정보 */}
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상점 부가 정보</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  <tr className="m-0 p-0">
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">홈페이지 주소</th>
                    <td className="flex border-r px-8 py-2 text-sm">
                      <div className="w-28">
                        <Form.Control value="http://" readOnly />
                      </div>
                      <Form.Control type="text" value={site} onChange={(e) => setSite(e.target.value)} placeholder="www.example.com" />
                    </td>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">블로그 주소</th>
                    <td className="flex border-r px-8 py-2 text-sm">
                      <div className="w-28">
                        <Form.Control value="http://" readOnly />
                      </div>
                      <Form.Control type="text" value={blog} onChange={(e) => setBlog(e.target.value)} placeholder="www.example.com" />
                    </td>
                  </tr>
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm">키워드</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm  ">
                      <ReactTags tags={tagTest} allowNew handleDelete={handleDelete} handleAddition={handleAddition} />
                    </td>
                  </tr>
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm">온라인 / 오프라인</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm  ">
                      <div className="max-w-xs">
                        <span className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                          <div className="flex col-sm-9 col-form-label p-0">
                            <div className="form-check mr-3">
                              <label className="form-check-label">
                                <input type="checkbox" className="form-check-input" checked={onLine} onChange={(e) => setOnLine(!onLine)} />
                                <i className="input-helper" />
                                온라인
                              </label>
                            </div>
                            <div className="form-check">
                              <label className="form-check-label">
                                <input
                                  type="checkbox"
                                  checked={offLine}
                                  onChange={(e) => setOffLine(!offLine)}
                                  className="form-check-input"
                                />
                                <i className="input-helper" />
                                오프라인
                              </label>
                            </div>
                          </div>
                        </span>
                      </div>
                    </td>
                  </tr>
                  {/* 상점 이미지 등록 */}
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm">
                      상점 이미지 <br /> <div className="text-red-500 text-xs">* 최대 5개</div>
                    </th>
                    <td colSpan="3" className="border px-8 py-2 text-sm">
                      <div className="flex my-3 ml-10">
                        <MultiImages
                          images={image}
                          handleImageDelete={handleImageDelete}
                          handleImageLeftMove={handleImageLeftMove}
                          handleImageRightMove={handleImageRightMove}
                        />
                        <div className="ml-12 flex-row-left">
                          <div className="w-16 font-bold">첨부파일</div>
                          <div className="mt-3 h-8 flex flex-row table-text relative">
                            <form
                              onSubmit={() => {
                                console.log("Submitted");
                              }}
                              encType="multipart/form-data"
                            >
                              <input name="document" multiple type="file" method="POST" onChange={handleDetailImageChange} />
                            </form>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
        {/* 상점 거래 정보 */}
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상점 거래 정보</h3>
              </div>
              <div className="mt-6 sm:mt-5">
                <table className="shadow-sm bg-white w-full">
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">사업자 번호</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm ">
                      <Form.Control
                        className="w-1/2"
                        type="text"
                        value={businessNumber}
                        placeholder="사업자번호"
                        onChange={(e) => setBusinessNumber(e.target.value)}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm">거래은행</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm ">
                      <Select
                        className="w-1/2"
                        isOptionSelected
                        value={bankName}
                        onChange={(e) => setBankName(e)}
                        options={[
                          { value: "경남은행", label: "경남은행" },
                          { value: "광주은행", label: "광주은행" },
                          { value: "국민은행", label: "국민은행" },
                          { value: "국민은행(구주택은행)", label: "국민은행(구주택은행)" },
                          { value: "기업은행", label: "기업은행" },
                          { value: "농협은행", label: "농협은행" },
                          { value: "단위농협", label: "단위농협" },
                          { value: "대구은행", label: "대구은행" },
                          { value: "부산은행", label: "부산은행" },
                          { value: "새마을금고", label: "새마을금고" },
                          { value: "수협은행", label: "수협은행" },
                          { value: "신한은행", label: "신한은행" },
                          { value: "씨티은행", label: "씨티은행" },
                          { value: "외환은행", label: "외환은행" },
                          { value: "우리은행", label: "우리은행" },
                          { value: "우체국", label: "우체국" },
                          { value: "전북은행", label: "전북은행" },
                          { value: "차타드은행(구SC제일은행)", label: "차타드은행(구SC제일은행)" },
                          { value: "제주은행", label: "제주은행" },
                          { value: "하나은행", label: "하나은행" },
                          { value: "한국산업은행", label: "한국산업은행" },
                          { value: "카카오뱅크", label: "카카오뱅크" },
                          { value: "케이뱅크", label: "케이뱅크" },
                        ]}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm">계좌번호</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm ">
                      <Form.Control
                        className="w-1/2"
                        type="text"
                        value={bankNumber}
                        placeholder="계좌번호"
                        onChange={(e) => setBankNumber(e.target.value)}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm">예금주</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm ">
                      <div className="flex mb-2">
                        <Form.Control
                          className="w-1/2"
                          type="text"
                          value={accountHolder}
                          placeholder="예금주"
                          onChange={(e) => setAccountHolder(e.target.value)}
                        />
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm">통장사본</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm">
                      <div className="flex my-3 ml-10">
                        <img src={bankImage && bankImage} className="w-64 h-64" alt="" />
                        <div className="ml-12 flex-row-left">
                          <div className="w-16 font-bold">첨부파일</div>
                          <div className="mt-3 h-8 flex flex-row table-text relative">
                            <form
                              onSubmit={() => {
                                console.log("Submitted");
                              }}
                              encType="multipart/form-data"
                            >
                              <input name="document" type="file" accept="image/*" method="POST" onChange={handleBankImageChange} />
                            </form>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th className="bg-gray-200 border text-left px-8 py-2 text-sm">사업자등록증</th>
                    <td colSpan="3" className="border px-8 py-2 text-sm">
                      <div className="flex my-3 ml-10">
                        <img src={businessNumberImage && businessNumberImage} className="w-64 h-64" alt="" />
                        <div className="ml-12 flex-row-left">
                          <div className="w-16 font-bold">첨부파일</div>
                          <div className="mt-3 h-8 flex flex-row table-text relative">
                            <form
                              onSubmit={() => {
                                console.log("Submitted");
                              }}
                              encType="multipart/form-data"
                            >
                              <input
                                name="document"
                                type="file"
                                accept="image/*"
                                method="POST"
                                onChange={(e) => handlebusinessNumberImageChange(e, "businessNumber")}
                              />
                            </form>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
        {/* 상점 소개 */}
        <div className="row">
          <div className="col-md-12">
            <div className="mt-6 border-gray-200">
              <div>
                <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상점 소개</h3>
              </div>
              <div className="mt-6 border-gray-200">
                {/* 사진 등록 */}
                <div className="border text-sm bg-white">
                  <div className="flex w-full my-3">
                    <MultiIntroImages images={introImages} handleImageClick={handleImageClick} />
                    <div className="ml-12 flex-row-left">
                      <div className="w-16 font-bold">첨부파일</div>
                      <div className="mt-3 h-8 flex flex-row table-text relative">
                        <form
                          onSubmit={() => {
                            console.log("Submitted");
                          }}
                          encType="multipart/form-data"
                        >
                          <input name="document" multiple type="file" method="POST" onChange={handleImageChange} />
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                {/* 상점 소개 */}
                <div className="row">
                  {/* 미리보기 */}
                  <div className="col-md-4">
                    <div className="mt-6 border-gray-200">
                      <div className="w-full">
                        <div
                          className="max-w-sm table-intro table-responsive  cart-table-intro w-full border p-1 bg-white"
                          introeditable="true"
                          dangerouslySetInnerHTML={{ __html: intro }}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="col-md-8">
                    <div className="mt-6 border-gray-200">
                      {/* Editor */}
                      {/* menubar: false, */}
                      <div className="mt-6 sm:mt-5">
                        <Editor
                          apiKey="qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc"
                          initialValue={intro}
                          value={intro}
                          init={{
                            height: 500,
                          }}
                          onEditorChange={handleEditorChange}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 grid-margin mt-3">
        <div className="row justify-end mr-1">
          <button type="button" className="btn btn-primary mr-2" onClick={handleUpdate}>
            저장
          </button>
          <button className="btn btn-light" onClick={goBack}>
            취소
          </button>
        </div>
      </div>
    </>
  );
};

// 상점 이미지 보여주기
const MultiImages = ({ images, handleImageDelete, handleImageLeftMove, handleImageRightMove }) => {
  // 이미지 삭제
  const handleDelete = (e, index) => {
    handleImageDelete(e, index);
  };

  const handleLeftMove = (e, index) => {
    handleImageLeftMove(e, index);
  };

  const handleRightMove = (e, index) => {
    handleImageRightMove(e, index);
  };

  if (!images || images === "") {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
  try {
    const detailImages = images;
    if (!detailImages || detailImages.length === 0) {
      return <img src={null} className="w-12 h-12" alt="" />;
    }
    const multiImages = detailImages.map((image, index) => {
      return (
        <div>
          <img src={image} className="w-12 h-12 mr-2" alt="" />
          <button onClick={(e) => handleDelete(image)}>x</button>
          <div className={`flex ${index === 0 ? "justify-end" : index === detailImages.length - 1 ? "justify-start" : "justify-between"} `}>
            {index !== 0 && (
              <button onClick={(e) => handleLeftMove(image)} className="bg-blue-700 px-2 text-white ml-1">
                {"< "}
              </button>
            )}

            {index !== detailImages.length - 1 && (
              <button onClick={(e) => handleRightMove(image)} className="bg-blue-700 px-2 text-white mr-1">
                {" >"}
              </button>
            )}
          </div>
        </div>
      );
    });
    return multiImages;
  } catch (e) {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
};

// 소개 이미지
const MultiIntroImages = ({ images, handleImageClick }) => {
  // 이미지 삭제
  const handleClickImage = (e, index) => {
    handleImageClick(e, index);
  };

  if (!images || images === "") {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
  try {
    const detailImages = images;
    if (!detailImages || detailImages.length === 0) {
      return <img src={null} className="w-12 h-12" alt="" />;
    }
    const multiImages = detailImages.map((image, index) => {
      return (
        <div className="items-center">
          <img src={image} className="w-12 h-12 mr-2" alt="" />
          <button onClick={(e) => handleClickImage(image)} className="border border-blue-500 rounded-md text-blue-700 px-1">
            선택
          </button>
        </div>
      );
    });
    return multiImages;
  } catch (e) {
    return <img src={null} className="w-12 h-12" alt="" />;
  }
};
EditPage.prototype = {
  id: PropTypes.string.isRequired,
};

export default EditPage;
