import React, { useState } from "react";
import ReactTable from "react-table";
import matchSorter from "match-sorter";
import { useHistory, Link } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import useSettings from "stores/settings";
import { Tabs, Tab } from "react-bootstrap";
import { ALL_STORES } from "graphql/gql/select";
import { FormatDateShort } from "components/FormatDate";
import { PhoneFormatter } from "components/PhoneFormatter";
import { SimpleNotification } from "components/SimpleNotification";

const ReactContentPage = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const [key, setKey] = useState("안경원");

  // store 정보
  const { loading, error, data, refetch } = useQuery(ALL_STORES, {
    fetchPolicy: "cache-and-network",
    variables: { value: settings.storeId },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    console.log(error);
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data && data.allStores) {
    // 대기중 임시로 추가하는 상점 테이블에서 빼기
    items = data.allStores;
    items = items.filter((f) => f.id !== "1");
    items = items.filter((f) => f.storeRegister === "승인");
    items = items.filter((f) => f.withdraw === false);

    if (key) {
      items = items.filter((f) => f.place === key);
    }

    // 최근 등록날짜 순으로 나타내기
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  const handleChangeKey = (k) => {
    setKey(k);
    refetch();
  };

  if (!items) return <div />;

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">등록관리</li>
            <Link to="/old/registered-store/store-table" className="breadcrumb-item active mt-1" aria-current="page">
              상점 목록
            </Link>
          </ol>
        </nav>
      </div>
      {/* Tab */}
      <div className="my-4 pl-0">
        <Tabs id="controlled-tab-example" activeKey={key} onSelect={(k) => handleChangeKey(k)}>
          <Tab eventKey="안경원" title="안경원">
            <Table items={items} />
          </Tab>
          <Tab eventKey="시기능연구소" title="시기능연구소">
            <Table items={items} />
          </Tab>
          <Tab eventKey="안과" title="안과">
            <Table items={items} />
          </Tab>
          <Tab eventKey="렌즈샵" title="렌즈샵">
            <Table items={items} />
          </Tab>
          <Tab eventKey="상점" title="상점">
            <Table items={items} />
          </Tab>
          <Tab eventKey="도매" title="도매">
            <Table items={items} />
          </Tab>
        </Tabs>
      </div>
    </div>
  );
};

const Table = ({ items }) => {
  const handleTelFormat = (data) => {
    const tel = PhoneFormatter(data.value);
    return <div>{tel}</div>;
  };
  return (
    <div className="row">
      <div className="col-12">
        <div className="card">
          <div className="card-body">
            <div className="flex justify-between">
              <h4 className="card-title">상점 목록</h4>
              <h4 className="card-title">{`Total : ${items.length}`}</h4>
            </div>
            <div className="row">
              <div className="col-12">
                <div>
                  <ReactTable
                    data={items}
                    filterable
                    defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                    defaultPageSize={10}
                    columns={[
                      {
                        Header: "이름",
                        id: "name",
                        accessor: (d) => d.name,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["name"] }),
                        filterAll: true,
                        Cell: (row) => (
                          <Link to={`/old/registered-store/store-detail/${row.original.id}`} className="text-blue-700 font-medium">
                            {row.value}
                          </Link>
                        ),
                      },
                      {
                        Header: "장소",
                        id: "place",
                        accessor: (d) => d.place,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["place"] }),
                        filterAll: true,
                        Cell: (row) => <div>{row.value}</div>,
                      },
                      {
                        Header: "전화번호",
                        id: "tel",
                        accessor: (d) => d.tel,
                        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["tel"] }),
                        filterAll: true,
                        Cell: handleTelFormat,
                      },
                      {
                        Header: "주소",
                        id: "address",
                        accessor: (d) => d.address,
                        filterMethod: (filter, rows) =>
                          matchSorter(rows, filter.value, {
                            keys: ["address"],
                          }),
                        filterAll: true,
                      },
                      {
                        Header: "온/오프라인",
                        accessor: "onOffLine",
                        Filter: () => false,
                        Cell: (row) => (
                          <div className="row">
                            <div className="form-check  mx-3">
                              <label className="form-check-label">
                                <input type="checkbox" disabled checked={row.original.onLine} className="form-check-input" />
                                <i className="input-helper" />
                                온라인
                              </label>
                            </div>
                            <div className="form-check">
                              <label className="form-check-label">
                                <input type="checkbox" disabled checked={row.original.offLine} className="form-check-input" />
                                <i className="input-helper" />
                                오프라인
                              </label>
                            </div>
                          </div>
                        ),
                      },
                      {
                        Header: "등록날짜",
                        id: "createAt",
                        accessor: (d) => FormatDateShort(new Date(parseInt(d.createAt, 10))),
                        filterMethod: (filter, rows) =>
                          matchSorter(rows, filter.value, {
                            keys: ["createAt"],
                          }),
                        filterAll: true,
                        Cell: (row) => <div>{row.value}</div>,
                      },
                      {
                        Header: "",
                        accessor: "edit",
                        Filter: () => false,
                        Cell: (row) => (
                          <div>
                            <Link className="ti-pencil-alt" to={`/old/registered-store/store-edit/${row.original.id}`}>
                              수정하기
                            </Link>
                          </div>
                        ),
                      },
                    ]}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReactContentPage;
