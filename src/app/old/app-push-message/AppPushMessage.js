import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { ALL_USERS, ALL_PRODUCTS, ALL_NOTICE_BOARDS, ALL_SERVICE_CENTERS } from "graphql/gql/select";
import { SEND_MANY_USER_FCM } from "graphql/gql/update";
import Paginator from "react-hooks-paginator";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Form, Modal, Button } from "react-bootstrap";
import { PhoneFormatter } from "components/PhoneFormatter";
import useSettings from "stores/settings";
import { SimpleNotification } from "components/SimpleNotification";
import { observer } from "mobx-react-lite";

const AppPushMessage = observer(() => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  return (
    <div>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">앱 푸쉬 메세지</li>
          </ol>
        </nav>
      </div>
      <MesssageForm />
    </div>
  );
});

const MesssageForm = () => {
  const [users, setUsers] = useState([]);
  const [title, setTitle] = useState();
  const [message, setMessage] = useState();
  const [type, setType] = useState("매거진");
  const [targetUrl, setTargetUrl] = useState();

  const [magazineShow, setMagazineShow] = useState(false);
  const [eventShow, setEventShow] = useState(false);
  const [productShow, setProductShow] = useState(false);
  const [noticeShow, setNoticeShow] = useState(false);

  const [sendShow, setSendShow] = useState(false);

  const handleTargetChoice = () => {
    if (type === "매거진") setMagazineShow(true);
    if (type === "이벤트") setEventShow(true);
    if (type === "상품") setProductShow(true);
    if (type === "공지") setNoticeShow(true);
  };

  const handleMagazineValue = (targetId) => {
    const url = `https://www.nenoons.com/app-magazine-detail/${targetId}`;
    setTargetUrl(url);
    setMagazineShow(false);
  };

  const handleEventValue = (targetId) => {
    const url = `https://www.nenoons.com/app-event-detail/${targetId}`;
    setTargetUrl(url);
    setEventShow(false);
  };

  const handleProductValue = (targetId) => {
    const url = `https://www.nenoons.com/app-product-detail/${targetId}`;
    setTargetUrl(url);
    setProductShow(false);
  };

  const handleNoticeValue = (targetId) => {
    const url = `https://www.nenoons.com/app-notice-detail/${targetId}`;
    setTargetUrl(url);
    setNoticeShow(false);
  };

  // 체크한 사용자 저장
  const handleUsers = (checkUser) => {
    setUsers(checkUser);

    console.log("checkUser :: ", checkUser);
  };

  const [sendManyUserFcm, { data: sendManyUserFcmData }] = useMutation(SEND_MANY_USER_FCM, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (sendManyUserFcmData !== undefined) {
      console.log("sendManyUserFcmData :: ", sendManyUserFcmData);
      if (sendManyUserFcmData.sendManyUserFcm.result === "ok") window.location.reload();
    }
  }, [sendManyUserFcmData]);

  //   푸쉬메세지 보내기
  const handleSendModal = async () => {
    setSendShow(false);

    await sendManyUserFcm({
      variables: {
        users: `[${users.map((m) => `"${m}"`)}]`,
        title,
        message,
        link: `${targetUrl}`,
        action: "",
        value: "",
      },
    });
  };

  const handleSend = () => {
    if (users.length === 0) {
      SimpleNotification({
        message: "푸쉬메세지를 전송할 사용자를 선택해주세요.",
      });
      return "";
    }

    if (!targetUrl) {
      SimpleNotification({
        message: "이동 타켓을 선택해주세요.",
      });
      return "";
    }

    if (!title || !message) {
      SimpleNotification({
        message: "내용을 입력해주세요.",
      });
      return "";
    }

    setSendShow(true);
  };
  return (
    <div className="flex">
      <div className="w-2/3">
        <div className="col-12 grid-margin">
          {/* 라디오버튼 */}
          <div className="flex items-center mt-8 justify-between w-2/3">
            {/* 매거진 */}
            <div className="flex items-center m-2">
              <input
                id="push_everything"
                name="form-input push_notifications"
                type="radio"
                className="form-radio h-6 w-6 text-purple-600 transition duration-150 ease-in-out"
                defaultChecked
                onChange={() => setType("매거진")}
              />
              <label htmlFor="push_everything" className="ml-2 flex justify-center text-lg items-center mb-0 font-medium text-gray-700">
                매거진
              </label>
            </div>
            {/* 이벤트 */}
            <div className="flex items-center m-2">
              <input
                id="push_everything"
                name="form-input push_notifications"
                type="radio"
                className="form-radio h-6 w-6 text-purple-600 transition duration-150 ease-in-out"
                onChange={() => setType("이벤트")}
              />
              <label htmlFor="push_everything" className="ml-2 flex justify-center text-lg items-center mb-0 font-medium text-gray-700">
                이벤트
              </label>
            </div>
            {/* 상품 */}
            <div className="flex items-center m-2">
              <input
                id="push_everything"
                name="form-input push_notifications"
                type="radio"
                className="form-radio h-6 w-6 text-purple-600 transition duration-150 ease-in-out"
                onChange={() => setType("상품")}
              />
              <label htmlFor="push_everything" className="ml-2 flex justify-center text-lg items-center mb-0 font-medium text-gray-700">
                상품
              </label>
            </div>
            {/* 공지 */}
            <div className="flex items-center m-2">
              <input
                id="push_everything"
                name="form-input push_notifications"
                type="radio"
                className="form-radio h-6 w-6 text-purple-600 transition duration-150 ease-in-out"
                onChange={() => setType("공지")}
              />
              <label htmlFor="push_everything" className="ml-2 flex justify-center text-lg items-center mb-0 font-medium text-gray-700">
                공지
              </label>
            </div>
          </div>
          {/* url 선택 */}
          <div className="flex items-center mt-8 justify-between">
            <div className="w-2/12">
              <button className="border rounded-md bg-white p-2" onClick={handleTargetChoice}>
                이동 타겟 선택
              </button>
            </div>
            {/* 매거진 모달 */}
            <Modal show={magazineShow} onHide={() => setMagazineShow(false)} aria-labelledby="example-modal-sizes-title-md">
              <Modal.Header closeButton>
                <MagazineModal handleValue={handleMagazineValue} />
              </Modal.Header>
            </Modal>
            {/* 이벤트 모달 */}
            <Modal show={eventShow} onHide={() => setEventShow(false)} aria-labelledby="example-modal-sizes-title-md">
              <Modal.Header closeButton>
                <EventModal handleValue={handleEventValue} />
              </Modal.Header>
            </Modal>
            {/* 상품 모달 */}
            <Modal show={productShow} onHide={() => setProductShow(false)} aria-labelledby="example-modal-sizes-title-md">
              <Modal.Header closeButton>
                <ProductModal handleValue={handleProductValue} />
              </Modal.Header>
            </Modal>
            {/* 공지 모달 */}
            <Modal show={noticeShow} onHide={() => setNoticeShow(false)} aria-labelledby="example-modal-sizes-title-md">
              <Modal.Header closeButton>
                <NoticeModal handleValue={handleNoticeValue} />
              </Modal.Header>
            </Modal>
            <div className="w-10/12">
              <Form.Control type="text" readOnly value={targetUrl} />
            </div>
          </div>
          {/* 제목 & 메세지 */}
          <table className="shadow-sm bg-white w-full mt-8">
            <tr>
              <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">제목</th>
              <td colSpan="3" className="border px-8 py-2 text-sm">
                <Form.Control type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
              </td>
            </tr>
            <tr>
              <th className="bg-gray-200 border text-left px-8 py-2 text-sm">내용</th>
              <td colSpan="3" className="border px-8 py-2 text-sm">
                <textarea
                  id="about"
                  rows="5"
                  className="p-4 text-gray-700 form-control form-textarea block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                />
              </td>
            </tr>
          </table>
          {/* 보내기 */}
          <div className="flex w-full items-center justify-center mt-8">
            <button className="border rounded-md bg-white p-2 px-3" onClick={handleSend}>
              보내기
            </button>
          </div>
          {/* Modal Starts  */}
          <Modal show={sendShow} onHide={() => setSendShow(false)} aria-labelledby="example-modal-sizes-title-md">
            <Modal.Header closeButton>
              <Modal.Title>푸쉬 메세지 전송</Modal.Title>
            </Modal.Header>

            <Modal.Body>
              <p>메세지를 전송하시겠습니까?</p>
            </Modal.Body>

            <Modal.Footer className="flex-wrap">
              <Button variant="btn btn-primary m-2" onClick={handleSendModal}>
                확인
              </Button>
              <Button variant="btn btn-light m-2" onClick={() => setSendShow(false)}>
                취소
              </Button>
            </Modal.Footer>
          </Modal>
          {/* Modal Ends */}
        </div>
      </div>
      <UserForm handleValue={handleUsers} />
    </div>
  );
};

const UserForm = ({ handleValue }) => {
  const [allUserCheck, setAllUserCheck] = useState(false);
  const [userCheckList, setUserCheckList] = useState([]);

  useEffect(() => {
    handleValue(userCheckList);
  }, [userCheckList]);
  // 구매 데이터 가져오기
  const { loading, error, data } = useQuery(ALL_USERS, {
    fetchPolicy: "cache-and-network",
  });
  if (loading) return <div />;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }
  let items;
  if (data && data.allUsers) {
    // 탈퇴되지않고 fcmToken이 있는 사용자
    items = data.allUsers;
    items = items.filter((f) => f.withdraw === false);
    items = items.filter((f) => f.fcmToken !== "");
  }

  //   사용자 체크
  const handleUserCheck = (id) => {
    const tempUserCheckList = userCheckList.map((m) => m);
    const idx = userCheckList.indexOf(id);

    if (idx > -1) tempUserCheckList.splice(idx, 1);
    if (idx === -1) tempUserCheckList.push(id);

    setUserCheckList(tempUserCheckList);
  };

  //   전체선택 & 전체선택해제
  const handleAllUserCheck = () => {
    if (allUserCheck) {
      setUserCheckList([]);
      setAllUserCheck(false);
    }

    if (!allUserCheck) {
      const tempUserCheckList = items.map((m) => m.id);
      setUserCheckList(tempUserCheckList);
      setAllUserCheck(true);
    }
  };

  if (!items) return <div />;
  return (
    <div className="w-1/3">
      <div className="col-12 grid-margin mt-4">
        {/* 예약 상품 내용 */}
        <div className="row">
          <div className="my-2 border-gray-200 w-full flex items-center justify-between">
            <h3 className="text-lg leading-6 font-medium text-black">사용자 목록</h3>
            <div className="flex mail-list  border-b">
              <div className="form-check">
                <label className="form-check-label">
                  <input type="checkbox" checked={allUserCheck} onChange={handleAllUserCheck} className="form-check-input" />
                  <i className="input-helper" />
                  전체선택
                </label>
              </div>
            </div>
          </div>
          <div className="pt-2 pb-2 border-right bg-white overflow-auto w-full" style={{ height: "40vh" }}>
            {items.map((m) => (
              <div className="flex mail-list my-2 p-2 border-b" key={m.id}>
                <div className="form-check">
                  <label className="form-check-label">
                    <input
                      type="checkbox"
                      checked={userCheckList.indexOf(m.id) > -1 || false}
                      onChange={(e) => handleUserCheck(m.id)}
                      className="form-check-input"
                    />
                    <i className="input-helper" />
                  </label>
                </div>
                <div className="content ml-2">
                  <div className="flex">
                    <div>
                      <p className="sender-name">{m.email}</p>
                    </div>
                    {m.name ? (
                      <>
                        <div className="mx-2">
                          <p className="sender-name">{" || "}</p>
                        </div>
                        <div>
                          <p className="sender-name">{m.name}</p>
                        </div>
                      </>
                    ) : (
                      ""
                    )}
                  </div>
                  <p className="message_text">{PhoneFormatter(m.tel) || ""}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

// 매거진 모달
const MagazineModal = ({ handleValue }) => {
  // 검색
  const [searchItem, setSearchItem] = useState();
  // 상품 관련 데이터

  const [items, setItems] = useState(null);
  const [products, setProducts] = useState();

  // 페이지네이션 적용
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentItem, setCurrentItem] = useState([]);

  const pageLimit = 7;

  // 페이지네이션 초기화
  useEffect(() => {
    if (products) {
      setCurrentItem(products.slice(offset, offset + pageLimit));
    }
  }, [offset, products]);

  //   배너 클릭시 이동할 상품 선택
  const handleProductChoice = (productId) => {
    handleValue(productId);
    setSearchItem();
    setProducts(items);
  };

  // 검색
  const handleSearch = () => {
    setProducts(items);

    if (searchItem) {
      setProducts(
        items.filter((f) => {
          return f.title.indexOf(searchItem) > -1;
        })
      );
    }
  };

  //   상품정보
  const { loading, error, data } = useQuery(ALL_NOTICE_BOARDS, {
    fetchPolicy: "cache-and-network",
    skip: !!items,
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  if (data && data.allNoticeBoards) {
    const resultData = data.allNoticeBoards.filter((f) => f.category === "매거진");
    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });

    setItems(resultData);
    setProducts(resultData);
  }

  if (!items) return <div />;

  return (
    <div className="w-full">
      <div className="sidebar-widget">
        <h4 className="pro-sidebar-title">Search </h4>
        <div className="pro-sidebar-search mt-15">
          <div className="pro-sidebar-search-form" action="#">
            <input type="text" placeholder="Search here..." value={searchItem} onChange={(e) => setSearchItem(e.target.value)} />
            <button onClick={handleSearch}>
              <i className="ti-search" />
            </button>
          </div>
        </div>
      </div>
      <div className="flex mx-auto">
        <div>
          {currentItem &&
            currentItem.map((m) => (
              <div className="px-3 pt-1 mt-1 flex justify-between border-b">
                <div className="justify-start w-3/4">
                  <img src={m.image} className="h-12 rounded-md w-full" alt="dp" />
                  <div className="flex justify-start flex-wrap pb-3 mt-2 w-full">
                    <div className="flex justify-start font-bold">{m.title}</div>
                  </div>
                </div>
                <div className="flex items-center justify-end text-black w-1/4">
                  <button type="button" className="btn btn-warning btn-rounded py-1 px-3" onClick={(e) => handleProductChoice(m.id)}>
                    선택
                  </button>
                </div>
              </div>
            ))}
        </div>
      </div>
      <div className="pro-pagination-style text-center mt-30">
        <Paginator
          totalRecords={products.length}
          pageLimit={pageLimit}
          pageNeighbours={1} // 0, 1, 2
          setOffset={setOffset}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          pageContainerClass="mb-0 mt-0"
          pagePrevText="«"
          pageNextText="»"
        />
      </div>
    </div>
  );
};

// 이벤트 모달
const EventModal = ({ handleValue }) => {
  // 검색
  const [searchItem, setSearchItem] = useState();
  // 상품 관련 데이터

  const [items, setItems] = useState(null);
  const [products, setProducts] = useState();

  // 페이지네이션 적용
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentItem, setCurrentItem] = useState([]);

  const pageLimit = 7;

  // 페이지네이션 초기화
  useEffect(() => {
    if (products) {
      setCurrentItem(products.slice(offset, offset + pageLimit));
    }
  }, [offset, products]);

  //   배너 클릭시 이동할 상품 선택
  const handleProductChoice = (productId) => {
    handleValue(productId);
    setSearchItem();
    setProducts(items);
  };

  // 검색
  const handleSearch = () => {
    setProducts(items);

    if (searchItem) {
      setProducts(
        items.filter((f) => {
          return f.title.indexOf(searchItem) > -1;
        })
      );
    }
  };

  //   상품정보
  const { loading, error, data } = useQuery(ALL_NOTICE_BOARDS, {
    fetchPolicy: "cache-and-network",
    skip: !!items,
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  if (data && data.allNoticeBoards) {
    const resultData = data.allNoticeBoards.filter((f) => f.category === "이벤트");
    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });

    setItems(resultData);
    setProducts(resultData);
  }

  if (!items) return <div />;

  return (
    <div className="w-full">
      <div className="sidebar-widget">
        <h4 className="pro-sidebar-title">Search </h4>
        <div className="pro-sidebar-search mt-15">
          <div className="pro-sidebar-search-form" action="#">
            <input type="text" placeholder="Search here..." value={searchItem} onChange={(e) => setSearchItem(e.target.value)} />
            <button onClick={handleSearch}>
              <i className="ti-search" />
            </button>
          </div>
        </div>
      </div>
      <div className="flex mx-auto">
        <div>
          {currentItem &&
            currentItem.map((m) => (
              <div className="px-3 pt-1 mt-1 flex justify-between border-b">
                <div className="justify-start w-3/4">
                  <img src={m.image} className="h-12 rounded-md w-full" alt="dp" />
                  <div className="flex justify-start flex-wrap pb-3 mt-2 w-full">
                    <div className="flex justify-start font-bold">{m.title}</div>
                  </div>
                </div>
                <div className="flex items-center justify-end text-black w-1/4">
                  <button type="button" className="btn btn-warning btn-rounded py-1 px-3" onClick={(e) => handleProductChoice(m.id)}>
                    선택
                  </button>
                </div>
              </div>
            ))}
        </div>
      </div>
      <div className="pro-pagination-style text-center mt-30">
        <Paginator
          totalRecords={products.length}
          pageLimit={pageLimit}
          pageNeighbours={1} // 0, 1, 2
          setOffset={setOffset}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          pageContainerClass="mb-0 mt-0"
          pagePrevText="«"
          pageNextText="»"
        />
      </div>
    </div>
  );
};

// 상품 모달
const ProductModal = ({ handleValue }) => {
  // 검색
  const [searchItem, setSearchItem] = useState();
  // 상품 관련 데이터

  const [items, setItems] = useState(null);
  const [products, setProducts] = useState();

  // 페이지네이션 적용
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentItem, setCurrentItem] = useState([]);

  const pageLimit = 7;

  // 페이지네이션 초기화
  useEffect(() => {
    if (products) {
      setCurrentItem(products.slice(offset, offset + pageLimit));
    }
  }, [offset, products]);

  //   배너 클릭시 이동할 상품 선택
  const handleProductChoice = (productId) => {
    handleValue(productId);
    setSearchItem();
    setProducts(items);
  };

  // 검색
  const handleSearch = () => {
    setProducts(items);

    if (searchItem) {
      setProducts(
        items.filter((f) => {
          return f.name.indexOf(searchItem) > -1;
        })
      );
    }
  };

  //   상품정보
  const { loading, error, data } = useQuery(ALL_PRODUCTS, {
    fetchPolicy: "cache-and-network",
    skip: !!items,
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  if (data) {
    const resultData = data.allProducts;
    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });

    setItems(resultData);
    setProducts(resultData);
  }

  if (!items) return <div />;

  return (
    <div className="w-full">
      <div className="sidebar-widget">
        <h4 className="pro-sidebar-title">Search </h4>
        <div className="pro-sidebar-search mt-15">
          <div className="pro-sidebar-search-form" action="#">
            <input type="text" placeholder="Search here..." value={searchItem} onChange={(e) => setSearchItem(e.target.value)} />
            <button onClick={handleSearch}>
              <i className="ti-search" />
            </button>
          </div>
        </div>
      </div>
      <div className="flex mx-auto">
        <div>
          {currentItem &&
            currentItem.map((m) => (
              <div className="px-3 pt-1 mt-1 flex justify-between border-b">
                <div className="flex justify-start w-3/4">
                  <img src={m.mainImage} className="w-12 h-12 rounded-full" alt="dp" />
                  <div className="flex justify-start flex-wrap ml-4 pb-3 mt-2 ">
                    <div className="flex justify-start font-bold">{m.name}</div>
                    <div className="inline-flex w-full text-sm text-gray-500">
                      <span className="pr-2">{`${m.brand} | ${m.model}`}</span>
                    </div>
                  </div>
                </div>
                <div className="flex items-center text-black w-1/4">
                  <button type="button" className="btn btn-warning btn-rounded py-1 px-3" onClick={(e) => handleProductChoice(m.id)}>
                    선택
                  </button>
                </div>
              </div>
            ))}
        </div>
      </div>
      <div className="pro-pagination-style text-center mt-30">
        <Paginator
          totalRecords={products.length}
          pageLimit={pageLimit}
          pageNeighbours={1} // 0, 1, 2
          setOffset={setOffset}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          pageContainerClass="mb-0 mt-0"
          pagePrevText="«"
          pageNextText="»"
        />
      </div>
    </div>
  );
};

// 공지 모달
const NoticeModal = ({ handleValue }) => {
  // 검색
  const [searchItem, setSearchItem] = useState();
  // 상품 관련 데이터

  const [items, setItems] = useState(null);
  const [products, setProducts] = useState();

  // 페이지네이션 적용
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentItem, setCurrentItem] = useState([]);

  const pageLimit = 7;

  // 페이지네이션 초기화
  useEffect(() => {
    if (products) {
      setCurrentItem(products.slice(offset, offset + pageLimit));
    }
  }, [offset, products]);

  //   배너 클릭시 이동할 상품 선택
  const handleProductChoice = (productId) => {
    handleValue(productId);
    setSearchItem();
    setProducts(items);
  };

  // 검색
  const handleSearch = () => {
    setProducts(items);

    if (searchItem) {
      setProducts(
        items.filter((f) => {
          return f.title.indexOf(searchItem) > -1;
        })
      );
    }
  };

  //   상품정보
  const { loading, error, data } = useQuery(ALL_SERVICE_CENTERS, {
    fetchPolicy: "cache-and-network",
    skip: !!items,
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  if (data && data.allServiceCenters) {
    const resultData = data.allServiceCenters.filter((f) => f.type === "notice");
    // 최근 등록날짜 순으로 나타내기
    resultData.sort((a, b) => {
      return b.createAt - a.createAt;
    });

    setItems(resultData);
    setProducts(resultData);
  }

  if (!items) return <div />;

  return (
    <div className="w-full">
      <div className="sidebar-widget">
        <h4 className="pro-sidebar-title">Search </h4>
        <div className="pro-sidebar-search mt-15">
          <div className="pro-sidebar-search-form" action="#">
            <input type="text" placeholder="Search here..." value={searchItem} onChange={(e) => setSearchItem(e.target.value)} />
            <button onClick={handleSearch}>
              <i className="ti-search" />
            </button>
          </div>
        </div>
      </div>
      <div className="flex mx-auto">
        <div className="w-full">
          {currentItem &&
            currentItem.map((m) => (
              <div className="px-3 pt-1 mt-1 flex justify-between border-b">
                <div className="justify-start w-3/4">
                  <div className="flex justify-start flex-wrap pb-3 mt-2 w-full">
                    <div className="flex justify-start font-bold">{m.title}</div>
                  </div>
                </div>
                <div className="flex items-center justify-end text-black w-1/4">
                  <button type="button" className="btn btn-warning btn-rounded py-1 px-3" onClick={(e) => handleProductChoice(m.id)}>
                    선택
                  </button>
                </div>
              </div>
            ))}
        </div>
      </div>
      <div className="pro-pagination-style text-center mt-30">
        <Paginator
          totalRecords={products.length}
          pageLimit={pageLimit}
          pageNeighbours={1} // 0, 1, 2
          setOffset={setOffset}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          pageContainerClass="mb-0 mt-0"
          pagePrevText="«"
          pageNextText="»"
        />
      </div>
    </div>
  );
};

export default AppPushMessage;
