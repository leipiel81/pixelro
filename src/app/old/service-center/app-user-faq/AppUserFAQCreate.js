import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Form } from "react-bootstrap";
import { Editor } from "@tinymce/tinymce-react";
import { SimpleNotification } from "components/SimpleNotification";
import { CREATE_SERVICE_CENTER } from "graphql/gql/insert";

const CreatePage = () => {
  const history = useHistory();

  const goBack = () => {
    history.push("/service-center/app-user-faq-table");
  };

  const [category, setCategory] = useState("회원");
  const [title, setTitle] = useState();
  const [content, setContent] = useState();

  const type = "user";

  const [noticeCreate, { data: createData }] = useMutation(CREATE_SERVICE_CENTER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (createData !== undefined) {
      goBack();
    }
  }, [createData]);

  const handleUpdate = () => {
    if (!title || !content) {
      SimpleNotification({
        title: "",
        message: "내용을 입력해주세요.",
      });
      return;
    }

    noticeCreate({
      variables: {
        type,
        category,
        title,
        content,
      },
    });
  };

  const handleEditorChange = (e) => {
    setContent(e.target.getContent());
  };
  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">고객센터</li>
            <Link
              to="/service-center/app-user-faq-table"
              className="breadcrumb-item active mt-1"
              aria-current="page"
            >
              멤버 FAQ
            </Link>
            <Link
              to="/service-center/app-user-faq-create"
              className="breadcrumb-item active mt-1"
              aria-current="page"
            >
              등록하기
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 멤버 FAQ 작성</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">카테고리</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <select
                      className="border border-black p-3 text-black w-1/4"
                      value={category}
                      onChange={(e) => setCategory(e.target.value)}
                    >
                      <option value="회원">회원</option>
                      <option value="상품">상품</option>
                      <option value="결제">결제</option>
                      <option value="주문/배송">주문/배송</option>
                      <option value="교환/반품/취소">교환/반품/취소</option>
                      <option value="사이트이용문의">사이트이용문의</option>
                    </select>
                  </td>
                </tr>
                <tr className="m-0 p-0">
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/12">제목</th>
                  <td className="border px-8 py-2 text-sm w-4/12">
                    <Form.Control
                      type="text"
                      value={title}
                      onChange={(e) => setTitle(e.target.value)}
                    />
                  </td>
                </tr>
              </table>
              <div className="row grid-margin">
                <div className="col-lg-12">
                  <div className="card">
                    <div className="card-body">
                      <h4 className="card-title">내용</h4>
                      <Editor
                        initialValue=""
                        init={{ height: 500 }}
                        onChange={handleEditorChange}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 grid-margin mt-3">
        <div className="row justify-end mr-1">
          <button type="button" className="btn btn-primary mr-2" onClick={handleUpdate}>
            저장
          </button>
          <button className="btn btn-light" onClick={goBack}>
            취소
          </button>
        </div>
      </div>
    </>
  );
};

CreatePage.prototype = {
  id: PropTypes.string.isRequired,
};

export default CreatePage;
