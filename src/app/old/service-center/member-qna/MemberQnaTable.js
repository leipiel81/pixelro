/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import useSettings from "stores/settings";
import PropTypes from "prop-types";
import { AllQnas } from "graphql/query/select";
import { FormatDate } from "components/FormatDate";
import Pagination from "components/Pagination";

const Tables = () => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }

  const { result } = AllQnas();
  let items;
  if (result) {
    items = result;

    // 최근 등록날짜 순으로 나타내기
    items.sort((a, b) => {
      return b.createAt - a.createAt;
    });
  }

  const [pageOfItems, setPageOfItems] = useState([]);
  const [page, setPage] = useState(1);

  function onChangePage(pageOfItems, pageNumber) {
    setPageOfItems(pageOfItems);
    setPage(pageNumber);
    // 페이지 이동시 맨 위로 스크롤 이동
    window.scrollTo(0, 0);
  }

  return (
    <>
      <div>
        <TableHeader />
        <div className="row grid-margin">
          <div className="col-12">
            <div className="flex justify-between mb-4">
              <h4 className="card-title mt-2 pt-1 text-xl">회원사 QNA 목록</h4>
            </div>
            <div className="flex flex-col">
              <div className="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                <div className="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                  <TableForm pageOfItems={pageOfItems} />
                  <Page items={items} page={page} onChangePage={onChangePage} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

// /* breadcrumb */
const TableHeader = () => {
  return (
    <div>
      <nav aria-label="breadcrumb" role="navigation">
        <ol className="breadcrumb">
          <li className="breadcrumb-item active">고객센터</li>
          <Link to="/service-center/member-qna-table" className="breadcrumb-item active mt-1" aria-current="page">
            공지사항
          </Link>
        </ol>
      </nav>
    </div>
  );
};

const TableForm = ({ pageOfItems }) => {
  return (
    <table className="min-w-full">
      <thead className="bg-gray-200">
        <tr>
          <th className="px-3 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider">
            구분
          </th>
          <th className="px-3 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider">
            제목
          </th>
          <th className="px-3 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-black uppercase tracking-wider">
            등록날짜
          </th>
          <th className="px-3 py-3 border-b border-gray-200 bg-gray-50" />
        </tr>
      </thead>
      {pageOfItems && (
        <tbody className="bg-white">
          {pageOfItems.map((m) => (
            <tr>
              <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900">
                {m.category}
              </td>
              <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-black">{m.title}</td>
              <td className="px-3 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-black">
                {FormatDate(new Date(parseInt(m.createAt, 10)))}
              </td>
              <td className="px-3 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium">
                <Link className="ti-pencil-alt" to={`/service-center/member-qna-detail/${m.id}`}>
                  상세보기
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      )}
    </table>
  );
};

Tables.propTypes = {
  pageOfItems: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
};

const Page = (props) => {
  const PAGE_SIZE = 10;
  const { items, page, onChangePage } = props;
  return (
    <div className="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">
      <div className="hidden sm:block">
        <p className="text-sm leading-5 text-gray-700">
          <span className="font-medium mx-1">{(page - 1) * PAGE_SIZE + 1}</span>
          {" ~  "}
          <span className="font-medium mx-1">{items && items.length < page * PAGE_SIZE ? items.length : page * PAGE_SIZE}</span>
          {" / 총 "}
          <span className="font-medium mx-1">{items && items.length}</span>
          {" 건"}
        </p>
      </div>
      <div className="flex-1 flex justify-between sm:justify-end">{items && <Pagination items={items} onChangePage={onChangePage} />}</div>
    </div>
  );
};

Page.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  page: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
};

export default Tables;
