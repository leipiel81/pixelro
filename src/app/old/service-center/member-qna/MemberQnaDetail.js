import React, { useState } from "react";
import PropTypes from "prop-types";
import { Qna } from "graphql/query/select";
import { Link, useHistory } from "react-router-dom";
import { FormatDate } from "components/FormatDate";
import { Button, Modal } from "react-bootstrap";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_QNA } from "graphql/gql/delete";
import { UPDATE_QNA } from "graphql/gql/update";

const DetailPage = ({ match }) => {
  const { id } = match.params;

  let items;
  if (id !== undefined && id !== "undefined") {
    const { result } = Qna(id);
    items = result;
  }

  return (
    <>
      {items && (
        <div>
          {/* breadcrumb */}
          <div>
            <nav aria-label="breadcrumb" role="navigation">
              <ol className="breadcrumb">
                <li className="breadcrumb-item active">고객센터</li>
                <Link to="/service-center/member-qna-table" className="breadcrumb-item active mt-1" aria-current="page">
                  QNA
                </Link>
                <Link to={`/service-center/member-qna-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
                  문의 내용
                </Link>
              </ol>
            </nav>
          </div>
          <div className="row">
            <InfoForm items={items} />
          </div>
        </div>
      )}
    </>
  );
};

const InfoForm = ({ items }) => {
  const [comment, setComment] = useState(items.comment);

  // 답변 수정
  const { id } = items;

  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  const [qnaUpdate] = useMutation(UPDATE_QNA, {
    update: (proxy, result) => {
      goBack();
    },
    variables: {
      id,
      comment,
      reply: `답변완료`,
    },
  });

  const handleUpdate = () => {
    qnaUpdate();
  };

  const [mdShow, setMdShow] = useState(false);

  const [noticeDelete] = useMutation(DELETE_QNA, {
    update: (proxy, result) => {
      goBack();
    },
    variables: {
      id,
    },
  });

  const handleDelete = () => {
    setMdShow(false);
    noticeDelete();
  };

  return (
    <>
      <div className="col-12 grid-margin mt-3">
        <div className="card">
          <div className="card-body">
            <form className="form-sample">
              <div className="row">
                <div className="col-md-12">
                  <div>
                    <div className="mt-2 border-gray-200">
                      <div>
                        <h3 className="text-lg leading-6 font-medium text-black">문의내용</h3>
                      </div>
                      <div className="mt-6 sm:mt-5">
                        <table className="shadow-sm bg-white w-full">
                          <tr>
                            <th className="bg-gray-200 border text-left px-8 py-2 text-sm">문의 유형</th>
                            <td colSpan="3" className="border px-8 py-2 text-sm  ">
                              {items.category}
                            </td>
                          </tr>
                          <tr className="m-0 p-0">
                            <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">등록자</th>
                            <td className="border px-8 py-2 text-sm w-4/12">{items.member.name}</td>
                            <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">등록 일시</th>
                            <td className="border px-8 py-2 text-sm w-4/12">{FormatDate(new Date(parseInt(items.createAt, 10)))}</td>
                          </tr>
                          <tr>
                            <th className="bg-gray-200 border text-left px-8 py-2 text-sm">상점 이름</th>
                            <td colSpan="3" className="border px-8 py-2 text-sm  ">
                              {items.store.name}
                            </td>
                          </tr>
                          <tr>
                            <th className="bg-gray-200 border text-left px-8 py-2 text-sm">제목</th>
                            <td colSpan="3" className="border px-8 py-2 text-sm  ">
                              {items.title}
                            </td>
                          </tr>
                          <tr>
                            <th className="bg-gray-200 border text-left px-8 py-2 text-sm">문의 내용</th>
                            <td colSpan="3" className="border px-8 py-2 text-sm  ">
                              {items.content}
                            </td>
                          </tr>
                        </table>
                      </div>
                      {/* 답변 */}
                      <div className="sm:gap-4 sm:items-start  sm:pt-5 sm:pb-5">
                        <div>
                          <h3 className="text-lg leading-6 font-medium text-black">답변</h3>
                        </div>
                        <div className="mt-6">
                          <table className="shadow-sm bg-white w-full">
                            <tr>
                              <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">답변 내용</th>
                              <td colSpan="3" className="border px-8 py-2 text-sm  ">
                                {items.reply === "대기" ? (
                                  <textarea
                                    id="about"
                                    rows="10"
                                    className="p-4 text-black form-textarea block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                    value={comment}
                                    onChange={(e) => setComment(e.target.value)}
                                  />
                                ) : (
                                  items.comment
                                )}
                              </td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="col-12 grid-margin mt-3">
        <div className="row justify-between mr-1">
          <button className="btn btn-danger mt-2" onClick={() => setMdShow(true)}>
            삭제하기
          </button>
          {items.reply === "대기" ? (
            <button className="btn btn-primary mt-2" onClick={handleUpdate}>
              답변하기
            </button>
          ) : (
            <div />
          )}
        </div>
        {/* Modal Starts  */}
        <Modal show={mdShow} onHide={() => setMdShow(false)} aria-labelledby="example-modal-sizes-title-md">
          <Modal.Header closeButton>
            <Modal.Title>해당 내용을 삭제하시겠습니까?</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <p>삭제를 원하시면 삭제 버튼을 눌러주세요.</p>
          </Modal.Body>

          <Modal.Footer className="fleex-wrap">
            <Button variant="danger m-2" onClick={handleDelete}>
              삭제
            </Button>
            <Button variant="primary m-2" onClick={() => setMdShow(false)}>
              취소
            </Button>
          </Modal.Footer>
        </Modal>
        {/* Modal Ends */}
      </div>
    </>
  );
};

DetailPage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default DetailPage;
