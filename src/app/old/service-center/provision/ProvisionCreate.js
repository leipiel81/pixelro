import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Form } from "react-bootstrap";
import { Editor } from "@tinymce/tinymce-react";
import { SimpleNotification } from "components/SimpleNotification";
import { CREATE_SERVICE_CENTER } from "graphql/gql/insert";

const CreatePage = () => {
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  const [category, setCategory] = useState("이용약관_회원사");
  const [topic, setTopic] = useState("");
  const [title, setTitle] = useState();
  const [content, setContent] = useState();

  const type = "provision";

  const [provisionCreate, { data: createData }] = useMutation(CREATE_SERVICE_CENTER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (createData !== undefined) {
      goBack();
    }
  }, [createData]);

  const handleUpdate = () => {
    if (!title || !content || !topic) {
      SimpleNotification({
        title: "",
        message: "내용을 입력해주세요.",
      });
      return;
    }

    provisionCreate({
      variables: {
        type,
        category,
        title,
        content,
        topic,
      },
    });
  };

  const handleEditorChange = (e) => {
    setContent(e.target.getContent());
  };
  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">고객센터</li>
            <Link to="/service-center/provision-table" className="breadcrumb-item active mt-1" aria-current="page">
              약관관리
            </Link>
            <Link to="/service-center/provision-create" className="breadcrumb-item active mt-1" aria-current="page">
              이용약관 등록
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 이용약관 작성</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">대상</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <select
                      className="border border-black p-3 text-black w-1/4"
                      value={category}
                      onChange={(e) => setCategory(e.target.value)}
                    >
                      <option value="이용약관_회원사">회원사</option>
                      <option value="이용약관_사용자">사용자</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">주제</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {category === "이용약관_회원사" ? (
                      <select className="border border-black p-3 text-black w-1/4" value={topic} onChange={(e) => setTopic(e.target.value)}>
                        <option value="">옵션을 선택해주세요.</option>
                        <option value="서비스이용약관">서비스 이용약관</option>
                        <option value="개인정보처리">개인정보 처리</option>
                        <option value="전자상거래">전자상거래</option>
                        <option value="온오프라인계약서">온오프라인계약서</option>
                      </select>
                    ) : (
                      <select className="border border-black p-3 text-black w-1/4" value={topic} onChange={(e) => setTopic(e.target.value)}>
                        <option value="">옵션을 선택해주세요.</option>
                        <option value="서비스이용약관">서비스 이용약관</option>
                        <option value="개인정보처리">개인정보 처리</option>
                        <option value="위치정보이용약관동의">위치정보 이용약관 동의</option>
                      </select>
                    )}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">제목</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
                  </td>
                </tr>
              </table>
              <div className="row grid-margin">
                <div className="col-lg-12">
                  <div className="card">
                    <div className="card-body">
                      <h4 className="card-title">내용</h4>
                      <Editor initialValue="" init={{ height: 500 }} onChange={handleEditorChange} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 grid-margin mt-3">
        <div className="row justify-end mr-1">
          <button type="button" className="btn btn-primary mr-2" onClick={handleUpdate}>
            저장
          </button>
          <button className="btn btn-light" onClick={goBack}>
            취소
          </button>
        </div>
      </div>
    </>
  );
};

CreatePage.prototype = {
  id: PropTypes.string.isRequired,
};

export default CreatePage;
