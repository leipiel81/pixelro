import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Form } from "react-bootstrap";
import useSettings from "stores/settings";
import { Editor } from "@tinymce/tinymce-react";
import { SERVICE_CENTER } from "graphql/gql/select";
import { SimpleNotification } from "components/SimpleNotification";
import { UPDATE_SERVICE_CENTER } from "graphql/gql/update";

const EditPage = ({ match }) => {
  const { id } = match.params;

  // 사용자 FAQ
  const { loading, error, data } = useQuery(SERVICE_CENTER, {
    fetchPolicy: "cache-and-network",
    variables: { id },
  });
  if (loading) return <p>Loading...</p>;
  if (error) {
    SimpleNotification({
      message: error.message || "Error",
    });
    return "";
  }

  let items;
  if (data && data.serviceCenter) {
    items = data.serviceCenter;
  }

  if (!items) return <div />;

  return <EditForm items={items} />;
};

const EditForm = ({ items }) => {
  const settings = useSettings();
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };

  // --------------------------
  // -- 데이터 값 저장 --
  const [category, setCategory] = useState(items.category);
  const [topic, setTopic] = useState(items.topic);
  const [title, setTitle] = useState(items.title);
  const { serviceCenterContent } = settings;
  const [content, setContent] = useState(serviceCenterContent);

  const { id, type } = items;

  const [provisionUpdate, { data: provisionUpdateData }] = useMutation(UPDATE_SERVICE_CENTER, {
    onError(err) {
      console.log(err);
      SimpleNotification({
        message: err.message || "Error",
      });
    },
  });

  useEffect(() => {
    if (provisionUpdateData !== undefined) {
      goBack();
    }
  }, [provisionUpdateData]);

  const handleUpdate = () => {
    if (!title || !content || !topic) {
      SimpleNotification({
        title: "",
        message: "내용을 입력해주세요.",
      });
      return;
    }

    provisionUpdate({
      variables: {
        id,
        type,
        category,
        title,
        topic,
        content,
      },
    });
  };

  const handleEditorChange = (e) => {
    setContent(e.target.getContent());
  };
  return (
    <>
      {/* breadcrumb */}
      <div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active">고객센터</li>
            <Link to="/service-center/provision-table" className="breadcrumb-item active mt-1" aria-current="page">
              약관관리
            </Link>

            <Link to={`/service-center/provision-edit/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
              이용약관 수정
            </Link>
          </ol>
        </nav>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="mt-6 border-gray-200">
            <div>
              <h3 className="text-ml leading-6 font-medium text-black">▶︎ 이용약관 수정</h3>
            </div>
            <div className="mt-6 sm:mt-5">
              <table className="shadow-sm bg-white w-full">
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">구분</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <select
                      className="border border-black p-3 text-black w-1/4"
                      value={category}
                      onChange={(e) => setCategory(e.target.value)}
                    >
                      <option value="이용약관_회원사">회원사</option>
                      <option value="이용약관_사용자">사용자</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">주제</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    {category === "이용약관_회원사" ? (
                      <select className="border border-black p-3 text-black w-1/4" value={topic} onChange={(e) => setTopic(e.target.value)}>
                        <option value="">옵션을 선택해주세요.</option>
                        <option value="서비스이용약관">서비스 이용약관</option>
                        <option value="개인정보처리">개인정보 처리</option>
                        <option value="전자상거래">전자상거래</option>
                        <option value="온오프라인계약서">온오프라인계약서</option>
                      </select>
                    ) : (
                      <select className="border border-black p-3 text-black w-1/4" value={topic} onChange={(e) => setTopic(e.target.value)}>
                        <option value="">옵션을 선택해주세요.</option>
                        <option value="서비스이용약관">서비스 이용약관</option>
                        <option value="개인정보처리">개인정보 처리</option>
                        <option value="위치정보이용약관동의">위치정보 이용약관 동의</option>
                      </select>
                    )}
                  </td>
                </tr>
                <tr>
                  <th className="bg-gray-200 border text-left px-8 py-2 text-sm">제목</th>
                  <td colSpan="3" className="border px-8 py-2 text-sm">
                    <Form.Control type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
                  </td>
                </tr>
              </table>
              <div className="row grid-margin">
                <div className="col-lg-12">
                  <div className="card">
                    <div className="card-body">
                      <h4 className="card-title">내용</h4>
                      <Editor initialValue={serviceCenterContent} init={{ height: 700 }} onChange={handleEditorChange} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 grid-margin mt-3">
        <div className="row justify-end mr-1">
          <button type="button" className="btn btn-primary mr-2" onClick={handleUpdate}>
            저장
          </button>
          <button className="btn btn-light" onClick={goBack}>
            취소
          </button>
        </div>
      </div>
    </>
  );
};
EditPage.prototype = {
  id: PropTypes.string.isRequired,
};

export default EditPage;
