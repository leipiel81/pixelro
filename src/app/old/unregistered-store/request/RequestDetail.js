import React from "react";
import PropTypes from "prop-types";
import { useHistory, Link } from "react-router-dom";
import { Store } from "graphql/query/select";
import { FormatDate } from "components/FormatDate";
import { observer } from "mobx-react-lite";
import { PhoneFormatter } from "components/PhoneFormatter";
import useSettings from "stores/settings";

const DetailPage = observer(({ match }) => {
  // 로그인 체크
  const settings = useSettings();
  const history = useHistory();
  console.log(settings.isLogin);
  if (!settings.isLogin) {
    history.push("/adminIn");
  }
  const { id } = match.params;

  const { result } = Store(id);

  let items;
  if (result) {
    items = result;
  }

  const goBack = () => {
    history.push("/unregistered-store/request-table");
  };

  return (
    <>
      {items && (
        <div>
          {/* breadcrumb */}
          <div>
            <nav aria-label="breadcrumb" role="navigation">
              <ol className="breadcrumb">
                <li className="breadcrumb-item active">미등록 관리</li>
                <Link to="/unregistered-store/request-table" className="breadcrumb-item active mt-1" aria-current="page">
                  신청 접수 목록
                </Link>
                <Link to={`/unregistered-store/request-detail/${id}`} className="breadcrumb-item active mt-1" aria-current="page">
                  신청 접수 정보
                </Link>
              </ol>
            </nav>
          </div>
          <InfoForm items={items} />
          <SubInfoForm items={items} />
          <AdminForm items={items} />
          <BusinessForm items={items} />
          <ReplyForm items={items} />
          <div className="col-12 grid-margin mt-3">
            <div className="row justify-end mr-1">
              <button className="btn btn-primary mr-2 mt-2 " onClick={goBack}>
                목록
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
});

const InfoForm = ({ items }) => {
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상점 기본 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">분류</th>
                <td colSpan="3" className="border px-8 py-2 text-sm">
                  {items.place}
                </td>
              </tr>
              <tr className="m-0 p-0">
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">상점 이름</th>
                <td className="border px-8 py-2 text-sm w-4/12">{items.name}</td>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">전화번호</th>
                <td className="border px-8 py-2 text-sm w-4/12">{PhoneFormatter(items.tel)}</td>
              </tr>

              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">주소</th>
                <td colSpan="3" className="border px-8 py-2 text-sm">
                  <div className="mb-1">{items.post}</div>
                  <div>
                    {items.address} {items.detailAddress}
                  </div>
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">등록날짜</th>
                <td colSpan="3" className="border px-8 py-2 text-sm  ">
                  {FormatDate(new Date(parseInt(items.createAt, 10)))}
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

const AdminForm = ({ items }) => {
  // 관리자 정보
  const { adminId } = items.storeGroup;

  const adminEmail = items.storeGroup.members.filter((f) => f.id === adminId).map((v) => v.email);

  const adminName = items.storeGroup.members.filter((f) => f.id === adminId).map((v) => v.name);

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 관리자 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">이름</th>
                <td colSpan="3" className="border px-8 py-2 text-sm">
                  {adminName}
                </td>
              </tr>
              <tr className="m-0 p-0">
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">이메일</th>
                <td className="border px-8 py-2 text-sm w-4/12">{adminEmail}</td>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">전화번호</th>
                <td className="border px-8 py-2 text-sm w-4/12">{PhoneFormatter(items.tel)}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

const SubInfoForm = ({ items }) => {
  let keywordCheck = false;
  if (items.keyword) {
    keywordCheck = JSON.parse(items.keyword).length !== 0;
  }

  let storeImages;
  if (items.image) {
    storeImages = JSON.parse(items.image);
  }

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상점 부가 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">상점 소개</th>
                <td colSpan="3" className="border px-8 py-2 text-sm">
                  {items.intro}
                </td>
              </tr>
              <tr className="m-0 p-0">
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">홈페이지 주소</th>
                <td className="border px-8 py-2 text-sm w-4/12">{items.site}</td>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-2/12">블로그 주소</th>
                <td className="border px-8 py-2 text-sm w-4/12">{items.blog}</td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">키워드</th>
                <td colSpan="3" className="border px-8 py-2 text-sm  ">
                  <div className="flex">
                    {keywordCheck &&
                      JSON.parse(items.keyword).map((m) => (
                        <div className="flex">
                          <span className="border p-2 mr-1">{m}</span>
                        </div>
                      ))}
                  </div>
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">온라인 / 오프라인</th>
                <td colSpan="3" className="border px-8 py-2 text-sm  ">
                  <div className="max-w-xs">
                    <span className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                      <div className="flex col-sm-9 col-form-label p-0">
                        <div className="form-check mr-3">
                          <label className="form-check-label">
                            <input type="checkbox" disabled checked={items.onLine} className="form-check-input" />
                            <i className="input-helper"></i>
                            온라인
                          </label>
                        </div>
                        <div className="form-check">
                          <label className="form-check-label">
                            <input type="checkbox" disabled checked={items.offLine} className="form-check-input" />
                            <i className="input-helper"></i>
                            오프라인
                          </label>
                        </div>
                      </div>
                    </span>
                  </div>
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">상점 이미지</th>
                <td colSpan="3" className="flex px-2 py-2 text-sm">
                  {storeImages && storeImages.map((m) => <img className="h-24 w-24 m-3" alt="" src={`${m}`} key={m} />)}
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

const BusinessForm = ({ items }) => {
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 상점 거래 정보</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">사업자 번호</th>
                <td colSpan="3" className="border px-8 py-2 text-sm ">
                  {items.businessNumber}
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">거래은행</th>
                <td colSpan="3" className="border px-8 py-2 text-sm ">
                  {items.bankName}
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">계좌번호</th>
                <td colSpan="3" className="border px-8 py-2 text-sm ">
                  {items.bankNumber}
                </td>
              </tr>
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm">예금주</th>
                <td colSpan="3" className="border px-8 py-2 text-sm ">
                  {items.accountHolder}
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

const ReplyForm = ({ items }) => {
  return (
    <div className="row">
      <div className="col-md-12">
        <div className="mt-6 border-gray-200">
          <div>
            <h3 className="text-ml leading-6 font-medium text-black">▶︎ 답변</h3>
          </div>
          <div className="mt-6 sm:mt-5">
            <table className="shadow-sm bg-white w-full">
              <tr>
                <th className="bg-gray-200 border text-left px-8 py-2 text-sm w-1/6">답변상태</th>
                <td colSpan="3" className="border px-8 py-2 text-sm  ">
                  {items.storeRegister}
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};
DetailPage.propTypes = {
  id: PropTypes.string.isRequired,
};

export default DetailPage;
