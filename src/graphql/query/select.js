import { useQuery } from "@apollo/react-hooks";
import {
  STORE,
  ALL_STORES,
  ALL_MEMBERS,
  MEMBER,
  ALL_PRODUCTS,
  PRODUCT,
  ALL_PRODUCT_REGISTERS,
  PRODUCT_REGISTER,
  ALL_USERS,
  USER,
  ALL_RESERVATIONS,
  RESERVATION,
  ALL_SERVICE_CENTERS,
  SERVICE_CENTER,
  ALL_QNAS,
  QNA,
  ALL_NOTICE_BOARDS,
  NOTICE_BOARD,
  ALL_BANNERS,
  BANNER,
  APP_QNA,
  ALL_APP_QNAS,
  ALL_COUPONS,
  ORDER,
  ALL_LENS_RESERVATIONS,
  LENS_RESERVATION,
  ALL_PIXELROS,
  PIXELRO,
  FILTER_SETTING_KEY_VALUE,
} from "../gql/select";

/* 관리자 전체 목록 가져오기 */
export const AllPixelros = () => {
  const { error, data } = useQuery(ALL_PIXELROS, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allPixelros,
    error: error,
  };
};

/* 관리자 정보 가져오기 */
export const Pixelro = (id) => {
  const { error, data } = useQuery(PIXELRO, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.pixelro,
    error: error,
  };
};

/* Store 정보 가져오기 */
export const Store = (id) => {
  const { error, data } = useQuery(STORE, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.store,
    error: error,
  };
};

/* Store 전체 목록 가져오기 */
export const AllStores = () => {
  const { error, data } = useQuery(ALL_STORES, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allStores,
    error: error,
  };
};

/* 회원사 전체 목록 가져오기 */
export const AllMembers = () => {
  const { error, data } = useQuery(ALL_MEMBERS, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allMembers,
    error: error,
  };
};

/* Member 정보 가져오기 */
export const Member = (id) => {
  const { error, data } = useQuery(MEMBER, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.member,
    error: error,
  };
};

/* 상품 전체 목록 가져오기 */
export const AllProducts = () => {
  const { error, data } = useQuery(ALL_PRODUCTS, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allProducts,
    error: error,
  };
};

/* 상품 정보 가져오기 */
export const Product = (id) => {
  const { error, data } = useQuery(PRODUCT, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.product,
    error: error,
  };
};

/* 상품 등록 전체 목록 가져오기 */
export const AllProductRegisters = () => {
  const { error, data } = useQuery(ALL_PRODUCT_REGISTERS, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allProductRegisters,
    error: error,
  };
};

/* 상품 등록 정보 가져오기 */
export const ProductRegister = (id) => {
  const { error, data } = useQuery(PRODUCT_REGISTER, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.productRegister,
    error: error,
  };
};

/* 앱 사용자 전체 목록 가져오기 */
export const AllUsers = () => {
  const { error, data } = useQuery(ALL_USERS, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allUsers,
    error: error,
  };
};

/* 앱 사용자 정보 가져오기 */
export const User = (id) => {
  const { error, data } = useQuery(USER, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.user,
    error: error,
  };
};

/* 주문 정보 가져오기 */
export const Order = (id) => {
  const { error, data } = useQuery(ORDER, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.order,
    error: error,
  };
};

/* 예약 관리 전체 목록 가져오기 */
export const AllReservations = () => {
  const { error, data } = useQuery(ALL_RESERVATIONS, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allReservations,
    error: error,
  };
};

/* 예약 관리 정보 가져오기 */
export const Reservation = (id) => {
  const { error, data } = useQuery(RESERVATION, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.reservation,
    error: error,
  };
};

/* 공지사항 & FAQ 전체 목록 가져오기 */
export const AllServiceCenter = () => {
  const { error, data } = useQuery(ALL_SERVICE_CENTERS, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allServiceCenters,
    error: error,
  };
};

/* 공지사항 & FAQ 전체 목록 가져오기 */
export const AllLensReservations = () => {
  const { error, data } = useQuery(ALL_LENS_RESERVATIONS, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allLensReservations,
    error: error,
  };
};

/* 공지사항 & FAQ 전체 목록 가져오기 */
export const LensReservation = (id) => {
  const { error, data } = useQuery(LENS_RESERVATION, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.lensReservation,
    error: error,
  };
};

/* 공지사항 & FAQ 정보 가져오기 */
export const ServiceCenter = (id) => {
  const { error, data } = useQuery(SERVICE_CENTER, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.serviceCenter,
    error: error,
  };
};

/* QNA 전체 목록 가져오기 */
export const AllQnas = () => {
  const { error, data } = useQuery(ALL_QNAS, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allQnas,
    error: error,
  };
};

/* QNA 정보 가져오기 */
export const Qna = (id) => {
  const { error, data } = useQuery(QNA, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.qna,
    error: error,
  };
};

/* QNA 전체 목록 가져오기 */
export const AllAppQnas = () => {
  const { error, data } = useQuery(ALL_APP_QNAS, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allAppQnas,
    error: error,
  };
};

/* QNA 정보 가져오기 */
export const AppQna = (id) => {
  const { error, data } = useQuery(APP_QNA, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.appQna,
    error: error,
  };
};

/* 게시판 관리 전체 목록 가져오기 */
export const AllNoticeBoards = () => {
  const { error, data } = useQuery(ALL_NOTICE_BOARDS, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allNoticeBoards,
    error: error,
  };
};

/* 게시판 관리 정보 가져오기 */
export const NoticeBoard = (id) => {
  const { error, data } = useQuery(NOTICE_BOARD, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.noticeBoard,
    error: error,
  };
};

/* 배너 전체 목록 가져오기 */
export const AllBanners = () => {
  const { error, data } = useQuery(ALL_BANNERS, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allBanners,
    error: error,
  };
};

/* 배너 정보 가져오기 */
export const Banner = (id) => {
  const { error, data } = useQuery(BANNER, {
    variables: { id },
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.banner,
    error: error,
  };
};

/* 쿠폰 전체 목록 가져오기 */
export const AllCoupons = () => {
  const { error, data } = useQuery(ALL_COUPONS, {
    fetchPolicy: "network-only",
  });
  return {
    result: data && data.allCoupons,
    error: error,
  };
};

/* 셋팅 값 가져오기 */
export const FilterSettingKeyValue = (value) => {
  const { error, data } = useQuery(FILTER_SETTING_KEY_VALUE, {
    fetchPolicy: "network-only",
    variables: { value },
  });
  return {
    result: data && data.filterSettingKeyValue,
    error: error,
  };
};
