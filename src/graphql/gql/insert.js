/* eslint-disable import/prefer-default-export */
import gql from "graphql-tag";

// 공지사항 & FAQ 등록
export const CREATE_SERVICE_CENTER = gql`
  mutation($type: String, $category: String, $title: String, $content: String, $topic: String) {
    createServiceCenter(data: { type: $type, category: $category, title: $title, content: $content, topic: $topic }) {
      id
    }
  }
`;

// 게시판 관리 - 매거진 & 이벤트 등록
export const CREATE_NOTICE_BOARD = gql`
  mutation(
    $category: String
    $title: String
    $content: String
    $image: String
    $link1: String
    $linkImage1: String
    $link2: String
    $linkImage2: String
    $link3: String
    $linkImage3: String
    $no: String
    $visible: Boolean
    $startAt: String
    $endAt: String
  ) {
    createNoticeBoard(
      data: {
        category: $category
        title: $title
        content: $content
        image: $image
        link1: $link1
        linkImage1: $linkImage1
        link2: $link2
        linkImage2: $linkImage2
        link3: $link3
        linkImage3: $linkImage3
        no: $no
        visible: $visible
        startAt: $startAt
        endAt: $endAt
      }
    ) {
      id
    }
  }
`;

// 배너 등록
export const CREATE_BANNER = gql`
  mutation(
    $category: String
    $title: String
    $content: String
    $image: String
    $targetId: String
    $targetUrl: String
    $no: String
    $visible: Boolean
  ) {
    createBanner(
      data: {
        category: $category
        title: $title
        content: $content
        image: $image
        targetId: $targetId
        targetUrl: $targetUrl
        no: $no
        visible: $visible
      }
    ) {
      id
    }
  }
`;

// Store Group 생성
export const CREATE_STORE_GROUP = gql`
  mutation($adminId: String) {
    createStoreGroup(data: { adminId: $adminId }) {
      id
    }
  }
`;

// 회원사 생성
export const CREATE_EMPLOYEE = gql`
  mutation(
    $email: String
    $name: String
    $tel: String
    $password: String
    $admin: Boolean
    $storeGroupId: String
    $defaultStoreId: String
  ) {
    createMember(
      data: {
        email: $email
        name: $name
        tel: $tel
        password: $password
        admin: $admin
        storeGroupId: $storeGroupId
        defaultStoreId: $defaultStoreId
      }
    ) {
      id
    }
  }
`;
// 회원사 생성
export const CREATE_MEMBER = gql`
  mutation(
    $email: String
    $name: String
    $tel: String
    $password: String
    $admin: Boolean
    $memberOpen: Boolean
    $storeGroupId: String
    $defaultStoreId: String
  ) {
    createMember(
      data: {
        email: $email
        name: $name
        tel: $tel
        password: $password
        admin: $admin
        memberOpen: $memberOpen
        storeGroupId: $storeGroupId
        defaultStoreId: $defaultStoreId
      }
    ) {
      id
    }
  }
`;

// signedAt: $signedAt

// 상점 생성
export const CREATE_STORE = gql`
  mutation(
    $place: String
    $name: String
    $tel: String
    $post: String
    $address: String
    $detailAddress: String
    $intro: String
    $onLine: Boolean
    $offLine: Boolean
    $category: String
    $keyword: String
    $site: String
    $blog: String
    $star: String
    $storeRegister: String
    $storeGroupId: String
    $businessNumber: String
    $bankName: String
    $bankNumber: String
    $businessNumberImage: String
    $bankImage: String
    $accountHolder: String
    $image: String
    $gpsLatitude: String
    $gpsLongitude: String
  ) {
    createStore(
      data: {
        place: $place
        name: $name
        tel: $tel
        post: $post
        address: $address
        detailAddress: $detailAddress
        intro: $intro
        onLine: $onLine
        offLine: $offLine
        category: $category
        keyword: $keyword
        site: $site
        blog: $blog
        star: $star
        storeRegister: $storeRegister
        storeGroupId: $storeGroupId
        businessNumber: $businessNumber
        bankName: $bankName
        bankNumber: $bankNumber
        businessNumberImage: $businessNumberImage
        bankImage: $bankImage
        accountHolder: $accountHolder
        image: $image
        gpsLatitude: $gpsLatitude
        gpsLongitude: $gpsLongitude
      }
    ) {
      id
    }
  }
`;
// pixelro 관리자가 상품 생성
export const CREATE_PRODUCT_ADMIN = gql`
  mutation(
    $name: String
    $price: String
    $discount: Int
    $description: String
    $productInfo: String
    $brand: String
    $model: String
    $option: String
    $mainImage: String
    $detailImage: String
    $category: String
    $categoryDetail: String
    $keyword: String
    $storeId: String
    $memberId: String
  ) {
    createProductAdmin(
      data: {
        name: $name
        price: $price
        discount: $discount
        description: $description
        productInfo: $productInfo
        brand: $brand
        model: $model
        option: $option
        mainImage: $mainImage
        detailImage: $detailImage
        category: $category
        categoryDetail: $categoryDetail
        keyword: $keyword
        storeId: $storeId
        memberId: $memberId
      }
    ) {
      id
    }
  }
`;

// 쿠폰 신청
export const CREATE_COUPON = gql`
  mutation(
    $type: String
    $storeId: String
    $startAt: String
    $endAt: String
    $downloadLimit: Int
    $downloadCount: Int
    $adminActive: Boolean
    $storeActive: Boolean
    $min: Int
    $feeFree: Boolean
    $cashDiscount: Int
    $percentDiscount: Int
    $visit: Boolean
    $visitName: String
    $couponName: String
  ) {
    createCoupon(
      data: {
        type: $type
        storeId: $storeId
        startAt: $startAt
        endAt: $endAt
        downloadLimit: $downloadLimit
        downloadCount: $downloadCount
        adminActive: $adminActive
        storeActive: $storeActive
        min: $min
        feeFree: $feeFree
        cashDiscount: $cashDiscount
        percentDiscount: $percentDiscount
        visit: $visit
        visitName: $visitName
        couponName: $couponName
      }
    ) {
      id
    }
  }
`;

// 픽셀로 관리자 등록
export const CREATE_PIXELRO = gql`
  mutation($name: String, $tel: String, $email: String, $password: String) {
    createPixelro(data: { name: $name, tel: $tel, email: $email, password: $password }) {
      id
    }
  }
`;

// 픽셀로 관리자 LOG
export const CREATE_LOG = gql`
  mutation($adminEmail: String, $ip: String, $url: String, $target: String, $detail: String) {
    createLog(data: { adminEmail: $adminEmail, ip: $ip, url: $url, target: $target, detail: $detail }) {
      id
    }
  }
`;

// 싱글 파일 업로드
export const SINGLE_FILE = gql`
  mutation($file: Upload!) {
    singleUpload(file: $file) {
      filename
      mimetype
      encoding
    }
  }
`;

// 멀티 파일 업로드
export const MULTI_FILE = gql`
  mutation($files: [Upload], $size: String) {
    multiUpload(files: $files, size: $size) {
      filename
      mimetype
      encoding
    }
  }
`;

// 멀티 파일 업로드, 리사이즈
export const MULTI_FILE_RESIZE = gql`
  mutation($files: [Upload], $size: String) {
    multiUploadResize(files: $files, size: $size) {
      filename
      mimetype
      encoding
    }
  }
`;
