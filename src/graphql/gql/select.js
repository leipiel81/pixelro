import gql from "graphql-tag";

export const LOGIN = gql`
  mutation($email: String!, $password: String!) {
    pixelroIn(email: $email, password: $password) {
      error
      token
    }
  }
`;

// StoreGroup 정보
export const STORE_GROUP = gql`
  query($id: String!) {
    storeGroup(where: { id: $id }) {
      id
      adminId
      members {
        id
        defaultStoreId
      }
    }
  }
`;

// Store 정보
export const STORE = gql`
  query($id: String!) {
    store(where: { id: $id }) {
      id
      place
      name
      tel
      post
      address
      detailAddress
      intro
      createAt
      onLine
      offLine
      blog
      category
      keyword
      site
      star
      storeRegister
      businessNumber
      bankName
      bankNumber
      businessNumberImage
      bankImage
      accountHolder
      image
      storeGroup {
        id
        adminId
        members {
          id
          name
          email
          tel
          admin
          createAt
          signedAt
        }
      }
    }
  }
`;

// Store 전체 정보
export const ALL_STORES = gql`
  query {
    allStores {
      id
      place
      name
      tel
      address
      createAt
      onLine
      offLine
      blog
      category
      keyword
      site
      star
      storeRegister
      businessNumber
      bankName
      bankNumber
      businessNumberImage
      bankImage
      accountHolder
      image
      withdraw
      withdrawStatus
      withdrawCreateAt
      storeGroup {
        id
        adminId
        members {
          id
          name
          email
          tel
          admin
          createAt
          signedAt
        }
      }
    }
  }
`;

// 회원사 전체 정보
export const ALL_MEMBERS = gql`
  query {
    allMembers {
      id
      email
      name
      tel
      admin
      memberOpen
      createAt
      signedAt
      storeGroup {
        id
        adminId
        stores {
          id
          place
          name
          tel
          address
          intro
          onLine
          offLine
          createAt
          category
          keyword
          site
          blog
          star
          storeRegister
        }
      }
    }
  }
`;

// Member 정보
export const MEMBER = gql`
  query($id: String!) {
    member(where: { id: $id }) {
      id
      email
      name
      tel
      password
      admin
      memberOpen
      defaultStoreId
      createAt
      signedAt
      storeGroup {
        id
        adminId
        stores {
          id
          place
          name
          tel
          address
          intro
          onLine
          offLine
          createAt
          category
          keyword
          site
          blog
          star
          storeRegister
        }
      }
    }
  }
`;

// 상품 전체 정보
export const ALL_PRODUCTS = gql`
  query {
    allProducts {
      id
      name
      price
      discount
      productInfo
      description
      createAt
      brand
      model
      option
      mainImage
      detailImage
      category
      categoryDetail
      keyword
      onSale
      visible
      store {
        id
        place
        name
        tel
        address
        createAt
        onLine
        offLine
        blog
        category
        keyword
        site
        star
        storeRegister
      }
      member {
        id
        email
        name
        tel
        admin
        memberOpen
        createAt
        signedAt
      }
    }
  }
`;

// 상품 정보
export const PRODUCT = gql`
  query($id: String!) {
    product(where: { id: $id }) {
      id
      name
      price
      discount
      productInfo
      description
      createAt
      brand
      model
      option
      mainImage
      detailImage
      category
      categoryDetail
      keyword
      onSale
      store {
        id
        place
        name
        tel
        address
        createAt
        onLine
        offLine
        blog
        category
        keyword
        site
        star
        storeRegister
      }
      member {
        id
        email
        name
        tel
        memberOpen
        admin
        createAt
        signedAt
      }
    }
  }
`;

// 상품 등록 전체 정보
export const ALL_PRODUCT_REGISTERS = gql`
  query {
    allProductRegisters {
      id
      name
      price
      discount
      productInfo
      description
      createAt
      brand
      model
      option
      mainImage
      detailImage
      reply
      category
      categoryDetail
      keyword
      store {
        id
        place
        name
        tel
        address
        createAt
        onLine
        offLine
        blog
        category
        keyword
        site
        star
        storeRegister
      }
      member {
        id
        email
        name
        tel
        admin
        memberOpen
        createAt
        signedAt
      }
    }
  }
`;

// 상품 등록 정보
export const PRODUCT_REGISTER = gql`
  query($id: String!) {
    productRegister(where: { id: $id }) {
      id
      name
      price
      discount
      productInfo
      description
      createAt
      brand
      model
      option
      mainImage
      detailImage
      reply
      category
      categoryDetail
      keyword
      store {
        id
        place
        name
        tel
        address
        createAt
        onLine
        offLine
        blog
        category
        keyword
        site
        star
        storeRegister
      }
      member {
        id
        email
        name
        tel
        admin
        memberOpen
        createAt
        signedAt
      }
    }
  }
`;

// 앱 사용자 전체 정보
export const ALL_USERS = gql`
  query {
    allUsers {
      id
      address
      birthday
      createAt
      detailAddress
      email
      exercise
      food
      gender
      glasses
      gpsAddress
      gpsLatitude
      gpsLongitude
      job
      left
      name
      password
      post
      right
      signedAt
      sns
      snsId
      status
      surgery
      tel
      save
      fcmToken
      point
      withdraw
    }
  }
`;

// 앱 사용자 정보
export const USER = gql`
  query($id: String!) {
    user(where: { id: $id }) {
      id
      address
      birthday
      createAt
      detailAddress
      email
      exercise
      food
      gender
      glasses
      gpsAddress
      gpsLatitude
      gpsLongitude
      job
      left
      name
      password
      post
      right
      signedAt
      sns
      snsId
      status
      surgery
      tel
      save
      fcmToken
      point
    }
  }
`;

// 예약 관리 전체 정보
export const ALL_RESERVATIONS = gql`
  query {
    allReservations {
      id
      inStoreId
      outStoreId
      status
      pay {
        id
        orderList
        process1
        process2
        process3
        process4
        process5
        processMemo
        status
        user {
          id
          name
          email
          tel
          point
        }
        name
        phone
        email
        receiveType
        receiverName
        receiverPhone1
        receiverPhone2
        receiverPost
        receiverAddr1
        receiverAddr2
        city
        state
        country
        memo
        paymentType
        productTotal
        feeTotal
        payTotal
        currency
        iniTid
        iniAuthDate
        iniAuthNo
        iniCardName
        iniHppNum
        invoiceNumber
        invoiceCompany
        invoicePhone
        createAt
        cancelCnt
        cancelPriceTotal
        createAt
        point
        pixelroCouponId
        pixelroCouponDiscount
        storeCoupon
      }
      createAt
      reservationAt
      visitAt
      storeMemo
      cancelMemo
      inStoreCheck
      inStoreMemo
      user {
        id
        name
      }
    }
  }
`;

// 예약 관리 정보
export const RESERVATION = gql`
  query($id: String!) {
    reservation(where: { id: $id }) {
      id
      inStoreId
      outStoreId
      status
      pay {
        id
        orderList
        process1
        process2
        process3
        process4
        process5
        processMemo
        status
        user {
          id
          name
          email
          tel
          point
        }
        name
        phone
        email
        receiveType
        receiverName
        receiverPhone1
        receiverPhone2
        receiverPost
        receiverAddr1
        receiverAddr2
        city
        state
        country
        memo
        paymentType
        productTotal
        feeTotal
        payTotal
        currency
        iniTid
        iniAuthDate
        iniAuthNo
        iniCardName
        iniHppNum
        invoiceNumber
        invoiceCompany
        invoicePhone
        createAt
        cancelCnt
        cancelPriceTotal
        createAt
        point
        pixelroCouponId
        pixelroCouponDiscount
        storeCoupon
      }
      createAt
      reservationAt
      visitAt
      storeMemo
      cancelMemo
      inStoreCheck
      inStoreMemo
      user {
        id
        name
      }
    }
  }
`;
export const ORDER = gql`
  query($id: String!) {
    order(where: { id: $id }) {
      id
      createAt
      user {
        id
        email
        name
        tel
        post
        address
        detailAddress
        createAt
        point
      }
      product {
        id
        name
        price
        discount
        productInfo
        description
        createAt
        brand
        model
        option
        mainImage
        detailImage
        category
        keyword
        store {
          id
          name
          tel
        }
      }
      payId
      productOption
      productCount
      productPrice
      productAddPrice
      productTotal
      currency
      reviewStar
      reviewImage
      reviewVideo
      reviewText
      reviewCreateAt
      returnType
      returnText
      returnAt
      returnOkAt
      reply
      replyCreateAt
      receiveType
      storeId
      delivery
      trackingNumber
      status
      process1
      process2
      process3
      process4
      process5
      cancelPrice
      cancel
      coupon
      couponDiscount
      point
      pixelroCouponDiscount
      changeDelivery
      changeTrackingNumber
      changeAt
      changeType
      changeText
      changeOkAt
      changeCheckAt
    }
  }
`;

// 사용자 렌즈 예약 정보 가져오기
export const ALL_LENS_RESERVATIONS = gql`
  query {
    allLensReservations {
      id
      createAt
      productOption
      productTotal
      status
      reservationAt
      visitAt
      cancelAt
      storeId
      product {
        id
        name
        mainImage
        option
      }
      user {
        id
        name
        tel
        email
      }
    }
  }
`;

// 사용자 렌즈 예약 정보 가져오기 - 하나
export const LENS_RESERVATION = gql`
  query($id: String!) {
    lensReservation(where: { id: $id }) {
      id
      createAt
      productOption
      productTotal
      status
      reservationAt
      visitAt
      cancelAt
      storeId
      product {
        id
        name
        mainImage
        option
      }
      user {
        id
        name
        tel
        email
      }
    }
  }
`;

// 고객센터 - 공지사항 & FAQ 전체 정보
export const ALL_SERVICE_CENTERS = gql`
  query {
    allServiceCenters {
      id
      type
      category
      title
      content
      topic
      createAt
    }
  }
`;

// 고객센터 - 공지사항 & FAQ 정보
export const SERVICE_CENTER = gql`
  query($id: String!) {
    serviceCenter(where: { id: $id }) {
      id
      type
      category
      title
      content
      topic
      createAt
    }
  }
`;

// QNA 전체 정보
export const ALL_QNAS = gql`
  query {
    allQnas {
      id
      category
      title
      content
      comment
      reply
      createAt
      member {
        id
        email
        name
        tel
        admin
      }
      store {
        id
        place
        name
        tel
        address
      }
    }
  }
`;

// QNA 정보
export const QNA = gql`
  query($id: String!) {
    qna(where: { id: $id }) {
      id
      category
      title
      content
      comment
      reply
      createAt
      member {
        id
        email
        name
        tel
        admin
      }
      store {
        id
        place
        name
        tel
        address
      }
    }
  }
`;

// 게시판 전체 정보
export const ALL_NOTICE_BOARDS = gql`
  query {
    allNoticeBoards {
      id
      category
      title
      content
      image
      link1
      linkImage1
      link2
      linkImage2
      link3
      linkImage3
      no
      visible
      startAt
      endAt
      createAt
    }
  }
`;

// 게시판 정보
export const NOTICE_BOARD = gql`
  query($id: String!) {
    noticeBoard(where: { id: $id }) {
      id
      category
      title
      content
      image
      link1
      linkImage1
      link2
      linkImage2
      link3
      linkImage3
      no
      visible
      startAt
      endAt
      createAt
    }
  }
`;

// 배너 전체 정보
export const ALL_BANNERS = gql`
  query {
    allBanners {
      id
      image
      category
      title
      content
      targetId
      targetUrl
      no
      visible
      createAt
    }
  }
`;

// 배너 정보
export const BANNER = gql`
  query($id: String!) {
    banner(where: { id: $id }) {
      id
      image
      category
      title
      content
      targetId
      targetUrl
      no
      visible
      createAt
    }
  }
`;

// 배너 정보
export const FILTER_BANNER = gql`
  query($value: String!) {
    filterBanner(searchString: $value) {
      id
      image
      category
      title
      content
      targetId
      targetUrl
      no
      visible
      createAt
    }
  }
`;

// 공지사항 & FAQ 정보
export const FILTER_SERVICE_CENTER = gql`
  query($value: String!) {
    filterServiceCenter(searchString: $value) {
      id
      type
      category
      title
      content
      topic
      createAt
    }
  }
`;

// 앱 사용자 문의
export const ALL_APP_QNAS = gql`
  query {
    allAppQnas {
      id
      category
      title
      content
      comment
      reply
      createAt
      user {
        id
        email
        name
        tel
      }
    }
  }
`;

// 앱 사용자 문의
export const APP_QNA = gql`
  query($id: String!) {
    appQna(where: { id: $id }) {
      id
      category
      title
      content
      comment
      reply
      createAt
      user {
        id
        email
        name
        tel
      }
    }
  }
`;

// 전체 쿠폰 목록
export const ALL_COUPONS = gql`
  query {
    allCoupons {
      id
      type
      storeId
      createAt
      openAt
      startAt
      endAt
      downloadCount
      downloadLimit
      adminActive
      storeActive
      min
      feeFree
      cashDiscount
      percentDiscount
      visit
      visitName
      couponName
      couponRecodes {
        id
        payId
      }
    }
  }
`;

// 쿠폰 정보
export const COUPON = gql`
  query($id: String!) {
    coupon(where: { id: $id }) {
      id
      type
      storeId
      createAt
      openAt
      startAt
      endAt
      downloadCount
      downloadLimit
      adminActive
      storeActive
      min
      feeFree
      cashDiscount
      percentDiscount
      visit
      visitName
      couponName
      couponRecodes {
        id
        payId
      }
    }
  }
`;

// Store 전체 정보
export const ALL_ORDERS = gql`
  query {
    allOrders {
      id
      createAt
      user {
        id
        email
        name
        tel
        post
        address
        detailAddress
        createAt
      }
      product {
        id
        name
        price
        discount
        productInfo
        description
        createAt
        brand
        model
        option
        mainImage
        detailImage
        category
        keyword
        store {
          id
          name
        }
      }
      payId
      productOption
      productCount
      productPrice
      productAddPrice
      productTotal
      currency
      reviewStar
      reviewImage
      reviewVideo
      reviewText
      reviewCreateAt
      returnType
      returnText
      returnAt
      reply
      replyCreateAt
      receiveType
      storeId
      delivery
      trackingNumber
      status
      process1
      process2
      process3
      process4
      process5
      cancelPrice
      cancel
      coupon
      couponDiscount
      point
      pixelroCouponDiscount
      changeDelivery
      changeTrackingNumber
      changeAt
      changeType
      changeText
      changeOkAt
      changeCheckAt
    }
  }
`;

// 정산 내역 가져오기
export const ALL_SETTLE_REPORTS = gql`
  query {
    allSettleReports {
      id
      startAt
      endAt
      cuid1
      cuid2
      createAt
      recodeCount
      productTotal
      pixelroTotal
      storeTotal
      pointTotal
      userTotal
      salesCommission
      settlementPrice
      tax
      supplyPrice
      year
      month
      status
      settlementRequestAt
      settlementAt
      taxBillAt
      store {
        id
        name
      }
    }
  }
`;

// 정산 내역 가져오기
export const SETTLE_REPORT = gql`
  query($id: String!) {
    settleReport(where: { id: $id }) {
      id
      startAt
      endAt
      cuid1
      cuid2
      createAt
      recodeCount
      productTotal
      pixelroTotal
      storeTotal
      pointTotal
      userTotal
      salesCommission
      settlementPrice
      tax
      supplyPrice
      year
      month
      status
      settlementRequestAt
      settlementAt
      taxBillAt
      store {
        id
        name
        tel
        accountHolder
        address
        bankName
        bankNumber
        businessNumber
      }
    }
  }
`;

// 정산 내역 가져오기
export const FILTER_SETTLE_REPORT = gql`
  query($value: String) {
    filterSettleReport(searchString: $value) {
      id
      startAt
      endAt
      cuid1
      cuid2
      createAt
      recodeCount
      productTotal
      pixelroTotal
      storeTotal
      pointTotal
      userTotal
      salesCommission
      settlementPrice
      tax
      supplyPrice
      year
      month
      status
      settlementRequestAt
      settlementAt
      taxBillAt
      store {
        id
        name
        tel
        accountHolder
        address
        bankName
        bankNumber
        businessNumber
      }
    }
  }
`;

// 정산 내역 가져오기
export const SETTLEMENT_ORDER_DATA = gql`
  query($id: String) {
    settlementOrderData(id: $id) {
      result
    }
  }
`;

// 관리자 전체 목록
export const ALL_PIXELROS = gql`
  query {
    allPixelros {
      id
      createAt
      email
      password
      name
      tel
    }
  }
`;
// 관리자 정보 필터
export const FILTER_PIXELRO = gql`
  query($value: String!) {
    filterPixelro(searchString: $value) {
      id
      createAt
      email
      password
      name
      tel
    }
  }
`;

// 관리자 정보
export const PIXELRO = gql`
  query($id: String!) {
    pixelro(where: { id: $id }) {
      id
      createAt
      email
      password
      name
      tel
    }
  }
`;

// 메세지 충전 신청 전체 정보
export const ALL_MESSAGES = gql`
  query {
    allMessages {
      id
      createAt
      chargePrice
      tax
      sendPrice
      depositor
      status
      checkAt
      store {
        id
        name
        messageCount
      }
    }
  }
`;

// 셋팅 값 가져오기
export const FILTER_SETTING_KEY_VALUE = gql`
  query($value: String!) {
    filterSettingKeyValue(searchString: $value) {
      id
      key
      value
    }
  }
`;
